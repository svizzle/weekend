<?php

namespace backend\forms;

use Yii;
use common\models\Setting;

class SettingUpdate extends \yii\base\Model
{
    public $recent_services_period;
    public $frontpage_static_banner;
    public $frontpage_dynamic_banner;

    public $frontpage_static_banner_file;
    public $frontpage_dynamic_banner_file;

    public $feedback_email;
    public $feedback_subject;

    protected $_settings = [
        'recent_services_period',
        'frontpage_static_banner',
        'frontpage_dynamic_banner',
        'feedback_email',
        'feedback_subject',
    ];

    public function init()
    {
        foreach($this->_settings as $name)
        {
            $setting = Setting::findOne(['name' => $name]);
            if (!$setting)
            {
                $setting = new Setting([
                    'name' => $name,
                ]);
                $setting->save();
            }
            $this->{$name} = $setting->value;
        }
    }

    public function rules()
    {
        return [
            ['recent_services_period', 'required'],
            ['recent_services_period', 'number', 'min' => '1'],
            ['frontpage_static_banner_file', 'image'],
            ['frontpage_dynamic_banner_file', 'image'],
            ['feedback_email', 'email'],
            ['feedback_subject', 'trim'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'recent_services_period' => 'Срок нахождения услуг в виджете "Новые", дней',
            'frontpage_static_banner_file' => 'Статический баннер',
            'frontpage_dynamic_banner_file' => 'Динамический баннер',
            'feedback_email' => 'Email обратной связи',
            'feedback_subject' => 'Тема письма обратной связи',
        ];
    }

    public function save()
    {
        if (!$this->validate()) return false;

        if ($this->frontpage_static_banner_file)
        {
            $this->frontpage_static_banner = 'static_banner.'.$this->frontpage_static_banner_file->extension;
            $this->frontpage_static_banner_file->saveAs(Yii::getAlias('@frontend')."/web/uploads/".$this->frontpage_static_banner);
        }

        if ($this->frontpage_dynamic_banner_file)
        {
            $this->frontpage_dynamic_banner = 'dynamic_banner.'.$this->frontpage_dynamic_banner_file->extension;
            $this->frontpage_dynamic_banner_file->saveAs(Yii::getAlias('@frontend')."/web/uploads/".$this->frontpage_dynamic_banner);
        }

        foreach($this->_settings as $name)
        {
            $setting = Setting::findOne(['name' => $name]);
            $setting->value = $this->{$name};
            $setting->save();
        }


        return true;
    }
}
