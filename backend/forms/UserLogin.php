<?php

namespace backend\forms;

use common\models\User;

class UserLogin extends \yii\base\Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeNames()
    {
        return [
            'rememberMe' => 'Запомнить меня',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        $user = $this->getUser();

        if (!$user || !$user->validatePassword($this->password))
        {
            return $this->addError($attribute, 'Неправильный адрес почты или пароль');
        }

        if (!$user->enabled)
        {
            return $this->addError('email', 'Эта учетная запись заблокирована');
        }
        
    }

    public function getUser()
    {
        if ($this->_user === null)
        {
            $this->_user = User::find()->andWhere([
                'email' => $this->email,
            ])->one();
        }

        return $this->_user;
    }
}
