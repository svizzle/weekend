<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

use common\models\Service;
use common\models\ServiceCompilation;
use common\models\ServiceSearch;
use common\models\ServiceType;

class ServiceCompilationController extends \yii\web\Controller
{
    public function actionItemAdd($compilation_id, $service_id)
    {
        $service = Service::findOne($service_id);
        if (!$service) throw new NotFoundHttpException;

        if ($compilation_id)
        {
            $model = ServiceCompilation::findOne($compilation_id);
            if (!$model) throw new NotFoundHttpException;
            $model->link('services', $service);
        }
        else
        {
            $command = Yii::$app->db->createCommand('INSERT INTO service_compilation_to_service (`compilation_id`, `service_id`) VALUES (null, "'.$service_id.'")');
            $command->execute();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionItemRemove($compilation_id, $service_id)
    {
        if ($compilation_id)
        {
            $model = ServiceCompilation::findOne($compilation_id);
            if (!$model) throw new NotFoundHttpException;

            $service = Service::findOne($service_id);
            if (!$service) throw new NotFoundHttpException;

            $model->unlink('services', $service, true);
        }
        else
        {
            $command = Yii::$app->db->createCommand('DELETE FROM service_compilation_to_service WHERE compilation_id IS NULL AND service_id = "'.$service_id.'"');
            $command->execute();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionItems($compilation_id)
    {
        $searchModel = new ServiceSearch;

        if ($compilation_id)
        {
            $model = ServiceCompilation::findOne($compilation_id);
            if (!$model) throw new NotFoundHttpException;

            $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->get(), [
                'ServiceSearch' => [
                    'type_id' => $model->service_type_id,
                ],
            ]));
        }
        else
        {
            $model = new ServiceCompilation;
            $dataProvider = $searchModel->search(Yii::$app->request->get());
        }  

        return $this->render('items', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRemove()
    {
        $models = ServiceCompilation::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();
        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные вкладки успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionAdd($id)
    {
        $type = ServiceType::findOne($id);
        if (!$type) throw new NotFoundHttpException;

        $model = new ServiceCompilation([
            'service_type_id' => $type->id,
        ]);
        $model->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ServiceCompilation::find(),            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
