<?php

namespace backend\controllers;

use Yii;
use common\repositories\FormsSettingsRepository;
use common\search\FormsSettingsSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FormsSettingsController implements the CRUD actions for FormsSettingsRepository model.
 */
class FormsSettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string|void
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('admin')) throw new ForbiddenHttpException();

        $searchModel = new FormsSettingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Если пришел ajax на изменение поля то:
        if(Yii::$app->request->post('hasEditable'))
        {
            $obj_id = Yii::$app->request->post('editableKey');
            $obj = FormsSettingsRepository::findOne($obj_id);

            $post = [];
            $posted = current($_POST['FormsSettingsRepository']);
            $post['FormsSettingsRepository'] = $posted;

            if ($obj->load($post))
            {

                if($obj->save())
                {
                    if (isset($posted['label']))
                        $output =  $obj->label;
                    if (isset($posted['hint']))
                        $output =  $obj->hint;
                    if (isset($posted['placeholder']))
                        $output =  $obj->placeholder;

                    $out = Json::encode(['output'=>$output, 'message'=>'']);
                }
            }

            echo $out;
            return;
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('admin')) throw new ForbiddenHttpException();

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FormsSettingsRepository model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    protected function actionCreate()
    {
        $model = new FormsSettingsRepository();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FormsSettingsRepository model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    protected function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FormsSettingsRepository model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    protected function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FormsSettingsRepository model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormsSettingsRepository the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormsSettingsRepository::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
