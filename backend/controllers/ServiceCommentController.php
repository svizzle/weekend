<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use common\models\ServiceComment;
use common\models\ServiceCommentSearch;

class ServiceCommentController extends \yii\web\Controller
{
    public function actionDelete()
    {
        $models = ServiceComment::find()->andWhere([
            'id' => Yii::$app->request->post('ids'),
        ])->all();

        foreach($models as $model) $model->delete();

        Yii::$app->session->addFlash('success', 'Выбранные комментарии успешно удалены');

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdate($id)
    {
        $model = ServiceComment::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->addFlash('success', 'Комментарий отредактирован');
            return $this->goBack();
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new ServiceCommentSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
