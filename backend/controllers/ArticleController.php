<?php

namespace backend\controllers;

use common\models\ArticleServiceTypes;
use common\models\ServiceType;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use common\models\Article;

class ArticleController extends \yii\web\Controller
{
    public function actionDelete()
    {
        $models = Article::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные статьи успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdate($id)
    {
        if ($id)
        {
            $model = Article::findOne($id);
            if (!$model) throw NotFoundHttpException;
        }
        else
        {
            $model = new Article;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            //var_dump($model->service_types_input); var_dump($_POST); die();
            $model->unlinkAll('articleServiceTypes', true);
            if (!$model->service_types_input) $model->service_types_input = [];
            foreach($model->service_types_input as $value)
            {
                $obj = new ArticleServiceTypes();
                $obj->article_id = $model->id;
                $obj->service_id = $value;
                $obj->save(false);
            }

            Yii::$app->session->addFlash('success', 'Статья успешно сохранена');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
