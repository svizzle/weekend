<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use common\models\MenuMain;

class MenuMainController extends \yii\web\Controller
{
    public function actionDelete()
    {
        $models = MenuMain::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();
        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные пункты меню успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdate($id)
    {
        if ($id)
        {
            $model = MenuMain::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new MenuMain;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->addFlash('success', 'Пункт меню успешно сохранен');
            return $this->goBack();
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MenuMain::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
