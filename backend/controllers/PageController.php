<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use common\models\Page;

class PageController extends \yii\web\Controller
{
    public function actionDelete()
    {
        $models = Page::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные страницы успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdate($id)
    {
        if ($id)
        {
            $model = Page::findOne($id);
            if (!$model) throw NotFoundHttpException;
        }
        else
        {
            $model = new Page;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->addFlash('success', 'Данные страницы успешно сохранены');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Page::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
