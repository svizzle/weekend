<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use common\models\Company;
use common\models\CompanyEmployee;
use common\models\CompanyPhone;
use common\models\CompanyPhoto;
use common\models\CompanySearch;
use common\models\CompanyType;
use common\models\Service;
use common\models\ServicePhoto;
use common\models\ServicePrice;
use common\models\ServiceType;
use common\models\ServiceTypeAttributeValue;

class CompanyController extends \yii\web\Controller
{
    public function actionPhotoSort()
    {
        $model = CompanyPhoto::findOne(Yii::$app->request->post('id'));
        if (!$model) throw new NotFoundHttpException;

        $position = Yii::$app->request->post('position');
        $model->order($position, 'company_id');
        $model->save();
    }


    public function actionDelete()
    {
        $models = Company::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();
        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные компании успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionToggle($id, $attribute)
    {
        $model = Company::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        $model->{$attribute} = !$model->{$attribute};
        $model->save(false, [$attribute]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPhotoDelete($company_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = CompanyPhoto::findOne(Yii::$app->request->post('key'));
        $model->delete();

        return [
            'key' => Yii::$app->request->post('key'),
        ];
    }

    public function actionPhotoUpload($company_id)
    {
        $file = UploadedFile::getInstanceByName('image');

        $model = new CompanyPhoto([
            'company_id' => $company_id ? $company_id : null,
            'path' => Yii::$app->security->generateRandomString(16).".".$file->extension,
        ]);
        $model->save();

        $path = Yii::getAlias('@frontend').'/web/uploads/companies/';
        $file->saveAs($path.$model->path);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    
        return [
            'initialPreview' => [
                \common\widgets\CompanyImage::widget(['model' => $model, 'width' => 150, 'height' => 100]),
            ],
            'initialPreviewConfig' => [
                [
                    'key' => $model->id,
                    'url' => Url::to(['/company/photo-delete', 'company_id' => $company_id]),
                ],
            ],
        ];

    }

    public function actionIndex()
    {
        $searchModel = new CompanySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionTypeDelete()
    {
        $models = CompanyType::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные виды компаний успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTypeUpdate($id)
    {
        if ($id)
        {
            $model = CompanyType::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new CompanyType;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->addFlash('success', 'Вид компании успешно сохранен');
            return $this->redirect(['type']);
        }

        return $this->render('type-update', [
            'model' => $model,
        ]);
    }

    public function actionTypeView($id)
    {
        $model = CompanyType::findOne($id);
        if (!$model) throw NotFoundHttpException;

        echo $model->description;
    }

    public function actionType()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CompanyType::find(),
        ]);

        return $this->render('type', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionServiceUpdate($id, $company_id)
    {
        if ($id)
        {
            $model = Service::findOne($id);
            if (!$model) throw NotFoundHttpException;
        }
        else
        {
            $model = new Service([
                'company_id' => $company_id,
            ]);
        }

        $service = Yii::$app->request->post('Service');
        $values = isset($service['attributeValues']) ? ServiceTypeAttributeValue::find()->andWhere([
            'id' => $service['attributeValues']
        ])->all() : $model->attributeValues;

        if (Yii::$app->request->isPost)
        {
            $count = count(Yii::$app->request->post('ServicePrice', []));
            $prices = [new ServicePrice];
            for($i = 1; $i < $count; $i++) $prices[] = new ServicePrice;
        }
        else
        {
            $prices = $model->prices;
            if (empty($prices)) $prices = [new ServicePrice];
        }


        if (
            $model->load(Yii::$app->request->post())
            && Model::loadMultiple($prices, Yii::$app->request->post()) 
        )
        {
            if (
                $model->validate()
                && Model::validateMultiple($prices)
            )
            {
                if ($model->save())
                {
                    $model->unlinkAll('attributeValues', true);
                    foreach($values as $value)
                    {
                        $model->link('attributeValues', $value);
                    }

                    foreach($model->prices as $price) $price->delete();
                    foreach($prices as $price)
                    {
                        $price->service_id = $model->id;
                        $price->save();
                    }

                    Yii::$app->session->addFlash('success', 'Услуга успешно сохранена');
                    return $this->redirect(['/company/service', 'company_id' => $company_id]);
                }
            }
        }
        else
        {
            $photos = ServicePhoto::find()->andWhere(['service_id' => null, 'user_id' => Yii::$app->user->id])->all();
            foreach($photos as $photo) $photo->delete();
        }

        $types = ServiceType::find()->orderBy(['name' => SORT_ASC])->all();

        $type = $model->type ? $model->type : $types[0];

        return $this->render('service-update', [
            'values' => $values,
            'prices' => $prices,
            'model' => $model,
            'types' => $types,
            'type' => $type,
        ]);
    }

    public function actionService($company_id)
    {
        $company = Company::findOne($company_id);
        if (!$company) throw new NotFoundHttpException;

        $dataProvider = new ActiveDataProvider([
            'query' => Service::find()->andWhere([
                'company_id' => $company_id,
            ]),
        ]);

        return $this->render("service", [
            'company' => $company,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionForm()
    {
        $company = Yii::$app->user->identity->company;
        return $this->actionUpdate($company ? $company->id : 0, Yii::$app->user->id);
    }

    public function actionUpdate($id, $user_id)
    {
        if ($id)
        {
            $model = Company::findOne($id);
            if (!$model) throw new NotFoundHttpException;

            if ((Yii::$app->user->can('partner'))){
                $services_count = Service::find()->where(['company_id'=>$id])->count();
                if ($services_count == 0)
                    return $this->redirect(['company/service', 'company_id'=>$id]);
            }
        }
        else
        {
            $model = new Company([
                'user_id' => $user_id,
                // Москва
                'lat' => 55.76,
                'lon' => 37.64,
            ]);
        }

        if (Yii::$app->request->isPost)
        {
            $count = count(Yii::$app->request->post('CompanyPhone', []));
            $phones = [new CompanyPhone];
            for($i = 1; $i < $count; $i++) $phones[] = new CompanyPhone;

            $count = count(Yii::$app->request->post('CompanyEmployee', []));
            $employee = [new CompanyEmployee];
            for($i = 1; $i < $count; $i++) $employee[] = new CompanyEmployee;
        }
        else
        {
            $phones = $model->phones;
            if (empty($phones)) $phones = [new CompanyPhone];
            $employee = $model->employee;            
            if (empty($employee)) $employee = [new CompanyEmployee];
        }

        if (
            $model->load(Yii::$app->request->post())
            && Model::loadMultiple($phones, Yii::$app->request->post()) 
            && Model::loadMultiple($employee, Yii::$app->request->post()) 
        )
        {
            if(
                $model->validate()
                && Model::validateMultiple($phones)
                && Model::validateMultiple($employee)
            )
            {
                if (!Yii::$app->user->can('admin'))
                {
                    $model->moderated = 0;
                }
                $model->save();

                foreach($model->phones as $phone) $phone->delete();
                foreach($phones as $phone)
                {
                    $phone->company_id = $model->id;
                    $phone->save();
                }

                foreach($model->employee as $person) $person->delete();
                foreach($employee as $person)
                {
                    $person->company_id = $model->id;
                    $person->save();
                }

                Yii::$app->session->addFlash('success', 'Данные компании успешно сохранены');
                return $this->goBack();
            }
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        $types = CompanyType::find()->orderBy(['name' => SORT_ASC])->all();

        return $this->render('update', [
            'model' => $model,
            'types' => $types,
            'phones' => $phones,
            'employee' => $employee,
        ]);
    }
}
