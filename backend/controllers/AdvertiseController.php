<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use common\models\Advertise;
use common\models\AdvertiseSearch;
use common\models\GeoLocation;
use common\models\GeoRegion;
use common\models\ServiceType;

class AdvertiseController extends \yii\web\Controller
{
    public function actionDelete()
    {
        $models = Advertise::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранная реклама успешно удалена');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdate($id)
    {
        if ($id)
        {
            $model = Advertise::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new Advertise;
        }

        if ($model->load(Yii::$app->request->post()))
        {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate())
            {
                $model->image = Yii::$app->security->generateRandomString(16).".".$model->file->extension;
                $model->file->saveAs(Yii::getAlias('@frontend')."/web/uploads/context/".$model->image, false);
                $model->save();

                Yii::$app->session->addFlash('success', 'Реклама успешно сохранена');
                return $this->goBack();
            }            
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        $serviceTypes = ServiceType::find()->orderBy(['name' => SORT_ASC])->all();

        $regions = GeoRegion::find()->orderBy(['name' => SORT_ASC])->all();

        return $this->render('update', [
            'model' => $model,
            'serviceTypes' => $serviceTypes,
            'regions' => $regions,
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new AdvertiseSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
