<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;

use common\models\ServiceType;
use common\models\ServiceTypeAttributeValue;
use common\models\ServiceTypeGroup;
use common\models\ServiceTypeGroupSearch;

class ServiceTypeGroupController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new ServiceTypeGroupSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        if (!Yii::$app->user->can('admin'))
            $dataProvider->query->andWhere(['user_id'=>Yii::$app->user->id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        if ($id)
        {
            $model = ServiceTypeGroup::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new ServiceTypeGroup;
        }

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save())
        {
            $model->unlinkAll('values', true);

            $t = isset($post['t']) ? $post['t'] : [];
            $model->tab_ids = json_encode($t);
            $model->save();
            
            $v = isset($post['v']) ? $post['v'] : [];
            foreach($v as $value_id)
            {
                $model->link('values', ServiceTypeAttributeValue::findOne($value_id));
            }

            Yii::$app->session->addFlash('success', 'Вид отдыха успешно сохранен');
            return $this->goBack();
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete()
    {
        $models = ServiceTypeGroup::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные виды отдыха удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }
}
