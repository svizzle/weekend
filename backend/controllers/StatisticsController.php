<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use common\models\Company;
use common\models\Statistics;

class StatisticsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index',[

        ]);
    }

    public function actionVisitors()
    {
        // TYPE_USER_VISIT = 2
        // Берем минимальную дату
        $min_date = Statistics::find()->andWhere([
            'type_id' => Statistics::TYPE_USER_VISIT,
        ])->min('created_at');

        $daterange = new \DatePeriod(new \DateTime($min_date), new \DateInterval('P1D'), new \DateTime('+1 day'));
        $result = [];

        foreach($daterange as $date)
        {
            $result[] = [
                strtotime($date->format('Y-m-d 00:00:00')) * 1000, // to javascript value
                (int) Statistics::find()->andWhere([
                    'type_id' => Statistics::TYPE_USER_VISIT,
                ])->andWhere([
                    '>=', 'created_at', $date->format('Y-m-d 00:00:00')
                ])->andWhere([
                    '<=', 'created_at', $date->format('Y-m-d 23:59:59')
                ])->select('ip')->distinct()->count(),
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionFavorite($company_id)
    {
        $model = Company::findOne($company_id);
        if (!$model) throw new NotFoundHttpException;

        // Берем минимальную дату
        $min_date = Statistics::find()->andWhere([
            'type_id' => [Statistics::TYPE_FAVORITE_REMOVED, Statistics::TYPE_FAVORITE_ADDED],
        ])->min('created_at');

        $daterange = new \DatePeriod(new \DateTime($min_date), new \DateInterval('P1D'), new \DateTime('+1 day'));
        $result = [
            'additions' => [],
            'removals' => [],
        ];

        foreach($daterange as $date)
        {
            $result['additions'][] = [
                strtotime($date->format('Y-m-d 00:00:00')) * 1000, // to javascript value
                (int) Statistics::find()->joinWith(['service'])->andWhere([
                    'statistics.type_id' => Statistics::TYPE_FAVORITE_ADDED,
                    'service.company_id' => $model->id,
                ])->andWhere([
                    '>=', 'statistics.created_at', $date->format('Y-m-d 00:00:00')
                ])->andWhere([
                    '<=', 'statistics.created_at', $date->format('Y-m-d 23:59:59')
                ])->select('ip')->distinct()->count(),
            ];
            $result['removals'][] = [
                strtotime($date->format('Y-m-d 00:00:00')) * 1000, // to javascript value
                (int) Statistics::find()->joinWith(['service'])->andWhere([
                    'statistics.type_id' => Statistics::TYPE_FAVORITE_REMOVED,
                    'service.company_id' => $model->id,
                ])->andWhere([
                    '>=', 'statistics.created_at', $date->format('Y-m-d 00:00:00')
                ])->andWhere([
                    '<=', 'statistics.created_at', $date->format('Y-m-d 23:59:59')
                ])->select('ip')->distinct()->count(),
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionServiceDisplays($company_id)
    {
        $model = Company::findOne($company_id);
        if (!$model) throw new NotFoundHttpException;

        // Берем минимальную дату
        $min_date = Statistics::find()->andWhere([
            'type_id' => Statistics::TYPE_SERVICE_DISPLAY,
        ])->min('created_at');

        $daterange = new \DatePeriod(new \DateTime($min_date), new \DateInterval('P1D'), new \DateTime('+1 day'));
        $result = [];

        foreach($daterange as $date)
        {
            $result[] = [
                strtotime($date->format('Y-m-d 00:00:00')) * 1000, // to javascript value
                (int) Statistics::find()->joinWith(['service'])->andWhere([
                    'statistics.type_id' => Statistics::TYPE_SERVICE_DISPLAY,
                    'service.company_id' => $model->id,
                ])->andWhere([
                    '>=', 'statistics.created_at', $date->format('Y-m-d 00:00:00')
                ])->andWhere([
                    '<=', 'statistics.created_at', $date->format('Y-m-d 23:59:59')
                ])->select('ip')->count(),
            ];
        }
    
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionServiceViews($company_id)
    {
        $model = Company::findOne($company_id);
        if (!$model) throw new NotFoundHttpException;

        // Берем минимальную дату
        $min_date = Statistics::find()->andWhere([
            'type_id' => Statistics::TYPE_SERVICE_VIEW,
        ])->min('created_at');

        $daterange = new \DatePeriod(new \DateTime($min_date), new \DateInterval('P1D'), new \DateTime('+1 day'));
        $result = [];

        foreach($daterange as $date)
        {
            $result[] = [
                strtotime($date->format('Y-m-d 00:00:00')) * 1000, // to javascript value
                (int) Statistics::find()->joinWith(['service'])->andWhere([
                    'statistics.type_id' => Statistics::TYPE_SERVICE_VIEW,
                    'service.company_id' => $model->id,
                ])->andWhere([
                    '>=', 'statistics.created_at', $date->format('Y-m-d 00:00:00')
                ])->andWhere([
                    '<=', 'statistics.created_at', $date->format('Y-m-d 23:59:59')
                ])->select('ip')->count(),
            ];
        }
    
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }
}
