<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

use common\models\GeoLocation;
use common\models\GeoRegion;

class GeoController extends \yii\web\Controller
{
    public function actionLocationDelete()
    {
        $models = GeoLocation::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные населенные пункты успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionLocationUpdate($region_id, $id)
    {
        $region = GeoRegion::findOne($region_id);
        if ($id)
        {
            $model = GeoLocation::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new GeoLocation([
                'region_id' => $region_id,
            ]);
        }        

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->addFlash('success', 'Данные населенного пункта успешно сохранены');
            return $this->redirect(['/geo/locations', 'region_id' => $region_id]);
        }
        return $this->render('location-update', [
            'model' => $model,
            'region' => $region,
        ]);
    }

    public function actionLocations($region_id)
    {
        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $locations = GeoLocation::find()->where([
                'region_id' => $region_id,
            ])->orderBy(['name' => SORT_ASC])->all();

            return array_map(function($model){
                return [
                    'id' => $model->id,
                    'name' => $model->name,
                ];
            }, $locations);
        }


        $model = GeoRegion::findOne($region_id);
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getLocations(),
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
            ],
        ]);
        return $this->render('locations', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    public function actionRegionDelete()
    {
        $models = GeoRegion::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные регионы успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRegionUpdate($id)
    {
        if ($id)
        {
            $model = GeoRegion::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new GeoRegion;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            \yii::$app->session->addFlash('success', 'Данные региона успешно сохранены');
            return $this->redirect(['/geo/regions']);
        }
    
        return $this->render('region-update', [
            'model' => $model,
        ]);
    }


    public function actionRegions()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => GeoRegion::find(),
            'sort' => [
                'defaultOrder' => [
                    'name'=>SORT_ASC
                ],
            ],
        ]);

        return $this->render('regions', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
