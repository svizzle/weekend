<?php

namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;

class SettingController extends \yii\web\Controller
{
    public function actionUpdate()
    {
        $model = new \backend\forms\SettingUpdate;

        if ($model->load(Yii::$app->request->post()))
        {
            $model->frontpage_static_banner_file = UploadedFile::getInstance($model, 'frontpage_static_banner_file');
            $model->frontpage_dynamic_banner_file = UploadedFile::getInstance($model, 'frontpage_dynamic_banner_file');

            if ($model->save())
            {
                Yii::$app->session->addFlash('success', 'Настройки успешно сохранены');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('update',[
            'model' => $model,
        ]);
    }
}
