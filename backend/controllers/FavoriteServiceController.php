<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;

use common\models\FavoriteService;
use yii\data\ArrayDataProvider;
use yii\db\Query;

class FavoriteServiceController extends \yii\web\Controller
{
    public function actionDelete()
    {
        $models = FavoriteService::find()
            ->where(['id' => Yii::$app->request->post('ids')])
        ->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные услуги удалены из избранного ');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionIndex()
    {
        $query = (new Query())
            ->select('fs.id, s.id as service_id, s.name, c.name as company_name')
            ->from('favorite_service fs')
            ->innerJoin('service s', 's.id = fs.service_id')
            ->innerJoin('company c', 'c.id = s.company_id');

        $dataProvider = new ArrayDataProvider([
            'key' => 'id',
            'allModels' => $query->all(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
