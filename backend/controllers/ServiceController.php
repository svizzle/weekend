<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use common\models\Service;
use common\models\ServicePhoto;
use common\models\ServicePrice;
use common\models\ServicePriceUnit;
use common\models\ServiceSearch;
use common\models\ServiceType;
use common\models\ServiceTypeAttribute;
use common\models\ServiceTypeAttributeValue;

class ServiceController extends \yii\web\Controller
{
    public function actionPhotoSort()
    {
        $model = ServicePhoto::findOne(Yii::$app->request->post('id'));
        if (!$model) throw new NotFoundHttpException;

        $position = Yii::$app->request->post('position');
        $model->order($position, 'service_id');
        $model->save();
    }

    public function actionTypeCopy($id)
    {
        $model = ServiceType::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        $copy = new ServiceType;
        $copy->attributes = $model->attributes;
        $copy->name .= " (Копия)";
        $copy->save();

        foreach($model->serviceAttributes as $oldAttribute)
        {
            $newAttribute = new ServiceTypeAttribute;
            $newAttribute->attributes = $oldAttribute->attributes;
            $newAttribute->service_type_id = $copy->id;
            $newAttribute->save(false);

            foreach($oldAttribute->attributeValues as $oldValue)
            {
                $newValue = new ServiceTypeAttributeValue;
                $newValue->attributes = $oldValue->attributes;
                $newValue->service_type_attribute_id = $newAttribute->id;

                if (!empty($oldValue->icon))
                {
                    $path = Yii::getAlias('@frontend').'/web/uploads/service_type_attribute_values/';
                    list($oldName, $extension) = explode(".", $oldValue->icon);
                    $newValue->icon = Yii::$app->security->generateRandomString(16).".".$extension;
                    copy($path.$oldValue->icon, $path.$newValue->icon);
                }
                $newValue->save(false);                
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPriceUnitDelete()
    {
        $models = ServicePriceUnit::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();
        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные единицы измерения успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPriceUnitUpdate($id)
    {
        if ($id)
        {
            $model = ServicePriceUnit::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new ServicePriceUnit;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->addFlash('success', 'Еденица измерения успешно сохранена');
            return $this->goBack();
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('price-unit-update', [
            'model' => $model,
        ]);
    }

    public function actionPriceUnit()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ServicePriceUnit::find(),
        ]);

        return $this->render('price-unit', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCopy($id)
    {
        $model = Service::findOne($id);
        if (!$model) throw NotFoundHttpException;

        $copy = new Service;
        $copy->attributes = $model->attributes;
        $copy->code = null;
        $copy->name = $copy->name.' (Копия)';
        $copy->save();

        foreach($model->attributeValues as $value)
        {
            $copy->link('attributeValues', $value);
        }

        foreach($model->prices as $price)
        {
            $priceCopy = new ServicePrice;
            $priceCopy->attributes = $price->attributes;
            $priceCopy->service_id = $copy->id;
            $priceCopy->save(false);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionToggle($id)
    {
        $model = Service::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        $model->enabled = !$model->enabled;
        $model->save(false, ['enabled']);

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDelete()
    {
        $models = Service::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные услуги удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionIndex()
    {
        $searchModel = new ServiceSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionUpdate($id, $company_id)
    {
        if ($id)
        {
            $model = Service::findOne($id);
            if (!$model) throw NotFoundHttpException;
        }
        else
        {
            $model = new Service([
                'company_id' => $company_id,
            ]);

            $model->validate(['code']); // инициализируем ID услуги
        }

        $service = Yii::$app->request->post('Service');
        $values = isset($service['attributeValues']) ? ServiceTypeAttributeValue::find()->andWhere([
            'id' => $service['attributeValues']
        ])->all() : $model->attributeValues;

        if (Yii::$app->request->isPost)
        {
            $count = count(Yii::$app->request->post('ServicePrice', []));
            $prices = [new ServicePrice];
            for($i = 1; $i < $count; $i++) $prices[] = new ServicePrice;
        }
        else
        {
            $prices = $model->prices;
            if (empty($prices)) $prices = [new ServicePrice];
        }


        if (
            $model->load(Yii::$app->request->post())
            && Model::loadMultiple($prices, Yii::$app->request->post()) 
        )
        {
            if (
                $model->validate()
                && Model::validateMultiple($prices)
            )
            {
                if ($model->save())
                {
                    $model->unlinkAll('attributeValues', true);
                    foreach($values as $value)
                    {
                        $model->link('attributeValues', $value);
                    }

                    foreach($model->prices as $price) $price->delete();
                    foreach($prices as $price)
                    {
                        $price->service_id = $model->id;
                        $price->save();
                    }

                    Yii::$app->session->addFlash('success', 'Услуга успешно сохранена');
                    return $this->goBack();
                }
            }
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
            $photos = ServicePhoto::find()->andWhere(['service_id' => null, 'user_id' => Yii::$app->user->id])->all();
            foreach($photos as $photo) $photo->delete();
        }

        $types = ServiceType::find()->orderBy(['name' => SORT_ASC])->all();

        $type = $model->type ? $model->type : $types[0];

        return $this->render('update', [
            'values' => $values,
            'prices' => $prices,
            'model' => $model,
            'types' => $types,
            'type' => $type,
        ]);
    }

    public function actionUpdateAttributes($service_id, $type_id)
    {
        if ($service_id)
        {
            $service = Service::findOne($service_id);
            if (!$service) throw NotFoundHttpException;
        }
        else
        {
            $service = new Service;
        }

        $type = ServiceType::findOne($type_id);
        if (!$type) throw new NotFoundHttpException;
        
        return $this->renderAjax('update-attributes', [
            'type' => $type,
            'service' => $service,
        ]);
    }

    public function actionPhotoDelete($service_id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = ServicePhoto::findOne(Yii::$app->request->post('key'));
        $model->delete();

        return [
            'key' => Yii::$app->request->post('key'),
        ];
    }

    public function actionPhotoUpload($service_id)
    {
        $file = UploadedFile::getInstanceByName('image');

        $model = new ServicePhoto([
            'service_id' => $service_id ? $service_id : null,
            'path' => Yii::$app->security->generateRandomString(16).".".$file->extension,
        ]);
        $model->save();

        $path = Yii::getAlias('@frontend').'/web/uploads/services/';
        $file->saveAs($path.$model->path);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    
        return [
            'initialPreview' => [
                \common\widgets\ServiceImage::widget(['model' => $model, 'width' => 150, 'height' => 100]),
            ],
            'initialPreviewConfig' => [
                [
                    'key' => $model->id,
                    'url' => Url::to(['/service/photo-delete', 'service_id' => $service_id]),
                ],
            ],
        ];

    }

    public function actionTypeAttributeValueDelete()
    {
        $models = ServiceTypeAttributeValue::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные значения успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTypeAttributeValueUpdate($id, $attribute_id)
    {
        $attribute = ServiceTypeAttribute::findOne($attribute_id);
        if (!$attribute) throw NotFoundHttpException;

        if ($id)
        {
            $model = ServiceTypeAttributeValue::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new ServiceTypeAttributeValue([
                'service_type_attribute_id' => $attribute_id,
            ]);
        }

        if ($model->load(Yii::$app->request->post()))
        {
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
            if ($model->validate())
            {
                if ($model->file)
                {
                    $path = Yii::getAlias('@frontend').'/web/uploads/service_type_attribute_values/';
                    $name = Yii::$app->security->generateRandomString(16).".".$model->file->extension;
                    $model->icon = $name;
                    $model->file->saveAs($path.$name, false);
                }
                $model->save();
                Yii::$app->session->addFlash('success', 'Значение успешно сохранено');
                return $this->goBack();
            }
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('type-attribute-value-update', [
            'attribute' => $attribute,
            'model' => $model,
        ]);
    }

    public function actionTypeAttributeValue($attribute_id)
    {
        $attribute = ServiceTypeAttribute::findOne($attribute_id);
        if (!$attribute) throw NotFoundHttpException;

        $dataProvider = new ActiveDataProvider([
            'query' => ServiceTypeAttributeValue::find()->andWhere([
                'service_type_attribute_id' => $attribute_id,
            ]),
        ]);

        return $this->render('type-attribute-value',[
            'attribute' => $attribute,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTypeAttributeDelete()
    {
        $models = ServiceTypeAttribute::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные характеристики успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTypeAttributeUpdate($id, $type_id)
    {
        $type = ServiceType::findOne($type_id);
        if (!$type) throw NotFoundHttpException;

        if ($id)
        {
            $model = ServiceTypeAttribute::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new ServiceTypeAttribute([
                'service_type_id' => $type_id,
            ]);
        }

        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $model->save();

                Yii::$app->session->addFlash('success', 'Характеристика успешно сохранена');
                return $this->redirect(['type-attribute', 'type_id' => $type_id]);
            }
        }

        return $this->render('type-attribute-update', [
            'type' => $type,
            'model' => $model,
        ]);
    }

    public function actionTypeAttribute($type_id)
    {
        $type = ServiceType::findOne($type_id);
        if (!$type) throw new NotFoundHttpException;

        $dataProvider = new ActiveDataProvider([
            'query' => ServiceTypeAttribute::find()->andWhere([
                'service_type_id' => $type_id,
            ]),
        ]);

        return $this->render('type-attribute', [
            'type' => $type,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTypeDelete()
    {
        $models = ServiceType::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные виды услуг успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTypeView($id)
    {
        $model = ServiceType::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        echo $model->description;
    }

    public function actionTypeUpdate($id)
    {
        if ($id)
        {
            $model = ServiceType::findOne($id);
            if (!$model) throw new NotFoundHttpException;
        }
        else
        {
            $model = new ServiceType;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->addFlash('success', 'Вид услуги успешно сохранен');
            return $this->redirect(['/service/type']);
        }

        return $this->render('type-update', [
            'model' => $model,
        ]);
    }

    public function actionType()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ServiceType::find(),
        ]);

        if (!Yii::$app->user->can('admin'))
        {
            $dataProvider->query->andWhere(['user_id' => Yii::$app->user->id]);
        }

        return $this->render('type', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
