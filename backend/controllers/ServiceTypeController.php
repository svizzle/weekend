<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use common\models\ServiceType;

class ServiceTypeController extends \yii\web\Controller
{
    public function actionGet($id)
    {
        $model = ServiceType::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'id' => $model->id,
            'name' => $model->name,
            'attributes' => array_map(function($attribute){
                return [
                    'id' => $attribute->id,
                    'name' => $attribute->name,
                    'values' => array_map(function($value){
                        return [
                            'id' => $value->id,
                            'name' => $value->name,
                        ];
                    }, $attribute->attributeValues),
                ];                
            }, $model->serviceAttributes),
        ];
    }
}
