<?php

namespace backend\controllers;

use common\models\Statistics;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use common\models\User;
use common\models\UserSearch;
use common\models\UserInvite;
use common\models\UserInviteSearch;

class UserController extends \yii\web\Controller
{
    public function actionToggle($id)
    {
        $model = User::findOne($id);
        if (!$model) throw new NotFoundHttpException;
    
        $model->enabled = !$model->enabled;
        $model->save(false, ['enabled']);

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDelete()
    {
        $ids = Yii::$app->request->post('ids');
        $models = User::find()->andWhere(['id' => $ids])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные пользователи удалены');

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdate($id)
    {
        if ($id)
        {
            $model = User::findOne($id);
            if (!$model) throw NotFoundHttpException;
        }
        else
        {
            $model = new User;
        }

        $passwordModel = new \common\forms\UserChangePassword([
            'isNewRecord' => $model->isNewRecord,
        ]);

        if ($model->load(Yii::$app->request->post()) && $passwordModel->load(Yii::$app->request->post()))
        {
            $model->photo_file = \yii\web\UploadedFile::getInstance($model, 'photo_file');
            if ($model->validate() && $passwordModel->validate())
            {   
                if ($model->photo_file)
                {
                    $path = Yii::getAlias('@frontend').'/web/uploads/users/';
                    $name = Yii::$app->security->generateRandomString(16).".".$model->photo_file->extension;
                    $model->photo = $name;
                    $model->photo_file->saveAs($path.$name, false);
                }
                
                if (!empty($passwordModel->password)) $model->password = $passwordModel->password;

                // Отключаем валидацию, т.к. она уже прошла и она ломает дату
                // @todo Переделать на DateBehavior
                $model->save(false);
                Yii::$app->session->addFlash('success', 'Данные пользователя успешно сохранены');
                return $this->goBack();
            }
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('update', [
            'model' => $model,
            'passwordModel' => $passwordModel,
        ]);
    }

    public function actionView($id)
    {
        $model = User::findOne($id);
        if (!$model) throw NotFoundHttpException;

        $visits_count = Statistics::find()
            ->where(['type_id'=>Statistics::TYPE_USER_VISIT])
            ->andWhere('created_at > DATE_SUB(now(), INTERVAL 30 DAY)')
            ->count();

        $service_display = Statistics::find()
            ->innerJoin(['service s', 'service_id = s.id'])
            ->innerJoin('company c', 'c.user_id = ' . $model->id)
            ->where(['statistics.type_id'=>Statistics::TYPE_SERVICE_DISPLAY])
            ->andWhere('s.company_id = c.id')
            ->andWhere('statistics.created_at > DATE_SUB(now(), INTERVAL 30 DAY)')
            ->count();

        $redirects_service =  Statistics::find()
            ->innerJoin(['service s', 'service_id = s.id'])
            ->innerJoin('company c', 'c.user_id = ' . $model->id)
            ->where(['statistics.type_id'=>Statistics::TYPE_SERVICE_VIEW])
            ->andWhere('s.company_id = c.id')
            ->andWhere('statistics.created_at > DATE_SUB(now(), INTERVAL 30 DAY)')
            ->count();

        $favorites = Statistics::find()
            ->innerJoin(['service s', 'service_id = s.id'])
            ->innerJoin('company c', 'c.user_id = ' . $model->id)
            ->where(['statistics.type_id'=>Statistics::TYPE_FAVORITE_ADDED])
            ->andWhere('s.company_id = c.id')
            ->andWhere('statistics.created_at > DATE_SUB(now(), INTERVAL 30 DAY)')
            ->count();

        return $this->render('view', [
            'model' => $model,
            'visits_count'=>$visits_count,
            'service_display'=>$service_display,
            'redirects_service'=>$redirects_service,
            'favorites'=>$favorites
        ]);
    }

    public function actionInviteDelete()
    {
        $models = UserInvite::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach($models as $model)
        {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные приглашения успешно удалены');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionInviteUpdate($id)
    {
        if ($id)
        {
            $model = UserInvite::findOne($id);
            if (!$model) throw NotFoundHttpException;
        }
        else
        {
            $model = new UserInvite;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $url = Yii::$app->frontendUrlManager->createAbsoluteUrl(['/user/register', 'code' => $model->code]);
            Yii::$app->session->addFlash('success', 'Приглашение успешно сохранено. Ссылка на регистрацию пользователя <a href="#">'.$url.'</a>');

            return $this->goBack();
        }
        else
        {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('invite-update', [
            'model' => $model,
        ]);
    }

    public function actionInvite()
    {
        $searchModel = new UserInviteSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('invite', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionRegister($code = false)
    {
        $invite = UserInvite::find()->andWhere(['code' => $code, 'user_id' => null])->one();
        //if (!$invite) throw new NotFoundHttpException;

        $model = new \frontend\forms\UserRegister;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $user = new User([
                'email' => $model->email,
                'contract_number' => $invite ? $invite->contract_number : null,
                'role' => 'partner', 
            ]);
            $user->setPassword($model->password);
            $user->save();
            
            $invite->user_id = $user->id;
            $invite->save(false, ['user_id']);

            Yii::$app->user->login($user, 60*60*24*365);
            return $this->redirect(['/company/form']);
        }

        $this->layout = 'login';
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) return $this->redirect(['/site/index']);

        $model = new \backend\forms\UserLogin;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            Yii::$app->user->login($model->getUser(), $model->rememberMe ? 60*60*24*365 : 0);
            return $this->redirect(['/site/index']);
        }

        $this->layout = 'login';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(Yii::$app->frontendUrlManager->createUrl(['/site/index']));
    }
}
