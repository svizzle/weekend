<?php

namespace backend\controllers;

use common\models\Setting;
use Yii;

class SiteController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['/user/view', 'id' => Yii::$app->user->id]);
    }

    public function actionFeedback()
    {
        $user = Yii::$app->user->identity;

        $model = new \frontend\forms\SiteFeedback;
        $model->name = $user->last_name . ' ' . $user->first_name . ' ' . $user->father_name;
        $model->email = $user->email;

        if ($model->load(Yii::$app->request->post()) ){

            $model->name = $user->last_name . ' ' . $user->first_name . ' ' . $user->father_name;
            $model->email = $user->email;

            if($model->validate()){
                Yii::$app->mailer->compose('site_feedback', ['model' => $model])
                    ->setTo(Setting::findOne(['name' => 'feedback_email'])->value)
                    ->setSubject(Setting::findOne(['name' => 'feedback_subject'])->value)
                    ->send();

                $model = new \frontend\forms\SiteFeedback;
                Yii::$app->session->addFlash('success', 'Спасибо, ваше сообщение отправлено.');
            }
        }

        return $this->render('feedback', [
            'model' => $model,
        ]);
    }
}
