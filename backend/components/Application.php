<?php

namespace backend\components;

use Yii;

class Application extends \yii\web\Application
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,            
                        'controllers' => ['user'],
                        'actions' => ['login', 'register'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,            
                        'controllers' => ['site'],
                        'actions' => ['error'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        if (!Yii::$app->user->isGuest && empty(Yii::$app->user->identity->email))
        {
            return Yii::$app->response->redirect(Yii::$app->frontendUrlManager->createUrl(['/user/email']));
        }

        return true;
    }
}
