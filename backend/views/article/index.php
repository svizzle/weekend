<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;

$this->title = 'Статьи';
$this->params['header'] = $this->title;
$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить', ['update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonDelete::widget(),
];

?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'width' => 50,
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->name, ['update', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'slug',
                'headerOptions' => [
                    'width' => 200,
                ],
            ],
        ],
    ])?>
    </div>
</div>
        
