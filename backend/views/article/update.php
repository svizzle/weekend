<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\ServiceType;

$this->params['header'] = $model->isNewRecord ? 'Добавление статьи' : 'Редактирование статьи';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Статьи',
        'url' => ['index'],
    ],
    $this->params['header']
];
?>
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

   // echo $form->field($model, 'service_type_id')
    //          ->dropDownList(ArrayHelper::map(ServiceType::find()->orderBy('name')->all(), 'id', 'name'));


    echo '<label class="control-label">Виды услуг</label>';
    echo \kartik\select2\Select2::widget([
        'value' => ArrayHelper::getColumn($model->articleServiceTypes, 'service_id'),
        'data' => ArrayHelper::map(ServiceType::find()->all(), 'id', 'name'),
        'options' => [
            'id' => Yii::$app->security->generateRandomString(),
            'multiple' => true,
            'prompt' => '...',
            'unselect' => null,
        ],
        'name' => 'Article[service_types_input][]',
    ]);

    echo $form->field($model, 'name');

    echo $form->field($model, 'slug');

    echo $form->field($model, 'short_text')->widget('backend\widgets\Imperavi');

    echo $form->field($model, 'full_text')->widget('backend\widgets\Imperavi');
?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
