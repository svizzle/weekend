<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;

$this->title = 'Дополнительное меню';
$this->params['header'] = $this->title;
$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить', ['update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonEdit::widget(),
    GridViewButtonDelete::widget(),
];

?>
<div class="box">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'headerOptions' => [
                    'width' => 50,
                ],
                'class' => CheckboxColumn::className(),
            ],
            'name',
            'url',
        ],
    ])?>
    </div>
</div>
