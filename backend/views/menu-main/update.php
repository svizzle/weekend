<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? 'Добавление' : 'редактирование').' пункта меню';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Дополнительное меню',
        'url' => ['index'],
    ],
    $this->title,
];

?>
<div class="box">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'name');

    echo $form->field($model, 'url');
?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
    

