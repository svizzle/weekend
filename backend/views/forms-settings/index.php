<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\editable\EditableAsset;

EditableAsset::register($this);

use kartik\editable\EditablePjaxAsset;

EditablePjaxAsset::register($this);

use kartik\popover\PopoverXAsset;

PopoverXAsset::register($this);


/* @var $this yii\web\View */
/* @var $searchModel common\search\FormsSettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки форм';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forms-settings-repository-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'form',
                'value'=>function($data){
                   return $data->formName;
                },
                'filter'=> Html::activeDropDownList($searchModel, 'form', \common\repositories\FormsSettingsRepository::$forms_list,['class'=>'form-control','prompt' => 'Выберите форму']),
            ],
            'field_name',
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'label',
                'pageSummary'=>true,
                'editableOptions'=> function ($model, $key, $index) {
                    return [
                        'header'=>'labelll',
                        'size'=>'md',
                        'asPopover'=>false,
                    ];
                }
            ],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'hint',
                'pageSummary'=>true,
                'editableOptions'=> function ($model, $key, $index) {
                    return [
                        'header'=>'Подсказка',
                        'size'=>'md',
                        'asPopover'=>false,
                    ];
                }
            ],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'placeholder',
                'pageSummary'=>true,
                'editableOptions'=> function ($model, $key, $index) {
                    return [
                        'header'=>'Placeholder',
                        'size'=>'md',
                        'asPopover'=>false,
                    ];
                }
            ],
            // 'default',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
