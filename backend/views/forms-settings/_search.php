<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\search\FormsSettingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="forms-settings-repository-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'form') ?>

    <?= $form->field($model, 'field_name') ?>

    <?= $form->field($model, 'label') ?>

    <?= $form->field($model, 'hint') ?>

    <?php // echo $form->field($model, 'default') ?>

    <?php // echo $form->field($model, 'placeholder') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
