<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\repositories\FormsSettingsRepository */

$this->title = 'Create Forms Settings Repository';
$this->params['breadcrumbs'][] = ['label' => 'Forms Settings Repositories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forms-settings-repository-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
