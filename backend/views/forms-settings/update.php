<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\repositories\FormsSettingsRepository */

$this->title = 'Update Forms Settings Repository: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Forms Settings Repositories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="forms-settings-repository-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
