<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\Advertise;

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование').' рекламы';
$this->params['header'] = $this->title;

?>

<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
]);
?>
<div class="box box-primary">
    <div class="box-body">
    <?php
        
        echo $form->field($model, 'type_id')
                  ->dropDownList([
                        Advertise::TYPE_GOOGLE => 'Google AdSense',
                        Advertise::TYPE_YANDEX => 'Яндекс.Директ',
                        Advertise::TYPE_BANNER => 'Баннер',
                        Advertise::TYPE_CUSTOM => 'Произвольный код',
                  ]);

        echo $form->field($model, 'position')
                  ->dropDownList([
                        Advertise::POSITION_CONTENT => 'Контент',
                        Advertise::POSITION_SIDEBAR => 'Сайдбар',
                  ]);    

        echo $form->field($model, 'name')
                  ->textInput();    

        echo $form->field($model, 'description')
                  ->textarea();

        echo $form->field($model, 'link')
                  ->textInput();

        if (!empty($model->image))
        {
            ?>
            <div class="form-group field-advertise-file">
                <label>Текущее изображение</label>
                <div>
                <?=\common\widgets\AdvertiseImage::widget(['model' => $model])?>
                </div>
            </div>
            <?php
        }

        echo $form->field($model, 'file')
                  ->fileInput();

        echo $form->field($model, 'code')
                  ->textarea(['rows' => 10]);

        echo $form->field($model, 'region_id')
                  ->dropDownList(ArrayHelper::map($regions, 'id', 'name'), ['prompt' => 'Любой']);

        echo $form->field($model, 'location_id')
                  ->dropDownList([], ['prompt' => 'Любой']);

        echo $form->field($model, 'service_type_id')
                  ->dropDownList(ArrayHelper::map($serviceTypes, 'id', 'name'), ['prompt' => 'Любой']);

        echo $form->field($model, 'radius')
                  ->textInput();

        echo $form->field($model, 'begin_date')
                  ->textInput();

        echo $form->field($model, 'end_date')
                  ->textInput();

    ?>
    </div>
    <div class="box-footer">
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    </div>
</div>
<?php
$form->end();

$this->registerJs("
    
    $('#advertise-type_id').change(function(){
        
        var type_id = parseInt($(this).val());

        $('.form-group').show();

        switch(type_id)
        {
            case 1:     // Google AdSense
                // Ссылка
                $('.field-advertise-link').hide();
                // Загрузка файла
                $('.field-advertise-file').hide();
                break;
            case 2:     // Яндекс.Директ
                // Ссылка
                $('.field-advertise-link').hide();
                // Загрузка файла
                $('.field-advertise-file').hide();
                break;
            case 3:     // Баннер
                // HTML код
                $('.field-advertise-code').hide();
                break;
            case 4:     // HTML код
                // Ссылка
                $('.field-advertise-link').hide();
                // Загрузка файла
                $('.field-advertise-file').hide();
                break;
        }

    }).change();


    $('#advertise-region_id').change(function(){
        
        var region_id = parseInt($(this).val() || 0);
        if (region_id != 0)
        {
            $('.field-advertise-location_id').show();
        }
        else
        {
            $('.field-advertise-location_id').hide();
        }        

    }).change();

    $('#advertise-region_id').change(function(){
        
        var region_id = parseInt($(this).val() || 0);
        
        var selector = '#advertise-location_id';
        $(selector).html('<option>Любой</option>');

        if (region_id == 0) return;

        $.get('/cabinet/geo/locations?region_id='+region_id, function(response){        
            for (var i in response)
            {
                $(selector).append($('<option value=\"'+ response[i].id + '\">' + response[i].name + '</option>'));
            }
        });
    });

    

");
