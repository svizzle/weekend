<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;

use common\models\Advertise;

$this->title = 'Реклама';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить', ['update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonDelete::widget(),
];

?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'width' => 50,
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->name, ['update', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'type_id',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model->type_id)
                    {
                        case Advertise::TYPE_GOOGLE: return 'Google AdSense';
                        case Advertise::TYPE_YANDEX: return 'Яндекс.Директ';
                        case Advertise::TYPE_BANNER: return 'Баннер';
                        case Advertise::TYPE_CUSTOM: return 'HTML Код';
                    }
                    return '???';
                }
            ],
            [
                'attribute' => 'position',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model->position)
                    {
                        case Advertise::POSITION_CONTENT: return 'Контент';
                        case Advertise::POSITION_SIDEBAR: return 'Сайдбар';
                    }
                    return '???';
                }
            ]
        ],
    ])?>
    </div>
</div>
