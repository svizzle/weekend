<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['header'] = $model->isNewRecord ? 'Добавление страницы' : 'Редактирование страницы';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Страницы',
        'url' => ['/page/index'],
    ],
    $this->params['header']
];
?>
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'name');

    echo $form->field($model, 'slug');

    echo $form->field($model, 'text')->widget('backend\widgets\Imperavi');
?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
