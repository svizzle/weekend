<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use backend\widgets\GridViewButtonAppend;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonFilter;
use common\models\CompanyType;

$this->params['header'] = 'Компании';

$this->params['toolbar-right'] = [
//    Html::a('<i class="fa fa-plus"></i> Добавить', '#', ['class' => 'btn btn-success', 'disabled' => true]),
    GridViewButtonAppend::widget(['title' => 'Услуга', 'url' => ['/service/update', 'id' => 0, 'company_id' => '_id_']]),
    GridViewButtonFilter::widget(),
    Html::a('<i class="fa fa-refresh"></i> Сброс', ['index'],  ['class' => 'btn btn-primary']),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model, $key, $index, $grid) {
            if (!$model->enabled) return ['class' => 'bg-danger'];
            return [];
        },
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'width' => 50
                ],
            ],
            [
                'header' => 'ID',
                'attribute' => 'userContractNumber',
                'format' => 'raw',
                'headerOptions' => [
                    'width' => 50
                ],
                'value' => function($model) {
                    return Yii::$app->formatter->asText(empty($model->user->contract_number) ? null : $model->user->contract_number);
                }
            ],
            [
                'header' =>'Наименование компании',
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a(Html::encode($model->name), ['/company/update', 'id' => $model->id, 'user_id' => $model->user_id]);
                }
            ],
            [
                'attribute' => 'type_id',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->type->name;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'type_id',
                    ArrayHelper::map(
                        CompanyType::find()->orderBy(['name' => SORT_ASC])->all(),
                        'id',
                        'name'
                    ),[
                        'class' => 'form-control',
                        'prompt' => '...',
                    ]
                ),
                'headerOptions' => [
                    'width' => 100,
                ],
            ],
            [
                'header' => 'Телефон',
                'attribute' => 'phone',
                'format' => 'text',
                'value' => function($model) {
                    return count($model->phones) ? $model->phones[0]->phone : null;
                },
                'headerOptions' => [
                    'width' => 100,
                ],
            ],
            [
                'attribute' => 'locationName',
                'format' => 'raw',
                'header' => 'Адрес',
                'value' => function($model) {
                    return $model->geoLocation->region->name.", ".$model->geoLocation->name;
                },
                'headerOptions' => [
                    //'width' => 100,
                ],
            ],
            [
                'attribute' => 'userName',
                'header' => 'Пользователь',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->user->name;
                },
                'headerOptions' => [
                    'width' => 100,
                ],
            ],
            [
                'header' => 'Проверена',
                'attribute' => 'moderated',
                'format' => 'raw',
                'headerOptions' => [
                    'width' => 100,
                ],
                'value' => function($model) {
                    return $model->moderated
                        ? 
                        Html::a('<i class="fa fa-check"></i>', ['/company/toggle', 'id' => $model->id, 'attribute' => 'moderated'], ['class' => 'btn-sm btn-success', 'title' => 'Заблокировать'])
                        : 
                        Html::a('<i class="fa fa-ban"></i>', ['/company/toggle', 'id' => $model->id, 'attribute' => 'moderated'], ['class' => 'btn-sm btn-danger', 'title' => 'Разблокировать']);
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'moderated',
                    [
                        '0' => 'Не проверена',
                        '1' => 'Проверена',
                    ],
                    [
                        'class' => 'form-control',
                        'prompt' => '...',
                    ]
                ),
            ]
        ],
    ])?>
    </div>
</div>
