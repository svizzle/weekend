<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['header'] = $model->isNewRecord ? 'Добавление вида компании' : 'Редактирование вида компании';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Виды компаний',
        'url' => ['/company/type'],
    ],
    $this->params['header'],
];

?>
<div class="box box-primary">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);

echo $form->field($model, 'name');

echo $form->field($model, 'description')->widget('backend\widgets\Imperavi');

echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();
