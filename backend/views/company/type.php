<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete; 

$this->title = 'Виды компаний';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить', ['type-update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonDelete::widget(['action' => ['type-delete']])
];

?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'width' => 50
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->name, ['type-update', 'id' => $model->id]);
                },
            ],
        ],
    ])?>
    </div>
</div>
