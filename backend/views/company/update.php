<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


use backend\widgets\YandexMapSelector;
use common\models\Company;
use \common\components\FormsSettingsHelper;
use \common\repositories\FormsSettingsRepository;
$this->params['header'] = 'Анкета компании';

$this->params['breadcrumbs'] = [
    $this->params['header'],
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'company-update-form',
]);

?>
<div class="box box-primary">
    <div class="box-body">
<?php

if (Yii::$app->user->can('admin'))
{
    echo $form->field($model, 'enabled')->dropDownList([
        '1' => 'Активна',
        '0' => 'Заблокирована',
    ],[
        'title' => 'Статус компании. Заблокирована или нет',
        'data-toggle' => 'tooltip',
    ]);

    echo $form->field($model, 'moderated')->checkbox([
        'title' => 'Статус модерации. Проверена или нет',
        'data-toggle' => 'tooltip',
    ]);

    ?>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
    <?php

}

echo $form->field($model, 'name', ['template' => '{label}{hint}{input}{error}'])->textInput([
    'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'name', 'title'),
    'data-toggle' => 'tooltip',
    'placeholder'=>FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'name', 'placeholder')
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'name', 'hint'))
    ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'name', 'label'));

if (Yii::$app->user->can('admin'))
    echo $form->field($model, 'sight', ['template' => '{label}{hint}{input}{error}'])->checkbox([
    'title' => 'Является достопримечательностью?',
    'data-toggle' => 'tooltip',
    'label'=>FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'sight', 'label')
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'sight', 'hint'));

echo $form->field($model, 'type_id', ['template' => '{label}{hint}{input}{error}'])->dropDownList(ArrayHelper::map($types, 'id', 'name'),[
    'title' => 'Вид компании',
    'data-toggle' => 'tooltip',
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'type_id', 'hint'))
    ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'type_id', 'label'));

echo $form->field($model, 'description', ['template' => '{label}{hint}<div id="company-description-hint-2" class="hint-block"></div>{input}{error}'])->widget('backend\widgets\Imperavi',[
    'containerOptions' => [
        'title' => 'Описание компании',
        'data-toggle' => 'tooltip',
    ],
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'description', 'hint'))
    ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'description', 'label'));

?>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
    <?php if (Yii::$app->user->can('admin')) : ?>
        <?=$form->field($model, 'contacts', ['template' => '{label}{hint}{input}{error}'])
            ->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'contacts', 'hint'))
            ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'contacts', 'label'))
            ->widget('backend\widgets\Imperavi');
        ?>
    <?php endif; ?>

<div class="hint-block"><?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'contacts_phones_block', 'hint') ?></div>

<?=\unclead\widgets\TabularInput::widget([
    'id' => 'phones-tabularinput',
    'models' => $phones,
    'columns' => [
        [
            'name' => 'phone',
            'title' => 'Номер телефона',
            'enableError' => true,
            'options' => [
                'data-mask' => '',
                'data-inputmask' => '"mask": "+7 (999) 999-99-99"',
            ],
        ],
        [
            'name' => 'comment',
            'title' => 'Комментарий',
        ],
    ],
])?>

    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
<?php


echo $form->field($model, 'geo_location_id')
    ->widget('backend\widgets\GeoLocationSelector', [
        'label_region' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'region_id', 'label'),
        'hint_region' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'region_id', 'hint'),
        'label_city' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'geo_location_id', 'label'),
        'hint_city' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'geo_location_id', 'hint')
    ])->label(false) ;

echo $form->field($model, 'address', ['template' => '{label}{hint}{input}{error}'])->textInput([
    'title' => 'Адрес компании',
    'data-toggle' => 'tooltip',
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'address', 'hint'))
    ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'address', 'label'));

echo $form->field($model, 'lat')->hiddenInput()->label(false);
echo $form->field($model, 'lon')->hiddenInput()->label(false);
echo YandexMapSelector::widget([
    'model' => $model,
]);
?>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
<?php
echo $form->field($model, 'photos', ['template' => '{label}{hint}{input}{error}'])->widget('backend\widgets\MultiImageUpload',[
    'sortable' => true,
    'sortUrl' => ['/company/photo-sort'],
    'uploadUrl' => ['/company/photo-upload', 'company_id' => (int)$model->id],
    'deleteUrl' => ['/company/photo-delete', 'company_id' => (int)$model->id],
    'initialPreview' => function($model) {
        return \common\widgets\CompanyImage::widget(['model' => $model, 'width' => 150, 'height' => 100]);
    },
])  ->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'photos', 'hint'))
    ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'photos', 'label'));
?>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
        <div class="hint-block"><?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_COMPANY, 'workers_phones', 'hint') ?></div>
<?php
echo \unclead\widgets\TabularInput::widget([
    'id' => 'employee-tabularinput',
    'models' => $employee,
    'columns' => [
        [
            'name' => 'name',
            'title' => 'ФИО сотрудника',
            'enableError' => true,
        ],
        [
            'name' => 'phone',
            'title' => 'Номер телефона',
            'enableError' => true,
            'options' => [
                'data-mask' => '',
                'data-inputmask' => '"mask": "+7 (999) 999-99-99"',
            ],
        ],
        [
            'name' => 'comment',
            'title' => 'Комментарий',
        ],
    ],
]);

?>
    </div>
    <div class="box-footer">
<?php
echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();
?>
    </div>
</div>

<?php

$this->registerJs("
    $('[data-mask]').inputmask();

    $('#phones-tabularinput').on('afterAddRow', function(e) {
        $('[data-mask]').inputmask();
    });

    $('#employee-tabularinput').on('afterAddRow', function(e) {
        $('[data-mask]').inputmask();
    });

    $('#company-type_id').change(function(){
        var prev = $(this).data('prev');
        var current = $(this).val();
        $(this).data('prev', current);

        var text = $('#company-description').val();

        if (!prev && text.length != 0) return;

        // Получаем текст соответствующий предыдущему значению
        $.get('/cabinet/company/type-view?id=' + (prev ? prev : current), function(prevText){

            if  (text != prevText && text.length != 0)
            {
                //if (!confirm('Заменить описание компании?')) return;
            }

            $.get('/cabinet/company/type-view?id=' + current, function(text){
                //$('#company-description').redactor('code.set', text);
                $('#company-description-hint-2').html(text);
            }) 

        }) 

    }).change();


    setTimeout(function(){
         $('#company-type_id').change();
      }
    , 400);

");
