<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use common\models\ServicePriceUnit;
use common\models\ServiceTypeAttribute;

$this->params['header'] = $model->isNewRecord ? 'Добавление услуги' : 'Редактирование услуги';

?>
<div class="box box-primary">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);

echo $form->field($model, 'name');

echo $form->field($model, 'type_id')->dropDownList(ArrayHelper::map($types, 'id', 'name'));

Pjax::begin([
    'enablePushState' => false,
    'id' => 'service-attributes',
]);

foreach($type->serviceAttributes as $attribute)
{
    echo '<div class="form-group">';
    echo '<label>'.$attribute->name.'</label>';        

    if ($attribute->type_id == ServiceTypeAttribute::TYPE_MULTIPLE)
    {
//        echo \kartik\select2\Select2::widget([
        echo \dosamigos\multiselect\MultiSelect::widget([
            'value' => ArrayHelper::getColumn($values, 'id'),
            'data' => ArrayHelper::map($attribute->attributeValues, 'id', 'name'),
            'options' => [
                'id' => Yii::$app->security->generateRandomString(),
                'multiple' => true,
                'prompt' => '...',
                'unselect' => null,
            ],
            'name' => 'Service[attributeValues][]',
        ]);
    }
    else
    {        
        $value = 0;
        foreach(ArrayHelper::getColumn($attribute->attributeValues, 'id') as $attributeValueId)
        {
            if (in_array($attributeValueId, ArrayHelper::getColumn($values, 'id')))
            {
                $value = $attributeValueId;
            }
        }

        echo Html::dropDownList('Service[attributeValues][]', $value, ArrayHelper::map($attribute->attributeValues, 'id', 'name'),[
            'prompt' => '...',
            'class' => 'form-control',
        ]);
    }

    echo '</div>';
}

Pjax::end();

echo \backend\widgets\TabularInput::widget([
    'id' => 'phones-tabularinput',
    'models' => $prices,
    'columns' => [
        [
            'title' => 'Дата',
            'name' => 'period',
            'type' => 'backend\widgets\PriceDateSelector',
            'headerOptions' => ['class' => 'col-md-3'],
            'enableError' => true,
        ],        
        [
            'title' => 'Дни недели',
            'name' => 'days',
            'type' => 'backend\widgets\PriceDaySelector',
            'options' => [
                'options' => [
                    'multiple' => true,
                ],
            ],
            'headerOptions' => ['class' => 'col-md-3'],
            'enableError' => true,
        ],
        [
            'title' => 'Время',
            'name' => 'worktime',
            'type' => 'backend\widgets\PriceTimeSelector',
            'headerOptions' => ['class' => 'col-md-3'],
            'enableError' => true,
        ],
        [
            'title' => 'Ед.измерения',
            'name' => 'unit_id',
            'type' => 'dropDownList',
            'items' => ArrayHelper::map(ServicePriceUnit::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
            'headerOptions' => ['class' => 'col-md-1'],
        ],
        [
            'title' => 'Цена',
            'name' => 'price',
            'enableError' => true,
            'headerOptions' => [
                'class' => 'col-md-1',
            ]
        ],
        [
            'title' => 'Скидка, %',
            'name' => 'discount',
            'enableError' => true,
            'headerOptions' => [
                'class' => 'col-md-1',
            ]
        ],
    ],
]);

echo $form->field($model, 'description')->widget('backend\widgets\Imperavi');

echo $form->field($model, 'photos')->widget('backend\widgets\MultiImageUpload',[
    'uploadUrl' => ['/service/photo-upload', 'service_id' => (int)$model->id],
    'deleteUrl' => ['/service/photo-delete', 'service_id' => (int)$model->id],
    'initialPreview' => function($model) {
        return \common\widgets\ServiceImage::widget(['model' => $model]);
    },
]);


echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();

$this->registerJs("

    $('#service-type_id').data('prev', $('#service-type_id').val());

    $('#service-type_id').change(function(){
        var prev = $(this).data('prev');
        var current = $(this).val();
        $(this).data('prev', current);

        if (prev != current)
        {
            $.pjax({url: '/cabinet/service/update-attributes?service_id=".(int)$model->id."&type_id=' + current, container: '#service-attributes', push: false});
        }

        // Получаем текст соответствующий предыдущему значению
        $.get('/cabinet/service/type-view?id=' + prev, function(prevText){

            var text = $('#service-description').val();

            if  (text != prevText && text.length != 0) return;

            $.get('/cabinet/service/type-view?id=' + current, function(text){
                $('#service-description').redactor('code.set', text);
            }) 

        }) 

    }).change();

");
