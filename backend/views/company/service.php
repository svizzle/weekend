<?php

use yii\grid\GridView;
use yii\helpers\Html;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;
use yii\grid\CheckboxColumn;
$this->params['header'] = 'Услуги';
$this->params['breadcrumbs'] = null;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить', ['/service/update', 'id' => 0, 'company_id' => $company->id], ['class' => 'btn btn-success']),
    GridViewButtonEdit::widget(['url' => ['/service/update', 'id' => '_id_', 'company_id'=>intval(Yii::$app->request->get('company_id', 0))]]),
    GridViewButtonDelete::widget(['action' => ['/service/delete']]),
];
?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::className(),
            'headerOptions' => [
                'width' => 50,
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($model) use($company) {
                return Html::a($model->name, ['/service/update', 'id' => $model->id, 'company_id' => $company->id]);
            }
        ],
        [
            'attribute' => 'type_id',
            'format' => 'raw',
            'value' => function($model) {
                return $model->type->name;
            }
        ],
    ],
])?>
    </div>
</div>
