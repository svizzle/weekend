<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Настройки';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];

?>
<div class="box box-primary">
    <div class="box-body">
    <?php
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => [
                'enctype' => 'multipart/form-data',
            ]
        ]);

        echo $form->field($model, 'recent_services_period');

        echo $form->field($model, 'frontpage_static_banner_file')->fileInput();
        echo $form->field($model, 'frontpage_dynamic_banner_file')->fileInput();

        echo $form->field($model, 'feedback_email');
        echo $form->field($model, 'feedback_subject');
    ?>
    </div>
    <div class="box-footer">
    <?php
        echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);

        $form->end();
    ?>
    </div>
</div>
