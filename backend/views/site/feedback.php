<?php
use \yii\widgets\Pjax;
use \yii\helpers\Html;

?>

<div class="main-block call-block">
    <h2>Обратная связь</h2>
    <div class="call-text">Напишите нам о недостатках в работе нашего сайта, что необходимо добавить или изменить для Вашего удобства?</div>
    <?php

    $form = \yii\widgets\ActiveForm::begin([
        'action' => ['/site/feedback'],
        'id' => 'site-feedback-form',
        'enableClientValidation' => false,
        'options' => [
          //  'data-pjax' => true,
        ],
    ]);

    echo $form->field($model, 'name')->textInput(['placeholder' => 'Ваше имя', 'readonly' =>true])->label(false);

    echo $form->field($model, 'email')->textInput(['placeholder' => 'Ваш E-mail', 'readonly' =>true])->label(false);

    echo $form->field($model, 'text')->textarea(['placeholder' => 'Текст сообщения', 'style' => 'height: 118px;'])->label(false);

    echo '<div style="height: 23px;">';
    echo "<input type='submit' class='btn btn-primary' value='Отправить'>";
    echo '</div>';

    $form->end();

    ?>
</div>