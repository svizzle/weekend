<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;
use common\models\ServiceTypeAttribute;

$this->title = $attribute->type->name." - ".$attribute->name." - Значения характеристики";
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-backward"></i> Назад', ['type-attribute', 'type_id' => $attribute->type->id], ['class' => 'btn btn-primary']),
    Html::a('<i class="fa fa-plus"></i> Добавить', ['type-attribute-value-update', 'id' => 0, 'attribute_id' => $attribute->id], ['class' => 'btn btn-success']),
    GridViewButtonEdit::widget(['url' => ['/service/type-attribute-value-update', 'attribute_id' => $attribute->id, 'id' => '_id_']]),
    GridViewButtonDelete::widget(['action' => ['/service/type-attribute-value-delete']]),
];

?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::className(),
            'headerOptions' => [
                'width' => 50
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($model) use($attribute) {
                return Html::a($model->name, ['type-attribute-value-update', 'attribute_id' => $attribute->id, 'id' => $model->id]);
            },
        ],
        [
            'header' => 'Иконка',
            'format' => 'raw',
            'value' => function($model) {
                return \common\widgets\ServiceTypeAttributeValueImage::widget(['model' => $model]);
            },
            'headerOptions' => [
                'width' => 50
            ],
        ],
        [
            'header' => 'Подказка',
            'format' => 'raw',
            'value' => function($model) {
                return $model->icon_title;
            },
            'headerOptions' => [
                'width' => 50
            ],
        ],
    ],
])?>
    </div>
</div>
