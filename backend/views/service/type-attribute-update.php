<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\ServiceTypeAttribute;

$this->params['header'] = $model->isNewRecord ? 'Добавление характеристики' : 'Редактирование характеристики';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Виды услуг',
        'url' => ['/service/type'],
    ],    
    [
        'label' => $type->name,
        'url' => ['/service/type-attribute', 'type_id' => $type->id],
    ],
    $this->params['header']
];

?>
<div class="box box-primary">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
]);

echo $form->field($model, 'name');

echo $form->field($model, 'type_id')->dropDownList([
    ServiceTypeAttribute::TYPE_MULTIPLE => 'Мультивыбор',
    ServiceTypeAttribute::TYPE_SINGLE => 'Одно значение',
]);
?>
    </div>
    <div class="box-footer">
<?php
echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();
?>
    </div>
</div>

