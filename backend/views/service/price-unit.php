<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;

$this->title = 'Единицы измерения';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить', ['/service/price-unit-update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonEdit::widget(['url' => ['/service/price-unit-update', 'id' => '_id_']]),
    GridViewButtonDelete::widget(['action' => ['/service/price-unit-delete']]),
];

?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'width' => 50,
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->name, ['/service/price-unit-update', 'id' => $model->id]);
                }
            ],
        ],
    ])?>
    </div>
</div>


