<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\ServiceTypeAttribute;

$this->params['header'] = $attribute->type->name." - ".$attribute->name." - ".($model->isNewRecord ? 'Добавление значения' : 'Редактирование значения');

$this->params['breadcrumbs'] = [
    [
        'label' => 'Виды услуг',
        'url' => ['/service/type'],
    ],
    [
        'label' => $attribute->type->name,
        'url' => ['/service/type-attribute', 'type_id' => $attribute->type->id],
    ],
    [
        'label' => $attribute->name,
        'url' => ['/service/type-attribute-value', 'attribute_id' => $attribute->id],
    ],
    ($model->isNewRecord ? 'Добавление значения' : 'Редактирование значения')
];
?>
<div class="box box-primary">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
]);

echo $form->field($model, 'name');
?>
    <div class="form-group current-image">    
        <label class="control-label">Текущее изображение</label>
        <div>
            <?=\common\widgets\ServiceTypeAttributeValueImage::widget([
                'model' => $model,
                'width' => 24,
                'height' => 21,
            ])?>
        </div>
    </div>
<?php
    echo $form->field($model, 'file')->fileInput();
    echo $form->field($model, 'icon_title');
?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>

