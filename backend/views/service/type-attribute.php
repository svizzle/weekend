<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;
use common\models\ServiceTypeAttribute;

$this->title = $type->name." - Характеристики";
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-backward"></i> Назад', ['/service/type'], ['class' => 'btn btn-primary']),
    Html::a('<i class="fa fa-plus"></i> Добавить', ['type-attribute-update', 'id' => 0, 'type_id' => $type->id], ['class' => 'btn btn-success']),
    GridViewButtonEdit::widget(['url' => ['/service/type-attribute-update', 'id' => '_id_', 'type_id' => $type->id]]),
    GridViewButtonDelete::widget(['action' => ['/service/type-attribute-delete']]),
];

?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [        
        [
            'class' => CheckboxColumn::className(),
            'headerOptions' => [
                'width' => 50
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($model) {
                return Html::a($model->name, ['type-attribute-value', 'attribute_id' => $model->id]);
            },
        ],
        [
            'attribute' => 'type_id',
            'headerOptions' => [
                'width' => 100,
            ],
            'format' => 'raw',
            'value' => function($model) {
                switch($model->type_id)
                {
                    case ServiceTypeAttribute::TYPE_MULTIPLE:
                        return 'Мультивыбор';
                    case ServiceTypeAttribute::TYPE_SINGLE:
                        return 'Одно значение';
                }
            },
        ],
    ],
])?>
    </div>
</div>
