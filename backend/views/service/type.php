<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonCopy;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;

$this->title = 'Виды услуг';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить', ['/service/type-update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonCopy::widget(['url' => ['type-copy', 'id' => '_id_']]),
    GridViewButtonEdit::widget(['url' => ['type-update', 'id' => '_id_']]),
    GridViewButtonDelete::widget(['action' => ['type-delete']]),
];

?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::className(),
            'headerOptions' => [
                'width' => 50,
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($model) {
                return Html::a($model->name, ['type-attribute', 'type_id' => $model->id]);
            },
        ],
    ],
])?>
    </div>
</div>
