<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use common\models\ServicePriceUnit;
use common\models\ServiceTypeAttribute;
use \common\repositories\FormsSettingsRepository;
use \common\components\FormsSettingsHelper;
$this->params['header'] = $model->isNewRecord ? 'Добавление услуги' : 'Редактирование услуги';

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">
<?php

if (Yii::$app->user->can('admin'))
{
    echo $form->field($model, 'code')->textInput([
        'title' => 'Код услуги - 8 цифр',
        'data-toggle' => 'tooltip',
        'data-mask' => '',
        'data-inputmask' => '"mask": "99999999"',
    ]);    
}

echo $form->field($model, 'name', ['template' => '{label}{hint}{input}{error}'])->textInput([
    'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'name', 'label'),
    'data-toggle' => 'tooltip',
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'name', 'hint'))
  ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'name', 'label'));

echo $form->field($model, 'type_id', ['template' => '{label}{hint}{input}{error}'])->dropDownList(ArrayHelper::map($types, 'id', 'name'), [
    'title' => 'Вид услуги',
    'data-toggle' => 'tooltip',
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'type_id', 'hint'))
  ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'type_id', 'label'));

echo $form->field($model, 'description', ['template' => '{label}{hint}<div id="description-hint-2" class="text-info"></div>{input}{error}'])->widget('backend\widgets\Imperavi',[
    'containerOptions' => [
        'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'description', 'label'),
        'data-toggle' => 'tooltip',
    ],
])->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'description', 'hint'))
  ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'description', 'label'));

?>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
<?php
Pjax::begin([
    'enablePushState' => false,
    'id' => 'service-attributes',
]);

foreach($type->serviceAttributes as $attribute)
{
    echo '<div class="form-group">';
    echo '<label>'.$attribute->name.'</label>';

    if ($attribute->type_id == ServiceTypeAttribute::TYPE_MULTIPLE)
    {
        echo \kartik\select2\Select2::widget([
            'value' => ArrayHelper::getColumn($values, 'id'),
            'data' => ArrayHelper::map($attribute->attributeValues, 'id', 'name'),
            'options' => [
                'id' => Yii::$app->security->generateRandomString(),
                'multiple' => true,
                'prompt' => '...',
                'unselect' => null,
            ],
            'name' => 'Service[attributeValues][]',
        ]);
    }
    else
    {
        $value = 0;
        foreach(ArrayHelper::getColumn($attribute->attributeValues, 'id') as $attributeValueId)
        {
            if (in_array($attributeValueId, ArrayHelper::getColumn($values, 'id')))
            {
                $value = $attributeValueId;
            }
        }

        echo Html::dropDownList('Service[attributeValues][]', $value, ArrayHelper::map($attribute->attributeValues, 'id', 'name'),[
            'prompt' => '...',
            'class' => 'form-control',
        ]);
    }

    echo '</div>';
}

Pjax::end();

?>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
        <div style="font-weight: bold;"><?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'price_block_header', 'label') ?></div>
        <div class="hint-block"><?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'price_block_header', 'hint') ?></div>
<?php
echo \backend\widgets\TabularInput::widget([
    'models' => $prices,
    'columns' => [
        [
            'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'date', 'label'),
            'name' => 'period',
            'type' => 'backend\widgets\PriceDateSelector',
            'headerOptions' => ['class' => 'col-md-2'],
            'enableError' => true,
        ],        
        [
            'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'days', 'label'),
            'name' => 'days',
            'type' => 'backend\widgets\PriceDaySelector',
            'options' => [
                'options' => [
                    'multiple' => true,
                ],
            ],
            'headerOptions' => ['class' => 'col-md-3'],
            'enableError' => true,
        ],
        [
            'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'time', 'label'),
            'name' => 'worktime',
            'type' => 'backend\widgets\PriceTimeSelector',
            'headerOptions' => ['class' => 'col-md-3'],
            'enableError' => true,
        ],
        [
            'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'dimension', 'label'),
            'name' => 'unit_id',
            'type' => 'dropDownList',
            'items' => ArrayHelper::map(ServicePriceUnit::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
            'headerOptions' => ['class' => 'col-md-1'],
        ],
        [
            'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'price', 'label'),
            'name' => 'price',
            'enableError' => true,
            'headerOptions' => [
                'class' => 'col-md-1',
            ]
        ],
        [
            'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'discount', 'label'),
            'name' => 'discount',
            'enableError' => true,
            'headerOptions' => [
                'class' => 'col-md-1',
            ]
        ],
        [
            'title' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'comment', 'label'),
            'type' => 'textarea',
            'name' => 'comment',
            'enableError' => true,
            'headerOptions' => [
                'class' => 'col-md-1',
            ],
        ]
    ],
]);

$this->registerJs("
    $('thead .list-cell__period').append('<div class=\'hint-block\'>" . FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'date', 'hint') . "</div>');
    $('thead .list-cell__days').append('<div class=\'hint-block\'>" . FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'days', 'hint') . "</div>');
    $('thead .list-cell__worktime').append('<div class=\'hint-block\'>" . FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'time', 'hint') . "</div>');
    $('thead .list-cell__unit_id').append('<div class=\'hint-block\'>" . FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'dimension', 'hint') . "abc</div>');
    $('thead .list-cell__price').append('<div class=\'hint-block\'>" . FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'price', 'hint') . "</div>');
    $('thead .list-cell__discount').append('<div class=\'hint-block\'>" . FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'discount', 'hint') . "</div>');
    $('thead .list-cell__comment').append('<div class=\'hint-block\'>" . FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'comment', 'hint') . "</div>');
");

?>
        <style type="text/css">
            thead .hint-block{
                font-size: 11px !important;
            }
        </style>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
<?php
echo $form->field($model, 'photos', ['template' => '{hint}{label}{input}{error}'])->widget('backend\widgets\MultiImageUpload',[
    'sortable' => 'true',
    'sortUrl' => ['/service/photo-sort'],
    'uploadUrl' => ['/service/photo-upload', 'service_id' => (int)$model->id],
    'deleteUrl' => ['/service/photo-delete', 'service_id' => (int)$model->id],
    'initialPreview' => function($model) {
        return \common\widgets\ServiceImage::widget(['model' => $model, 'width' => 150, 'height' => 100]);
    },
])->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'photo', 'label'))
    ->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_EDIT_SERVICE, 'photo', 'hint'));


echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();

$this->registerJs("

    $('[data-mask]').inputmask();

    $('#service-type_id').change(function(){
        var prev = $(this).data('prev');
        var current = $(this).val();
        $(this).data('prev', current);

        if (prev != current && prev !== undefined)
        {
            $.pjax({url: '/cabinet/service/update-attributes?service_id=".(int)$model->id."&type_id=' + current, container: '#service-attributes', push: false});
        }

        var text = $('#service-description').val();

        if (!prev && text.length != 0) return;

        // Получаем текст соответствующий предыдущему значению
        $.get('/cabinet/service/type-view?id=' + (prev ? prev : current), function(prevText){

            if  (text != prevText && text.length != 0)
            {
                //if (!confirm('Заменить описание услуги?')) return;
            }

            $.get('/cabinet/service/type-view?id=' + current, function(text){
                //$('#service-description').redactor('code.set', text);
                $('#description-hint-2').html(text);
            }) 

        }) 

    }).change();

");
