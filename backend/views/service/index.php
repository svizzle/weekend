<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use backend\widgets\GridViewButtonCopy;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonFilter;
use common\models\ServiceType;

$this->title = 'Услуги';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
//    Html::a('<i class="fa fa-plus"></i> Добавить', '#', ['class' => 'btn btn-success', 'disabled' => true]),
//    GridViewButtonAppend::widget(['title' => 'Добавить услугу', 'url' => ['/service/update', 'id' => 0, 'company_id' => '_id_']]),
    GridViewButtonCopy::widget(),
    GridViewButtonFilter::widget(),
    Html::a('<i class="fa fa-refresh"></i> Сброс', ['index'],  ['class' => 'btn btn-primary']),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">  
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'width' => 50,
                ],
            ],
            [
                'header' => 'ID',
                'attribute' => 'code',
                'headerOptions' => [
                    'width' => 100,
                ],
            ],
            [
                'header' => 'Наименование услуги',
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a(Html::encode($model->name), ['/service/update', 'id' => $model->id, 'company_id' => $model->company_id]);
                }
            ],
            [
                'header' => 'Тип услуги',
                'format' => 'raw',
                'attribute' => 'type_id',
                'value' => function($model) {
                    return $model->type->name;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'type_id',
                    ArrayHelper::map(ServiceType::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
                    [
                        'class' => 'form-control',
                        'prompt' => '...',
                    ]
                ),
            ],
            [
                'header' => 'Компания',
                'attribute' => 'companyName',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->company->name;
                }
            ],
            [
                'header' => 'Пользователь',
                'attribute' => 'userName',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->company->user->name;
                }
            ],
            [
                'header' => 'Статус',
                'attribute' => 'enabled',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->enabled
                        ? 
                        Html::a('<i class="fa fa-check"></i>', ['/service/toggle', 'id' => $model->id], ['class' => 'btn-sm btn-success', 'title' => 'Заблокировать'])
                        : 
                        Html::a('<i class="fa fa-ban"></i>', ['/service/toggle', 'id' => $model->id], ['class' => 'btn-sm btn-danger', 'title' => 'Разблокировать']);
                },
                'headerOptions' => [
                    'width' => 100,
                ],
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'enabled',
                    [
                        '0' => 'Заблокирована',
                        '1' => 'Активна',
                    ],
                    [
                        'class' => 'form-control',
                        'prompt' => '...',
                    ]
                ),
            ]
        ],
    ])?>
    </div>
</div>
