<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование').' единицы измерения';
$this->params['header'] = $this->title;

$this->params['breadcrumbs'] = [
    [
        'label' => 'Единицы измерения',
        'url' => ['/service/price-unit'],
    ],
    $this->title
];

?>
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'name');
?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
