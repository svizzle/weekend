<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use common\models\ServiceTypeAttribute;

$form = ActiveForm::begin();

Pjax::begin([
    'enablePushState' => false,
    'id' => 'service-attributes',
]);


foreach($type->serviceAttributes as $attribute)
{
    echo '<div class="form-group">';
    echo '<label>'.$attribute->name.'</label>';        
    if ($attribute->type_id == ServiceTypeAttribute::TYPE_MULTIPLE)
    {
        echo \kartik\select2\Select2::widget([
            'data' => ArrayHelper::map($attribute->attributeValues, 'id', 'name'),
            'options' => [
                'id' => Yii::$app->security->generateRandomString(),
                'multiple' => true,
                'prompt' => '...',
                'unselect' => null,
            ],
            'name' => 'Service[attributeValues][]',
        ]);
    }
    else
    {        
        echo Html::dropDownList('Service[attributeValues][]', 0, ArrayHelper::map($attribute->attributeValues, 'id', 'name'),[
            'prompt' => '...',
            'class' => 'form-control',
        ]);
    }
    echo '</div>';
}

Pjax::end();

$form->end();
