<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['header'] = $model->isNewRecord ? 'Добавление населенного пункта' : 'Редактирование населенного пункта';

$this->params['breadcrumbs'] = [
    [
        'label' => 'География',
        'url' => ['/geo/regions'],
    ],
    [
        'label' => $region->name,
        'url' => ['/geo/locations', 'region_id' => $region->id],
    ],
    $this->params['header'],
]

?>
<div class="box box-primary">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);

echo $form->field($model, 'name');

echo $form->field($model, 'lat');
echo $form->field($model, 'lon');

echo $form->field($model, 'gismeteo_id');

?>
    </div>
    <div class="box-footer">
<?php
echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();
?>
    </div>
</div>
