<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;


$this->title = 'Населенные пункты';
$this->params['header'] = $this->title;

$this->params['breadcrumbs'] = [
    [
        'label' => 'География',
        'url' => ['/geo/regions'],
    ],
    $model->name,
];

$this->params['toolbar'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить населенный пункт', ['/geo/location-update', 'region_id' => $model->id, 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonDelete::widget(['action' => ['/geo/location-delete']]),
]

?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::className(),
            'headerOptions' => [
                'width' => 50,
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($location) use ($model){
                return Html::a($location->name, ['/geo/location-update', 'id' => $location->id, 'region_id' => $model->id]);
            }

        ],
    ],
])?>
    </div>
</div>
