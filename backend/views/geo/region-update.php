<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['header'] = $model->isNewRecord ? 'Добавление региона' : 'Редактирование региона';

$this->params['breadcrumbs'] = [
    [
        'label' => 'География',
        'url' => ['/geo/regions'],
    ],
    $this->params['header'],
]

?>
<div class="box box-primary">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);

echo $form->field($model, 'name');

echo $form->field($model, 'yandex_id');

?>
    </div>
    <div class="box-footer">
<?php
echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();
?>
    </div>
</div>
