<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;
use backend\widgets\GridViewButtonFilter;

$this->title = 'Отзывы';
$this->params['header'] = $this->title;
$this->params['toolbar-right'] = [
    GridViewButtonEdit::widget(),
    GridViewButtonFilter::widget(),
    Html::a('<i class="fa fa-refresh"></i> Сброс', ['index'],  ['class' => 'btn btn-primary']),
    GridViewButtonDelete::widget(),
];

?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'headerOptions' => [
                    'with' => 50,
                ],
                'class' => CheckboxColumn::className(),
            ],
            [
                'attribute' => 'serviceName',
                'label' => 'Услуга',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::encode($model->service->name);
                }
            ],
            [
                'attribute' => 'userName',
                'label' => 'Пользователь',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::encode($model->user->name);
                }
            ],
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::encode($model->text);
                }
            ],
            [
                'attribute' => 'rate',
            ],
            [
                'label' => 'Дата',
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function($model) {
                    return Yii::$app->formatter->asDateTime($model->created_at);
                }
            ],
        ],
    ])?>
    </div>
</div>
