<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="box box-primary">
    <div class="box-body">
    <?php
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]);

        echo $form->field($model, 'text')->widget('backend\widgets\Imperavi');
    ?>
    </div>
    <div class="box-footer">
    <?php
        echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);
        $form->end();
    ?>
    </div>
</div>
