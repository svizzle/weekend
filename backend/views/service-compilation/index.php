<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use backend\widgets\GridViewButtonDelete;

use common\models\ServiceCompilation;
use common\models\ServiceType;

$this->title = 'Подборка предложений';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить вид услуг', '#', ['id' => 'service-type-add', 'class' => 'btn btn-success']),
    GridViewButtonDelete::widget(['action' => 'remove']),
];

?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'emptyText' => Html::a('Специальные предложения ('.(new ServiceCompilation)->getServices()->count().')', ['/service-compilation/items', 'compilation_id' => 0]),
            'beforeRow' => function($model, $key, $index, $grid) {
                if ($index != 0) return;

                return '<tr><td>&nbsp;</td><td>'.$grid->emptyText.'</td></tr>';
            },
            'columns' => [
                [
                    'class' => CheckboxColumn::className(),
                    'headerOptions' => [
                        'width' => 50,
                    ],
                ],
                [
                    'attribute' => 'service_type_id',
                    'format' => 'raw',
                    'value' => function($model){
                        return Html::a($model->type->name.' ('.$model->getServices()->count().')', ['/service-compilation/items', 'compilation_id' => $model->id]);
                    }
                ],
            ],
        ])?>
    </div>
</div>

<div class="modal" id="service-type-selector" role="dialog" aria-labelledby="service-type-selector-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="service-type-selector-label">Выберите вид услуги</h4>
              </div>
              <div class="modal-body">
                <?=Html::beginForm(['/service-compilation/add'], 'get')?>
                <?=Html::dropDownList('id', null, ArrayHelper::map(ServiceType::find()->orderBy('name')->all(),'id', 'name'), [
                    'class' => 'form-control',
                ])?>
                <?=Html::endForm()?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php

$this->registerJs("
    
    $('#service-type-add').click(function(){
        $('#service-type-selector').modal();
        
        return false;
    });

    $('#service-type-selector .btn-primary').click(function(){
        $('#service-type-selector form').submit();
    });

");
