<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Подборка предложений: '.($model->isNewRecord ? 'Специальные предложения' : $model->type->name);
$this->params['header'] = $this->title;

$this->params['breadcrumbs'] = [
    [
        'label' => 'Подборка предложений',
        'url' => ['index'],
    ],
    $model->isNewRecord ? 'Специальные предложения' : $model->type->name,
];

?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'format' => 'raw',
                'value' => function($service) use ($model){
                    if (in_array($service->id, ArrayHelper::getColumn($model->services, 'id')))
                    {
                        return Html::a('<span class="label label-success"><i class="fa fa-check"></i></span>', [
                            'item-remove', 
                            'compilation_id' => (int) $model->id,
                            'service_id' => $service->id,
                        ]);
                    }
                    else
                    {
                        return Html::a('<span class="label label-danger"><i class="fa fa-ban"></i></span>', [
                            'item-add',
                            'compilation_id' => (int) $model->id,
                            'service_id' => $service->id,
                        ]);
                    }
                },
                'headerOptions' => [
                    'width' => 50,
                ],
            ],
            [
                'attribute' => 'name',
            ],
        ],
    ])?>
    </div>
</div>
