<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJs("

    jQuery('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

");

?>
<div class="login-box">
    <div class="login-logo">
        Отдых выходного дня
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Введите свой email и пароль</p>

    <?php
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]);

        echo $form->field($model, 'email')
                  ->input('email', ['placeholder' => 'Email'])
                  ->label(false);
        //<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        
        echo $form->field($model, 'password')
                  ->passwordInput(['placeholder' => 'Пароль'])
                  ->label(false);
        //<span class="glyphicon glyphicon-lock form-control-feedback"></span>
        ?>
        
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <?php
                        echo $form->field($model, 'rememberMe')
                                  ->checkbox(['label' => 'Запомнить меня']);
                    ?>
                </div>
            </div>
            <div class="col-xs-4">
                <?=Html::submitButton('Войти', [ 'class' => "btn btn-primary btn-block btn-flat"])?>
            </div>
        </div>
    <?php
        $form->end();
    ?>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
