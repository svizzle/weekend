<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\User;

$this->params['header'] = 'Редактирование профиля';
$this->params['breadcrumbs'] = [
    $this->params['header'],
];

$this->registerJs("

    $('[data-mask]').inputmask();

");

?>
<div class="user_update">
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);

    if (Yii::$app->user->can('admin'))
    {
        // @todo Сделать доступным только админам
        echo $form->field($model, 'contract_number')->textInput([
            'data-mask' => '',
            'data-inputmask' => '"mask": "99999999"',
        ]);

        echo $form->field($model, 'role')->dropDownList([
            'user'  => 'Пользователь',
            'partner' => 'Партнер',
            'admin' => 'Администратор',
        ]);
    }

    echo $form->field($model, 'last_name');
    echo $form->field($model, 'first_name');

    echo $form->field($model, 'email');
    echo $form->field($model, 'phone')->textInput([
        'data-mask' => '',
        'data-inputmask' => '"mask": "+7 (999) 999-99-99"',
    ]);

    $model->birthday = $model->birthday ? Yii::$app->formatter->asDate($model->birthday, 'php:d.m.Y') : null; 
    echo $form->field($model, 'birthday')->widget('dosamigos\datepicker\DatePicker', [
        'language' => 'ru',
        'clientOptions' => [
            'endDate' => '-18y',
            'dateFormat' => 'dd.mm.yy',
        ],
        'options' => [
            'data-mask' => '',
            'data-inputmask' => '"mask": "99.99.9999"',
        ],
    ]);
?>
    <div class="form-group current-photo">    
        <label class="control-label">Текущее фото</label>
        <div>
            <?=\common\widgets\UserImage::widget([
                'model' => $model,
                'width' => 120,
                'height' => 120,
            ])?>
        </div>
    </div>
<?php
    
    echo $form->field($model, 'photo_file')->fileInput();

    echo '<a id="pass"></a>';
    if (!$model->isNewRecord)
    {
        echo $form->field($passwordModel, 'old_password')->passwordInput();
    }
    echo $form->field($passwordModel, 'password')->passwordInput();
    echo $form->field($passwordModel, 'password_confirmation')->passwordInput();

?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
</div>

<?php
    $this->registerJs("
            setTimeout(function(){
                //var url = location.href;
                //var h = url.substring(url.indexOf('#'));
                var hash = window.location.hash;
                if (!hash) return;
                $(document.body).animate({
                    'scrollTop':   $(hash).offset().top
                }, 550);
            }, 1200);
    ");
?>

