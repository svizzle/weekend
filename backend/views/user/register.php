<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\components\FormsSettingsHelper;
use \common\repositories\FormsSettingsRepository;

$this->registerJs("

    jQuery('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

");

?>
<div class="login-box">
    <div class="login-logo">
        <?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'window_header', 'label') ?>
       <small class="hint-block" style="font-size: 13px;"><?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'window_header', 'hint') ?></small>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
        <p class="login-box-msg" style="padding: 0;"><?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'form_header', 'label') ?></p>
      <div class="text-center" style="color: dimgrey; font-size: 11px;padding-bottom: 3px;"><?= FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'form_header', 'hint') ?></div>
    <?php
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]);

        echo $form->field($model, 'email')
                  ->input('email', ['placeholder' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'email', 'placeholder')])
                  ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'email', 'label'))
                    ->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'email', 'hint'));
        
        echo $form->field($model, 'password')
                  ->textInput(['placeholder' => FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'password', 'placeholder')])
                  ->label(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'password', 'label'))
                  ->hint(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'password', 'hint')) ;
        ?>
        
        <div class="row">
            <div class="col-md-12">
                <div class="checkbox icheck">
                    <?php
                        echo $form->field($model, 'terms')
                                   ->checkbox(['label' => "Я принимаю условия <a href='/agreement' target='_blank'>пользовательского соглашения</a>"]);
                    ?>
                </div>
            </div>
            <div class="col-md-12">
                <?=Html::submitButton(FormsSettingsHelper::getField(FormsSettingsRepository::FORM_CABINET_REG, 'submit_btn', 'label'), [ 'class' => "btn btn-primary btn-block btn-flat"])?>
            </div>
        </div>
    <?php
        $form->end();
    ?>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
