<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\widgets\GridViewButtonAppend;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonFilter;
use common\models\User;

$this->params['header'] = 'Пользователи';

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-user-plus"></i> Пользователь', ['/user/update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonAppend::widget(['title' => 'Компания', 'url' => ['/company/update', 'id' => 0, 'user_id' => '_id_']]),
    GridViewButtonFilter::widget(),
    Html::a('<i class="fa fa-refresh"></i> Сброс', ['/user/index'],  ['class' => 'btn btn-primary']),
    GridViewButtonDelete::widget(),
];

?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::className(),
            'headerOptions' => [
                'width' => 50,
            ],
        ],
        [
            'header' => 'ID',
            'attribute' => 'contract_number',
            'format' => 'raw',
            'value' => function($model) {
                return Yii::$app->formatter->asText(empty($model->contract_number) ? null : $model->contract_number);
            },
            'headerOptions' => [
                'width' => 50,
            ],
        ],
        [
            'attribute' => 'name',
            'header' => 'Наименование',
            'format' => 'raw',
            'value' => function($model) {
                if (empty($model->name))
                {
                    $name = Yii::$app->formatter->asText(null);
                }
                else
                {
                    $name = $model->name;
                }
                return Html::a($name, ['/user/update', 'id' => $model->id]);
            }
        ],
        [
            'attribute' => 'email',
            'header' => 'E-mail',
        ],
        [
            'header' => 'Роль',
            'attribute' => 'role',
            'format' => 'raw',
            'headerOptions' => [
                'width' => 50,
            ],
            'value' => function($model) {
                switch($model->role)
                {
                    case 'partner':
                        return 'Партнер';
                    case 'admin':
                        return 'Администратор';
                    default:
                        return 'Пользователь';
                }
            },
            'filter' => Html::activeDropDownList(
                $searchModel,
                'role',
                [
                    'user' => 'Пользователь',
                    'partner' => 'Партнер',
                    'admin' => 'Администратор',
                ],
                [
                    'class' => 'form-control',
                    'prompt' => '...',
                ]
            ),
        ],
        [
            'attribute' => 'tarif_id',
            'header' => 'Тарифный план',
            'format' => 'raw',
            'value' => function($model) {
                return 'Базовый';
            },
            'filter' => Html::activeDropDownList(
                $searchModel,
                'enabled',
                [
                    '1' => 'Базовый',
                ],
                [
                    'class' => 'form-control',
                    'prompt' => '...',
                ]
            ),
        ],
        [
            'header' => 'Статус',
            'attribute' => 'enabled',
            'format' => 'raw',
            'value' => function($model) {
                return $model->enabled
                    ? 
                    Html::a('<i class="fa fa-check"></i>', ['/user/toggle', 'id' => $model->id], ['class' => 'btn-sm btn-success', 'title' => 'Заблокировать'])
                    : 
                    Html::a('<i class="fa fa-ban"></i>', ['/user/toggle', 'id' => $model->id], ['class' => 'btn-sm btn-danger', 'title' => 'Разблокировать']);
            },
            'headerOptions' => [
                'width' => 100,
            ],
            'filter' => Html::activeDropDownList(
                $searchModel,
                'enabled',
                [
                    '0' => 'Заблокирован',
                    '1' => 'Активен',
                ],
                [
                    'class' => 'form-control',
                    'prompt' => '...',
                ]
            ),
        ]
    ],
])?>
    </div>
</div>
