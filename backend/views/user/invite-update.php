<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $model->isNewRecord ? 'Создание приглашения' : 'Редактирование приглашения';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Приглашения',
        'url' => ['invite'],
    ],
    $this->title
];

$this->registerJs("

    $('[data-mask]').inputmask();

");

?>
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'phone')->textInput([    
        'data-mask' => '',
        'data-inputmask' => '"mask": "+7 (999) 999-99-99"',
    ]);

    echo $form->field($model, 'email');

    echo $form->field($model, 'contract_number')->textInput([
        'data-mask' => '',
        'data-inputmask' => '"mask": "99999999"',
    ]);

    echo $form->field($model, 'comment')->textarea();

    echo $form->field($model, 'text')->textarea();
?>
    </div>
    <div class="box-footer">
<?php

    if ($model->isNewRecord) echo Html::submitButton('Пригласить', ['class' => 'btn btn-success']);

    $form->end();
?>
    </div>
</div>
