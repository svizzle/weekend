<?php

use yii\helpers\Html;

use common\models\Company;

\backend\assets\AdminLTEAsset::register($this);

$this->params['header'] = "Статистика пользователя";
$this->params['breadcrumbs'] = null;



$this->params['toolbar-right'] = [
    Html::a('Изменить пароль', ['/user/update', 'id' => $model->id, '#'=>'pass'], ['class' => 'btn btn-default ',]),
    Html::a('Редактировать', ['/user/update', 'id' => $model->id], ['class' => 'btn btn-success ',]),
];

?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-person-stalker"></i></span>

            <div class="info-box-content">
                <a href="<?= \yii\helpers\Url::to(['statistics/index', '#'=>'uniqs']) ?>">
                    <span class="info-box-text">Посетители</span>
                    <span class="info-box-number"><?= $visits_count ?></span>
                </a>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-stats-bars"></i></span>

            <div class="info-box-content">
                <a href="<?= \yii\helpers\Url::to(['statistics/index', '#'=>'services']) ?>">
                    <span class="info-box-text">Показы</span>
                    <span class="info-box-number"><?= $service_display ?></span>
                </a>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-link"></i></span>

            <div class="info-box-content">
                <a href="<?= \yii\helpers\Url::to(['statistics/index', '#'=>'views']) ?>">
                    <span class="info-box-text">Переходы</span>
                    <span class="info-box-number"><?= $redirects_service ?></span>
                </a>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-android-bookmark"></i></span>

            <div class="info-box-content">
                <a href="<?= \yii\helpers\Url::to(['statistics/index', '#'=>'favs']) ?>">
                    <span class="info-box-text">Избранные</span>
                    <span class="info-box-number"><?= $favorites ?></span>
                </a>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <?=\common\widgets\UserImage::widget([
                    'model' => $model,
                    'width' => 120,
                    'height' => 120,
                    'htmlOptions' => [
                        'class' => 'profile-user-img img-responsive img-circle',
                    ],
                ])?>
                <h3 class="profile-username text-center"><?=Html::encode($model->name)?></h3>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Номер договора</b> <a class="pull-right"><?=Yii::$app->formatter->asText($model->contract_number)?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Тарифный план</b> <a class="pull-right">Базовый</a>
                    </li>
                    <li class="list-group-item">
                        <b>Баланс</b> <a class="pull-right">0</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">    
                    <div class="col-md-12">
                        <strong><i class="fa fa-envelope margin-r-5"></i> Электронная почта</strong>
                        <p class="text-muted"><?=$model->email?></p>
                        <hr>
                        <strong><i class="fa fa-phone margin-r-5"></i> Телефон</strong>
                        <p class="text-muted"><?=Yii::$app->formatter->asText($model->phone)?></p>
                        <hr>
                        <strong><i class="fa fa-birthday-cake margin-r-5"></i> Дата рождения</strong>
                        <p class="text-muted"><?=Yii::$app->formatter->asDate($model->birthday)?></p>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Компания</h3>
            </div>
            <div class="box-body">
                <?php if (!$model->company) : ?>
                    <div class="row">
                        <div class="col-md-12">
                            Еще не создана <br/><br/>
                            <?php if (Yii::$app->user->id == $model->id && Yii::$app->user->can('partner')) : ?>
                                <?=Html::a('Создать', ['/company/update', 'id' => 0, 'user_id' => $model->id], ['class' => 'btn btn-success'])?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-md-12">
                            <strong><i class="fa fa-info margin-r-5"></i> Фирменное наименование компании</strong>
                            <p class="text-muted"><?=$model->company->name?></p>
                            <hr>
                            <strong><i class="fa fa-cog margin-r-5"></i> Вид компании</strong>
                            <p class="text-muted"><?=$model->company->type->name?></p>
                            <hr>
                            <strong><i class="fa fa-phone margin-r-5"></i> Телефон</strong>
                            <p class="text-muted"><?=$model->company->phones[0]->phone?></p>
                            <hr>
                            <strong><i class="fa fa-building margin-r-5"></i> Адрес</strong>
                            <p class="text-muted"><?=$model->company->fullAddress?></p>
                            <hr>
                            <?=\backend\widgets\YandexMap::widget(['model' => $model->company])?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
