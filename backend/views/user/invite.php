<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

use backend\widgets\GridViewButtonFilter;
use backend\widgets\GridViewButtonDelete;



$this->params['header'] = 'Приглашения пользователей';
$this->params['toolbar-right'] = [
    '<small>SMS баланс: <b>'.Yii::$app->sms->balance()->data.'</b> руб.</small>',
    Html::a('<i class="fa fa-user-plus"></i> Приглашение', ['/user/invite-update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonFilter::widget(),
    Html::a('<i class="fa fa-refresh"></i> Сброс', ['/user/invite'],  ['class' => 'btn btn-primary']),
    GridViewButtonDelete::widget(['action' => ['invite-delete']]),
];

?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'width' => 50
                ],
            ],
            [
                'header' => 'Дата',
                'attribute' => 'created_at',
                'format' => 'date',
                'headerOptions' => [
                    'width' => 150
                ],
                'filter' => \dosamigos\datepicker\DatePicker::widget([
                    'language' => 'ru',
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'dateFormat' => 'dd.mm.yy',
                    ],
                ]),
            ],
            [
                'header' => 'Телефон',
                'attribute' => 'phone',
                'format' => 'raw',
                'value' => function($model){
                    if (empty($model->phone)) return Yii::$app->formatter->asText(null);

                    if ($model->phone_status === null)
                    {
                        $response = Yii::$app->sms->status($model->phone_token);
                        switch($response->code)
                        {
                            case -1:
                            case 104:
                            case 105:
                            case 106:
                            case 107:
                            case 108:
                                $model->phone_status = 0;
                                $model->save(false, ['phone_status']);
                                break;
                            case 103:
                                $model->phone_status = 1;
                                $model->save(false, ['phone_status']);
                                break;
                        }
                    }

                    if ($model->phone_status === null) return '<span class="label label-default"><i class="fa fa-question"></i></span> '.$model->phone;
                    if ($model->phone_status === 1)
                    {
                        return '<span class="label label-success"><i class="fa fa-check"></i></span> '.$model->phone;
                    }
                    else
                    {
                        return '<span class="label label-danger"><i class="fa fa-close"></i></span> '.$model->phone;
                    }
                }
            ],
            [
                'header' => 'E-mail',
                'attribute' => 'email',
            ],
            [
                'header' => 'ID клиента',
                'attribute' => 'contract_number',
            ],
            [
                'header' => 'Комментарий',
                'attribute' => 'comment',
            ],
            [
                'header' => 'Код регистрации',
                'attribute' => 'code',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->code, ['invite-update', 'id' => $model->id]);
                }
            ],
            [
                'header' => 'Статус',
                'attribute' => 'user_id',
                'format' => 'boolean',
                'value' => function($model) {
                    return (bool) $model->user_id;
                },
                'headerOptions' => [
                    'width' => 100,
                ],
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'user_id',
                    [
                        '1' => 'Нет',
                        '2' => 'Да',
                    ],
                    [
                        'class' => 'form-control',
                        'prompt' => '...',
                    ]
                ),
            ],
        ],
    ])?>
    </div>
</div>
