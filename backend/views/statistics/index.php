<?php

$this->title = 'Статистика';
$this->params['header'] = $this->title;

?>
<div class="box box-primary">
    <div class="box-header with-border">
        <a id="uniqs"></a>
        <h3 class="box-title">Кол-во уникальных посетителей сайта</h3>
    </div>
    <div class="box-body">
        <?=\backend\widgets\StatisticsVisitors::widget()?>
    </div>
</div>

<?php if (Yii::$app->user->identity->company) : ?>
<div class="box box-primary">
    <div class="box-header with-border">
        <a id="services"></a>
        <h3 class="box-title">Показы услуг на сайте</h3>
    </div>
    <div class="box-body">
        <?=\backend\widgets\StatisticsServiceDisplays::widget(['model' => Yii::$app->user->identity->company])?>
    </div>
</div>
<?php endif; ?>

<?php if (Yii::$app->user->identity->company) : ?>
<div class="box box-primary">
    <div class="box-header with-border">
        <a id="views"></a>
        <h3 class="box-title">Переходы на страницы услуг</h3>
    </div>
    <div class="box-body">
        <?=\backend\widgets\StatisticsServiceViews::widget(['model' => Yii::$app->user->identity->company])?>
    </div>
</div>
<?php endif; ?>

<?php if (Yii::$app->user->identity->company) : ?>
<div class="box box-primary">
    <div class="box-header with-border">
        <a id="favs"></a>
        <h3 class="box-title">Добавления в избранное</h3>
    </div>
    <div class="box-body">
        <?=\backend\widgets\StatisticsFavorite::widget(['model' => Yii::$app->user->identity->company])?>
    </div>
</div>
<?php endif; ?>

<script type="text/javascript">
    setTimeout(function(){
        var url = location.href;
        //var h = url.substring(url.indexOf('#'));
        var hash = window.location.hash;
        if (!hash) return;
        $(document.body).animate({
            'scrollTop':   $(hash).offset().top
        }, 550);
    }, 1200);
</script>
