<?php

use yii\helpers\Html;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

use backend\widgets\GridViewButtonDelete;

$this->title = 'Избранное';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    GridViewButtonDelete::widget(['action' => ['/favorite-service/delete']]),
];

?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::className(),
            'checkboxOptions' => function ($model, $key, $index, $column) {
                return ['value' => $model['id']];
            },
            'headerOptions' => [
                'width' => 50,
            ],
        ],
        [
           'attribute'=>'service_id',
            'label'=>'id'
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'label'=>'Услуга',
            'value' => function($model){
                return Html::a($model['name'], Yii::$app->frontendUrlManager->createUrl(['/service/view', 'id' => $model['id']]), ['target' => '_blank']);
            }
        ],
        [
            'attribute'=>'company_name',
            'label'=>'Название компании'
        ],
    ],
])?>
    </div>
</div>

