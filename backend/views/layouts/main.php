<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

use common\models\Company;

\backend\assets\AppAsset::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<?php $this->beginBody(); ?>
<div class="wrapper">
  <header class="main-header">
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Logo -->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li><?=Html::a('<i class="fa fa-globe"></i> Сайт', '/')?></li>
            <li><a href="#">ID: <?=Yii::$app->formatter->asText(Yii::$app->user->identity->contract_number)?></a></li>
            <li><?=Html::a('<i class="fa fa-sign-out"></i> Выход', ['/user/logout'])?></li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">ПОЛЬЗОВАТЕЛЬ</li>
        </ul>
        <?=Menu::widget([
            'encodeLabels' => false,
            'options' => [
                'class' => 'sidebar-menu',
            ],
            'items' => [
                [
                    'label' => '<i class="fa fa-user"></i> <span>Профиль пользователя</span>',
                    'url' => ['/user/view', 'id' => Yii::$app->user->id],
                ],
                [
                    'label' => '<i class="fa fa-info"></i> <span>Сведения о компании</span>',
                    'url' => ['/company/form'],
                    'visible' => Yii::$app->user->can('partner') && !Yii::$app->user->can('admin'),
                ],
                [
                    'visible' => Yii::$app->user->can('partner') && Yii::$app->user->identity->company && !Yii::$app->user->can('admin'),
                    'url' => ['/company/service', 'company_id' => Yii::$app->user->identity->company ? Yii::$app->user->identity->company->id : 0],
                    'label' => '<i class="fa fa-cubes"></i> <span>Услуги компании</span>',
                ],
                [
                    'visible' => !Yii::$app->user->can('admin'),
                    'url' => ['/site/feedback'],
                    'label' => '<i class="fa fa-commenting"></i> <span>Обратная связь</span>',
                ],
                [
                    'label' => '<i class="fa fa-line-chart"></i> <span>Статистика</span>',
                    'url' => ['/statistics/index'],
                    'visible' => Yii::$app->user->can('partner') && !Yii::$app->user->can('admin'),
                ],
                [
                    'label' => '<i class="fa fa-heart"></i> <span>Избранное</span>',
                    'url' => ['/favorite-service/index'],
                ],
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Шаблоны поиска</span>',
                    'url' => ['/service-type-group/index'],
                ],
            ],
        ])?>

<?php if (Yii::$app->user->can('admin')): ?>
        <ul class="sidebar-menu">
            <li class="header">КОМПАНИИ И УСЛУГИ</li>
        </ul>
        <?php

            $unmoderatedCompanies = Company::find()->andWhere([
                'moderated' => 0,
            ])->count();

        ?>
        <?=Menu::widget([
            'encodeLabels' => false,
            'options' => [
                'class' => 'sidebar-menu',
            ],
            'items' => [
                [
                    'label' => '<i class="fa fa-user"></i> <span>Пользователи</span>',
                    'url' => ['/user/index'],
                ],
                [
                    'label' => '<i class="fa fa-building"></i> <span>Компании'.(
                        $unmoderatedCompanies 
                        ? '<small class="label pull-right bg-red">'.$unmoderatedCompanies.'</small>'
                        : ''
                    ).'</span>',
                    'url' => ['/company/index'],
                ],
                [
                    'label' => '<i class="fa fa-cubes"></i> <span>Услуги</span>',
                    'url' => ['/service/index'],
                ],
                [
                    'label' => '<i class="fa fa-comment"></i> <span>Отзывы</span>',
                    'url' => ['/service-comment/index'],
                ],
                [
                    'label' => '<i class="fa fa-cubes"></i> <span>Подборка предложений</span>',
                    'url' => ['/service-compilation/index'],
                ],
                [
                    'label' => '<i class="fa fa-user-plus"></i> <span>Приглашения</span>',
                    'url' => ['/user/invite'],
                ],
            ],
        ])?>
        <ul class="sidebar-menu">
            <li class="header">СПРАВОЧНИКИ</li>
        </ul>
        <?=Menu::widget([
            'encodeLabels' => false,
            'options' => [
                'class' => 'sidebar-menu',
            ],
            'items' => [
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Виды компаний</span>',
                    'url' => ['/company/type'],
                ],
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Виды услуг</span>',
                    'url' => ['/service/type'],
                ],
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Виды отдыха</span>',
                    'url' => ['/service-type-group/index'],
                ],
                [
                    'label' => '<i class="fa fa-map"></i> <span>География</span>',
                    'url' => ['/geo/regions'],
                ],
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Ед. измерения</span>',
                    'url' => ['/service/price-unit'],
                ],
            ],
        ])?>
        <ul class="sidebar-menu">
            <li class="header">КОНТЕНТ ПОРТАЛА</li>
        </ul>
        <?=Menu::widget([
            'encodeLabels' => false,
            'options' => [
                'class' => 'sidebar-menu',
            ],
            'items' => [
                [
                    'label' => '<i class="fa fa-file-text"></i> <span>Страницы</span>',
                    'url' => ['/page/index'],
                ],
                [
                    'label' => '<i class="fa fa-file-text"></i> <span>Статьи</span>',
                    'url' => ['/article/index'],
                ],
                [
                    'label' => '<i class="fa fa-money"></i> <span>Реклама</span>',
                    'url' => ['/advertise/index'],
                ],
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Настройки форм</span>',
                    'url' => ['/forms-settings'],
                ],
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Дополнительное меню</span>',
                    'url' => ['/menu-main/index'],
                ],
                [
                    'label' => '<i class="fa fa-cog"></i> <span>Настройки</span>',
                    'url' => ['/setting/update'],
                ],
            ],
        ])?>
<?php endif; ?>
    </section>
    <!-- /.sidebar -->
  </aside>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php if (isset($this->params['header'])) : ?>
            <h1><?=$this->params['header']?>
        <?php
            if (isset($this->params['toolbar-right']))
            {
                echo '<div class="pull-right">';
                foreach($this->params['toolbar-right'] as $button)
                {
                    echo "&nbsp;&nbsp;".$button;
                }
                echo '</div>';
            }
        ?>
            </h1>
        <?php endif; ?>
        <?php if(isset($this->params['breadcrumbs'])) : ?>
        <?=Breadcrumbs::widget([
            'options' => [
                'class' => 'breadcrumb',
            ],
            'encodeLabels' => false,
            'homeLink' => [
                'label' => '<i class="fa fa-dashboard"></i> Главная',
                'url' => ['/site/index'],
            ],
            'links' => $this->params['breadcrumbs'],
        ])?>
        <?php endif; ?>
    </section>

        <?php
            if (isset($this->params['toolbar']))
            {
                echo '<div class="content-header">';
                foreach($this->params['toolbar'] as $button)
                {
                    echo $button."&nbsp;&nbsp;";
                }
                echo '</div>';
            }
        ?>
    <!-- Main content -->
    <section class="content">
    <?php
    if (count(Yii::$app->session->getAllFlashes())) 
    {    
        foreach (Yii::$app->session->getAllFlashes() as $key => $messages)
        {
            if ($messages === true) continue;
            foreach($messages as $message)
            {
                echo '<div class="callout callout-' . $key . '"><p>' . \yii::t('app', $message) . '</p></div>';
            }
        }
    }
    ?>
            <?=$content?>
    </section>
</div>
</div>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
