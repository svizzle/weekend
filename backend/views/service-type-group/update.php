<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use common\models\ServiceType;

$this->params['header'] = ($model->isNewRecord ? 'Добавление' : 'Редактирование').' вида отдыха';

$this->params['breadcrumbs'] = [
    [
        'label' => 'Виды отдыха',
        'url' => ['index'],
    ],
    $this->params['header'],
]

?>
<div class="box box-primary">
    <div class="box-body">
<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);

echo $form->field($model, 'name');

echo \backend\widgets\TabularInput::widget([
    'id' => 'types-tabularinput',
    'models' => count($model->types) ? $model->types : [ServiceType::find()->orderBy(['name' => SORT_ASC])->one()],
    'columns' => [
        [
            'title' => 'Виды услуг',
            'name' => 'id',
            'type' => 'static',
            'value' => function($type) use ($model){
                $html = Html::dropDownList('t[]', $type->id, ArrayHelper::map(ServiceType::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'), ['class' => 'form-control selector']);

                $value_ids = ArrayHelper::getColumn($model->values, 'id');
                
                foreach($type->serviceAttributes as $attribute)
                {
                    $html.= '<div class="form-group inner">';
                    $html.= '<label>'.$attribute->name.'</label>';
                    $html.= '<select name="v[]" class="attribute form-control" multiple>';
                    foreach($attribute->attributeValues as $value)
                    {
                        $html.= "<option value='".$value->id."' ".(in_array($value->id, $value_ids) ? "selected" : "").">".$value->name."</option>";
                    }
                    $html.= '</select>';
                    $html.= '</div>';
                }

                return $html;
            },
            'enableError' => true,
        ],  
    ]
]);    

?>
    </div>
    <div class="box-footer">
<?php
echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);

$form->end();
?>
    </div>
</div>

<?php

\kartik\select2\Select2::widget(['name' => 'for-asset-bundle']);
$this->registerJs("

    $('#types-tabularinput').on('change', 'select.selector', function(){
        var td = $(this).parents('td');
    
        td.find('.form-group.inner').remove();

        $.get('".Url::to(['/service-type/get'])."', {id: $(this).val()}, function(type){
            $.each(type.attributes, function(index, attribute){
                
                var div = $('<div>').addClass('form-group').addClass('inner');
                var label = $('<label>').html(attribute.name);
                var select = $('<select class=\"form-control attribute\" name=v[] multiple>');

                $.each(attribute.values, function(index, value){
                    var option = $('<option>').attr('value', value.id).html(value.name);
                    select.append(option);
                });
                
                div.append(label);
                div.append(select);
                td.append(div);
            });

            td.find('select.attribute').select2({multiple: true});
        }, 'json');




    });

    $('#types-tabularinput').on('afterAddRow', function(e) {
        var td = $('#types-tabularinput .multiple-input-list__item:last td');
        
        td.find('select').trigger('change');

    })   

    $('select.attribute').select2({multiple: true});
");

$this->registerCss('
#types-tabularinput .form-group.inner {
    margin-left: 30px !important;    
}
');
