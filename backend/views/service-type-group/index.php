<?php

use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonEdit;

$this->title = 'Виды отдыха';
$this->params['header'] = $this->title;

$this->params['toolbar-right'] = [
    Html::a('<i class="fa fa-plus"></i> Добавить вид отдыха', ['update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonEdit::widget(['url' => ['update', 'id' => '_id_']]),
    GridViewButtonDelete::widget(['action' => ['delete']]),
]

?>
<div class="box box-primary">
    <div class="box-body">
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::className(),
            'headerOptions' => [
                'width' => 50,
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function($model) {
                return Html::a($model->name, ['update', 'id' => $model->id]);
            }
        ],
    ],
])?>
    </div>
</div>
