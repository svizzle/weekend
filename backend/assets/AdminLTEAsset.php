<?php

namespace backend\assets;

class AdminLTEAsset extends \yii\web\AssetBundle
{
    public $sourcePath = "@bower/adminlte";

    public $css = [
        "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css",
        "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css",
        'dist/css/AdminLTE.css',
        'dist/css/skins/skin-blue-light.css',        
        "plugins/iCheck/square/blue.css",
    ];

    public $js = [
        "dist/js/app.js",
        "plugins/iCheck/icheck.min.js",
        "plugins/input-mask/jquery.inputmask.js",
        "plugins/input-mask/jquery.inputmask.date.extensions.js",
        "plugins/input-mask/jquery.inputmask.extensions.js",
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
