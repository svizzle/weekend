<?php

namespace backend\assets;

class HighchartsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = "@bower/highcharts";

    public $css = [
    
    ];

    public $js = [
        'highcharts.js', 
    ];

    public $depends = [
    
    ];
}
