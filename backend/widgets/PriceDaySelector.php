<?php

namespace backend\widgets;

use yii\helpers\Html;

class PriceDaySelector extends \yii\widgets\InputWidget
{
    public function run()
    {        
        $values = [];

        for ($i = 1; $i <= 7; $i++)
        {
            if ($this->model->{"d$i"} === null || $this->model->{"d$i"}) $values[] = $i;

            echo Html::activeHiddenInput($this->model, "d$i", [
                'id' => str_replace('days', "d$i", $this->options['id']),
                'name' => str_replace('days', "d$i", $this->options['name']),
            ]);
        }

        echo \kartik\select2\Select2::widget([
            'id' => $this->options['id'],
            'name' => $this->options['name'],
            'value' => $values,
            'options' => [
                'multiple' => true,
            ],
            'data' => [
                '1' => 'Пн',
                '2' => 'Вт',
                '3' => 'Ср',
                '4' => 'Чт',
                '5' => 'Пт',
                '6' => 'Сб',
                '7' => 'Вс',
            ],
        ]);

        $this->view->registerJs("
            
            $('#".$this->options['id']."').change(function(){
                
                var id = $(this).attr('id');

                $(this).find('option').each(function(index, option){
                    
                    var value = $(option).attr('value');

                    $('#' + id.replace('days', 'd' + value)).val($(option).is(':checked') ? 1 : 0);

                });

            }).change();

        ");
    }

}
