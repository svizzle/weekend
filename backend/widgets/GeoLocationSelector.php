<?php

namespace backend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\Select2;

use common\models\GeoLocation;
use common\models\GeoRegion;

class GeoLocationSelector extends \yii\widgets\InputWidget
{
    public $label_region = 'Регион';
    public  $hint_region = null;

    public $label_city = 'Населенный пункт';
    public $hint_city = null;

    public function run()
    {        
        $this->view->registerJs("            

            window.doNotChangeMap = false;

            $('#region-selector').change( function(event, params){
            
                params = params || {localities: []};

                console.log(event, params);
                var region_id = $(this).val();

                $.get('/cabinet/geo/locations?region_id='+region_id, function(response){
                    var selector = '#location-selector';
                    $(selector).html('');
                    for (var i in response)
                    {
                        $(selector).append($('<option value=\"'+ response[i].id + '\">' + response[i].name + '</option>'));
                    }

                    $('#location-selector').select2('destroy');
                    $('#location-selector').select2({'theme':'krajee','width':'100%','language':'ru'})

                    var found = false;
                    for(var i in params.localities)
                    {
                        var locality = params.localities[i];
                        console.log(locality);
                        $('#location-selector option').each(function(index, option){
                            if ($(option).text() == locality)
                            {
                                //found = true;
                                found = $(option).attr('value');
                            }
                        });
                    }
                    if (found)
                    {
                        $('#location-selector').val(found);
                        $('#location-selector').select2('destroy');
                        $('#location-selector').select2({'theme':'krajee','width':'100%','language':'ru'})
                    }

                    if (!window.doNotChangeMap)
                    {
                        var text = 'Россия, ' + $('#region-selector :selected').text();
                        var geocoder = ymaps.geocode(text);
                        geocoder.then(
                            function (res) {
                                map.setCenter(res.geoObjects.get(0).geometry.getCoordinates());
                                map.setZoom(7);
                            },
                            function (err) {
                            }
                        );    
                    }
                    window.doNotChangeMap = false;

                }, 'json');

            });
    
            $('#location-selector').change(function(){
                    var text = 'Россия, ' + $('#region-selector :selected').text() + ', ' + $('#location-selector :selected').text();
                    var geocoder = ymaps.geocode(text);
                    geocoder.then(
                        function (res) {
                            map.setCenter(res.geoObjects.get(0).geometry.getCoordinates());
                            map.setZoom(12);
                        },
                        function (err) {
                        }
                    );    


    });

        ");  

        $regions = GeoRegion::find()->orderBy(['name' => SORT_ASC])->all();
        $locations = GeoLocation::find()->andWhere([
            'region_id' => $this->model->geoLocation ? $this->model->geoLocation->region_id : $regions[0]->id,
        ])->orderBy(['name' => SORT_ASC])->all();

        return '<label>' . $this->label_region . '</label>' . '<div class="hint-block">' . $this->hint_region . '</div>'
        .Html::tag('div',
            Select2::widget([
                'model' => $this->model->geoLocation ? $this->model->geoLocation : new GeoLocation,
                'attribute' => 'region_id',
                'data' => ArrayHelper::map($regions, 'id', 'name'),
                'options' => [
                    'id' => 'region-selector',
                    'class' => 'form-control',
                ],
            ]),
            [
                'title' => $this->label_region,
                'data-toggle' => 'tooltip',
            ]
        )
        .'</div>'
        .'<div class="form-group">'
        .'<label>' . $this->label_city .'</label>'
        . '<div class="hint-block">' . $this->hint_city . '</div>'
        .Html::tag('div',
            Select2::widget([
                'model' => $this->model,
                'attribute' => $this->attribute,
                'data' => ArrayHelper::map($locations, 'id', 'name'),
                'options' => [
                    'id' => 'location-selector',
                    'class' => 'form-control',
                ],
            ]),
            [
                'title' => 'Населенный пункт',
                'data-toggle' => 'tooltip',
            ]
        );
    }
}
