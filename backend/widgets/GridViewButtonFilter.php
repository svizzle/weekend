<?php

namespace backend\widgets;

use yii\helpers\Html;

class GridViewButtonFilter extends \yii\base\Widget
{
    public $gridSelector = "#w0";
    
    public function run()
    {
        $this->view->registerJs("

            $('{$this->gridSelector} .filters').find('input,select').change(function(e){
                e.preventDefault();
                e.stopPropagation();
                return false;
            });

            $('.btn-gridview-filter').click(function(e){
                e.preventDefault();
                $('{$this->gridSelector}').yiiGridView('applyFilter');
                return false;
            });

        ");

        return Html::a('<i class="fa fa-search"></i> Фильтр', '#',[
            'class' => 'btn btn-primary btn-gridview-filter',
        ]);
    }    

}
