<?php

namespace backend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class MultiImageUpload extends \yii\widgets\InputWidget
{
    public $deleteUrl;
    public $sortUrl;
    public $uploadUrl;

    public $sortable = false;

    public $initialPreview;

    public function run()
    {
        $this->getView()->registerJS('
            $("#multiupload").on("fileimagesloaded", function(event) {
                //console.log(event);
                $("#multiupload").fileinput("upload");
            });
        ');

        $self = $this;

        echo \kartik\widgets\FileInput::widget([
            'name' => 'image',
            'options' => [
                'multiple' => true,
                'id' => 'multiupload',
            ],
            'pluginOptions' => [
                'uploadUrl' => Url::to($this->uploadUrl),
                'overwriteInitial' => false,
                'append' => false,
                'validateInitialCount' => true,
                'maxFileCount' => 10,
                'initialPreview' => array_map($this->initialPreview, $this->model->{$this->attribute}),
                'initialPreviewConfig' => array_map(function($model) use($self) {
                    return [
                        'url' => Url::to($self->deleteUrl),
                        'key' => $model->id,
                    ];
                }, $this->model->{$this->attribute}),
            ],
        ]);

        if ($this->sortable)
        {
            $this->getView()->registerJS("

                $('.file-preview-thumbnails').sortable({
                    placeholder: 'ui-state-highlight file-preview-frame',
                    stop: function(event, ui){

                        var item = ui.item;
                        var id = item.find('.kv-file-remove').data('key');
                        var index = item.parent().find('.file-preview-frame').index(item);

                        $.post('".Url::to($this->sortUrl)."', {id: id, position: index});

                    }
                }).disableSelection();

            ");
        }
    }
}
