<?php

namespace backend\widgets;

use yii\web\View;

class YandexMap extends \yii\base\Widget
{
    public $model;

    public function init()
    {
        $this->view->registerJsFile('//api-maps.yandex.ru/2.1/?lang=ru_RU', [
            'position' => View::POS_END,
        ], 'yandex-maps');
    }

    public function run()
    {
        if (!$this->model->lat) return;
        if (!$this->model->lon) return;

        $this->view->registerJs("

            ymaps.ready(function() {

                var lat = {$this->model->lat};
                var lon = {$this->model->lon};

                var map = new ymaps.Map('yandex-map', {
                    center: [lat, lon],
                    zoom: 14
                }, {
                });

                var marker = new ymaps.GeoObject({
                    geometry: {
                        type: 'Point',
                        coordinates: [lat, lon]
                    },

                },{
                    draggable: false
                });
                map.geoObjects.add(marker);

            });
        ");
    
        echo '<div id="yandex-map" style="height: 300px;"></div>';
    }
}
