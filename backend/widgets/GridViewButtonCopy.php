<?php

namespace backend\widgets;

use yii\helpers\Html;

class GridViewButtonCopy extends \yii\base\Widget
{
    public $title = 'Копировать';
    public $url = ['copy', 'id' => '_id_'];

    public $gridSelector = "#w0";
    
    public function run()
    {
        $this->view->registerJs("

            $('.btn-gridview-copy').click(function(e){
                    
                if ($(this).attr('disabled'))
                {
                    e.preventDefault();
                    return false;
                }
                else
                {
                    var ids = $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                    $(this).attr('href', $(this).attr('href').replace('_id_', ids[0]));
                }

            });
        
            $('{$this->gridSelector}').find(\"[name='selection[]'],[name=selection_all]\").change(function(){
                var ids = $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                $('.btn-gridview-copy').attr('disabled', ids.length != 1);
            });

        ");

        return Html::a('<i class="fa fa-copy"></i> '.$this->title, $this->url, [
            'class' => 'btn btn-success btn-gridview-copy',
            'disabled' => true,
        ]);
    }    

}
