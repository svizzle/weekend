<?php

namespace backend\widgets;

use yii\helpers\Html;

class GridViewButtonAppend extends \yii\base\Widget
{
    public $title;
    public $url;

    public $gridSelector = "#w0";
    
    public function run()
    {
        $this->view->registerJs("

            $('.btn-gridview-append').click(function(e){
                    
                if ($(this).attr('disabled'))
                {
                    e.preventDefault();
                    return false;
                }
                else
                {
                    var ids = $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                    $(this).attr('href', $(this).attr('href').replace('_id_', ids[0]));
                }

            });
        
            $('{$this->gridSelector}').find(\"[name='selection[]'],[name=selection_all]\").change(function(){
                var ids = $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                $('.btn-gridview-append').attr('disabled', ids.length != 1);
            });

        ");

        return Html::a('<i class="fa fa-plus"></i> '.$this->title, $this->url, [
            'class' => 'btn btn-success btn-gridview-append',
            'disabled' => true,
        ]);
    }    

}
