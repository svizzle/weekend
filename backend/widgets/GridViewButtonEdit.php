<?php

namespace backend\widgets;

use yii\helpers\Html;

class GridViewButtonEdit extends \yii\base\Widget
{
    public $title = 'Редактировать';
    public $url = ['update', 'id' => '_id_'];

    public $gridSelector = "#w0";
    
    public function run()
    {
        $this->view->registerJs("

            $('.btn-gridview-edit').click(function(e){
                    
                if ($(this).attr('disabled'))
                {
                    e.preventDefault();
                    return false;
                }
                else
                {
                    var ids = $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                    $(this).attr('href', $(this).attr('href').replace('_id_', ids[0]));
                }

            });
        
            $('{$this->gridSelector}').find(\"[name='selection[]'],[name=selection_all]\").change(function(){
                var ids = $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                $('.btn-gridview-edit').attr('disabled', ids.length != 1);
            });

        ");

        return Html::a('<i class="fa fa-pencil"></i> '.$this->title, $this->url, [
            'class' => 'btn btn-info btn-gridview-edit',
            'disabled' => true,
        ]);
    }    

}
