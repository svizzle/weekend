<?php

namespace backend\widgets;

use yii\helpers\Html;
use yii\web\View;

class YandexMapSelector extends \yii\base\Widget
{
    public $model;

    public function init()
    {
        $this->view->registerJsFile('//api-maps.yandex.ru/2.1/?lang=ru_RU', [
            'position' => View::POS_END,
        ], 'yandex-maps');
    }

    public function run()
    {
        if (!$this->model->lat) $this->model->lat = '55.76';
        if (!$this->model->lon) $this->model->lon = '37.64';

        $this->view->registerJs("

            function updateCoordinates(coordinates) {
                $('#".Html::getInputId($this->model, 'lat')."').val(coordinates[0]);
                $('#".Html::getInputId($this->model, 'lon')."').val(coordinates[1]);
            }

            ymaps.ready(function() {

                var lat = {$this->model->lat};
                var lon = {$this->model->lon};

                window.map = new ymaps.Map('yandex-map', {
                    center: [lat, lon],
                    zoom: 14
                }, {
                });

                map.controls.remove('searchControl');


                var marker = new ymaps.GeoObject({
                    geometry: {
                        type: 'Point',
                        coordinates: [lat, lon]
                    },

                },{
                    draggable: true
                });
                marker.events.add('dragend', function(e){
                    updateCoordinates(marker.geometry.getCoordinates());
                });
                map.geoObjects.add(marker);

                map.events.add('click', function(e){
                    var coords = e.get('coords');
                    var lat = coords[0];//.toPrecision(6);
                    var lon = coords[1];//.toPrecision(6);

                    marker.geometry.setCoordinates([lat, lon]);
                    updateCoordinates(marker.geometry.getCoordinates());

                    ymaps.geocode(coords).then(function(res) {
                        var geoObject = res.geoObjects.get(0);
                    
                        var found = false;

                        var areas = geoObject.getAdministrativeAreas();// - массив областей
                        for(var i in areas)
                        {
                            var area = areas[i];
                            $('#region-selector option').each(function(index, option){
                                if ($(option).text() == area)
                                {
                                    //found = true;
                                    found = $(option).attr('value');
                                }
                            });
                        }
                        if (found)
                        {
                            window.doNotChangeMap = true;
                            $('#region-selector').val(found).trigger('change', {localities: geoObject.getLocalities()});
                        }
                
                    });
                });

                

            });

        ");

        echo Html::tag('div', '', [
            'id' => 'yandex-map',
            'title' => 'Отметьте местоположение компании',
            'data-toggle' => 'tooltip',
            'style' => 'height: 500px;',
        ]);
    }
    
}
