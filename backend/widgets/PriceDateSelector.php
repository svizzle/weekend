<?php

namespace backend\widgets;

use yii\helpers\Html;

class PriceDateSelector extends \yii\widgets\InputWidget
{
    public function run()
    {
        $this->view->registerJs("

            $('#".str_replace('period', 'date', $this->options['id'])."').parents('.row').find('input[type=checkbox]').change(function(){
                var parent = $(this).parents('.row');
                if ($(this).is(':checked'))
                {
                    parent.find('input[type=text]').attr('disabled', false);
//                    parent.find('.input-group-addon').removeClass('disabled-addon');
                }
                else
                {
                    parent.find('input[type=text]').attr('disabled', true);
//                    parent.find('.input-group-addon').addClass('disabled-addon');
                }
            }).change();
            
    ");

?>
<div class="row">
    <div class="col-md-2 text-center">   
        <?php
            echo Html::activeCheckbox($this->model, 'date', [
                'label' => false,
                'id' => str_replace('period', 'date', $this->options['id']),
                'name' => str_replace('period', 'date', $this->options['name']),
            ]);
        ?>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-2">
                От
            </div>
            <div class="col-md-10">
            <?php
                echo \kartik\widgets\DatePicker::widget([
                    'id' => str_replace('period', 'date_begin', $this->options['id']),
                    'removeButton' => false,
                    'value' => $this->model->date_begin,
                    'type' => 3,
                    'name' => str_replace('period', 'date_begin', $this->options['name']),
                ]);
            ?>
            </div>
            <div class="col-md-2">
                До
            </div>
            <div class="col-md-10">
            <?php
                echo \kartik\widgets\DatePicker::widget([
                    'id' => str_replace('period', 'date_end', $this->options['id']),
                    'removeButton' => false,
                    'value' => $this->model->date_end,
                    'type' => 3,
                    'name' => str_replace('period', 'date_end', $this->options['name']),
                ]);
            ?>
            </div>
        </div>
    </div>        
</div>
<?php
    }
}
