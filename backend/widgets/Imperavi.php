<?php

namespace backend\widgets;

use yii\helpers\Html;

class Imperavi extends \vova07\imperavi\Widget
{
    public $containerOptions = [];

    public $settings = [
        'lang' => 'ru',
        'minHeight' => 200,
    ];

    public function run()
    {
        return Html::tag('div', parent::run(), $this->containerOptions);
    }
}
