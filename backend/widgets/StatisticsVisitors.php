<?php

namespace backend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class StatisticsVisitors extends \yii\base\Widget
{
    public function run()
    {
        $this->view->registerJS("
            $.get('".Url::to(['/statistics/visitors'])."', function (data) {

                $('#statistics-visitors').highcharts({
                    title: false,
                    xAxis: {
                        type: 'datetime',
                        title: 'Дата',
                    },
                    yAxis: {
                        title: 'Кол-во посетителей',
                    },
                    series: [{
                        name: 'Кол-во посетителей',
                        type: 'area',
                        data: data,
                    }]
                });

            }, 'json');
        ");

        return Html::tag('div', '', ['id' => 'statistics-visitors']);
    }
}
