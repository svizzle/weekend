<?php

namespace backend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class StatisticsFavorite extends \yii\base\Widget
{
    public $model;

    public function run()
    {
        $this->view->registerJS("
            $.get('".Url::to(['/statistics/favorite', 'company_id' => $this->model->id])."', function (data) {

                $('#statistics-favorite').highcharts({
                    title: false,
                    xAxis: {
                        type: 'datetime',
                        title: 'Дата',
                    },
                    yAxis: {
                        title: 'Кол-во',
                    },
                    series: [{
                        name: 'Кол-во добавлений',
                        type: 'area',
                        data: data.additions,
                    },
                    {
                        name: 'Кол-во удалений',
                        type: 'area',
                        data: data.removals,
                    }]
                });

            }, 'json');
        ");

        return Html::tag('div', '', ['id' => 'statistics-favorite']);
    }
}
