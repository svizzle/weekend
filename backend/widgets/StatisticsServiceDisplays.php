<?php

namespace backend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class StatisticsServiceDisplays extends \yii\base\Widget
{
    public $model;

    public function run()
    {
        $this->view->registerJS("
            $.get('".Url::to(['/statistics/service-displays', 'company_id' => $this->model->id])."', function (data) {

                $('#statistics-service-displays').highcharts({
                    title: false,
                    xAxis: {
                        type: 'datetime',
                        title: 'Дата',
                    },
                    yAxis: {
                        title: 'Кол-во',
                    },
                    series: [{
                        name: 'Кол-во показов',
                        type: 'area',
                        data: data,
                    }]
                });

            }, 'json');
        ");

        return Html::tag('div', '', ['id' => 'statistics-service-displays']);
    }
}
