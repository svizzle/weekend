<?php

namespace backend\widgets;

use yii\helpers\Html;

class PriceTimeSelector extends \yii\widgets\InputWidget
{
    public function run()
    {

        $this->view->registerJs("
            
            $('#".str_replace('worktime', 'time', $this->options['id'])."').parents('.row').find('input[type=checkbox]').change(function(){
                var parent = $(this).parents('.row');
                if ($(this).is(':checked'))
                {
                    parent.find('input[type=text]').attr('disabled', false);
                    parent.find('.input-group-addon').removeClass('disabled-addon');
                }
                else
                {
                    parent.find('input[type=text]').attr('disabled', true);
                    parent.find('.input-group-addon').addClass('disabled-addon');
                }
            });
            
        ");
?>
<div class="row">
    <div class="col-md-2 text-center">        
        <?php
            echo Html::activeCheckbox($this->model, 'time', [
                'label' => false,
                'id' => str_replace('worktime', 'time', $this->options['id']),
                'name' => str_replace('worktime', 'time', $this->options['name']),
            ]);
        ?>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-2">
                От
            </div>
            <div class="col-md-10">
    <?php
        echo \kartik\widgets\TimePicker::widget([
            'id' => str_replace('worktime', 'time_begin', $this->options['id']),
            'disabled' => !$this->model->time,
            'pluginOptions' => [
                'showMeridian' => false,
                'showSeconds' => false,
                'minuteStep' => 60,
                'defaultTime' => '00:00:00',
            ],
            'value' => $this->model->time_begin,
            'name' => str_replace('worktime', 'time_begin', $this->options['name']),
        ]);
?>
            </div>
            <div class="col-md-2">
                До
            </div>
            <div class="col-md-10">
    <?php
        echo \kartik\widgets\TimePicker::widget([
            'id' => str_replace('worktime', 'time_end', $this->options['id']),
            'disabled' => !$this->model->time,
            'value' => $this->model->time_end,
            'pluginOptions' => [
                'showMeridian' => false,
                'showSeconds' => false,
                'minuteStep' => 60,
                'defaultTime' => '00:00:00',
            ],
            'name' => str_replace('worktime', 'time_end', $this->options['name']),
        ]);
    ?>
            </div>
        </div>
    </div>
</div>
<?php

    }
}
