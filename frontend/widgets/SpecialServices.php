<?php

namespace frontend\widgets;

use common\models\ServiceCompilation;

class SpecialServices extends \yii\base\Widget
{
    public function run()
    {
        $models = ServiceCompilation::find()->all();
        array_unshift($models, new ServiceCompilation);
        ?>
            <div class="main-block widgets_special-services">
                <h2>Подборка интересных предложений</h2>
                <div class="inter-preds">
                    <div class="control-tabs">
                        <div class="tabs-container">
                            <?php foreach($models as $i => $model) : ?>
                                <div class="the-tab <?=($i == 0 ? 'tab-active' : '')?>" rel="inter-items-<?=$i?>" data-id="<?=$i?>">
                                    <span><?=($i ? $model->type->name : 'Специальные предложения')?></span>
                                </div>
                            <?php endforeach; ?>
                            <div class="the-tab">
                                <span class="empty-tab">&nbsp;</span>
                            </div>
                            <div class="scroll-box">
                                <div class="scroll-arrows">
                                    <div class="scroll-left scroll-disabled"><i class="fa fa-angle-left"></i></div>
                                    <div class="scroll-right"><i class="fa fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php foreach($models as $i => $model) : ?>
                    <div class="inter-items <?=($i==0 ? 'active' : '')?> slider" id="inter-items-<?=$i?>">
                    <?php
                        $j = 0;
                        foreach($model->getServices()->all() as $service)
                        {
                            $j++;
                            if ($j == 1) echo '<div class="slide">';

                            echo \frontend\widgets\Service::widget([
                                'model' => $service,
                                'special' => true,
                            ]);
                            
                            if ($j == 6)
                            {
                                echo '</div>';
                                $j = 0;
                            }
                        }
                        if ($j != 0) echo '</div>';
                    ?>
                    </div>
                    <div class="adv-controls <?=($i==0 ? 'active' : '')?>" id="adv-controls-<?=$i?>">
                        <?php for($i = 1; $i <= ceil($model->getServices()->count()/6); $i++) : ?>
                        <span class="adv-btn <?=($i == 1 ? 'adv-active' : '')?>"> </span>
                        <?php endfor; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php
        $this->view->registerJs("
            
            $('.widgets_special-services .the-tab').click(function(){

                $('.widgets_special-services .the-tab').removeClass('tab-active');
                $(this).addClass('tab-active');

                $('.widgets_special-services .inter-items').removeClass('active');
                $('.widgets_special-services .adv-controls').removeClass('active');
                $('#' + $(this).attr('rel')).addClass('active');
                $('#adv-controls-' + $(this).data('id')).addClass('active');
            });

            $('.scroll-box .scroll-left').click(function(){
                $(this).addClass('scroll-disabled');
                $(this).parent().find('.scroll-right').removeClass('scroll-disabled');
                $(this).parents('.tabs-container').scrollLeft(0);
            });

            $('.scroll-box .scroll-right').click(function(){
                $(this).addClass('scroll-disabled');
                $(this).parent().find('.scroll-left').removeClass('scroll-disabled');
                $(this).parents('.tabs-container').scrollLeft(10000);
           });

        $('.widgets_special-services .slider').each(function(i, slider){
            
                $('#inter-items-' + i).slick({
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    autoplay: true,
                    variableWidth: true,
                    autoplaySpeed: 3000
                }).on('afterChange',function(event, slick, currentSlide){
                    var index = currentSlide;
                    var buttons = $('#adv-controls-' + i + ' .adv-btn');
                    buttons.removeClass('adv-active');
                    buttons.eq(index).addClass('adv-active');
                });
    
                $('#adv-controls-'+ i +' .adv-btn').click(function(e){
                    var buttons = $(this).parent().find('.adv-btn');
                    buttons.removeClass('adv-active');
                    $(this).addClass('adv-active');
                    var index = buttons.index($(this));
                    $('#inter-items-'+i).slick('slickGoTo', index);

                });
            });

        ");

    }
}
