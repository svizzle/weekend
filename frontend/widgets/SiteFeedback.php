<?php

namespace frontend\widgets;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

class SiteFeedback extends \yii\base\Widget
{
    public $model;

    public $success = false;

    public function run()
    {
    ?>
        <div class="main-block call-block">
            <h2>Обратная связь</h2>
            <div class="call-text">Напишите нам о недостатках в работе нашего сайта, что необходимо добавить или изменить для Вашего удобства?</div>
    <?php
        Pjax::begin([
            'enablePushState' => false,
            'id' => 'site-feedback-pjax',
        ]);

        if ($this->success) $this->view->registerJs('alert("Спасибо, ваше сообщение отправлено.");');

        $form = ActiveForm::begin([
            'action' => ['/site/feedback'],
            'id' => 'site-feedback-form',
            'enableClientValidation' => false,
            'options' => [
                'data-pjax' => true,
            ],
        ]);

        echo $form->field($this->model, 'name')->textInput(['placeholder' => 'Ваше имя'])->label(false);

        echo $form->field($this->model, 'email')->textInput(['placeholder' => 'Ваш E-mail'])->label(false);

        echo $form->field($this->model, 'text')->textarea(['placeholder' => 'Текст сообщения', 'style' => 'height: 78px;'])->label(false);
        
        echo '<div style="height: 23px;">';
        echo Html::submitButton('Отправить');
        echo '</div>';

        $form->end();

        Pjax::end();
    ?>
        </div>
    <?php
    }
}
