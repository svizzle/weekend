<?php

namespace frontend\widgets;

use Yii;
use yii\data\Sort;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

use kop\y2sp\ScrollPager;

use common\models\Service;
use common\models\ServiceType;

use frontend\data\NearestServiceActiveDataProvider;

class NearestServices extends \yii\base\Widget
{
    public $model;

    public function run()
    {        
        $searchParams = Yii::$app->session->get('search-params', false);

        if (!$searchParams)
        {
            /*
            $service_type_ids = Service::find()
                ->enabled()
                ->joinWith(['company.geoLocation'])
                ->andWhere([
                    'geo_location.region_id' => $this->model->company->geoLocation->region_id,
                ])
                ->andWhere(['!=', 'company.id', $this->model->company_id])
                ->andWhere(['!=', 'service.type_id', $this->model->type_id])
                ->select('service.type_id')->distinct(true)->column();
             */
            $service_type_ids = [];
        }
        else
        {
            $service_type_ids = ServiceType::find()
                ->andWhere(['id' => $searchParams['tabs']])
                ->andWhere(['!=', 'id', $this->model->type_id])
                ->select('id')->column();
        }

        $service_types = ServiceType::find()->andWhere(['id' => $service_type_ids])->all();

        $active_tab = isset($_REQUEST['active_tab']) ? $_REQUEST['active_tab'] : 0;

        $sort = new Sort([
            'attributes' => [
                'price' => [
                    'asc' => ['min_price' => SORT_ASC],
                    'desc' => ['max_price' => SORT_DESC],
                    'default' => SORT_ASC,
                    'label' => 'Цена',
                ],
                'rate' => [
                    'asc' => ['rate' => SORT_ASC],
                    'desc' => ['rate' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'Рейтинг',
                ],
                'distance' => [
                    'asc' => ['distance' => SORT_ASC],
                    'desc' => ['distance' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'Расстояние',
                ],
            ],
        ]);

        $ascSort = new Sort([
            'params' => array_merge($_GET, ['sort' => '-price', 'active_tab' => $active_tab]),
            'attributes' => [
                'price' => [
                    'default' => SORT_ASC,
                ],
                'rate' => [
                    'default' => SORT_ASC,
                ],
                'distance' => [
                    'default' => SORT_ASC,
                ],
            ],
        ]);

        $descSort = new Sort([
            'params' => array_merge($_GET, ['sort' => 'price', 'active_tab' => $active_tab]),
            'attributes' => [
                'price' => [
                    'default' => SORT_DESC,
                ],
                'rate' => [
                    'default' => SORT_DESC,
                ],
                'distance' => [
                    'default' => SORT_DESC,
                ],
            ],
        ]);

        $dataProvider = new NearestServiceActiveDataProvider([
            'lat' => $this->model->company->geoLocation->lat,
            'lon' => $this->model->company->geoLocation->lon,
            'distance' => 300,
            'options' => [
                'activeTab' => $active_tab,
                'tabs' => array_map(function($type){return ['id' => $type->id];},$service_types),
            ],
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => $sort,
            'where' => "AND company.id != {$this->model->company_id} 
                AND service.type_id != {$this->model->type_id} 
                AND geo_location.region_id = {$this->model->company->geoLocation->region_id}",            
        ]);
        
        Pjax::begin([
            'id' => 'nearest-services',
            'enablePushState' => false,
        ]);

        \frontend\assets\AppAsset::register($this->view);

        echo Html::beginForm(null, 'POST', [
            'data-pjax' => true,
        ]);
        echo Html::hiddenInput('active_tab', $active_tab, ['class' => 'active_tab']);
        ?>
        <div class="main-block">
            <h2>Развлечения рядом</h2>
            <div class="result-box">
                <div class="control-tabs">
                    <div class="tabs-container">
                    <?php
                    foreach($service_types as $i => $type)
                    {
                        echo Html::tag('div', '<span>'.$type->name.'</span>', [
                            'class' => 'the-tab '.($type->id == $active_tab ? 'tab-active' : ''),
                            'data-id' => $type->id,
                        ]);
                    }
                    echo Html::tag('div', '<span>Дополнительно</span>', [
                        'class' => 'the-tab '.($active_tab == 0 ? 'tab-active' : ''),
                        'data-id' => 0,
                    ]);
                    ?>
                        <div class="empty-tab"></div>
                    </div>
                </div>
                <div class="scroll-box">
                            <div class="scroll-arrows">
                                <div class="scroll-left scroll-disabled"><i class="fa fa-angle-left"></i></div>
                                <div class="scroll-right"><i class="fa fa-angle-right"></i></div>
                            </div>
                </div>
                <div class="result-sort">
                    <span>Сортировать по:</span>
                    <div>Цена <?=Html::a('<i class="sort-down"> </i>', $descSort->createUrl('price'))?><?=Html::a('<i class="sort-up"> </i>', $ascSort->createUrl('price'))?></div>
                    <div>Рейтинг <?=Html::a('<i class="sort-down"> </i>', $descSort->createUrl('rate'))?><?=Html::a('<i class="sort-up"> </i>', $ascSort->createUrl('rate'))?></div>
                <div>Расстояние <?=Html::a('<i class="sort-down"> </i>', $descSort->createUrl('distance'))?><?=Html::a('<i class="sort-up"> </i>', $ascSort->createUrl('distance'))?></div>
                </div>
                <div class="result-items">
        <?=ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/service/list_item',
            'itemOptions' => [
                'class' => 'item',
            ],
            'summary' => false,
            'pager' => [
                'class' => ScrollPager::className(),
                'triggerText' => 'Показать больше',
                'triggerTemplate' => '<a class="result-show-more ias-trigger">{text}</a>',
                'enabledExtensions' => [
                    ScrollPager::EXTENSION_TRIGGER,
                    ScrollPager::EXTENSION_SPINNER,
                    ScrollPager::EXTENSION_NONE_LEFT,
                    ScrollPager::EXTENSION_PAGING,
                    //ScrollPager::EXTENSION_HISTORY
                ],                
            ],
        ])?>
                </div>
            </div>
        </div>
        <?php
        echo Html::endForm();

        Pjax::end();

        $this->view->registerJs("
            $('#nearest-services').on('click', '.the-tab', function(){
                if ($(this).hasClass('tab-active')) return;

                var active_tab = $(this).data('id');
                $('#nearest-services .active_tab').val(active_tab);
                $('#nearest-services form').submit();
            });
        ");
    }
}
