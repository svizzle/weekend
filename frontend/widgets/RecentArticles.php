<?php

namespace frontend\widgets;

use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Article;

class RecentArticles extends \yii\base\Widget
{
    public $service_types;
    public $showAll = false;

    /**
     * Показывать только статьи имеющие приявязанные услуги
     * @var bool
     */
    public $onlyWithHaveServices = false;

    public function run()
    {
        $models = (new Query())
            ->select('a.*')
            ->from('article a')
            ->limit(5)
            ->orderBy('RAND()');

        if ($this->onlyWithHaveServices){
            $models->innerJoin('article_service_types ast', 'ast.article_id = a.id')
                    ->groupBy('a.id');
        }

        if (!$this->showAll){
            $models
                ->innerJoin('article_service_types ast', 'ast.article_id = a.id')
                ->where(['ast.service_id' => $this->service_types]);
        }

        $models = $models->all();

        if (!count($models)) return;

        ?>
        <div class="main-block news-block recent-articles-widget">
            <h2>Полезные статьи</h2>
        <?php foreach($models as $model) : ?>
            <div class="the-new-block">
                <h3><?=$model['name']?></h3>
                <div class="the-new-content">
                    <div class="ndet"></div>
                    <div class="ntext">
                        <?=$model['short_text']?>
                        <p style="padding-top: 5px;">
                            <a href="<?=Url::to(['/article/view', 'id' => $model['id']])?>">Подробнее...</a>
                        </p>
                    </div>            
                </div>
            </div>
        <?php endforeach; ?>
        </div>
        <?php
    }
}
