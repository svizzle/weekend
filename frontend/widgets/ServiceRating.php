<?php

namespace frontend\widgets;

use yii\helpers\Html;

class ServiceRating extends \yii\base\Widget
{
    public $value = 0;

    public function run()
    {
        return Html::tag('div',
            Html::tag('span', '&#9733;', ['class' => $this->value >= 1 ? 'pred-voted' : '']).
            Html::tag('span', '&#9733;', ['class' => $this->value >= 2 ? 'pred-voted' : '']).
            Html::tag('span', '&#9733;', ['class' => $this->value >= 3 ? 'pred-voted' : '']).
            Html::tag('span', '&#9733;', ['class' => $this->value >= 4 ? 'pred-voted' : '']).
            Html::tag('span', '&#9733;', ['class' => $this->value >= 5 ? 'pred-voted' : ''])
            ,[
                'class' => 'pred-rate',
        ]);
    }
}
