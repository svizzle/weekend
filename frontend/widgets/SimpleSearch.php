<?php

namespace frontend\widgets;

use yii\helpers\Url;

class SimpleSearch extends \yii\base\Widget
{
    public function run()
    {
        $this->view->registerJs("

            $('.search-box .search-input').autocomplete({
                minLength: 3,
                source: '".Url::to(['/site/simple-search'])."',
                select: function(event, ui) {
                    window.location.href = ui.item.url;
                }
            }).autocomplete('instance')._renderItem = function(ul, item) {
                ul.addClass('autocomplete-simple-search');
                return $('<li>')
                    .append('<div class=\"line-1\"><div class=\"col-1\">ID: ' + item.code + '</div><div class=\"col-2\">' + item.name+ '</div></div>')
                    .append('<div class=\"line-2\"><div class=\"col-1\">&nbsp;</div><div class=\"col-2\">' + item.company.name+ '</div></div>')
                    .append('<div class=\"line-3\"><div class=\"col-1\">&nbsp;</div><div class=\"col-2\">' + item.company.address+ '</div></div>').appendTo(ul);
            };
        
        ");
?>
        <div class="search-box">
            <input name="text" class="search-input" type="text" placeholder="Найти по названию или по ID" />
            <button type="submit" class="search-btn">
                <i class="fa fa-search"></i>
            </button>
        </div>
<?php
    }
}
