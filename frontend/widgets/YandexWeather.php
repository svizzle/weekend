<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\Html;

use common\models\GeoLocation;

if (!function_exists('mb_ucfirst') && function_exists('mb_substr'))
{
    function mb_ucfirst($string)
    {
        mb_internal_encoding("UTF-8");
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
    }
}

class YandexWeather extends \yii\base\Widget
{
    public $city_id;

    public $cacheTime = 7200; // 2 часа
    
    public function run()
    {
        if (!$this->city_id) return;

        $html = file_get_contents("https://www.gismeteo.ru/ajax/print/".$this->city_id."/weekly/");
        var_dump($html);
        die();

        $xml = Yii::$app->session->get('yandex-weather-'.$city_id);
        if (!$xml)
        {
            for($i = 0; $i !=100; $i++)
            {
                $xml = @file_get_contents('https://export.yandex.ru/weather-ng/forecasts/'.$city_id.'.xml');
                if ($xml) break;
            }
            if (!$xml) return;        
            Yii::$app->session->set('yandex-weather-'.$city_id, $xml, $this->cacheTime);
        }

        try
        {
            $xml = new \SimpleXMLElement($xml);
        }
        catch(\Exception $e)
        {
            return;
        }


        echo '<div class="main-block widgets_yandex-weather">';
        echo '  <h2>Прогноз погоды</h2>';        
        echo '  <div class="city-name">'.$this->model->name.'</div>';
        for($i = 0; $i != 5; $i++)
        {
            $day = $xml->day[$i];
            echo '<div class="line">';
                echo '<div class="date">';
                    echo '<div class="name">'.($i == 0 ? 'Cегодня' : mb_ucfirst(Yii::$app->formatter->asDate($day['date'], 'eeee'))).'</div>';
                    echo '<div class="number">'.Yii::$app->formatter->asDate($day['date'], 'd MMMM').'</div>';
                echo '</div>';
                echo '<div class="temperature">';
                    $daily = $day->day_part[1]->{'temperature-data'}->avg;
                    if ($daily > 0) $daily = '+'.$daily;
                    $nightly = $day->day_part[3]->{'temperature-data'}->avg;
                    if ($nightly > 0) $nightly = '+'.$nightly;
                    echo '<div class="daily">'.$daily.'</div>';
                    echo '<div class="nightly">'.$nightly.'</div>';
                echo '</div>';
                echo '<div class="icon">';
                    $code = $day->day_part[1]->{'image-v3'};
                    echo Html::img("https://yastatic.net/weather/i/icons/blueye/color/svg/$code.svg");
                echo '</div>';
                echo '<div class="description">';
                    // текстовое описание погодных условий
                    echo $day->day_part[1]->weather_type;
                echo '</div>';    
            echo '</div>';
        }

        echo '</div>';
    }
}
