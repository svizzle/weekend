<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\Html;

use common\models\GeoLocation;

if (!function_exists('mb_ucfirst') && function_exists('mb_substr'))
{
    function mb_ucfirst($string)
    {
        mb_internal_encoding("UTF-8");
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
    }
}
class GismeteoWeather extends \yii\base\Widget
{
    public $model;

    public $cacheTime = 7200; // 2 часа

    public function run()
    {
        
        if (!$this->model->gismeteo_id) return;

        $weather = Yii::$app->cache->get('gismeteo-'.$this->model->gismeteo_id);
        if (!$weather)
        {
            $weather = [];

            $html = file_get_contents("https://www.gismeteo.ru/ajax/print/{$this->model->gismeteo_id}/weekly/");

            if (preg_match_all('/(<tr>\s*<td class="c_date.*">[\s\S]*<\/td>\s*<\/tr>)/Um', $html, $rows))
            {
                foreach($rows[0] as $row)
                {
                    if (count($weather) == 5) break;
                    if (preg_match('/<span class="n_date">(.*)<\/span>[\s\S]*<td class="n_temp">\s*(\S+)\s*<\/td>[\s\S]*<td class="n_temp r2">\s*(\S+)\s*<\/td>[\s\S]*<div class="icon"><img src="(\S*)"><\/div>[\s\S]*<div class="text_limit">([\s\S]*)<\/div>/Um', $row, $cols))
                    {
                        $weather[] = [
                            'date' => $cols[1],
                            'night' => $cols[2],
                            'day' => $cols[3],
                            'image' => $cols[4],
                            'text' => $cols[5],                        
                        ];
                    }
                }
            }
            Yii::$app->cache->set('gismeteo-'.$this->model->gismeteo_id, $weather, $this->cacheTime);
        }
    
        echo '<div class="main-block widgets_yandex-weather">';
        echo '  <h2>Прогноз погоды</h2>';        
        echo '  <div class="city-name">'.$this->model->name.'</div>';
        foreach($weather as $i => $row)
        {
            echo '<div class="line">';
                echo '<div class="date">';
                    echo '<div class="name">'.($i == 0 ? 'Cегодня' : mb_ucfirst(Yii::$app->formatter->asDate(strtotime("+$i day"), 'eeee'))).'</div>';
                    echo '<div class="number">'.$row['date'].'</div>';
                echo '</div>';
                echo '<div class="temperature">';
                    echo '<div class="daily">'.$row['day'].'</div>';
                    echo '<div class="nightly">'.$row['night'].'</div>';
                echo '</div>';
                echo '<div class="icon">';
                    echo Html::img($row['image']);
                echo '</div>';
                echo '<div class="description">';
                // текстовое описание погодных условий
                echo $row['text'];
                echo '</div>';    
            echo '</div>';
        }

        echo '</div>';
    }
}
