<?php

namespace frontend\widgets;

use yii\helpers\Html;

use common\models\Advertise;

class AdvertiseSidebar extends AdvertiseContent
{
    public function run()
    {
        $models = $this->getModels();
        if(!count($models)) return;
    ?>
        <div class="main-block widgets_rek">
            <h2>Контекстная реклама</h2>
            <div class="cntnr slider">
                <?php foreach($models as $model): ?>
                    <?=$this->renderModel($model)?>
                <?php endforeach; ?>
            </div>
            <div class="adv-controls">
                <?php for($i = 1; $i <= count($models); $i++) : ?>
                    <span class="adv-btn <?=($i == 1 ? 'adv-active' : '')?>"> </span>
                <?php endfor; ?>
            </div>
        </div>
    <?php
$this->view->registerJs("

    $('.widgets_rek .slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    }).on('afterChange',function(event, slick, currentSlide){
        var index = currentSlide;
        var buttons = $('.widgets_rek .adv-controls .adv-btn');
        buttons.removeClass('adv-active');
        buttons.eq(index).addClass('adv-active');
    });
     
    $('.widgets_rek .adv-controls .adv-btn').click(function(e){
        var buttons = $(this).parent().find('.adv-btn');

        buttons.removeClass('adv-active');
        $(this).addClass('adv-active');
        var index = buttons.index($(this));
        
        var slide = index;
        $('.widgets_rek .slider').slick('slickGoTo', slide);

    });
    
");
    }

    public function getModels()
    {
        $models = Advertise::find()->andWhere([
            'position' => Advertise::POSITION_SIDEBAR,
        ])->orderBy('RAND()')->limit(3)->all();
        return $models;
    }

    public function renderModel($model)
    {
        switch($model->type_id)
        {
            case Advertise::TYPE_BANNER:
                $image = \common\widgets\AdvertiseImage::widget(['model' => $model]);
                return Html::a($image, $model->link, ['target' => '_blank']);

            default:
                return $model->code;
        }
    }
}
