<?php

namespace frontend\widgets;

use yii\data\ActiveDataProvider;

use common\models\Setting;

class RecentServices extends \yii\base\Widget
{
    public function run()
    {        
        $setting = Setting::findOne(['name' => 'recent_services_period']);
        

        $models = \common\models\Service::find()
            ->enabled()
            ->andWhere([
                '>=', 'service.created_at', date('Y-m-d H:i:s', time() - (60*60*24*$setting->value)),
            ])
            ->orderBy(['service.created_at' => SORT_DESC])
            ->limit(15)
            ->all();

        if (!count($models)) return;

        return $this->render('recentServices', [
            'models' => $models,
        ]);
    }
}
