<?php

namespace frontend\widgets;

use yii\helpers\Html;

use common\models\Advertise;

class AdvertiseContent extends \yii\base\Widget
{
    public function run()
    {
        $model = $this->getModel();
        if(!$model) return;
    ?>
        <div class="main-block widgets_rek">
            <h2>Контекстная реклама</h2>
            <div class="cntnr">
                <?=$this->renderModel($model)?>
            </div>
        </div>
    <?php
    }

    public function getModel()
    {
        $model = Advertise::find()->andWhere([
            'position' => Advertise::POSITION_CONTENT,
        ])->orderBy('RAND()')->one();
        return $model;
    }

    public function renderModel($model)
    {
        switch($model->type_id)
        {
            case Advertise::TYPE_BANNER:
                $image = \common\widgets\AdvertiseImage::widget(['model' => $model]);
                return Html::a($image, $model->link, ['target' => '_blank']);

            default:
                return $model->code;
        }
    }
}
