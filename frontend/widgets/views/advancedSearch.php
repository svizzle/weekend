<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

use common\models\GeoLocation;
use common\models\ServiceType;
use common\models\ServiceTypeGroup;


$this->registerJs("

    $('.advanced-search-widget').advancedSearch(".Json::encode([
        'regions' => array_map(function($region){
            return [
                'id' => $region->id,
                'name' => $region->name,
                'locations' => array_map(function($location){
                    return [
                        'id' => $location->id,
                        'name' => $location->name,
                    ];
                }, $region->getLocations()->orderBy(['name' => SORT_ASC])->all()),
            ];
        }, $regions),
        'templates' => array_map(function($template){
            return [
                'id' => $template->id,
                'name' => $template->name,
                'custom' => (bool)$template->user_id,
                'tabs' => array_map(function($tab){
                    return [
                        'id' => $tab->id,
                    ];
                }, $template->types),
                'values' => ArrayHelper::getColumn($template->values, 'id'),
            ];
        }, $templates),
        'tabs' => array_map(function($tab){
            return [
                'id' => $tab->id,
                'name' => $tab->name,
                'count' => $tab->getServices()->count(),
                'attributes' => array_map(function($attribute){
                    return [
                        'id' => $attribute->id,
                        'name' => $attribute->name,
                        'values' => array_map(function($value){
                            return [
                                'id' => $value->id,
                                'name' => $value->name,
                            ];
                        }, $attribute->attributeValues),
                    ];
                }, $tab->serviceAttributes),
            ];
        },$tabs),
        'searchParams' => $searchParams,
    ]).");

");

?>
<div class="search-engine-box advanced-search-widget">
    <div class="search-engine-small">
    <?php 
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'id' => 'search-form',
            'action' => ['/service/advanced-search'],
            'method' => 'get',
            'options' => [
                'onsubmit' => 'return false;',
            ],
        ]);
    ?>
        <div class="select-container regions">
            <input name="region_id" type="hidden">
            <input class="select-select ui-autocomplete-input" placeholder="Место отдыха" autocomplete="off">
            <span class="select-addit" style="display: none;">ВСЕ</span>
            <span class="select-showall"></span>
            <ul class="select-addit-box" style="display: none;"></ul>
            <ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-1" tabindex="0" style="display: none;"></ul>
        </div>
        <div class="select-container templates">
            <input name="template_id" type="hidden">
            <input class="select-select ui-autocomplete-input" placeholder="Вид отдыха" autocomplete="off">
            <span class="select-addit" style="display: none;">ВСЕ</span>
            <span class="select-showall"></span>
            <ul class="select-addit-box" style="display: none;"></ul>
            <ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-2" tabindex="0" style="display: none;"></ul>
        </div>
        <div class="search-tool">
            <button id="search-send">Найти</button>
            <a class="show-advanced">Расширенный поиск</a>
        </div>
        <?php
            $form->end();
        ?>
    </div>
</div>

            <div class="advanced-search modal-group-1" style="display: none;">
				<div class="advanced-tab-box"></div>
                <div class="advanced-wnd-box"></div>
				<div class="advanced-footer">
					<div class="advanced-left">
						<button class="add-tab">Добавить вкладку</button>
					</div>
                    <div class="advanced-right">
<?php if (!Yii::$app->user->isGuest) : ?>
                        <button class="save-template">Сохранить</button>
<?php endif; ?>
						<button class="apply-template">Применить</button>
						<button onclick="$('.modal-group-1').hide(); $.fn.advancedSearch.resetTabs();">Отменить</button>
					</div>					
				</div>
            </div>

<div class="modal fade new-tab-dialog" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 400px">
    <div class="modal-content">
      <div class="modal-header" style="padding: 11px 15px;">
        <button type="button" style="margin-top: 2px;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Выберите вид услуги</h4>
      </div>
      <div class="modal-body">
        <?=Html::dropDownList('name', null, ArrayHelper::map(ServiceType::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),[     
            'style' => 'background-position: calc(97%) calc(51%); width: 100%;',
        ])?>
      </div>
      <div class="modal-footer" style="padding: 10px 15px;">
        <button type="button" class="button btn-primary">Добавить</button>
        <button type="button" class="button btn-default" data-dismiss="modal">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade search-save-dialog" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="padding: 11px 15px;">
        <button type="button" style="margin-top: 2px;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Введите название для нового Вида отдыха</h4>
      </div>
      <div class="modal-body">
        <?php
            $model = new ServiceTypeGroup;
                            
            $form = ActiveForm::begin([
                'action' => ['/search/save'],
                'options' => [
                    'onsubmit' => 'return false;',
                ],
            ]);

            echo $form->field($model, 'name')->textInput([
                'required' => true,
            ])->label(false);

            $form->end();
        ?>
      </div>
      <div class="modal-footer" style="padding: 10px 15px;">
        <button type="button" class="button btn-primary">Сохранить</button>
        <button type="button" class="button btn-default" data-dismiss="modal">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
