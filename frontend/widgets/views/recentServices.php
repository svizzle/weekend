<?php

use yii\helpers\Html;

use common\models\Page;

use frontend\helpers\ServiceHelper;

$this->title = 'Отдых выходного дня';


$this->registerJs("

    $('.recent-services .slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    }).on('afterChange',function(event, slick, currentSlide){
        var index = currentSlide / 3;
        var buttons = $('.recent-services .adv-controls .adv-btn');
        buttons.removeClass('adv-active');
        buttons.eq(index).addClass('adv-active');
    });
    
    $('.recent-services .adv-controls .adv-btn').click(function(e){
        var buttons = $(this).parent().find('.adv-btn');

        buttons.removeClass('adv-active');
        $(this).addClass('adv-active');
        var index = buttons.index($(this));
        
        var slide = index * 3;
        $('.recent-services .slider').slick('slickGoTo', slide);

    });
    
");

$pages = ceil(count($models)/3);

?>
<div class="main-block recent-services">
    <h2>Новые предложения</h2>
    <div class="new-item-block slider">
        <?php foreach($models as $model) : ?>
            <?=\frontend\widgets\Service::widget(['model' => $model])?>
        <?php endforeach; ?>
    </div>
    <div class="adv-controls">
        <?php for($i = 1; $i <= $pages; $i++) : ?>
        <span class="adv-btn <?=($i == 1 ? 'adv-active' : '')?>"> </span>
        <?php endfor; ?>
    </div>
</div>
