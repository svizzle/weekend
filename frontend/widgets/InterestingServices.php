<?php

namespace frontend\widgets;

use common\models\ServiceCompilation;

class InterestingServices extends \yii\base\Widget
{
    public function run()
    {   
        $model = new ServiceCompilation;

        return $this->render('interestingServices', [
            'models' => $model->services,
        ]);
    }
}
