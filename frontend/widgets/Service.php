<?php

namespace frontend\widgets;

use yii\helpers\Html;

use frontend\helpers\ServiceHelper;

mb_internal_encoding('utf-8');

class Service extends \yii\base\Widget
{
    public $model;

    public $special = false;

    public $discount = false;

    public function run()
    {
        $model = $this->model;
        ?>
        <div class="pred-item">
            <div class="pred-badge"><?=$model->type->name?></div>
            <div class="pred-item-addit">
                <?php if ($this->special) : ?>
                    <div class="pred-addon addon-yellow">VIP</div>
                <?php elseif ($this->discount) : ?>
                    <div class="pred-addon addon-blue">%</div>
                <?php endif; ?>
                <div class="pred-det">
                    <?=Html::a(Html::encode(mb_strlen($model->name) > 29 ? mb_substr($model->name, 0, 26).'...' : $model->name), ['/service/view', 'id' => $model->id], ['title' => $model->name])?>
                    <br /><span class="pred-place"><?=$model->company->geoLocation->region->name.", ".$model->company->geoLocation->name?></span>
                    <br />
                    <?=\frontend\widgets\ServiceRating::widget(['value' => $model->rate])?>
                    <div class="pred-price">
                        <?=ServiceHelper::getPrice($model)?>
                    </div>
<?php /*
                    <div class="pred-tags">
                        <?php foreach($model->getAttributeValues()->limit(3)->all() as $attributeValue) : ?>
                            <?=\common\widgets\ServiceTypeAttributeValueImage::widget([
                                'model' => $attributeValue,
                                'htmlOptions' => [
                                    'class' => 'item-tag',
                                ],
                            ])?>
                        <?php endforeach; ?>
                    </div>
 */ ?>
                </div>
                <?=Html::a(\common\widgets\ServiceImage::widget([
                    'model' => $model->photo, 
                    'width' => 243, 
                    'height' => 252,
                ]), ['/service/view', 'id' => $model->id])?>
            </div>
        </div>
        <?php        
    }
}
