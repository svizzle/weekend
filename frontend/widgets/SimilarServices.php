<?php

namespace frontend\widgets;

use yii\helpers\ArrayHelper;

class SimilarServices extends \yii\base\Widget
{
    public $model;
    public $dataProvider;
    public $type_id;

    public function run()
    {
        if ($this->model)
        {
            $models = \common\models\Service::find()
                ->joinWith(['company', 'company.geoLocation'])
                ->andWhere([
                    '!=',
                    'company.id', 
                    $this->model->company_id
                ])->andWhere([
                    'service.type_id' => $this->model->type_id,
                    'geo_location.region_id' => $this->model->company->geoLocation->region_id,
                ])->limit(5)
                ->orderBy('RAND()')
                ->all();
        }
        else
        {
            $models = \common\models\Service::find()
                ->andWhere([
                    'not in',
                    'service.id', 
                    ArrayHelper::getColumn($this->dataProvider->getModels(), 'id')
                ])->andWhere([
                    'service.type_id' => $this->type_id,
                ])
                ->limit(5)->orderBy('RAND()')->all();
        }

        if (!count($models)) return;

        ?>
        <div class="main-block">
            <h2>Похожие предложения</h2>
            <div class="result-box">
                <div class="result-items">
                <?php
                    foreach($models as $model) 
                    {
                        echo $this->render('/service/list_item', ['model' => $model]);
                    }
                ?>
                </div>
            </div>
        </div>
        <?php
    }
}
