<?php

namespace frontend\widgets;

use Yii;

use common\models\GeoRegion;
use common\models\GeoLocation;
use common\models\ServiceType;
use common\models\ServiceTypeGroup;
use common\models\ServiceTypeAttribute;

class AdvancedSearch extends \yii\base\Widget
{
    public function run()
    {
        $searchParams = Yii::$app->session->get('search-params', false);

        if ($searchParams)
        {
            if ($searchParams['region'])
            {
                $searchParams['region_name'] = GeoRegion::findOne($searchParams['region'])->name;
            }
            if ($searchParams['template'])
            {
                $searchParams['template_name'] = ServiceTypeGroup::findOne($searchParams['template'])->name;
            }
        }

        // ---
        $regions = GeoRegion::find()->orderBy(['name' => SORT_ASC])->all();

        // ---
        $templates = ServiceTypeGroup::find()
            ->orWhere(['is', 'user_id', null])
            ->orderBy(['user_id'=>SORT_DESC]);
        if (!Yii::$app->user->isGuest)
        {
            $templates->orWhere(['user_id' => Yii::$app->user->id]);
            $templates->orderBy([
                'user_id' => SORT_DESC,
                'name' => SORT_ASC,
            ]);
        }
        else
        {
            $templates->orderBy(['name' => SORT_ASC]);
        }
        $templates = $templates->all();

        // ---
        $tabs = ServiceType::find()->orderBy(['name' => SORT_ASC])->all();

        return $this->render('advancedSearch',[
            'regions' => $regions,
            'templates' => $templates,
            'tabs' => $tabs,
            'searchParams' => $searchParams,
        ]);
    }
}
