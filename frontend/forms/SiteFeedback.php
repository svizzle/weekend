<?php

namespace frontend\forms;

class SiteFeedback extends \yii\base\Model
{
    public $name;
    public $email;
    public $text;

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => 1, 'max' => 64],

            ['email', 'required'],
            ['email', 'email', 'checkDNS' => true],

            ['text', 'required'],
            ['text', 'string', 'max' => '4000'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'text' => 'Текст',
        ];
    }
}
