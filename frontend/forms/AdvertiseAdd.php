<?php

namespace frontend\forms;

class AdvertiseAdd extends \yii\base\Model
{
    public $name;
    public $email;
    public $phone;
    public $text;

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => 1, 'max' => 64],

            ['phone', 'required'],

            ['email', 'required'],
            ['email', 'email', 'checkDNS' => true],

            ['text', 'required'],
            ['text', 'string', 'max' => '10000'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'text' => 'Описание',
        ];
    }
}
