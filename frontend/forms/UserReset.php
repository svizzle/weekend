<?php

namespace frontend\forms;

class UserReset extends \yii\base\Model
{
    public $password;
    public $password_confirmation;

    public function rules()
    {
        return [
            ['password', 'required'],
            ['password_confirmation', 'required'],
            ['password_confirmation', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новый пароль',
            'password_confirmation' => 'Подтверждение пароля',
        ];
    }
}
