<?php

namespace frontend\forms;

use common\models\User;

class UserRestore extends \yii\base\Model
{
    public $email;

    protected $_user;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'validateEmail'],
        ];
    }

    public function validateEmail()
    {
        if (!$this->user)
        {
            $this->addError('email', 'Пользователь с таким адресом не найден');
        }
    }

    public function getUser()
    {
        if (!$this->_user)
        {
            $this->_user = User::find()->andWhere(['email' => $this->email])->one();
        }
        return $this->_user;
    }

}
