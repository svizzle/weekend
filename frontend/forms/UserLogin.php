<?php

namespace frontend\forms;

use common\models\User;

class UserLogin extends \yii\base\Model
{
    public $email;
    public $password;
    public $rememberMe = false;

    protected $_user;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['password', 'validatePassword'],
        ];
    }

    public function getUser()
    {
        if (!$this->_user)
        {
            $this->_user = User::find()->andWhere([
                'email' => $this->email,
            ])->one();
        }
        return $this->_user;
    }

    public function validatePassword()
    {
        $user = $this->getUser();
        if (!$user)
        {
            return $this->addError('email', 'Email не найден');
        }

        if ($user->activation_token)
        {
            return $this->addError('email', 'Email не активирован. Пройдите по ссылке в письме');
        }

        if (!$user->validatePassword($this->password))
        {
            return $this->addError('password', 'Неправильный пароль');
        }

        if (!$user->enabled)
        {
            return $this->addError('email', 'Эта учетная запись заблокирована');
        }
    }
}
