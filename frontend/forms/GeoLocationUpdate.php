<?php

namespace frontend\forms;

use Yii;

class GeoLocationUpdate extends \yii\base\Model
{
    public $location_id;

    public function rules()
    {
        return [
            ['location_id', 'required'],
        ];
    }

    public function init()
    {
        parent::init();
        $this->location_id = Yii::$app->session->get('location_id');
    }
}
