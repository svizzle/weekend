<?php

namespace frontend\forms;

use yii\helpers\Html;

use common\models\User;

class UserRegister extends \yii\base\Model
{
    public $email;
    public $password;
    public $terms = false;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'validateEmail'],
            ['password', 'required'],
            ['terms', 'validateTerms'],
        ];
    }    

    public function validateTerms()
    {
        if (!$this->terms)
        {
            $this->addError('terms', 'Вы должны принять условия пользовательского соглашения');
        }
    }

    public function validateEmail()
    {
        $model = User::findOne(['email' => $this->email]);
        if ($model)
        {
            $this->addError('email', 'Этот адрес уже используется на сайте. '.Html::a('Забыли пароль?', ['/user/restore']));
        }
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'name' => 'Имя',
        ];
    }
}
