<?php

namespace frontend\data;

use Yii;

use common\models\Service;

class ServiceActiveDataProvider extends \yii\data\SqlDataProvider
{
    public $options = [];

    public $where = '';

    public $lat;
    public $lon;
    public $distance;

    /**
     * Если лимит установлен, пагинация отключается. Пока и не надо
     * @var null
     */
    public $limit = null;

    public function calculateTotalCount()
    {
        $params = $this->options;

        // Если не задана активная вкладка, значит это специальные предложения
        if (!isset($params['activeTab']) || !$params['activeTab'])
        {
            //@todo
            return 0;
        }

        foreach($params['tabs'] as $tab)
        {
            if ($tab['id'] != $params['activeTab']) continue;

            $sql = "SELECT count(*) FROM (".$this->calculateSql().") foo2";
            return (int) Yii::$app->db->createCommand($sql)->queryScalar();
        }
    }

    public function calculateSql()
    {
        //@TODO Выводить только enabled услуги

        $params = $this->options;

        // Если не задана активная вкладка, значит это специальные предложения
        if (!isset($params['activeTab']) || !$params['activeTab'])
        {
            //@todo
            $limit_sql = '';
            if ($this->limit !== null) $limit_sql = ' limit ' . $this->limit;
            return "select id from service $limit_sql;";
        }

        foreach($params['tabs'] as $tab)
        {
            if ($tab['id'] != $params['activeTab']) continue;

            // Никакие значения не выбраны. Ищем все услуги
            if (!isset($tab['attributes']) || !count($tab['attributes']))
            {
                $sql = 'SELECT * FROM (select service.id, 
                                       min(service_price.price) as min_price, 
                                       max(service_price.price) as max_price, 
                                       service.rate'.($this->lat ? ', 
                                       geodist(geo_location.lat, geo_location.lon, '.$this->lat.', '.$this->lon.') as distance' : '').' from service
                            LEFT JOIN company ON service.company_id = company.id                             
                            LEFT JOIN geo_location ON geo_location.id = company.geo_location_id 
                            LEFT JOIN service_price ON service_price.service_id = service.id ';
            }
            else
            {

                $sql = "SELECT id FROM (
                            SELECT service.id, count(service.id) FROM service 
                                LEFT JOIN company ON service.company_id = company.id 
                                LEFT JOIN geo_location ON geo_location.id = company.geo_location_id 
                                RIGHT JOIN (";

                foreach($tab['attributes'] as $i => $attribute)
                {
                    if ($i == 0)
                    {
                        $sql.= "SELECT service_id as service_id FROM service_to_attribute_value t0 ";
                    }
                    else
                    {
                        $sql.= "INNER JOIN (SELECT service_id as service_id$i FROM service_to_attribute_value WHERE value_id IN (".implode(",", $attribute['values']).")) t$i ON t".($i-1).".service_id".($i == 1 ? '' : $i-1)." = t$i.service_id$i ";
                    }
                }

                $sql.= " WHERE t0.value_id IN (".implode(",", $tab['attributes'][0]['values']).")) s2v ON service.id = s2v.service_id ";
            }
        
            $sql.= ' WHERE service.type_id = '.$tab['id']." ";

            if (isset($params['locations']) && count($params['locations']))
            {
                $sql.= "AND company.geo_location_id IN(".implode(",", $params['locations']).") ";
            }
            elseif(!empty($params['region']))
            {
                $sql.= "AND geo_location.region_id = ".$params['region']." ";
            }

            $sql.=" ".$this->where." ";

            $sql.= "    GROUP BY service.id) foo ";
            if ($this->distance) 
            {
                $sql .= ' WHERE foo.distance <= '.$this->distance;
            }

            if ($this->limit !== null){
                $this->pagination = false;
                $sql .= ' limit ' . $this->limit . '';
            }

            return $sql;
        }
    }

    public function init()
    {
        $this->totalCount = $this->calculateTotalCount();
        $this->sql = $this->calculateSql();

        parent::init();
    }
    
    public function getModels()
    {
        $models = [];

        if ($this->pagination && $this->totalCount == 0) return $models;

        foreach(parent::getModels() as $row)
        {
            $models[] = Service::findOne($row['id']);
        }
        return $models;
    }
}
