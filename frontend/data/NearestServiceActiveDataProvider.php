<?php

namespace frontend\data;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\Service;

class NearestServiceActiveDataProvider extends \yii\data\SqlDataProvider
{
    public $options = [];

    public $where = '';

    public $lat;
    public $lon;
    public $distance;

    public function calculateTotalCount()
    {
        $sql = "SELECT count(*) FROM (".$this->calculateSql().") foo2";
        return (int) Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function calculateSql()
    {
        //@TODO Выводить только enabled услуги

        $params = $this->options;

        $sql = 'SELECT * FROM (select service.id, 
                               min(service_price.price) as min_price, 
                               max(service_price.price) as max_price, 
                               service.rate'.($this->lat ? ', 
                               geodist(geo_location.lat, geo_location.lon, '.$this->lat.', '.$this->lon.') as distance' : '').' from service
                    LEFT JOIN company ON service.company_id = company.id                             
                    LEFT JOIN geo_location ON geo_location.id = company.geo_location_id 
                    LEFT JOIN service_price ON service_price.service_id = service.id WHERE 1=1 ';
        if ($params['activeTab'])
        {
            $sql.= ' AND service.type_id = '.$params['activeTab'];
        }
        else
        {
            if (count($params['tabs']))
            {
                $sql.= ' AND service.type_id NOT IN ('.implode(',', ArrayHelper::getColumn($params['tabs'], 'id')).')';
            }
        }

        $sql.=" ".$this->where." ";
        $sql.= '    GROUP BY service.id) foo WHERE foo.distance <= '.$this->distance;

        return $sql;
    }

    public function init()
    {
        $this->totalCount = $this->calculateTotalCount();
        $this->sql = $this->calculateSql();

        parent::init();
    }
    
    public function getModels()
    {
        $models = [];

        if ($this->pagination && $this->totalCount == 0) return $models;

        foreach(parent::getModels() as $row)
        {
            $models[] = Service::findOne($row['id']);
        }
        return $models;
    }
}
