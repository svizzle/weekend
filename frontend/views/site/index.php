<?php

use yii\helpers\Html;

use common\models\Page;

$this->title = 'Отдых выходного дня';

echo \frontend\widgets\SpecialServices::widget();

echo \frontend\widgets\AdvertiseContent::widget();

echo \frontend\widgets\RecentServices::widget();

$about = Page::findOne(1);
?>
<div class="main-block">
    <h2><?=$about->name?></h2>
    <?=$about->text?>
</div>
<?php

$this->registerJs("

    function iedetect(v)
    {
        var r = RegExp('msie' + (!isNaN(v) ? ('\\s' + v) : ''), 'i');
        return r.test(navigator.userAgent);                            
    }

    // Для мобильных экранов просто покажите изображение под названием 'poster.jpg'. Мобильные
    // экраны не поддерживают автопроигрывание видео, или для IE.
    if(screen.width < 800 || iedetect(8) || iedetect(7) || 'ontouchstart' in window)
    {
        
    }
    else
    {
        // Подождите, пока загрузятся метаданные видео
        $('header .video video').on('loadedmetadata', function(){        
            var width, height;                  // Ширина и высота экрана
            var videoWidth = this.videoWidth;   // Ширина видео (настоящая)
            var videoHeight = this.videoHeight; // Высота видео (настоящая)
            var aspectRatio = videoWidth / videoHeight; // Соотношение высоты и ширины видео

            (adjSize = function() {
                var height = Math.ceil(($(window).height() * 0.9) - 32);
                var width  = Math.ceil($('body').width());
                                    
                var boxRatio = width / height; // Соотношение экрана
                var adjRatio = aspectRatio / boxRatio; // Соотношение видео, разделенное на размер экрана

                // Установите контейнер на ширину и высоту экрана
                $('header.frontpage').height(height);
                $('header.frontpage .video').height(height);
                $('header.frontpage .header-text').css({'margin-top': height - 275});
                $('header.frontpage .advanced-search').css({'top': height + 25});

                // Если соотношение экрана меньше соотношения размеров...
                if(boxRatio < aspectRatio)
                {
                    // Установите ширину видео на размер экрана, умноженный на adjRatio
                    $('header .video video').css({'width' : width*adjRatio+'px'}); 
                }                
                else
                {
                    // Еще раз установите видео на ширину экрана/контейнера
                    $('header .video video').css({'width' : width+'px'});
                }
            })();

            $(window).resize(adjSize);
        });
    }


");


