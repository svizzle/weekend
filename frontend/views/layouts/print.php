<?php $this->beginPage()?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
</head>
<body>
<?php $this->beginBody(); ?>
<?=$content?>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
