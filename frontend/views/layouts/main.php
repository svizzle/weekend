<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

use common\models\GeoLocation;
use common\models\MenuMain;
use common\models\Setting;

\frontend\assets\AppAsset::register($this);

$isFrontpage = (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index');

$this->registerJs("
    $('.header-place').click(function(){
        $('.geo-location-update-dialog').modal();
    });
");

if (Yii::$app->session->getFlash('show_location_modal', false)){
    $this->registerJs("$('.geo-location-update-dialog').modal();");
}

if (Yii::$app->session->getFlash('its_time_to_confirm_location', false)){
    $this->registerJs("
    $(window).bind('load', function() {
        setTimeout(function(){ $('#confirm-light').click(); },900, function(){
            $('#darktooltip-confirm-light').css('left', $('.header-place').position().left - ($('.header-place').width() / 2) -3);
            $('#darktooltip-confirm-light').css('top', $('.header-place').offset().top - ($('#darktooltip-confirm-light').height()+30));
        });

        $( window ).resize(function() {
            $('#darktooltip-confirm-light').css('left', $('.header-place').position().left - ($('.header-place').width() / 2) -3);
            $('#darktooltip-confirm-light').css('top', $('.header-place').offset().top - ($('#darktooltip-confirm-light').height()+30));
        });
    });");
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody(); ?>
        <?=$this->render('/geo/location-update', ['model' => new \frontend\forms\GeoLocationUpdate])?>
		<div class="layer-gray modal-group-1" onclick="$('.modal-group-1').hide();"></div>
        <header class="<?=($isFrontpage ? 'frontpage' : '')?>">
            <?php if ($isFrontpage) : ?>
            <div class="video">
                <video autoplay loop muted>
                    <source src="/media/0.mp4" type="video/mp4">
                </video>
            </div>
            <?php else : ?>
            <?php endif; ?>
			<nav>
                <div class="nav-container">
                    <?=Menu::widget([
                        'options' => [
                            'class' => 'menu-left menu',
                        ],
                        'itemOptions' => [
                            'class' => 'menu-item',
                        ],
                        'items' => [
                            [
                                'label' => 'Главная',
                                'url' => ['/site/index'],
                            ],
                            [
                                'label' => 'Как искать?',
                                'url' => ['/page/view', 'id' => 3],
                            ],
                            [
                                'label' => 'Полезные статьи',
                                'url' => ['/article/index'],
                            ],
                            [
                                'label' => 'Добавить услугу',
                                'url' => ['/service/add'],
                            ],
                            [
                                'label' => 'Реклама на сайте',
                                'url' => ['/advertise/add'],
                            ],
                            [
                                'label' => 'Дополнительно',
                                'url' => '#',
                                'options' => [
                                    'class' => 'menu-item menu-parent',
                                ],
                                'submenuTemplate' => '<i class="fa fa-caret-down"></i><ul class="menu-dropdown">{items}</ul>',
                                'items' => array_map(function($menu){
                                    return [
                                        'label' => $menu->name,
                                        'url' => $menu->url,
                                    ];
                                }, MenuMain::find()->all()),
                            ],
                        ],
                    ]);?>
                    <div class="menu-center menu">
                        <?=\frontend\widgets\SimpleSearch::widget()?>
                    </div>
                    <?php if (Yii::$app->user->isGuest) : ?>
                    <div class="menu-right menu">
                        <div class="menu-group">
                            <a class="menu-login-open">Вход</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a class="menu-register-open">Регистрация</a>
					    </div>
                        <div class="menu-auth-box">
                            <?=$this->render("/user/login", ['model' => new \frontend\forms\UserLogin])?>
                            <?=$this->render('/user/register', ['model' => new \frontend\forms\UserRegister])?>
			    		</div>
    				</div>
                    <?php else : ?>
                        <?= \yii\widgets\Menu::widget([
                            'options' => [
                                'class' => 'menu-right menu',
                            ],
                            'itemOptions' => [
                                'class' => 'menu-item',
                            ],
                                'items' => [
                                    [
                                        'label' => 'ID '.Yii::$app->user->identity->contract_number,
                                        'url' => '#',
                                        'options' => [
                                            'class' => 'menu-item menu-parent',
                                         ],
                                        'submenuTemplate' => '<i class="fa fa-caret-down"></i><ul class="menu-dropdown">{items}</ul>',
                                        'items' => [
                                            [
                                                'label' => 'Профиль пользователя',
                                                'url' => Yii::$app->backendUrlManager->createUrl(['/user/view', 'id' => Yii::$app->user->id]),
                                            ],
                                            [
                                                'label' => 'Сведения о компании',
                                                'url' => Yii::$app->backendUrlManager->createUrl(['/company/form']),
                                                'visible' => Yii::$app->user->can('partner') && !Yii::$app->user->can('admin'),
                                            ],
                                            [
                                                'label' => 'Услуги компании',
                                                'url' => Yii::$app->backendUrlManager->createUrl(['/company/service', 'company_id' => Yii::$app->user->identity->company ? Yii::$app->user->identity->company->id : 0]),
                                                'visible' => Yii::$app->user->can('partner') && Yii::$app->user->identity->company && !Yii::$app->user->can('admin'),
                                            ],
                                            [
                                                'label' => 'Обратная связь',
                                                'url' => '#',
                                                'visible' => !Yii::$app->user->can('admin'),
                                            ],
                                            [
                                                'label' => 'Статистика',
                                                'url' => Yii::$app->backendUrlManager->createUrl(['/statistics/index']),
                                                'visible' => Yii::$app->user->can('partner') && !Yii::$app->user->can('admin'),
                                            ],
                                            [
                                                'label' => 'Избранное',
                                                'url' => Yii::$app->backendUrlManager->createUrl(['/favorite-service/index']),
                                            ],
                                            [
                                                'label' => 'Шаблоны поиска',
                                                'url' => Yii::$app->backendUrlManager->createUrl(['/service-type-group/index']),
                                            ],
                                            [
                                                'label' => 'Выход',
                                                'url' => ['/user/logout'],
                                            ],
                                        ],
                                    ],
                                ],
                            ]);?>
                        <?php endif; ?>
				</div>
			</nav>
			<div class="header-text">
				<div class="header-text-up">
					<a href="/">Отдых выходного дня</a>
                        <div class="header-place"><div  id="confirm-light"  data-tooltip="Ваш город?"></div>
                        <?php
                        $location = GeoLocation::findOne(Yii::$app->session->get('location_id'));
                        echo $location->name;

                    ?></div>
				</div>
				<div class="header-text-down">Самый большой каталог развлечений на выходные дни или отпуск</div>
            </div>
            <?=\frontend\widgets\AdvancedSearch::widget()?>
        </header>
        <?php if (isset($this->params['breadcrumbs'])) : ?>
            <main style="margin-top:18px;">
        <?php else: ?>
            <main>
        <?php endif; ?>
            <?php
                if(isset($this->params['breadcrumbs']))
                {
                    echo Breadcrumbs::widget([
                        'tag' => 'div',
                        'itemTemplate' => '{link} <span></span>',
                        'activeItemTemplate' => '<a>{link}</a>',
                        'links' => $this->params['breadcrumbs'],
                        'options' => [
                            'class' => 'breadcrumbs',
                        ],
                    ]);
                }
            ?>
			<article><?=$content?></article>
            <aside>
                <?php
                    if (isset($this->blocks['sidebar']))
                    {
                        echo $this->blocks['sidebar'];
                    }
                    else
                    {
                        echo \frontend\widgets\AdvertiseSidebar::widget();
                        echo \frontend\widgets\SiteFeedback::widget(['model' => new \frontend\forms\SiteFeedback]);
                        echo \frontend\widgets\RecentArticles::widget(['showAll'=>true, 'onlyWithHaveServices'=>true]);
                    }
                ?>
            </aside>
        </main>
        <footer>
            <div class="footer-content">
                <div class="footer-block">
                    <p>tour-ra.ru &copy; 2015-<?=date('Y')?> Все права защищены.<br />
                    При использовании материалов сайта ссылка обязательна</p>
                </div>
                <div class="footer-separator"></div>
                <div class="footer-block">
                    <p>Если Вам понравился портал, разместите нашу ссылку<br />
                    на своей странице в социальной сети</p>
                </div>
                <div class="footer-block" style="display: flex; width: 214px;">
                    <a class="social-share share-FB" data-type="fb"> </a>
                    <a class="social-share share-GP" data-type="gp"> </a>
                    <a class="social-share share-OK" data-type="ok"> </a>
                    <a class="social-share share-VK" data-type="vk"> </a>
<?php /*
                    <a class="social-share share-TW"> </a>
                    <a class="social-share share-RSA"> </a>
 */ ?>
                </div>
            </div>
        </footer>
    <?php $this->endBody(); ?>
    <div style="display: none;">
    <?php
        /** @var \rmrevin\yii\geoip\HostInfo $Info */
//        $Info = \Yii::createObject([
//            'class' => '\rmrevin\yii\geoip\HostInfo',
//            'host' => 'phptime.ru', // some host or ip
//        ]);

        //var_dump($Info->getData());
        ?>
    </div>
                <style type="text/css">
                    .header-place{

                        height: 30px;
                    }
                    #confirm-light{
                        float: left;
                        margin-top: -20px;
                        width: 100%;
                        height: 1px;
                    }
                </style>
                <script type="text/javascript">
                    $('#confirm-light').darkTooltip({
                        trigger:'click',
                        animation:'flipIn',
                        gravity:'south',
                        confirm:true,
                        theme:'dark',
                        yes: 'Да',
                        no: 'Нет',
                        onYes: function(){
                            $.ajax({
                                type:'post',
                                url: '<?= \yii\helpers\Url::to(['/geo/confirmed-location']) ?>'
                            });
                        },
                        onNo: function(){
                            $('.header-place').click();
                        }
                    });

                </script>
	</body>
</html>
<?php $this->endPage(); ?>
