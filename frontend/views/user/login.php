<?php

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

Pjax::begin([
    'enablePushState' => false,
    'id' => 'user-login-pjax',
]);

$this->registerJs('

    $(".password-btn").click(function(){
        $(this).find(".fa").toggleClass("fa-eye fa-eye-slash");
        $(this).parent().find("input").attr("type", function(idx, attr){
            return attr == "text" ? "password" : "text";
        });
    });


');
?>
<div class="menu-login-container">
<?php
    $form = ActiveForm::begin([
        'id' => 'user-login-form',
        'action' => ['/user/login'],
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => true,
        ],
    ]);

    echo $form->field($model, 'email')->textInput([
        'class' => 'login-input',
        'placeholder' => 'Ваш E-mail',
    ])->label(false);
?>
    <div class="password-box">
    <?php
        echo $form->field($model, 'password')->passwordInput([
            'class' => 'password-input',
            'placeholder' => "Ваш пароль",
        ])->label(false);
    ?>
        <button type="button" onclick="" class="password-btn">
            <i class="fa fa-eye"></i>
        </button>
    </div>
    <div class="addit-box">
        <div class="addit-left">
            <input class="addit-checkbox" type="checkbox" id="checkbox-remember-me" />
            <label class="addit-checkbox-label" for="checkbox-remember-me">Запомнить меня</label><br />
            <div class="addit-question">
                <i class="fa">?</i> <?=Html::a('Забыли пароль?', ['/user/restore'], ['data-pjax' => 0])?>
            </div>
        </div>




        <div class="addit-right">
            <button type="submit">Войти</button>

        </div>
    </div>
    <div class="help-block"><?php Yii::$app->request->isAjax?(!Yii::$app->request->validateCsrfToken()?'Данные устарели. Обновите страницу.':''):'' ?></div>
    <div class="auth-separator"></div>
    <div class="social-box">
        <span>Вход через социальные сети</span>
        <?php 
            $authChoice = AuthChoice::begin([
                'baseAuthUrl' => ['/user/auth'],
                'popupMode' => true,
            ]);
        ?>
        <div class="social-icons">
            <?php
                foreach($authChoice->clients as $client)
                {
                    switch($client->id)
                    {
                        case 'google':
                            $class = 'social-gp';
                            break;
                        case 'vkontakte':
                            $class = 'social-vk';
                            break;
                        case 'odnoklassniki':
                            $class = 'social-ok';
                            break;
                        case 'facebook':
                            $class = 'social-fb';
                            break;
                    }
                    echo Html::a('', $authChoice->createClientUrl($client), ['class' => $class]);
                }
            ?>
        </div>
        <?php
            $authChoice->end();
        ?>
    </div>
<?php
    $form->end();
?>
</div>
<?php
    Pjax::end();
