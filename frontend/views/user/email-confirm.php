<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$this->title?></h2>
    <p>На указанный вами адрес была выслана ссылка для продолжения регистрации. Пожалуйста, пройдите по ссылке в письме</p>
</div>
