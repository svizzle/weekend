<?php

$this->title = 'Смена пароля';
$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$this->title?></h2>
    <p>Пароль успешно изменен!</p>
</div>
