<?php

$this->title = 'Подтверждение регистрации';
$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$this->title?></h2>
    <p>Вы успешно зарегистрированы. На вашу почту отправлена ссылка для активации аккаунта. Пройдите по ссылке в письме чтобы продолжить работу с сайтом.</p>
</div>
