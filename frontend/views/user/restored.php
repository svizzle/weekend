<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$this->title?></h2>
    <p>Ссылка для восстановления пароля отправлена на указанный почтовый адрес</p>
</div>
