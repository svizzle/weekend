<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$this->title?></h2>
    <p>Для продолжения работы с сайтом, необходимо указать E-mail адрес</p>
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'email')
              ->textInput([
                  'placehodler' => 'E-mail',
              ])
              ->label(false);
?>
<div class="text-center">
<?php
    echo Html::submitButton('Далее', ['class' => 'button']);

    $form->end();
?>
</div>
</div>
