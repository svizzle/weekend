<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

\frontend\assets\AppAsset::register($this);

Pjax::begin([
    'enablePushState' => false,
    'id' => 'user-register-pjax',
]);

?>
<div class="menu-register-container">
<?php
    $form = ActiveForm::begin([
        'id' => 'user-register-form',
        'action' => ['/user/register'],
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => true,
        ],
    ]);

echo $form->field($model, 'email',[
        'errorOptions' => [
            'encode' => false,
            'class' => 'help-block',
        ],
    ])->textInput([
        'class' => 'login-input',
        'placeholder' => 'Ваш E-mail',
    ])->label(false);
?>
    <div class="password-box">
    <?php
        echo $form->field($model, 'password')->textInput([
            'class' => 'password-input',
            'placeholder' => 'Ваш пароль',
        ])->label(false);
    ?>
        <button type="button" class="password-btn">
            <i class="fa fa-eye-slash"></i>
        </button>
    </div>
        <?=$form->field($model, 'terms', [
        'template' => '{label}{input}<label class="reg-checkbox-label" for="userregister-terms">Я принимаю условия <a href="'.Url::to(['/page/view', 'id' => 6]).'" target="_blank" data-pjax=0>пользовательского соглашения</a></label>{hint}{error}',
        ])->checkbox([
                    'label' => '', //'Я принимаю условия пользовательского соглашения',
                    //'id' => 'checkbox-accept-text',
                    //'class' => 'reg-checkbox',
            ], false);?>
    <button class="reg-btn" type="submit" class="reg-btn">Регистрация</button>
<?php
    $form->end();
?>
</div>
<?php

Pjax::end();
