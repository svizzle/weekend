<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$this->title?></h2>
    <?=$page->text?>
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'email')->textInput(['placeholder' => 'Ваш E-mail'])->label(false);

?>
    <div class="text-center">    
        <?=Html::submitButton('Восстановить', ['class' => 'button'])?>
    </div>
<?php
    $form->end();
?>
</div>
