<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Смена пароля';
$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$this->title?></h2>
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'password')->passwordInput(['placeholder' => 'Новый пароль'])->label(false);

    echo $form->field($model, 'password_confirmation')->passwordInput(['placeholder' => 'Подтверждение пароля'])->label(false);


    echo Html::submitButton('Изменить пароль', ['class' => 'btn btn-primary']);

    $form->end();
?>
</div>
