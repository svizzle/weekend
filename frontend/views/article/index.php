<?php

use yii\widgets\ListView;

$this->title = "Полезные статьи";
$this->params['breadcrumbs'] = [
    $this->title,
];

?>

<?=ListView::widget([
    'summary' => '',
    'itemView' => '_item',
    'dataProvider' => $dataProvider,
])?>

