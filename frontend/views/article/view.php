<?php

$this->title = $model->name;

$this->params['breadcrumbs'] = [
    [
        'label' => 'Полезные статьи',
        'url' => ['/article/index'],        
    ],
    $this->title
];

?>
<div class="main-block">
    <h2><?=$model->name?></h2>
    <?=$model->full_text?>
</div>
