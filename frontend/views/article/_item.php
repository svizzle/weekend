<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="main-block">
    <h2><?=$model->name?></h2>
    <div class="row">
        <div class="col-md-12">
            <?=$model->short_text?>
        </div>
        <div class="col-md-12" style="padding-top: 10px; text-align: right;">
            <a href="<?=Url::to(['/article/view', 'id' => $model->id])?>">Подробнее...</a>
        </div>
    </div>
</div>
