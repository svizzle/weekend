<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\GeoRegion;
use common\models\GeoLocation;

$this->registerJs("
    
$('.geo-location-update-dialog .btn-primary').click(function(){
    $('.geo-location-update-dialog form').submit();
});

$('.geo-location-update-dialog .region_id').change(function(){

    $('#geolocationupdate-location_id').html('');
    $.get('/geo/locations?region_id=' + $(this).val(), function(response){

        $.each(response, function(index, location){
            $('#geolocationupdate-location_id').append($('<option value=\"'+location.id+'\">'+location.name+'</option>'));
        });

    }, 'json');

});

");

?>
<div class="modal fade geo-location-update-dialog" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 400px">
    <div class="modal-content">
      <div class="modal-header" style="padding: 11px 15px;">
        <button type="button" style="margin-top: 2px;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Выберите ваше местоположение</h4>
      </div>
      <div class="modal-body" style="padding-bottom: 10px;">
<?php
    
    $location = GeoLocation::findOne($model->location_id);

    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'action' => ['/geo/location-update'],
    ]);

    echo Html::dropDownList('region_id', $location->region_id, ArrayHelper::map(GeoRegion::find()->orderBy('name')->all(), 'id', 'name'), [
        'class' => 'form-control region_id',       
        'style' => 'margin-bottom: 5px;', 
    ]);

    echo $form->field($model, 'location_id')
              ->dropDownList(ArrayHelper::map($location->region->getLocations()->orderBy('name')->all(), 'id', 'name'),[])->label(false);

    $form->end();
?>
      </div>
      <div class="modal-footer" style="padding: 10px 15px;">
        <button type="button" class="button btn-primary">Выбрать</button>
        <button type="button" class="button btn-default" data-dismiss="modal">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
