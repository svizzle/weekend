<?php

$this->title = $model->name;

$this->params['breadcrumbs'] = [
    $this->title
];

?>
<div class="main-block">
    <h2><?=$model->name?></h2>
    <?=$model->text?>
</div>
