<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

$this->title = $model->name;

$this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'][] = $this->title;

$this->beginBlock('sidebar');
    echo \frontend\widgets\GismeteoWeather::widget(['model' => $model->geoLocation]);
    echo \frontend\widgets\AdvertiseSidebar::widget();
    echo \frontend\widgets\SiteFeedback::widget(['model' => new \frontend\forms\SiteFeedback]);
    echo \frontend\widgets\RecentArticles::widget();
$this->endBlock();
?>
    <div class="main-block service_view">
    <h2>Информация о компании</h2>
    <div class="detail-padding">
        <div class="detail-header">
            <div class="result-header-left">
                <div class="result-item-name"><?=Html::encode($model->name)?> <span>(ID <?=$model->user->contract_number?>)</span></div>
                    <div class="result-item-place"><?=$model->geoLocation->region->name.", ".$model->geoLocation->name?></div>
                </div>
                <div class="result-header-right">
                    <div class="result-item-price">
                    </div>
                </div>
            </div>
            <div class="detail-media">
                <div class="detail-left">
                    <div class="pred-badge"><?=$model->type->name?></div>
                        <div class="detail-image">
                            <div class="slider">
                            <?php
                                if (!$model->photo)
                                {
                                    echo \common\widgets\ServiceImage::widget([
                                        'model' => null,
                                        'width' => 602,
                                        'height' => 311
                                    ]);
                                }
                                else
                                {
                                    foreach($model->photos as $photo)
                                    {
                                        echo \common\widgets\ServiceImage::widget([
                                            'model' => $photo,
                                            'width' => 602,
                                            'height' => 311
                                        ]);
                                    }
                                }
                            ?>
                            </div>
                            <div class="detail-infobar">
                                <div class="detail-info-left">
                                    <?php for($i = 1; $i <= count($model->photos); $i++) : ?>
                                        <span class="slider-btn <?=($i == 1 ? 'slider-btn-active' : '')?>">&#9679;</span>
                                    <?php endfor; ?>
                                </div>
                                        <div class="detail-info-right">
                                            <?=\frontend\widgets\ServiceRating::widget(['value' => $model->rate])?>
											<div class="result-views">
                                                <span><?=$model->views?></span>
											</div>
                                        </div>
                            </div>
                            <div class="detail-submenu-box contacts">
                            <?php if ($model->contacts) : ?>
                                <?=$model->contacts?>
                            <?php else : ?>
                                <h3>Телефоны:</h3>
                                <p>
                                    <?php foreach($model->phones as $phone) echo $phone->phone."<br/>";?>
                                </p>
                                <span>При звонке просьба ссылаться на сервис нашего сайта</span>
                                <h3>Email:</h3>
                                <p>
                                    <?=$model->user->email?>
                                </p>
                                <h3>Адрес:</h3>
                                <p>
                                    <?=$model->address?>
                                </p>
                            <?php endif; ?>
                            </div>
                            <div class="detail-submenu-box services">
                                <div class="detail-submenu-up"></div>
                                <div class="detail-submenu">
                                    <?php foreach($model->services as $service) : ?>
                                        <a href="<?=Url::to(['/service/view', 'id' => $service->id])?>">
                                            <div class="detail-submenu-item"><?=Html::encode($service->name)?></div>
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                                <div class="detail-submenu-down"></div>
                            </div>
                        </div>
                    </div>
							<div class="detail-right">
                            <div class="detail-menuitem menuitem-contacts" rel="<?=$model->id?>"><span>Контакты</span></div>
                                <div class="detail-menuitem menuitem-onmap" rel="<?=$model->id?>"><span>На карте</span></div>

                                <a href="<?=Url::to(['/company/print', 'id' => $model->id])?>" target="_blank">
                                    <div class="detail-menuitem menuitem-print">
                                        <span>На печать</span>
                                    </div>
                                </a>
                                <div class="detail-menuitem menuitem-favorite <?=($favorite ? 'favorite' : '')?>"><span><?=($favorite ? 'В избранном' : 'В избранное')?></span></div>
								<div class="detail-menuitem menuitem-addit"><span>Услуги</span></div>
							</div>
						</div>
						<hr />
						<div class="detail-text">
                            <h3>Дополнительная информация</h3>
                            <?=$model->description?>
                        </div>
                        <a class="result-show-more hidden" style="margin-top: -21px;">Показать больше</a>
					</div>
                </div>

<?php
    //\frontend\widgets\NearestServices::widget(['model' => $model])
?>
<?php 
    //\frontend\widgets\AdvertiseContent::widget()
?>

<?php

$this->registerJs("

    // На карте
    $('.menuitem-onmap').click(function(){
        $.popup('".Url::to(['/company/map', 'id' => $model->id])."');
    });

    // В избранное
    $('.menuitem-favorite').click(function(){
        $.get('".Url::to(['/company/favorite', 'id' => $model->id])."');
        if ($(this).hasClass('favorite'))
        {
            $(this).removeClass('favorite');
            $(this).find('span').html('В избранное');
        }
        else
        {            
            $(this).addClass('favorite');
            $(this).find('span').html('В избранном');
        }
    });
    
    // Контакты, доп. услуги
    $('.menuitem-contacts, .detail-submenu-item.company').click(function(){
        $('.detail-submenu-box.contacts').toggle();
        $('.detail-submenu-box.services').hide();
//        $.popup('".Url::to(['/company/contacts', 'id' => 0])."'.replace('0', $(this).attr('rel')));
    });

    $('.menuitem-addit').click(function(){
        $('.detail-submenu-box.services').toggle();
        $('.detail-submenu-box.contacts').hide();
    });

    // Слайдер
    $('.detail-image .slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    }).on('afterChange',function(event, slick, currentSlide){
        var buttons = $('.detail-infobar .slider-btn');
        buttons.removeClass('slider-btn-active');
        buttons.eq(currentSlide).addClass('slider-btn-active');
    });

    // Кнопки под слайдером
    $('.detail-infobar .slider-btn').click(function(e){
        var buttons = $(this).parent().find('.slider-btn');

        buttons.removeClass('slider-btn-active');
        $(this).addClass('slider-btn-active');
        var index = buttons.index($(this));
        
        $('.detail-image .slider').slick('slickGoTo', index);

    });

    // Скрытие полного описания
    if ($('.detail-text').height() > 135)
    {
        var height = $('.detail-text').outerHeight();
        
        $('.result-show-more').removeClass('hidden').click(function(e){
            e.preventDefault();
            $('.detail-text').animate({
                height: height,
            }, 1000);
            
            $(this).addClass('hidden');

            return false;
        });
        $('.detail-text').css({
            height: '135px',
            overflow: 'hidden'
        });
    }

");
