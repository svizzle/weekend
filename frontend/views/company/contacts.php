<?php

use yii\helpers\Html;

use frontend\helpers\CompanyHelper;

?>
<div class="company_contacts">
    <h2><?=Html::encode($model->name)?></h2>
                                <h3>Телефоны:</h3>
                                <p>
                                    <?php foreach($model->phones as $phone) echo $phone->phone."<br/>";?>
                                </p>
                                <span>При звонке просьба ссылаться на сервис нашего сайта</span>
                                <h3>Email:</h3>
                                <p>
                                    <?=$model->user->email?>
                                </p>
                                <h3>Адрес:</h3>
                                <p>
                                    <?=$model->address?>
                                </p>
                            </div>
</div>

