<?php

use yii\helpers\Html;
use yii\helpers\Url;

use frontend\helpers\CompanyHelper;
use frontend\helpers\ServiceHelper;

$favorite = false;
if (!Yii::$app->user->isGuest)
{
    $favorite = Yii::$app->user->identity->getFavoriteServices()->andWhere(['id' => $model->id])->exists();
}

?>
							<div class="result-item">
								<div class="result-left">
                                    <div class="result-badge"><?=$model->type->name?></div>
                                    <div class="result-image">
                                        <?=Html::a(\common\widgets\ServiceImage::widget(['model' => $model->photo, 'width' => 170, 'height' => 161]), ['/service/view', 'id' => $model->id])?>
                                        <div class="result-specs">
                                            <?=\frontend\widgets\ServiceRating::widget(['value' => $model->rate])?>
                                            <div class="result-comments">
                                                <span><?=$model->getComments()->count()?></span>
                                            </div>
                                            <div class="result-views">
                                                <span><?=$model->views?></span>
                                            </div>
                                        </div>
									</div>
								</div>
								<div class="result-right">
									<div class="result-item-header">
										<div class="result-header-left">
                                        <div class="result-item-name"><?=Html::a(Html::encode($model->name), ['/service/view', 'id' => $model->id],['data-pjax' => 0])?> <span>(ID <?=$model->code?>)</span></div>
                                        <div class="result-item-place"><?=$model->company->geoLocation->region->name.", ".$model->company->geoLocation->name?></div>
										</div>
										<div class="result-header-right">
                                            <div class="result-item-price">
                                                <big><?=ServiceHelper::getPrice($model)?></big>
                                            </div>
<!--
											<div class="result-item-through">
												от <strong>1600</strong> руб./сут.<br/>
                                            </div>
-->
										</div>
									</div>
									<div class="result-item-tags">
                                        <?php foreach($model->attributeValues as $attributeValue) : ?>
                                            <?=\common\widgets\ServiceTypeAttributeValueImage::widget(['model' => $attributeValue])?>
                                        <?php endforeach; ?>
									</div>
									<div class="result-item-buttons">
                                        <div class="description">
                                            <?=$model->description?>
                                        </div>
										<a data-pjax="0" href="#" class="result-left-eye" style="z-index:10"> </a>
                                        <a data-pjax="0" href="<?=Url::to(['/service/view', 'id' => $model->id])?>" class="result-btn result-btn-more">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Подробнее</a>
                                        <a data-pjax="0" href="#" class="result-btn result-btn-map" data-id='<?=$model->id?>' rel="<?=$model->id?>">&nbsp;&nbsp;На карте</a>
                                        <a data-pjax="0" href="#" class="result-btn result-btn-favorite <?=($favorite ? 'favorite' : '')?>" data-id="<?=$model->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=($favorite ? 'В избранном' : 'В избранное')?></a>
                                        <a data-pjax="0" href="#" class="result-btn result-btn-phone" data-id='<?=$model->id?>' rel="<?=CompanyHelper::getPhone($model->company)?>">&nbsp;&nbsp;Телефон</a>
                                        <a data-pjax="0" href="#" class="result-right-eye" data-id="<?=$model->id?>" data-company_id="<?=$model->company_id?>"> </a>
                                        <div class="contacts">
                                        <?php if ($model->company->contacts) : ?>
                                            <?=$model->company->contacts?>
                                        <?php else : ?>
                                            <h3>Телефоны:</h3>
                                            <p>
                                                <?php foreach($model->company->phones as $phone) echo $phone->phone."<br/>";?>
                                            </p>
                                            <span>При звонке просьба ссылаться на сервис нашего сайта</span>
                                            <h3>Email:</h3>
                                            <p>
                                                <?=$model->company->user->email?>
                                            </p>
                                            <h3>Адрес:</h3>
                                            <p>
                                                <?=$model->company->address?>
                                            </p>
                                        <?php endif; ?>
                                        </div>
									</div>
								</div>
                            </div>
<hr/>

<?php

