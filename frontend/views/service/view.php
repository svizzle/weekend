<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

use kop\y2sp\ScrollPager;

use frontend\helpers\ServiceHelper;

$this->title = $model->name;

$this->params['breadcrumbs'] = [];

if ($params = Yii::$app->session->get('search-params'))
{
    $this->params['breadcrumbs'][] = [
        'label' => 'Результаты поиска',
        'url' => [
            '/service/advanced-search',
            'q' => base64_encode(json_encode($params)),
        ],
    ];
}
$this->params['breadcrumbs'][] = $this->title;

$this->beginBlock('sidebar');
    echo \frontend\widgets\GismeteoWeather::widget(['model' => $model->company->geoLocation]);
    echo \frontend\widgets\AdvertiseSidebar::widget();
    echo \frontend\widgets\SiteFeedback::widget(['model' => new \frontend\forms\SiteFeedback]);
    echo \frontend\widgets\RecentArticles::widget(['service_types' => $model->type_id]);
$this->endBlock();

$favorite = false;
if (!Yii::$app->user->isGuest)
{
    $favorite = Yii::$app->user->identity->getFavoriteServices()->andWhere(['id' => $model->id])->exists();
}

?>
    <div class="main-block service_view">
    <h2>Подробности предложения</h2>
    <div class="detail-padding">
        <div class="detail-header">
            <div class="result-header-left">
                <div class="result-item-name"><?=Html::encode($model->name)?> <span>(ID <?=$model->code?>)</span></div>
                    <div class="result-item-place"><?=$model->company->geoLocation->region->name.", ".$model->company->geoLocation->name?></div>
                </div>
                <div class="result-header-right">
                    <div class="result-item-price">
                        <big><?=ServiceHelper::getPrice($model)?></big>
                    </div>
                </div>
            </div>
            <div class="detail-media">
                <div class="detail-left">
                    <div class="pred-badge"><?=$model->type->name?></div>
                        <div class="detail-image">
                            <div class="slider">
                            <?php
                                if (!$model->photo)
                                {
                                    echo \common\widgets\ServiceImage::widget([
                                        'model' => null,
                                        'width' => 602,
                                        'height' => 311
                                    ]);
                                }
                                else
                                {
                                    foreach($model->photos as $photo)
                                    {
                                        echo \common\widgets\ServiceImage::widget([
                                            'model' => $photo,
                                            'width' => 602,
                                            'height' => 311
                                        ]);
                                    }
                                }
                            ?>
                            </div>
                            <div class="detail-infobar">
                                <div class="detail-info-left">
                                    <?php for($i = 1; $i <= count($model->photos); $i++) : ?>
                                        <span class="slider-btn <?=($i == 1 ? 'slider-btn-active' : '')?>">&#9679;</span>
                                    <?php endfor; ?>
                                </div>
                                        <div class="detail-info-right">
                                            <?=\frontend\widgets\ServiceRating::widget(['value' => $model->rate])?>
											<div class="result-comments">
                                                <span><?=$model->getComments()->count()?></span>
											</div>
											<div class="result-views">
                                                <span><?=$model->views?></span>
											</div>
                                        </div>
                            </div>
                            <div class="detail-submenu-box contacts">
                            <?php if ($model->company->contacts) : ?>
                                <?=$model->company->contacts?>
                            <?php else : ?>
                                <h3>Телефоны:</h3>
                                <p>
                                    <?php foreach($model->company->phones as $phone) echo $phone->phone."<br/>";?>
                                </p>
                                <span>При звонке просьба ссылаться на сервис нашего сайта</span>
                                <h3>Email:</h3>
                                <p>
                                    <?=$model->company->user->email?>
                                </p>
                                <h3>Адрес:</h3>
                                <p>
                                    <?=$model->company->address?>
                                </p>
                            <?php endif; ?>
                            </div>
                            <div class="detail-submenu-box services">
                                <div class="detail-submenu-up"></div>
                                <div class="detail-submenu">
                                    <?php if (!$model->company->sight) : ?>
                                        <a href="<?=Url::to(['/company/view', 'id' => $model->company_id])?>">
                                            <div class="detail-submenu-item company">Информация о компании</div>
                                        </a>
                                    <?php endif; ?>
                                    <?php foreach($additionalServices as $service) : ?>
                                        <a href="<?=Url::to(['/service/view', 'id' => $service->id])?>">
                                            <div class="detail-submenu-item"><?=Html::encode($service->name)?></div>
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                                <div class="detail-submenu-down"></div>
                            </div>
                        </div>
                    </div>
							<div class="detail-right">
                            <div class="detail-menuitem menuitem-contacts" rel="<?=$model->company->id?>"><span>Контакты</span></div>
                                <div class="detail-menuitem menuitem-onmap" rel="<?=$model->id?>"><span>На карте</span></div>

                                <a href="<?=Url::to(['/service/print', 'id' => $model->id])?>" target="_blank">
                                    <div class="detail-menuitem menuitem-print">
                                        <span>На печать</span>
                                    </div>
                                </a>
                                <div class="detail-menuitem menuitem-favorite <?=($favorite ? 'favorite' : '')?>"><span><?=($favorite ? 'В избранном' : 'В избранное')?></span></div>
								<div class="detail-menuitem menuitem-addit"><span>Доп. услуги</span></div>
							</div>
						</div>
                        <div class="detail-tags">
                            <?php foreach($model->attributeValues as $attributeValue) : ?>
                                <?=\common\widgets\ServiceTypeAttributeValueImage::widget(['model' => $attributeValue])?>
                            <?php endforeach; ?>
						</div>
						<hr />
						<div class="detail-text">
                            <h3>Дополнительная информация</h3>
                            <?=$model->description?>
                        </div>
                        <a class="result-show-more hidden" style="margin-top: -21px;">Показать больше</a>

<?php if ($model->getComments()->count()) : ?>
                        <div class="detail-comments">
                            <span><strong>Отзывы</strong> (<?=$model->getComments()->count()?>)</span>
        <?=ListView::widget([
            'dataProvider' => $comments,
            'itemView' => function ($model, $key, $index, $widget){
                return Html::tag('div',
                           Html::tag('strong', Html::encode($model->user->name)).
                           ' '.
                           Html::tag('span',
                               '('.Yii::$app->formatter->asDate($model->created_at, 'php:d.m.Y').')'.
                               \frontend\widgets\ServiceRating::widget(['value' => $model->rate])
                          )
                          ,[
                              'class' => 'detail-comment-header',
                      ]).
                      Html::tag('div', Html::encode($model->text), ['class' => 'detail-comment-text']);
            },
            'itemOptions' => [
                'class' => 'detail-comment',
            ],
            'summary' => false,
            'pager' => [
                'class' => ScrollPager::className(),
                'triggerText' => 'Показать больше',
                'triggerTemplate' => '<a class="result-show-more ias-trigger">{text}</a>',
                'enabledExtensions' => [
                    ScrollPager::EXTENSION_TRIGGER,
                    ScrollPager::EXTENSION_SPINNER,
                    ScrollPager::EXTENSION_NONE_LEFT,
                    ScrollPager::EXTENSION_PAGING,
                ],                
            ],
        ])?>
                        </div>
<?php endif; ?>                            
<?php if (!Yii::$app->user->isGuest) : ?>
                        <div class="detail-send">
                        <?php
                            $form = ActiveForm::begin([
                                'enableClientValidation' => false,
                            ]);
                        ?>
                                <strong>Оставить отзыв</strong>
                                <?=$form->field($comment, 'text')->textarea([
                                    'style' => 'width: 100%; height: 62px;',
                                ])->label(false)?>
                                <div class="detail-flex">
                                    <div class="detail-send-left">
                                    <?=$form->field($comment, 'rate')->radioList([
                                        1 => '1',
                                        2 => '2',
                                        3 => '3',
                                        4 => '4',
                                        5 => '5'
                                    ],[
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                            return '
                                            <input '.($checked ? 'checked="checked"' : '' ).'value="'.$value.'" name="ServiceComment[rate]" type="radio" id="radio-rate-'.$index.'" />
                                            <label for="radio-rate-'.$index.'">'.$value.'</label>';

                                        }
                                    ])->label(false);?>
                                    </div>
                                    <div class="detail-send-right">
                                        <?=Html::submitButton('Отправить')?>
                                    </div>
                                </div>
                        <?php
                            $form->end();
                        ?>
                        </div>
<?php endif; // comment form ?>
					</div>
                </div>

<?=\frontend\widgets\NearestServices::widget(['model' => $model])?>
<?=\frontend\widgets\AdvertiseContent::widget()?>
<?=\frontend\widgets\SimilarServices::widget(['model' => $model])?>

<?php

$this->registerJs("

    // На карте
    $('.menuitem-onmap').click(function(){
        $.popup('".Url::to(['/service/map', 'id' => 0])."'.replace('0', $(this).attr('rel')));
    });

    // В избранное
    $('.menuitem-favorite').click(function(){
        $.get('".Url::to(['/service/favorite', 'id' => $model->id])."');
        if ($(this).hasClass('favorite'))
        {
            $(this).removeClass('favorite');
            $(this).find('span').html('В избранное');
        }
        else
        {            
            $(this).addClass('favorite');
            $(this).find('span').html('В избранном');
        }
    });
    
    // Контакты, доп. услуги
    $('.menuitem-contacts').click(function(){
        $('.detail-submenu-box.contacts').toggle();
        $('.detail-submenu-box.services').hide();
        $.get('/statistics/service-action?service_id=".$model->id."&type_id=8');
//        $.popup('".Url::to(['/company/contacts', 'id' => 0])."'.replace('0', $(this).attr('rel')));
    });

    $('.menuitem-addit').click(function(){
        $('.detail-submenu-box.services').toggle();
        $('.detail-submenu-box.contacts').hide();
        $.get('/statistics/service-action?service_id=".$model->id."&type_id=9');
    });

    // Слайдер
    $('.detail-image .slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    }).on('afterChange',function(event, slick, currentSlide){
        var buttons = $('.detail-infobar .slider-btn');
        buttons.removeClass('slider-btn-active');
        buttons.eq(currentSlide).addClass('slider-btn-active');
    });

    // Кнопки под слайдером
    $('.detail-infobar .slider-btn').click(function(e){
        var buttons = $(this).parent().find('.slider-btn');

        buttons.removeClass('slider-btn-active');
        $(this).addClass('slider-btn-active');
        var index = buttons.index($(this));
        
        $('.detail-image .slider').slick('slickGoTo', index);

    });

    // Скрытие полного описания
    if ($('.detail-text').height() > 135)
    {
        var height = $('.detail-text').outerHeight();
        
        $('.result-show-more').removeClass('hidden').click(function(e){
            e.preventDefault();
            $('.detail-text').animate({
                height: height,
            }, 1000);
            
            $(this).addClass('hidden');

            return false;
        });
        $('.detail-text').css({
            height: '135px',
            overflow: 'hidden'
        });
    }

    $(document).on('click', function(e){

        if ( 
            $(e.target).hasClass('detail-submenu-box')
         || $(e.target).hasClass('menuitem-addit')
         || $(e.target).hasClass('menuitem-contacts')
         || $(e.target).parents('.detail-submenu-box').length
         || $(e.target).parents('.menuitem-addit').length
         || $(e.target).parents('.menuitem-contacts').length
        ) return;
        $('.detail-submenu-box.contacts').hide();
        $('.detail-submenu-box.services').hide();
    });

");
