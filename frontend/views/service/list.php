<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

use kop\y2sp\ScrollPager;

$this->title = 'Результаты поиска';
$this->params['breadcrumbs'] = [
    $this->title
];

$this->beginBlock('sidebar');
    echo \frontend\widgets\AdvertiseSidebar::widget();
    echo \frontend\widgets\SiteFeedback::widget(['model' => new \frontend\forms\SiteFeedback]);
    echo \frontend\widgets\RecentArticles::widget(['service_types' => $activeTab->id ? $activeTab->id : null]);
$this->endBlock();

?>
<?=\frontend\widgets\InterestingServices::widget()?>
<div class="main-block service_list">
    <h2>Результаты поиска</h2>
    <div class="result-box">
        <div class="control-tabs">
            <div class="tabs-container">
            <?php foreach($tabs as $tab) : ?>
            <?php
                $params = $searchParams;
                $params['activeTab'] = $tab->id;
            ?>
                <div rel="<?=base64_encode(json_encode($params))?>" class="the-tab <?=($tab->id == $activeTab->id ? 'tab-active' : '')?>">
                    <span><?=$tab->name?></span>
                </div>
            <?php endforeach; ?>
                <div class="the-tab">
                    <span class="empty-tab">&nbsp;</span>
                </div>
            </div>
            <div class="scroll-box">
                <div class="scroll-arrows">
                    <div class="scroll-left scroll-disabled"><i class="fa fa-angle-left"></i></div>
                    <div class="scroll-right"><i class="fa fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="result-sort">
            <span>Сортировать по:</span>
            <div>Цена <?=Html::a('<i class="sort-down"> </i>', $descSort->createUrl('price'))?><?=Html::a('<i class="sort-up"> </i>', $ascSort->createUrl('price'))?></div>
            <div>Рейтинг <?=Html::a('<i class="sort-down"> </i>', $descSort->createUrl('rate'))?><?=Html::a('<i class="sort-up"> </i>', $ascSort->createUrl('rate'))?></div>
            <div>Расстояние <?=Html::a('<i class="sort-down"> </i>', $descSort->createUrl('distance'))?><?=Html::a('<i class="sort-up"> </i>', $ascSort->createUrl('distance'))?></div>
        </div>
        <div class="result-items">
        <?=ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'list_item',
            'itemOptions' => [
                'class' => 'item',
            ],
            'summary' => false,
            'pager' => [
                'class' => ScrollPager::className(),
                'triggerText' => 'Показать больше',
                'triggerTemplate' => '<a class="result-show-more ias-trigger">{text}</a>',
                'enabledExtensions' => [
                    ScrollPager::EXTENSION_TRIGGER,
                    ScrollPager::EXTENSION_SPINNER,
                    ScrollPager::EXTENSION_NONE_LEFT,
                    ScrollPager::EXTENSION_PAGING,
                    //ScrollPager::EXTENSION_HISTORY
                ],                
            ],
        ])?>
        </div>
    </div>
</div>

<?=\frontend\widgets\AdvertiseContent::widget()?>
<?=\frontend\widgets\SimilarServices::widget(['dataProvider' => $dataProvider, 'type_id' => $activeTab->id])?>

<?php

$this->registerJs("
        


    $('.result-box .the-tab').click(function(){
        window.location.href = '/service/advanced-search?q=' + $(this).attr('rel');
    });
            $('.result-box .scroll-box .scroll-left').click(function(){
                $(this).addClass('scroll-disabled');
                $(this).parent().find('.scroll-right').removeClass('scroll-disabled');
                $(this).parents('.control-tabs').find('.tabs-container').scrollLeft(0);
            });

            $('.result-box .scroll-box .scroll-right').click(function(){
                $(this).addClass('scroll-disabled');
                $(this).parent().find('.scroll-left').removeClass('scroll-disabled');
                $(this).parents('.control-tabs').find('.tabs-container').scrollLeft(10000);
           });
");
