<?php

use yii\helpers\Html;

use frontend\helpers\ServiceHelper;

frontend\assets\YandexMapAsset::register($this);

?>
<p><b>ID услуги:</b> <?=Html::encode($model->code)?></p>
<p><b>Наименование:</b> <?=Html::encode($model->name)?></p>
<p><b>Цена:</b> <?=ServiceHelper::getPrice($model)?></p>
<p><b>Вид услуги:</b> <?=Html::encode($model->type->name)?></p>
<?php

$tags = [];
$attribute = null;
foreach($model->attributeValues as $value)
{
    if ($attribute != $value->service_type_attribute_id)
    {
        $tags[$value->serviceTypeAttribute->name] = [];
        $attribute = $value->service_type_attribute_id;
    }

    $tags[$value->serviceTypeAttribute->name][] = $value->name;
}

foreach($tags as $attribute => $values)
{
    echo '<p><b>'.$attribute.':</b> '.implode(', ', $values).'</p>';
}

?>
<p><b>Описание:</b></p>
<div>
    <?=$model->description?>
</div>
<img src="https://static-maps.yandex.ru/1.x/?ll=<?=$model->company->lon?>,<?=$model->company->lat?>&size=600,400&z=14&l=map&pt=<?=$model->company->lon?>,<?=$model->company->lat?>">

<?php 

$this->registerJs("
    
    window.print();

");
