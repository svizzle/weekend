<?php

use yii\web\View;

    $this->registerJsFile('//api-maps.yandex.ru/2.1/?lang=ru_RU');
    $this->registerJs("

        ymaps.ready(function() {

                var lat = {$model->company->lat};
                var lon = {$model->company->lon};

                var map = new ymaps.Map('yandex-map', {
                    center: [lat, lon],
                    zoom: 14
                }, {
                });

                var marker = new ymaps.GeoObject({
                    geometry: {
                        type: 'Point',
                        coordinates: [lat, lon]
                    },

                },{
                    draggable: false
                });
                map.geoObjects.add(marker);

        });
    ");
?>
<div class="service_map">
    <div id="yandex-map" class="map">
    </div>
</div>
