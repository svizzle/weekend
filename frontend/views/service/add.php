<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

//\yii\bootstrap\BootstrapAsset::register($this);

$this->title = 'Добавить услугу';

$this->params['breadcrumbs'] = [
    $this->title
];
?>
<div class="main-block">
    <h2><?=$this->title?></h2>

<?=$page->text?>

<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);

echo $form->field($model, 'name')->textInput(['placeholder' => 'Ваше Имя'])->label(false);

echo $form->field($model, 'email')->textInput(['placeholder' => 'Ваш E-mail'])->label(false);

echo $form->field($model, 'phone')->textInput(['placeholder' => 'Ваш телефон для связи'])->label(false);

echo $form->field($model, 'text')->textarea(['rows' => 10, 'placeholder' => 'Опишите услугу которую вы хотели бы добавить на сайт'])->label(false);

echo '<div class="form-group text-center">';
echo Html::submitButton('Отправить', ['class' => 'button']);
echo '</div>';

$form->end();
?>
</div>
