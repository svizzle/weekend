<?php

namespace frontend\helpers;

use common\models\Service;

class ServiceHelper
{
    public static function getPrice(Service $model)
    {
        return 'от <strong>'.$model->price->price.'</strong> руб./'.$model->price->unit->name.'.';

        /*
        <div class="result-item-through">
            от <strong>1600</strong> руб./сут.<br>
        </div>
        */
    }
}
