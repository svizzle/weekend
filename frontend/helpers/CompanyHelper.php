<?php

namespace frontend\helpers;

use common\models\Company;

class CompanyHelper
{
    public static function getPhone(Company $model)
    {
        if (!count($model->phones)) return 'Нет';

        return $model->phones[0]->phone;
    }

    public static function getAddress(Company $model)
    {
        return $model->geoLocation->region->name.", ".$model->geoLocation->name;
    }

    public static function getFullAddress(Company $model)
    {
        return static::getAddress($model).", ".$model->address;
    }
}
