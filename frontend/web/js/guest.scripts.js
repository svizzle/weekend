/*
 * base64 v1.0 - jQuery base64 encode\decode plugin
 *
 * Copyright (c) 2012 Dmitry V. Alexeev
 *
 * Licensed under the GPL license:
 *   http://www.gnu.org/licenses/gpl.html
 *
 * URL:
 *   http://alexeevdv.ru/projects/jquery/base64/
 *
 * Author URL:
 *   http://alexeevdv.ru/
 *
 */
(function($) {
    $.extend({
        base64: {
            codeTable : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
            encode: function(text) {
                text = this.utf8encode(text);
                var encodedText = '';
                for (var i = 0; i < text.length; i+=3) {
                    
                    var byte1 = (text[i] !== undefined ? text.charCodeAt(i) : 0);
                    var byte2 = (text[i+1] !== undefined ? text.charCodeAt(i+1) : 0);
                    var byte3 = (text[i+2] !== undefined ? text.charCodeAt(i+2) : 0);
                    
                    var newByte1 = byte1 >> 2;
                    encodedText += this.codeTable[newByte1];
                    
                    var newByte2 = ((byte1 & 3) << 4) | ((byte2 & 240) >> 4);
                    encodedText += this.codeTable[newByte2];
                    
                    var newByte3 = ((byte2 & 15) << 2) | ((byte3 & 192) >> 6);
                    encodedText += (byte2 ? this.codeTable[newByte3] : '=' );
                    
                    var newByte4 = byte3 & 63;
                    encodedText += (byte3 ? this.codeTable[newByte4] : '=' )
                }
                return encodedText;
            },
            
            decode: function(text) {
                var decodedText = '';
                for (var i = 0; i < text.length; i+=4) {
                    var byte1 = this.codeTable.indexOf(text[i]);
                    var byte2 = this.codeTable.indexOf(text[i+1]);
                    var byte3 = (text[i+2] !== "=" ? this.codeTable.indexOf(text[i+2]) : 0);
                    var byte4 = (text[i+3] !== "=" ? this.codeTable.indexOf(text[i+3]) : 0);
                
                    var newByte1 = (byte1 << 2) | ((byte2 & 48) >> 4);
                    decodedText += (newByte1 ? String.fromCharCode(newByte1) : '' );
                    var newByte2 = ((byte2 & 15) << 4) | ((byte3 & 60) >> 2);
                    decodedText += (newByte2 ? String.fromCharCode(newByte2) : '' );
                    var newByte3 = ((byte3 & 3) << 6) | (byte4 & 63);
                    decodedText += (newByte3 ? String.fromCharCode(newByte3) : '' );

                }
                decodedText = this.utf8decode(decodedText);
                return decodedText;
            },
            
            utf8encode: function(text) {
                var utf8text = "";

                for (var n = 0; n < text.length; n++) {

                    var c = text.charCodeAt(n);

                    if (c < 128) {
                        utf8text += String.fromCharCode(c);
                    }
                    else if((c > 127) && (c < 2048)) {
                        utf8text += String.fromCharCode((c >> 6) | 192);
                        utf8text += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        utf8text += String.fromCharCode((c >> 12) | 224);
                        utf8text += String.fromCharCode(((c >> 6) & 63) | 128);
                        utf8text += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utf8text;
            },
        
            utf8decode: function(utf8text){
		var text = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utf8text.length ) {
 
			c = utf8text.charCodeAt(i);
 
			if (c < 128) {
				text += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utf8text.charCodeAt(i+1);
				text += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utf8text.charCodeAt(i+1);
				c3 = utf8text.charCodeAt(i+2);
				text += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
		return text;
            }
        }
    });
})(jQuery);

$(function(){

    $(document).click(function(){
        $(".menu-auth-box").hide()
    });

    $('.menu-auth-box').click(function(e){
        e.stopPropagation();
    });

    $(document).on('click', ".menu-register-open", function(e){
        e.stopPropagation();
		if ($(".menu-register-container").is(":visible"))
        {
			$(".menu-auth-box").hide();
			$(".menu-register-container").hide();
			$(".menu-login-container").hide();
		}
        else
        {
			$(".menu-auth-box").show();
			$(".menu-register-container").show();
			$(".menu-login-container").hide();
		}
	});

    $(document).on('click', ".menu-login-open", function(e){
        e.stopPropagation();
        if ($(".menu-login-container").is(":visible"))
        {
            $(".menu-auth-box").hide();
            $(".menu-register-container").hide();
            $(".menu-login-container").hide();
        }
        else
        {
            $(".menu-auth-box").show();
            $(".menu-login-container").show();
            $(".menu-register-container").hide();
        }
    });

});

(function($){

    $.extend({

        popup: function(url){

            var div = $(            
                '<div class="popup">' +
                    '<div class="popup-overlay"></div>' + 
                    '<div class="popup-content">' +
                        '<a href="#" class="popup-close"></a>' +
                        '<div class="content">Загрузка...</div>' +
                    '</div>' +
                '</div>'
            );

            $('body').append(div);

            div.find('.popup-close').click(function(){
                div.fadeOut(function(){
                    div.remove();
                });
            });

            div.find('.popup-overlay').click(function(){
                div.find('.popup-close').click();
            });

            $.get(url, function(html){
            
                div.find('.content').html(html);

                div.find(".popup-content").css({
                    "margin-left": "-" + Math.floor(div.find('.popup-content').outerWidth()/2) + "px",
                    "margin-top": "-" + Math.floor(div.find('.popup-content').outerHeight()/2) + "px",
                });
            
            });
            

        }

    });

    $.fn.popup = function() {
        
        var self = this;

        this.click(function(e){
            e.preventDefault();

            var url = $(this).attr('data-popup');
            
            $.ajax({
                type: "GET",
                url: url,
                xhrFields: {withCredentials: true}, 
                success: function(response) {

                    $(".popup").remove();

                    var div = $(
                    '<div class="popup">' + 
                        '<div class="popup-overlay"></div>' +
                        '<div class="popup-content">' +
                            '<a href="#" class="popup-close"></a>' +
                            response +
                        '</div>' +
                    '</div>'
                    );
    
                    $('body').append(div);
                    div.find(".popup-content").css({
                        "margin-left": "-" + Math.floor(div.find('.popup-content').outerWidth()/2) + "px",
                        "margin-top": "-" + Math.floor(div.find('.popup-content').outerHeight()/2) + "px",
                    });

                    div.find('.popup-close').click(function(){
                        div.fadeOut(function(){
                            div.remove();
                        });
                    });
                    div.find('.popup-overlay').click(function(){
                        div.find('.popup-close').click();
                    });

                },
                crossDomain: true,
            });

            return false;
        });
    };

})(jQuery);

(function($){

    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
        },
  });

    $.fn.advancedSearch = function(options) {
    
        this.options = options;

        var self = this;

        var $regions   = self.find('.regions');
        var $templates = self.find('.templates');
        var $advanced  = $(".advanced-search");


        // Инициализация автокомплита регионов
        $regions.find('.select-select').autocomplete({
            minLength: 0,
            source: self.options.regions.map(function(region){return region.name})
        });

        $regions.find('.select-select').on('autocompleteopen', function(e, ui){
            $regions.find('.select-addit-box').hide();
            $(this).data('is_open', true);  
        });

        $regions.find('.select-select').on('autocompleteclose', function(event, ui) {
            $(this).data('is_open', false);
        });

        $regions.find('.select-select').on('autocompletesearch', function(e, ui){
            var value = $(this).val();
            var found = false;
            
            $.each(options.regions, function(index, region){
                if (region.name == value) found = true;
            });
            
            found == false ? $regions.find('.select-addit').hide() : $regions.find('.select-addit').show();
        });

        $regions.find('.select-select').on('autocompleteselect', function(e, ui){
            $regions.find('.select-addit-box').empty();
            $regions.find('.select-addit').text('ВСЕ').show();
            $.each(self.options.regions, function(index, region){
                if (region.name == ui.item.value)
                {
                    $regions.find('input[name=region_id]').attr('value', region.id);
                }
            });
            AdvancedSearch.recalculateTemplateTabs();
            AdvancedSearch.recalculateTabs();
            AdvancedSearch.enableTemplatesIfRegionSet();
        });

        $regions.find('.select-select').focus(function(){
            var is_open = $regions.find('.select-select').data('is_open');
            if (!is_open) {
                $regions.find('.select-select').autocomplete('search', '');
            } else {
                $regions.find('.select-select').autocomplete('close');
            }
        });

        $regions.find('.select-showall').click(function(){
            $regions.find('.select-select').focus();
        });

        $regions.find('.select-addit').click(function(){
            var selbox = $regions.find('.select-addit-box');
            var selsel = $regions.find('.select-select');
            
            if (!selbox.is(':hidden'))
            {
                selbox.hide();
            }
            else
            {
                if (selbox.is(':empty'))
                {
                    var value = selsel.val();
                    
                    var li = $('<li style="border-bottom: 1px solid #ddd;padding-bottom: 10px; cursor: default;" class="select-addit-item"><span class="uncheck-all"></span> Снять выделение <span class="check-all"></span> Выделить все</li>');
                    $regions.find('.select-addit-box').append(li);
                    
                    $.each(self.options.regions, function(index, region) {
                        if (region.name == value)
                        {
                            for (var i in region.locations)
                            {
                                var location = region.locations[i];                                
                                
                                var input = $('<input name="location_id[]" type="checkbox"/>');
                                input.val(location.id).prop('checked', true).prop('id', 'location-' + location.id);

                                var label = $('<label/>');
                                label.prop('for', 'location-' + location.id).html(location.name);

                                var li = $('<li class=\"select-addit-item\"></li>');
                                li.append(input).append(label);

                                selbox.append(li);
                            }
                        }
                    });
                }
                selbox.show();
                selsel.autocomplete('close');
            }
        });

        $regions.on('change', 'input[type=checkbox]', function(){
            var num = $regions.find('input[type=checkbox]:checked').length;
            if (num == $regions.find('input[type=checkbox]').length || num == 0)
            {
                $regions.find('.select-addit').text('ВСЕ');
            }
            else
            {
                $regions.find('.select-addit').text(num);
            }
            AdvancedSearch.recalculateTabs();
            AdvancedSearch.enableTemplatesIfRegionSet();
        });

        $regions.bind('change', function() {
            if ($('.regions').find('.select-select').val() == '') {
                $(".select-container.templates .select-select").prop('disabled', true);
                $('.select-container.templates .select-select').autocomplete('disable');
                $.fn.advancedSearch.resetTabs();
                $('.templates .select-addit').hide();
            }
        });

        // Инициализация автокомплита видов отдыха
        $templates.find('.select-select').autocomplete({
            minLength: 0,
            source: self.options.templates.map(function(template){
                function getCategoryName()
                {   //alert(template.tabs.length);
                    if (template.custom)
                    return 'Пользовательские';
                    if (template.tabs.length > 1)
                        return 'Предустановленные';
                    else
                        return 'Вид услуги';
                }
                return {

                    label: template.name,
                    category: getCategoryName()
                };
            })
        }).autocomplete('instance')._renderMenu = function( ul, items ) {
            $(ul).css({'padding-left': '5px'});
            var that = this,
            currentCategory = "";
            $.each( items, function( index, item ) {
                var li;
                if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category' style='font-weight: bold;'>" + item.category + "</li>" );
                    currentCategory = item.category;
                }
                li = that._renderItemData( ul, item );
                if ( item.category ) {
                    li.attr( "aria-label", item.category + " : " + item.label );
                }
            });
        };
        $templates.find('.select-select').autocomplete('instance').widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );

        $templates.find('.select-select').on('autocompleteopen', function(e, ui){
            $templates.find('.select-addit-box').hide();
            $(this).data('is_open', true);  
        });

        $templates.find('.select-select').on('autocompleteclose', function(event, ui) {
            $(this).data('is_open', false);
        });

        $templates.find('.select-select').on('autocompletesearch', function(e, ui){
            var value = $(this).val();
            var found = false;

            $.each(options.templates, function(index, template){
                if (template.name == value) found = true;
            });
            console.log(found);
            found == false ? $templates.find('.select-addit').hide() : $templates.find('.select-addit').show();
        });

        $templates.find('.select-select').on('autocompleteselect', function(e, ui){
            $.each(self.options.templates, function(index, template){
                if (template.name == ui.item.value)
                {
                    $templates.find('input[name=template_id]').attr('value', template.id);
                    $templates.find('.select-addit').show()
                    $templates.find('.select-addit-box input[type=checkbox]').each(function(index, checkbox){                        

                        var found = false;

                        $.each(template.tabs, function(index, tab) {
                            if ($(checkbox).val() == tab.id) found = true;
                        });

                        if (found)
                        {
                            if (!$(checkbox).is(':checked'))
                            {
                                $(checkbox).prop('checked', true).trigger('change');
                            }
                        }
                        else
                        {
                            if ($(checkbox).is(':checked'))
                            {
                                $(checkbox).prop('checked', false).trigger('change');
                            }
                        }
                    });
                    
                    $('.advanced-input-group input[type=checkbox]').prop('checked', false);
                    $.each(template.values, function(index, value) {
                        $('.advanced-input-group input[type=checkbox][value='+ value +']').prop('checked', true);
                    });
                    AdvancedSearch.recalculateTabs();
                }
            });
        });

        $templates.find('.select-select').focus(function(){
            var is_open = $templates.find('.select-select').data('is_open');
            if (!is_open) {
                $templates.find('.select-select').autocomplete('search', '');
            } else {
                $templates.find('.select-select').autocomplete('close');
            }
        });

        $templates.find('.select-showall').click(function(){
            $templates.find('.select-select').focus();
        });

        $templates.find('.select-addit').click(function(){
            var selbox = $templates.find('.select-addit-box');
            var selsel = $templates.find('.select-select');
            
            if (!selbox.is(':hidden'))
            {
                selbox.hide();
            }
            else
            {
                selbox.show();
            }
            $templates.find('.select-select').autocomplete('close');
        });

        $templates.find('.select-addit-box').on('change', 'input[type=checkbox]', function(){
            var tab_id = $(this).val();

            var count = $templates.find('.select-addit-box input[type=checkbox]:checked').length;

            if (count == $templates.find('.select-addit-box input[type=checkbox]').length)
            {
                $templates.find('.select-addit').text('ВСЕ');
            }
            else
            {
                $templates.find('.select-addit').text(count);
            }

            if ($(this).is(':checked'))
            {
                // add tab
                for(var i in self.options.tabs)
                {
                    var tab = self.options.tabs[i];
                    if (tab.id == $(this).val())
                    {
                        AdvancedSearch.appendTab(tab);
                    }
                }
                //$.get('/search/get-service-type?id=' + $(this).val(), function(type){
                //    AdvancedSearch.appendTab(type);
                //}, 'json');
            }
            else
            {
                // remove tab
                $('[rel=tab-' + tab_id + ']').remove();
            }

            var tab = $advanced.find('.advanced-tab:first');
            var tabId = $(tab).attr('rel');
            $advanced.find('.advanced-tab').removeClass('active-advanced-tab');
            $(tab).addClass('active-advanced-tab');
        
            $advanced.find(".wnd-tab").removeClass('active');
            $advanced.find(".wnd-tab[rel="+ tabId+"]").addClass('active');
        });
        
        var li = $('<li style="border-bottom: 1px solid #ddd;padding-bottom: 10px;cursor: default;" class="select-addit-item"><span class="uncheck-all"></span> Снять выделение <span class="check-all"></span> Выделить все</li>');
        $templates.find('.select-addit-box').append(li);

        for(var i in self.options.tabs)
        {
            var tab = self.options.tabs[i];
            
            var input = $('<input name="tab_id[]" type="checkbox"/>');
            input.val(tab.id);
//            input.prop('checked', true);
            input.prop('id', 'tab-' + tab.id);

            var label = $('<label/>');
            label.prop('for', 'tab-' + tab.id).html(tab.name + ' (' + tab.count  + ')');

            var li = $('<li class=\"select-addit-item\"></li>');
            li.append(input).append(label);

            $templates.find('.select-addit-box').append(li);

            if (self.options.searchParams == false)
            {
                input.trigger('change');
            }
        }

        AdvancedSearch.hideOrShowSaveTmpBtn();

        // Инициализация расширенной формы
        self.find('.show-advanced').click(function(){
            $('.modal-group-1').show();
            AdvancedSearch.hideOrShowSaveTmpBtn();
        });
    
        $advanced.find('.advanced-tab-box').on('click', '.advanced-close', function(){
            $templates.find("[value='"+$(this).attr('rel')+"']").prop('checked', false).trigger('change');

            var tabsCount = $templates.find('.select-addit-box input[type=checkbox]:checked').length;

            if (tabsCount == 0){
                $templates.find('.select-addit').hide();
            }

            AdvancedSearch.hideOrShowSaveTmpBtn();
        });

        $advanced.on('click', '.advanced-tab', function(){
            var tabId = $(this).attr('rel');
            $advanced.find('.advanced-tab').removeClass('active-advanced-tab');
            $(this).addClass('active-advanced-tab');
        
            $advanced.find(".wnd-tab").removeClass('active');
            $advanced.find(".wnd-tab[rel="+ tabId+"]").addClass('active');
        });

        $advanced.find('.advanced-footer .add-tab').click(function(){

            var openedTabs = $advanced.find('.advanced-tab').toArray().map(function(tab){
                return $(tab).data('id');
            });
   
            var val = 0;

            $('.new-tab-dialog select option').each(function(index, option){

                var found = false;
                for (var i in openedTabs)
                {
                    if (parseInt($(option).attr('value')) == parseInt(openedTabs[i]))
                    {
                        found = true;                    
                    }            
                }

                if (found)
                {
                    $(option).hide();
                }
                else
                {
                    if (val == 0) val = $(option).attr('value');
                    $(option).show();
                }
            });

            if (openedTabs.length == $('.new-tab-dialog select option').toArray().length) return;

            $('.new-tab-dialog select').val(val);

            $('.new-tab-dialog').modal();
        });

        $('.new-tab-dialog .btn-primary').click(function(){
            $('.new-tab-dialog').modal('hide');

            var id = $('.new-tab-dialog select').val();

            var checkbox = $templates.find('input[type=checkbox][value='+id+']');
            if (!checkbox.is(':checked')) checkbox.prop('checked', true).trigger('change');
            $('.templates').find('.select-addit').show();
            AdvancedSearch.hideOrShowSaveTmpBtn();
        });

        $advanced.find('.advanced-wnd-box').on('change', 'input[type=checkbox]', function(){

            var checkbox = $(this);

            var parent = checkbox.parents('.wnd-tab');

            AdvancedSearch.recalculateTab(parent.data('id'));
            $templates.find('.select-select').val('Расширенный поиск');
        });
    
        $('#search-send').click(function(){

            var params = AdvancedSearch.getParams();
            var region_id = parseInt(params.region);
            if (!region_id)
            {
                customAlert('Выберите место отдыха и вид отдыха, потом нажмите кнопку "Найти"');
                return false;
            }

            window.location.href = '/service/advanced-search?q=' + $.base64.encode(JSON.stringify(params));
        });

        $advanced.find('.apply-template').click(function(){
            var params = AdvancedSearch.getParams();
            var region_id = parseInt(params.region);
            if (!region_id)
            {
                customAlert('Выберите место отдыха и вид отдыха, потом нажмите кнопку "Применить"');
                return false;
            }
            $('#search-send').click();
        });

        // Инициализируем контролы выбранными ранее значениями
        if (self.options.searchParams !== false) 
        {
            var searchParams = self.options.searchParams;
            if (parseInt(searchParams.region))
            {
                $regions.find('.select-select').val(searchParams.region_name).autocomplete('search').autocomplete('close');
                $regions.find('input[name=region_id]').val(searchParams.region);
//                AdvancedSearch.recalculateTemplateTabs();
            }

                    $.each(self.options.regions, function(index, region) {
                        if (region.name == searchParams.region_name)
                        {
                            var li = $('<li style="border-bottom: 1px solid #ddd;padding-bottom: 10px;cursor: default;" class="select-addit-item"><span class="uncheck-all"></span> Снять выделение <span class="check-all"></span> Выделить все</li>');
                            $regions.find('.select-addit-box').append(li);

                            for (var i in region.locations)
                            {
                                var location = region.locations[i];                                
                                
                                var input = $('<input name="location_id[]" type="checkbox"/>');
                                input.val(location.id).prop('checked', false).prop('id', 'location-' + location.id);
                                if (searchParams.locations.length == 0 || searchParams.locations.indexOf(location.id + '') >= 0)
                                {
                                    input.prop('checked', true);
                                }

                                var label = $('<label/>');
                                label.prop('for', 'location-' + location.id).html(location.name);

                                var li = $('<li class=\"select-addit-item\"></li>');
                                li.append(input).append(label);

                                $regions.find('.select-addit-box').append(li);
                            }

                            if (region.locations.length != searchParams.locations.length && searchParams.locations.length != 0)
                            {
                                $regions.find('.select-addit').text(searchParams.locations.length);
                            }
                        }
                    });

                    $templates.find('.select-addit-box input[type=checkbox]').prop('checked', false);
                    $.each(searchParams.tabs, function(index, tab){
                        $templates.find('.select-addit-box input[type=checkbox][value='+tab.id+']').prop('checked', true).trigger('change');
                    });

                    if ($templates.find('.select-addit-box input[type=checkbox]').length != searchParams.tabs.length)
                    {
                        $templates.find('.select-addit').text(searchParams.tabs.length);
                    }

            if (parseInt(searchParams.template))
            {
                $templates.find('.select-select').val(searchParams.template_name).autocomplete('search').autocomplete('close');
                $templates.find('input[name=template_id]').val(searchParams.template);
                $templates.find('.select-addit').show()
            }        

            $.each(searchParams.tabs, function(index, tab) {
                if (tab.attributes)
                {
                    $.each(tab.attributes, function(index, attribute) {
                        $.each(attribute.values, function(index, value) {
                            $('.advanced-input-group input[type=checkbox][value='+ value +']').prop('checked', true);
                        });
                    });
                }
            });
        }
        $advanced.find('.advanced-tab:first').click();

        $templates.find('.select-addit-box').on('change', 'input[type=checkbox]', function(){
            $templates.find('.select-select').val('Расширенный поиск');
        });

        var tabsCount = $templates.find('.select-addit-box input[type=checkbox]:checked').length;
        //alert($('input[name=template_id]').val());
        if (tabsCount > 0){
            $('.templates').find('.select-addit').show();
            if ($('input[name=template_id]').val() == ''){
                $templates.find('.select-select').val('Расширенный поиск');
            }
        }

        $(document).click(function(e){
            if (!$(e.target).parents('.select-container').length)
            {
                $regions.find('.select-addit-box').hide();
                $templates.find('.select-addit-box').hide();
            }
        });

        AdvancedSearch.enableTemplatesIfRegionSet();

        return this;
    };

}(jQuery));


var AdvancedSearch = function() {};

AdvancedSearch.__proto__.getParams = function(){

    var $regions   = $('.regions');
    var $templates = $('.templates');
    var $advanced  = $(".advanced-search");

            var activeTab = $advanced.find('.advanced-tab-box .advanced-tab.active-advanced-tab').data('id');
            var tabs = $templates.find('.select-addit-box input[type=checkbox]:checked').toArray().map(function(el){ return $(el).val()});

            var params = {

                region: $regions.find('input[name=region_id]').val(),
                locations: $regions.find('input[type=checkbox]:checked').toArray().map(function(checkbox){ return $(checkbox).val()}) || [],
                template: $templates.find('input[name=template_id]').val(),
                tabs: tabs.map(function(id){
                    return {
                        id: id,
                        attributes: $advanced.find('.wnd-tab[rel=tab-'+id+'] .advanced-wnd').toArray().map(function(attribute){
                            return {
                                id: $(attribute).data('id'),
                                values: $(attribute).find('input[type=checkbox]:checked').toArray().map(function(checkbox){
                                    return $(checkbox).val();
                                })
                            };
                        }).filter(function(attribute){ return attribute.values.length > 0;})
                    };
                }),
                activeTab: activeTab
            };

    return params;
};

AdvancedSearch.__proto__.recalculateTemplateTab = function(id) {
    var params = AdvancedSearch.getParams();

    params.template = '';
    params.tabs = [
    {
        id: id,
        attributes: []
    }];
    params.activeTab = id;

    $.post('/service/count-search', params, function(count){
        var label = $('.templates .select-addit-item input[value=' + id + ']').next();
        label.html(label.html().replace(/\((\d+\))/, '('+count+')'));
    });
};

AdvancedSearch.__proto__.recalculateTemplateTabs = function() {
    $('.templates .select-addit-item input').each(function(index, input){
        var id = $(input).val();

        AdvancedSearch.recalculateTemplateTab(id);
    });
};

AdvancedSearch.__proto__.recalculateTab = function(tabId) {
    var params = AdvancedSearch.getParams();
    params.activeTab = tabId;

    $.post('/service/count-search', params, function(count){
        var title = $('.advanced-tab[rel=tab-' + tabId + '] .advanced-title');
        title.html(title.html().replace(/\((\d+\))/, '('+count+')'));
    });
};

AdvancedSearch.__proto__.recalculateTabs = function() {
    $('.wnd-tab').each(function(index, tab){
        AdvancedSearch.recalculateTab($(tab).data('id'));
    });
};

AdvancedSearch.__proto__.resetTabs = function() {
    $('.templates').find("[value]").prop('checked', false).trigger('change');
};

AdvancedSearch.__proto__.enableTemplatesIfRegionSet = function() {
    if (($('.regions').find('input[name=region_id]').val() == '') ) {
        console.log('region is not set');
        $(".select-container.templates .select-select").prop('disabled', true);
        $('.select-container.templates .select-select').autocomplete('disable');
    }else{
        console.log('region is  set');
        $(".select-container.templates .select-select").prop('disabled', false);
        $('.select-container.templates .select-select').autocomplete('enable');
    }
};

AdvancedSearch.__proto__.hideOrShowSaveTmpBtn = function() {
    var tabsCount = $('.templates .select-addit-box input[type=checkbox]:checked').length;
    if  (tabsCount == 0){
        $('.save-template').hide();
    }else{
        $('.save-template').show();
    }
};

AdvancedSearch.__proto__.appendTab = function(type) {
        
        var div = $('<div class="advanced-tab" rel="tab-'+ type.id + '" data-id="'+type.id+'">' +
                        '<div class="advanced-title">' +
                            '<strong>' + type.name + '</strong> (0)' +
                        '</div>' +
                        '<div class="advanced-close" rel="'+ type.id +'"></div>' +
                    '</div>');

        $('.advanced-search .advanced-tab-box').append(div);


        var wndTab = $('<div class="wnd-tab" rel="tab-' + type.id + '" data-id="'+type.id+'"></div>');
        for(var i in type.attributes)
        {
            var attribute = type.attributes[i];

            var wnd = $('<div class="advanced-wnd" data-id="'+attribute.id+'">' +
                            '<div class="advanced-wnd-title">' + attribute.name + '</div>' +
                            '<div class="advanced-scrollbox">' +
                            '</div>' +
                        '</div>');

            for(var j in attribute.values)
            {
                var id = Math.random() + i + j;
                var value = attribute.values[j];
                var check = $('<div class="advanced-input-group">' +
                                    '<input name="v[]" value="' + value.id+ '" type="checkbox" id="advanced-checkbox-' + id + '" data-id="'+value.id+'">' + 
                                    '<label for="advanced-checkbox-' + id + '">' + value.name + '</label>' +
                              '</div>');

                wnd.find('.advanced-scrollbox').append(check);
            }


            wndTab.append(wnd);

        }
        $(".advanced-search .advanced-wnd-box").append(wndTab);

        this.recalculateTab(type.id);

        if ($(".advanced-tab").length == 1)
        {
            $(".advanced-tab").click();
        }

}

$(function(){


    $('.advanced-search .save-template').click(function(){
        $(".search-save-dialog").modal();

        $(".search-save-dialog .btn-primary").click(function(){
            var params = AdvancedSearch.getParams();
            params.name = $('#servicetypegroup-name').val();

            if (!params.name.length)
            {
                customAlert('Введите имя шаблона!', 'error');
                return;
            }

            var source = $('.select-container.templates .select-select').autocomplete('option', 'source');
            source.push({category: 'Пользовательские', 'label': params.name});
            source.sort(function(a, b){
                if (a.category < b.category) return -1;
                if (a.category > b.category) return 1;
                return 0;
            });
            var source = $('.select-container.templates .select-select').autocomplete('option', 'source', source);

            $.post('/search/save', params);
            $(".search-save-dialog").modal('hide');
        });

    });

    $('.advanced-wnd-box').on('change', 'input[type=checkbox]', function(){

        var ids = $('.advanced-wnd-box input[type=checkbox]:checked').toArray().map(function(input){
            return $(input).val();
        });

        $("#search-form .hidden-attribute-value").remove();
        for (var i in ids)
        {
            var id = ids[i];
            $("#search-form").append($('<input type="hidden" name="v[]" value="'+id+'">'));
        }

    });

    $('.select-addit-box').on('click', '.check-all', function(){
        var inputs = [];
        $(this).parents('.select-addit-box').find('input').each(function(index, input){
            if (!$(input).is(':checked'))
            {
                $(input).prop('checked', true);
                inputs.push(input);
            }
        });

        if ($(this).parents('.regions').length)
        {
            $(this).parents('.select-addit-box').find('input:first').trigger('change');
        }
        else
        {
            $.each(inputs, function(index, input) {
                $(input).trigger('change');
            });
        }

        $('.select-addit-box').hide();
    });

    $('.select-addit-box').on('click', '.uncheck-all', function(){
        var inputs = [];
        $(this).parents('.select-addit-box').find('input').each(function(index, input){
            if ($(input).is(':checked'))
            {
                $(input).prop('checked', false);
                inputs.push(input);
            }
        });
        if ($(this).parents('.regions').length)
        {
            $(this).parents('.select-addit-box').find('input:first').trigger('change');
        }
        else
        {
            $.each(inputs, function(index, input) {
                $(input).trigger('change');
            });
        }
    });

});


Share = {
    /**
     * Показать пользователю дилог шаринга в сооветствии с опциями
     * Метод для использования в inline-js в ссылках
     * При блокировке всплывающего окна подставит нужный адрес и ползволит браузеру перейти по нему
     *
     * @example <a href="" onclick="return share.go(this)">like+</a>
     *
     * @param Object _element - элемент DOM, для которого
     * @param Object _options - опции, все необязательны
     */
    go: function(_element, _options) {
        var
            self = Share,
            options = $.extend(
                {
                    type:       'vk',    // тип соцсети
                    url:        location.href,  // какую ссылку шарим
                    count_url:  location.href,  // для какой ссылки крутим счётчик
                    title:      document.title, // заголовок шаринга
                    image:        '',             // картинка шаринга
                    text:       '',             // текст шаринга
                },
                $(_element).data(), // Если параметры заданы в data, то читаем их
                _options            // Параметры из вызова метода имеют наивысший приоритет
            );

        if (self.popup(link = self[options.type](options)) === null) {
            // Если не удалось открыть попап
            if ( $(_element).is('a') ) {
                // Если это <a>, то подставляем адрес и просим браузер продолжить переход по ссылке
                $(_element).prop('href', link);
                return true;
            }
            else {
                // Если это не <a>, то пытаемся перейти по адресу
                location.href = link;
                return false;
            }
        }
        else {
            // Попап успешно открыт, просим браузер не продолжать обработку
            return false;
        }
    },

    // ВКонтакте
    vk: function(_options) {
        var options = $.extend({
                url:    location.href,
                title:  document.title,
                image:  '',
                text:   '',
            }, _options);

        return 'http://vkontakte.ru/share.php?'
            + 'url='          + encodeURIComponent(options.url)
            + '&title='       + encodeURIComponent(options.title)
            + '&description=' + encodeURIComponent(options.text)
            + '&image='       + encodeURIComponent(options.image)
            + '&noparse=true';
    },

    // Одноклассники
    ok: function(_options) {
        var options = $.extend({
                url:    location.href,
                text:   '',
            }, _options);

        return 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1'
            + '&st.comments=' + encodeURIComponent(options.text)
            + '&st._surl='    + encodeURIComponent(options.url);
    },

    // Facebook
    fb: function(_options) {
        var options = $.extend({
                url:    location.href,
                title:  document.title,
                image:  '',
                text:   '',
            }, _options);

        return 'http://www.facebook.com/sharer.php?s=100'
            + '&p[title]='     + encodeURIComponent(options.title)
            + '&p[summary]='   + encodeURIComponent(options.text)
            + '&p[url]='       + encodeURIComponent(options.url)
            + '&p[images][0]=' + encodeURIComponent(options.image);
    },

    // Живой Журнал
    lj: function(_options) {
        var options = $.extend({
                url:    location.href,
                title:  document.title,
                text:   '',
            }, _options);

        return 'http://livejournal.com/update.bml?'
            + 'subject='        + encodeURIComponent(options.title)
            + '&event='         + encodeURIComponent(options.text + '<br/><a href="' + options.url + '">' + options.title + '</a>')
            + '&transform=1';
    },

    // Твиттер
    tw: function(_options) {
        var options = $.extend({
                url:        location.href,
                count_url:  location.href,
                title:      document.title,
            }, _options);

        return 'http://twitter.com/share?'
            + 'text='      + encodeURIComponent(options.title)
            + '&url='      + encodeURIComponent(options.url)
            + '&counturl=' + encodeURIComponent(options.count_url);
    },

    // Mail.Ru
    mr: function(_options) {
        var options = $.extend({
                url:    location.href,
                title:  document.title,
                image:  '',
                text:   '',
            }, _options);

        return 'http://connect.mail.ru/share?'
            + 'url='          + encodeURIComponent(options.url)
            + '&title='       + encodeURIComponent(options.title)
            + '&description=' + encodeURIComponent(options.text)
            + '&imageurl='    + encodeURIComponent(options.image);
    },
// Google+
	gp: function (_options) {
		var options = $.extend({
			url: location.href			
		}, _options);

		return 'https://plus.google.com/share?url='
			+ encodeURIComponent(options.url);
	},

    // Открыть окно шаринга
    popup: function(url) {
        return window.open(url,'','toolbar=0,status=0,scrollbars=1,width=626,height=436');
    }
}
$(document).on('click', '.social-share', function(){
    Share.go(this);
});


    
    // Левый глаз
    $(document).on('click', '.result-items .result-left-eye', function(e){
        e.preventDefault();
        e.stopPropagation();
        if (!$(this).parent().find('.description').is(':visible'))
        {
            $('.result-items .result-item-buttons .description').hide();
        }
        $(this).parent().find('.description').toggle();
        return false;
    });

    // На карте
    $(document).on('click', '.result-items .result-btn-map', function(e){
        e.preventDefault();
        $.popup('/service/map?id=0'.replace('0', $(this).attr('rel')));
        return false;
    });

    // В избранное
    $(document).on('click', '.result-items .result-btn-favorite', function(e){
        e.preventDefault();
        $.get('/service/favorite?id=' + $(this).data('id'));
        if ($(this).hasClass('favorite'))
        {
            $(this).removeClass('favorite');
            $(this).html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В избранное');
        }
        else
        {            
            $(this).addClass('favorite');
            $(this).html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В избранном');
        }
    });

    // Телефон
    $(document).on('click', '.result-items .result-btn-phone', function(e){
        e.preventDefault();
        $(this).text($(this).attr('rel'));
        $(this).removeClass('result-btn-phone').addClass('result-btn-phoned');
        $.get('/statistics/service-action?service_id=' + $(this).data('id') + '&type_id=6');
        return false;
    });

    // Показанные телефон
    $(document).on('click', '.result-items .result-btn-phoned', function(e){
        e.preventDefault();
        return false;
    });

    // Правый глаз 
    $(document).on('click', '.result-items .result-right-eye', function(e){
        e.preventDefault();
        e.stopPropagation();
        if (!$(this).parent().find('.contacts').is(':visible'))
        {
            $('.result-items .result-item-buttons .contacts').hide();
        }
        $(this).parent().find('.contacts').toggle();
//        $.popup('/company/contacts?id=0'.replace('0', $(this).data('company_id')));
        return false;
    });

    $(document).on('click', function(e){
        $el = $(e.target);

        if ($el.hasClass('.contacts') || $el.parents('.contacts').length) return;
        if ($el.hasClass('.description') || $el.parents('.description').length) return;

        $('.result-items .result-item-buttons .contacts').hide();
        $('.result-items .result-item-buttons .description').hide();

    });

!function(e){e(["jquery"],function(e){return function(){function t(e,t,n){return g({type:O.error,iconClass:m().iconClasses.error,message:e,optionsOverride:n,title:t})}function n(t,n){return t||(t=m()),v=e("#"+t.containerId),v.length?v:(n&&(v=u(t)),v)}function i(e,t,n){return g({type:O.info,iconClass:m().iconClasses.info,message:e,optionsOverride:n,title:t})}function o(e){w=e}function s(e,t,n){return g({type:O.success,iconClass:m().iconClasses.success,message:e,optionsOverride:n,title:t})}function a(e,t,n){return g({type:O.warning,iconClass:m().iconClasses.warning,message:e,optionsOverride:n,title:t})}function r(e,t){var i=m();v||n(i),l(e,i,t)||d(i)}function c(t){var i=m();return v||n(i),t&&0===e(":focus",t).length?void h(t):void(v.children().length&&v.remove())}function d(t){for(var n=v.children(),i=n.length-1;i>=0;i--)l(e(n[i]),t)}function l(t,n,i){var o=i&&i.force?i.force:!1;return t&&(o||0===e(":focus",t).length)?(t[n.hideMethod]({duration:n.hideDuration,easing:n.hideEasing,complete:function(){h(t)}}),!0):!1}function u(t){return v=e("<div/>").attr("id",t.containerId).addClass(t.positionClass).attr("aria-live","polite").attr("role","alert"),v.appendTo(e(t.target)),v}function p(){return{tapToDismiss:!0,toastClass:"toast",containerId:"toast-container",debug:!1,showMethod:"fadeIn",showDuration:300,showEasing:"swing",onShown:void 0,hideMethod:"fadeOut",hideDuration:1e3,hideEasing:"swing",onHidden:void 0,closeMethod:!1,closeDuration:!1,closeEasing:!1,extendedTimeOut:1e3,iconClasses:{error:"toast-error",info:"toast-info",success:"toast-success",warning:"toast-warning"},iconClass:"toast-info",positionClass:"toast-top-right",timeOut:5e3,titleClass:"toast-title",messageClass:"toast-message",escapeHtml:!1,target:"body",closeHtml:'<button type="button">&times;</button>',newestOnTop:!0,preventDuplicates:!1,progressBar:!1}}function f(e){w&&w(e)}function g(t){function i(e){return null==e&&(e=""),new String(e).replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;")}function o(){r(),d(),l(),u(),p(),c()}function s(){y.hover(b,O),!x.onclick&&x.tapToDismiss&&y.click(w),x.closeButton&&k&&k.click(function(e){e.stopPropagation?e.stopPropagation():void 0!==e.cancelBubble&&e.cancelBubble!==!0&&(e.cancelBubble=!0),w(!0)}),x.onclick&&y.click(function(e){x.onclick(e),w()})}function a(){y.hide(),y[x.showMethod]({duration:x.showDuration,easing:x.showEasing,complete:x.onShown}),x.timeOut>0&&(H=setTimeout(w,x.timeOut),q.maxHideTime=parseFloat(x.timeOut),q.hideEta=(new Date).getTime()+q.maxHideTime,x.progressBar&&(q.intervalId=setInterval(D,10)))}function r(){t.iconClass&&y.addClass(x.toastClass).addClass(E)}function c(){x.newestOnTop?v.prepend(y):v.append(y)}function d(){t.title&&(I.append(x.escapeHtml?i(t.title):t.title).addClass(x.titleClass),y.append(I))}function l(){t.message&&(M.append(x.escapeHtml?i(t.message):t.message).addClass(x.messageClass),y.append(M))}function u(){x.closeButton&&(k.addClass("toast-close-button").attr("role","button"),y.prepend(k))}function p(){x.progressBar&&(B.addClass("toast-progress"),y.prepend(B))}function g(e,t){if(e.preventDuplicates){if(t.message===C)return!0;C=t.message}return!1}function w(t){var n=t&&x.closeMethod!==!1?x.closeMethod:x.hideMethod,i=t&&x.closeDuration!==!1?x.closeDuration:x.hideDuration,o=t&&x.closeEasing!==!1?x.closeEasing:x.hideEasing;return!e(":focus",y).length||t?(clearTimeout(q.intervalId),y[n]({duration:i,easing:o,complete:function(){h(y),x.onHidden&&"hidden"!==j.state&&x.onHidden(),j.state="hidden",j.endTime=new Date,f(j)}})):void 0}function O(){(x.timeOut>0||x.extendedTimeOut>0)&&(H=setTimeout(w,x.extendedTimeOut),q.maxHideTime=parseFloat(x.extendedTimeOut),q.hideEta=(new Date).getTime()+q.maxHideTime)}function b(){clearTimeout(H),q.hideEta=0,y.stop(!0,!0)[x.showMethod]({duration:x.showDuration,easing:x.showEasing})}function D(){var e=(q.hideEta-(new Date).getTime())/q.maxHideTime*100;B.width(e+"%")}var x=m(),E=t.iconClass||x.iconClass;if("undefined"!=typeof t.optionsOverride&&(x=e.extend(x,t.optionsOverride),E=t.optionsOverride.iconClass||E),!g(x,t)){T++,v=n(x,!0);var H=null,y=e("<div/>"),I=e("<div/>"),M=e("<div/>"),B=e("<div/>"),k=e(x.closeHtml),q={intervalId:null,hideEta:null,maxHideTime:null},j={toastId:T,state:"visible",startTime:new Date,options:x,map:t};return o(),a(),s(),f(j),x.debug&&console&&console.log(j),y}}function m(){return e.extend({},p(),b.options)}function h(e){v||(v=n()),e.is(":visible")||(e.remove(),e=null,0===v.children().length&&(v.remove(),C=void 0))}var v,w,C,T=0,O={error:"error",info:"info",success:"success",warning:"warning"},b={clear:r,remove:c,error:t,getContainer:n,info:i,options:{},subscribe:o,success:s,version:"2.1.2",warning:a};return b}()})}("function"==typeof define&&define.amd?define:function(e,t){"undefined"!=typeof module&&module.exports?module.exports=t(require("jquery")):window.toastr=t(window.jQuery)});
//# sourceMappingURL=toastr.js.map
function customAlert(msg, alert_type){
    if (alert_type == 'unknown')
        alert_type = 'warning';

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "200",
        "hideDuration": "1000",
        "timeOut": "99999",
        "extendedTimeOut": "777",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    switch (alert_type){
        case 'success':
            toastr.success(msg);
            break;
        case 'warning':
            toastr.warning(msg);
            break;
        case 'error':
            toastr.error(msg);
            break;
        default:
            toastr.warning(msg);
            break;
    }
}