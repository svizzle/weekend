function customAlert(msg, alert_type){
    if (alert_type == 'unknown')
        alert_type = 'warning';

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "200",
        "hideDuration": "1000",
        "timeOut": "99999",
        "extendedTimeOut": "777",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    switch (alert_type){
        case 'success':
            toastr.success(msg);
            break;
        case 'warning':
            toastr.warning(msg);
            break;
        case 'error':
            toastr.error(msg);
            break;
        default:
            toastr.warning(msg);
            break;
    }
}