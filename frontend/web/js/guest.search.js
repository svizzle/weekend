(function($){

    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
        },
  });

    $.fn.advancedSearch = function(options) {
    
        this.options = options;

        var self = this;

        var $regions   = self.find('.regions');
        var $templates = self.find('.templates');
        var $advanced  = $(".advanced-search");


        // Инициализация автокомплита регионов
        $regions.find('.select-select').autocomplete({
            minLength: 0,
            source: self.options.regions.map(function(region){return region.name})
        });

        $regions.find('.select-select').on('autocompleteopen', function(e, ui){
            $regions.find('.select-addit-box').hide();
            $(this).data('is_open', true);  
        });

        $regions.find('.select-select').on('autocompleteclose', function(event, ui) {
            $(this).data('is_open', false);
        });

        $regions.find('.select-select').on('autocompletesearch', function(e, ui){
            var value = $(this).val();
            var found = false;
            
            $.each(options.regions, function(index, region){
                if (region.name == value) found = true;
            });
            
            found == false ? $regions.find('.select-addit').hide() : $regions.find('.select-addit').show();
        });

        $regions.find('.select-select').on('autocompleteselect', function(e, ui){
            $regions.find('.select-addit-box').empty();
            $regions.find('.select-addit').text('ВСЕ').show();
            $.each(self.options.regions, function(index, region){
                if (region.name == ui.item.value)
                {
                    $regions.find('input[name=region_id]').attr('value', region.id);
                }
            });
            AdvancedSearch.recalculateTemplateTabs();
            AdvancedSearch.recalculateTabs();
            AdvancedSearch.enableTemplatesIfRegionSet();
        });

        $regions.find('.select-select').focus(function(){
            var is_open = $regions.find('.select-select').data('is_open');
            if (!is_open) {
                $regions.find('.select-select').autocomplete('search', '');
            } else {
                $regions.find('.select-select').autocomplete('close');
            }
        });

        $regions.find('.select-showall').click(function(){
            $regions.find('.select-select').focus();
        });

        $regions.find('.select-addit').click(function(){
            var selbox = $regions.find('.select-addit-box');
            var selsel = $regions.find('.select-select');
            
            if (!selbox.is(':hidden'))
            {
                selbox.hide();
            }
            else
            {
                if (selbox.is(':empty'))
                {
                    var value = selsel.val();
                    
                    var li = $('<li style="border-bottom: 1px solid #ddd;padding-bottom: 10px; cursor: default;" class="select-addit-item"><span class="uncheck-all"></span> Снять выделение <span class="check-all"></span> Выделить все</li>');
                    $regions.find('.select-addit-box').append(li);
                    
                    $.each(self.options.regions, function(index, region) {
                        if (region.name == value)
                        {
                            for (var i in region.locations)
                            {
                                var location = region.locations[i];                                
                                
                                var input = $('<input name="location_id[]" type="checkbox"/>');
                                input.val(location.id).prop('checked', true).prop('id', 'location-' + location.id);

                                var label = $('<label/>');
                                label.prop('for', 'location-' + location.id).html(location.name);

                                var li = $('<li class=\"select-addit-item\"></li>');
                                li.append(input).append(label);

                                selbox.append(li);
                            }
                        }
                    });
                }
                selbox.show();
                selsel.autocomplete('close');
            }
        });

        $regions.on('change', 'input[type=checkbox]', function(){
            var num = $regions.find('input[type=checkbox]:checked').length;
            if (num == $regions.find('input[type=checkbox]').length || num == 0)
            {
                $regions.find('.select-addit').text('ВСЕ');
            }
            else
            {
                $regions.find('.select-addit').text(num);
            }
            AdvancedSearch.recalculateTabs();
            AdvancedSearch.enableTemplatesIfRegionSet();
        });

        $regions.bind('change', function() {
            if ($('.regions').find('.select-select').val() == '') {
                $(".select-container.templates .select-select").prop('disabled', true);
                $('.select-container.templates .select-select').autocomplete('disable');
                $.fn.advancedSearch.resetTabs();
                $('.templates .select-addit').hide();
            }
        });

        // Инициализация автокомплита видов отдыха
        $templates.find('.select-select').autocomplete({
            minLength: 0,
            source: self.options.templates.map(function(template){
                function getCategoryName()
                {   //alert(template.tabs.length);
                    if (template.custom)
                    return 'Пользовательские';
                    if (template.tabs.length > 1)
                        return 'Предустановленные';
                    else
                        return 'Вид услуги';
                }
                return {

                    label: template.name,
                    category: getCategoryName()
                };
            })
        }).autocomplete('instance')._renderMenu = function( ul, items ) {
            $(ul).css({'padding-left': '5px'});
            var that = this,
            currentCategory = "";
            $.each( items, function( index, item ) {
                var li;
                if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category' style='font-weight: bold;'>" + item.category + "</li>" );
                    currentCategory = item.category;
                }
                li = that._renderItemData( ul, item );
                if ( item.category ) {
                    li.attr( "aria-label", item.category + " : " + item.label );
                }
            });
        };
        $templates.find('.select-select').autocomplete('instance').widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );

        $templates.find('.select-select').on('autocompleteopen', function(e, ui){
            $templates.find('.select-addit-box').hide();
            $(this).data('is_open', true);  
        });

        $templates.find('.select-select').on('autocompleteclose', function(event, ui) {
            $(this).data('is_open', false);
        });

        $templates.find('.select-select').on('autocompletesearch', function(e, ui){
            var value = $(this).val();
            var found = false;

            $.each(options.templates, function(index, template){
                if (template.name == value) found = true;
            });
            console.log(found);
            found == false ? $templates.find('.select-addit').hide() : $templates.find('.select-addit').show();
        });

        $templates.find('.select-select').on('autocompleteselect', function(e, ui){
            $.each(self.options.templates, function(index, template){
                if (template.name == ui.item.value)
                {
                    $templates.find('input[name=template_id]').attr('value', template.id);
                    $templates.find('.select-addit').show()
                    $templates.find('.select-addit-box input[type=checkbox]').each(function(index, checkbox){                        

                        var found = false;

                        $.each(template.tabs, function(index, tab) {
                            if ($(checkbox).val() == tab.id) found = true;
                        });

                        if (found)
                        {
                            if (!$(checkbox).is(':checked'))
                            {
                                $(checkbox).prop('checked', true).trigger('change');
                            }
                        }
                        else
                        {
                            if ($(checkbox).is(':checked'))
                            {
                                $(checkbox).prop('checked', false).trigger('change');
                            }
                        }
                    });
                    
                    $('.advanced-input-group input[type=checkbox]').prop('checked', false);
                    $.each(template.values, function(index, value) {
                        $('.advanced-input-group input[type=checkbox][value='+ value +']').prop('checked', true);
                    });
                    AdvancedSearch.recalculateTabs();
                }
            });
        });

        $templates.find('.select-select').focus(function(){
            var is_open = $templates.find('.select-select').data('is_open');
            if (!is_open) {
                $templates.find('.select-select').autocomplete('search', '');
            } else {
                $templates.find('.select-select').autocomplete('close');
            }
        });

        $templates.find('.select-showall').click(function(){
            $templates.find('.select-select').focus();
        });

        $templates.find('.select-addit').click(function(){
            var selbox = $templates.find('.select-addit-box');
            var selsel = $templates.find('.select-select');
            
            if (!selbox.is(':hidden'))
            {
                selbox.hide();
            }
            else
            {
                selbox.show();
            }
            $templates.find('.select-select').autocomplete('close');
        });

        $templates.find('.select-addit-box').on('change', 'input[type=checkbox]', function(){
            var tab_id = $(this).val();

            var count = $templates.find('.select-addit-box input[type=checkbox]:checked').length;

            if (count == $templates.find('.select-addit-box input[type=checkbox]').length)
            {
                $templates.find('.select-addit').text('ВСЕ');
            }
            else
            {
                $templates.find('.select-addit').text(count);
            }

            if ($(this).is(':checked'))
            {
                // add tab
                for(var i in self.options.tabs)
                {
                    var tab = self.options.tabs[i];
                    if (tab.id == $(this).val())
                    {
                        AdvancedSearch.appendTab(tab);
                    }
                }
                //$.get('/search/get-service-type?id=' + $(this).val(), function(type){
                //    AdvancedSearch.appendTab(type);
                //}, 'json');
            }
            else
            {
                // remove tab
                $('[rel=tab-' + tab_id + ']').remove();
            }

            var tab = $advanced.find('.advanced-tab:first');
            var tabId = $(tab).attr('rel');
            $advanced.find('.advanced-tab').removeClass('active-advanced-tab');
            $(tab).addClass('active-advanced-tab');
        
            $advanced.find(".wnd-tab").removeClass('active');
            $advanced.find(".wnd-tab[rel="+ tabId+"]").addClass('active');
        });
        
        var li = $('<li style="border-bottom: 1px solid #ddd;padding-bottom: 10px;cursor: default;" class="select-addit-item"><span class="uncheck-all"></span> Снять выделение <span class="check-all"></span> Выделить все</li>');
        $templates.find('.select-addit-box').append(li);

        for(var i in self.options.tabs)
        {
            var tab = self.options.tabs[i];
            
            var input = $('<input name="tab_id[]" type="checkbox"/>');
            input.val(tab.id);
//            input.prop('checked', true);
            input.prop('id', 'tab-' + tab.id);

            var label = $('<label/>');
            label.prop('for', 'tab-' + tab.id).html(tab.name + ' (' + tab.count  + ')');

            var li = $('<li class=\"select-addit-item\"></li>');
            li.append(input).append(label);

            $templates.find('.select-addit-box').append(li);

            if (self.options.searchParams == false)
            {
                input.trigger('change');
            }
        }

        AdvancedSearch.hideOrShowSaveTmpBtn();

        // Инициализация расширенной формы
        self.find('.show-advanced').click(function(){
            $('.modal-group-1').show();
            AdvancedSearch.hideOrShowSaveTmpBtn();
        });
    
        $advanced.find('.advanced-tab-box').on('click', '.advanced-close', function(){
            $templates.find("[value='"+$(this).attr('rel')+"']").prop('checked', false).trigger('change');

            var tabsCount = $templates.find('.select-addit-box input[type=checkbox]:checked').length;

            if (tabsCount == 0){
                $templates.find('.select-addit').hide();
            }

            AdvancedSearch.hideOrShowSaveTmpBtn();
        });

        $advanced.on('click', '.advanced-tab', function(){
            var tabId = $(this).attr('rel');
            $advanced.find('.advanced-tab').removeClass('active-advanced-tab');
            $(this).addClass('active-advanced-tab');
        
            $advanced.find(".wnd-tab").removeClass('active');
            $advanced.find(".wnd-tab[rel="+ tabId+"]").addClass('active');
        });

        $advanced.find('.advanced-footer .add-tab').click(function(){

            var openedTabs = $advanced.find('.advanced-tab').toArray().map(function(tab){
                return $(tab).data('id');
            });
   
            var val = 0;

            $('.new-tab-dialog select option').each(function(index, option){

                var found = false;
                for (var i in openedTabs)
                {
                    if (parseInt($(option).attr('value')) == parseInt(openedTabs[i]))
                    {
                        found = true;                    
                    }            
                }

                if (found)
                {
                    $(option).hide();
                }
                else
                {
                    if (val == 0) val = $(option).attr('value');
                    $(option).show();
                }
            });

            if (openedTabs.length == $('.new-tab-dialog select option').toArray().length) return;

            $('.new-tab-dialog select').val(val);

            $('.new-tab-dialog').modal();
        });

        $('.new-tab-dialog .btn-primary').click(function(){
            $('.new-tab-dialog').modal('hide');

            var id = $('.new-tab-dialog select').val();

            var checkbox = $templates.find('input[type=checkbox][value='+id+']');
            if (!checkbox.is(':checked')) checkbox.prop('checked', true).trigger('change');
            $('.templates').find('.select-addit').show();
            AdvancedSearch.hideOrShowSaveTmpBtn();
        });

        $advanced.find('.advanced-wnd-box').on('change', 'input[type=checkbox]', function(){

            var checkbox = $(this);

            var parent = checkbox.parents('.wnd-tab');

            AdvancedSearch.recalculateTab(parent.data('id'));
            $templates.find('.select-select').val('Расширенный поиск');
        });
    
        $('#search-send').click(function(){

            var params = AdvancedSearch.getParams();
            var region_id = parseInt(params.region);
            if (!region_id)
            {
                customAlert('Выберите место отдыха и вид отдыха, потом нажмите кнопку "Найти"');
                return false;
            }

            window.location.href = '/service/advanced-search?q=' + $.base64.encode(JSON.stringify(params));
        });

        $advanced.find('.apply-template').click(function(){
            var params = AdvancedSearch.getParams();
            var region_id = parseInt(params.region);
            if (!region_id)
            {
                customAlert('Выберите место отдыха и вид отдыха, потом нажмите кнопку "Применить"');
                return false;
            }
            $('#search-send').click();
        });

        // Инициализируем контролы выбранными ранее значениями
        if (self.options.searchParams !== false) 
        {
            var searchParams = self.options.searchParams;
            if (parseInt(searchParams.region))
            {
                $regions.find('.select-select').val(searchParams.region_name).autocomplete('search').autocomplete('close');
                $regions.find('input[name=region_id]').val(searchParams.region);
//                AdvancedSearch.recalculateTemplateTabs();
            }

                    $.each(self.options.regions, function(index, region) {
                        if (region.name == searchParams.region_name)
                        {
                            var li = $('<li style="border-bottom: 1px solid #ddd;padding-bottom: 10px;cursor: default;" class="select-addit-item"><span class="uncheck-all"></span> Снять выделение <span class="check-all"></span> Выделить все</li>');
                            $regions.find('.select-addit-box').append(li);

                            for (var i in region.locations)
                            {
                                var location = region.locations[i];                                
                                
                                var input = $('<input name="location_id[]" type="checkbox"/>');
                                input.val(location.id).prop('checked', false).prop('id', 'location-' + location.id);
                                if (searchParams.locations.length == 0 || searchParams.locations.indexOf(location.id + '') >= 0)
                                {
                                    input.prop('checked', true);
                                }

                                var label = $('<label/>');
                                label.prop('for', 'location-' + location.id).html(location.name);

                                var li = $('<li class=\"select-addit-item\"></li>');
                                li.append(input).append(label);

                                $regions.find('.select-addit-box').append(li);
                            }

                            if (region.locations.length != searchParams.locations.length && searchParams.locations.length != 0)
                            {
                                $regions.find('.select-addit').text(searchParams.locations.length);
                            }
                        }
                    });

                    $templates.find('.select-addit-box input[type=checkbox]').prop('checked', false);
                    $.each(searchParams.tabs, function(index, tab){
                        $templates.find('.select-addit-box input[type=checkbox][value='+tab.id+']').prop('checked', true).trigger('change');
                    });

                    if ($templates.find('.select-addit-box input[type=checkbox]').length != searchParams.tabs.length)
                    {
                        $templates.find('.select-addit').text(searchParams.tabs.length);
                    }

            if (parseInt(searchParams.template))
            {
                $templates.find('.select-select').val(searchParams.template_name).autocomplete('search').autocomplete('close');
                $templates.find('input[name=template_id]').val(searchParams.template);
                $templates.find('.select-addit').show()
            }        

            $.each(searchParams.tabs, function(index, tab) {
                if (tab.attributes)
                {
                    $.each(tab.attributes, function(index, attribute) {
                        $.each(attribute.values, function(index, value) {
                            $('.advanced-input-group input[type=checkbox][value='+ value +']').prop('checked', true);
                        });
                    });
                }
            });
        }
        $advanced.find('.advanced-tab:first').click();

        $templates.find('.select-addit-box').on('change', 'input[type=checkbox]', function(){
            $templates.find('.select-select').val('Расширенный поиск');
        });

        var tabsCount = $templates.find('.select-addit-box input[type=checkbox]:checked').length;
        //alert($('input[name=template_id]').val());
        if (tabsCount > 0){
            $('.templates').find('.select-addit').show();
            if ($('input[name=template_id]').val() == ''){
                $templates.find('.select-select').val('Расширенный поиск');
            }
        }

        $(document).click(function(e){
            if (!$(e.target).parents('.select-container').length)
            {
                $regions.find('.select-addit-box').hide();
                $templates.find('.select-addit-box').hide();
            }
        });

        AdvancedSearch.enableTemplatesIfRegionSet();

        return this;
    };

}(jQuery));


var AdvancedSearch = function() {};

AdvancedSearch.__proto__.getParams = function(){

    var $regions   = $('.regions');
    var $templates = $('.templates');
    var $advanced  = $(".advanced-search");

            var activeTab = $advanced.find('.advanced-tab-box .advanced-tab.active-advanced-tab').data('id');
            var tabs = $templates.find('.select-addit-box input[type=checkbox]:checked').toArray().map(function(el){ return $(el).val()});

            var params = {

                region: $regions.find('input[name=region_id]').val(),
                locations: $regions.find('input[type=checkbox]:checked').toArray().map(function(checkbox){ return $(checkbox).val()}) || [],
                template: $templates.find('input[name=template_id]').val(),
                tabs: tabs.map(function(id){
                    return {
                        id: id,
                        attributes: $advanced.find('.wnd-tab[rel=tab-'+id+'] .advanced-wnd').toArray().map(function(attribute){
                            return {
                                id: $(attribute).data('id'),
                                values: $(attribute).find('input[type=checkbox]:checked').toArray().map(function(checkbox){
                                    return $(checkbox).val();
                                })
                            };
                        }).filter(function(attribute){ return attribute.values.length > 0;})
                    };
                }),
                activeTab: activeTab
            };

    return params;
};

AdvancedSearch.__proto__.recalculateTemplateTab = function(id) {
    var params = AdvancedSearch.getParams();

    params.template = '';
    params.tabs = [
    {
        id: id,
        attributes: []
    }];
    params.activeTab = id;

    $.post('/service/count-search', params, function(count){
        var label = $('.templates .select-addit-item input[value=' + id + ']').next();
        label.html(label.html().replace(/\((\d+\))/, '('+count+')'));
    });
};

AdvancedSearch.__proto__.recalculateTemplateTabs = function() {
    $('.templates .select-addit-item input').each(function(index, input){
        var id = $(input).val();

        AdvancedSearch.recalculateTemplateTab(id);
    });
};

AdvancedSearch.__proto__.recalculateTab = function(tabId) {
    var params = AdvancedSearch.getParams();
    params.activeTab = tabId;

    $.post('/service/count-search', params, function(count){
        var title = $('.advanced-tab[rel=tab-' + tabId + '] .advanced-title');
        title.html(title.html().replace(/\((\d+\))/, '('+count+')'));
    });
};

AdvancedSearch.__proto__.recalculateTabs = function() {
    $('.wnd-tab').each(function(index, tab){
        AdvancedSearch.recalculateTab($(tab).data('id'));
    });
};

AdvancedSearch.__proto__.resetTabs = function() {
    $('.templates').find("[value]").prop('checked', false).trigger('change');
};

AdvancedSearch.__proto__.enableTemplatesIfRegionSet = function() {
    if (($('.regions').find('input[name=region_id]').val() == '') ) {
        console.log('region is not set');
        $(".select-container.templates .select-select").prop('disabled', true);
        $('.select-container.templates .select-select').autocomplete('disable');
    }else{
        console.log('region is  set');
        $(".select-container.templates .select-select").prop('disabled', false);
        $('.select-container.templates .select-select').autocomplete('enable');
    }
};

AdvancedSearch.__proto__.hideOrShowSaveTmpBtn = function() {
    var tabsCount = $('.templates .select-addit-box input[type=checkbox]:checked').length;
    if  (tabsCount == 0){
        $('.save-template').hide();
    }else{
        $('.save-template').show();
    }
};

AdvancedSearch.__proto__.appendTab = function(type) {
        
        var div = $('<div class="advanced-tab" rel="tab-'+ type.id + '" data-id="'+type.id+'">' +
                        '<div class="advanced-title">' +
                            '<strong>' + type.name + '</strong> (0)' +
                        '</div>' +
                        '<div class="advanced-close" rel="'+ type.id +'"></div>' +
                    '</div>');

        $('.advanced-search .advanced-tab-box').append(div);


        var wndTab = $('<div class="wnd-tab" rel="tab-' + type.id + '" data-id="'+type.id+'"></div>');
        for(var i in type.attributes)
        {
            var attribute = type.attributes[i];

            var wnd = $('<div class="advanced-wnd" data-id="'+attribute.id+'">' +
                            '<div class="advanced-wnd-title">' + attribute.name + '</div>' +
                            '<div class="advanced-scrollbox">' +
                            '</div>' +
                        '</div>');

            for(var j in attribute.values)
            {
                var id = Math.random() + i + j;
                var value = attribute.values[j];
                var check = $('<div class="advanced-input-group">' +
                                    '<input name="v[]" value="' + value.id+ '" type="checkbox" id="advanced-checkbox-' + id + '" data-id="'+value.id+'">' + 
                                    '<label for="advanced-checkbox-' + id + '">' + value.name + '</label>' +
                              '</div>');

                wnd.find('.advanced-scrollbox').append(check);
            }


            wndTab.append(wnd);

        }
        $(".advanced-search .advanced-wnd-box").append(wndTab);

        this.recalculateTab(type.id);

        if ($(".advanced-tab").length == 1)
        {
            $(".advanced-tab").click();
        }

}

$(function(){


    $('.advanced-search .save-template').click(function(){
        $(".search-save-dialog").modal();

        $(".search-save-dialog .btn-primary").click(function(){
            var params = AdvancedSearch.getParams();
            params.name = $('#servicetypegroup-name').val();

            if (!params.name.length)
            {
                customAlert('Введите имя шаблона!', 'error');
                return;
            }

            var source = $('.select-container.templates .select-select').autocomplete('option', 'source');
            source.push({category: 'Пользовательские', 'label': params.name});
            source.sort(function(a, b){
                if (a.category < b.category) return -1;
                if (a.category > b.category) return 1;
                return 0;
            });
            var source = $('.select-container.templates .select-select').autocomplete('option', 'source', source);

            $.post('/search/save', params);
            $(".search-save-dialog").modal('hide');
        });

    });

    $('.advanced-wnd-box').on('change', 'input[type=checkbox]', function(){

        var ids = $('.advanced-wnd-box input[type=checkbox]:checked').toArray().map(function(input){
            return $(input).val();
        });

        $("#search-form .hidden-attribute-value").remove();
        for (var i in ids)
        {
            var id = ids[i];
            $("#search-form").append($('<input type="hidden" name="v[]" value="'+id+'">'));
        }

    });

    $('.select-addit-box').on('click', '.check-all', function(){
        var inputs = [];
        $(this).parents('.select-addit-box').find('input').each(function(index, input){
            if (!$(input).is(':checked'))
            {
                $(input).prop('checked', true);
                inputs.push(input);
            }
        });

        if ($(this).parents('.regions').length)
        {
            $(this).parents('.select-addit-box').find('input:first').trigger('change');
        }
        else
        {
            $.each(inputs, function(index, input) {
                $(input).trigger('change');
            });
        }

        $('.select-addit-box').hide();
    });

    $('.select-addit-box').on('click', '.uncheck-all', function(){
        var inputs = [];
        $(this).parents('.select-addit-box').find('input').each(function(index, input){
            if ($(input).is(':checked'))
            {
                $(input).prop('checked', false);
                inputs.push(input);
            }
        });
        if ($(this).parents('.regions').length)
        {
            $(this).parents('.select-addit-box').find('input:first').trigger('change');
        }
        else
        {
            $.each(inputs, function(index, input) {
                $(input).trigger('change');
            });
        }
    });

});

