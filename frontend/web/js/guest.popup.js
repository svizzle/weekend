(function($){

    $.extend({

        popup: function(url){

            var div = $(            
                '<div class="popup">' +
                    '<div class="popup-overlay"></div>' + 
                    '<div class="popup-content">' +
                        '<a href="#" class="popup-close"></a>' +
                        '<div class="content">Загрузка...</div>' +
                    '</div>' +
                '</div>'
            );

            $('body').append(div);

            div.find('.popup-close').click(function(){
                div.fadeOut(function(){
                    div.remove();
                });
            });

            div.find('.popup-overlay').click(function(){
                div.find('.popup-close').click();
            });

            $.get(url, function(html){
            
                div.find('.content').html(html);

                div.find(".popup-content").css({
                    "margin-left": "-" + Math.floor(div.find('.popup-content').outerWidth()/2) + "px",
                    "margin-top": "-" + Math.floor(div.find('.popup-content').outerHeight()/2) + "px",
                });
            
            });
            

        }

    });

    $.fn.popup = function() {
        
        var self = this;

        this.click(function(e){
            e.preventDefault();

            var url = $(this).attr('data-popup');
            
            $.ajax({
                type: "GET",
                url: url,
                xhrFields: {withCredentials: true}, 
                success: function(response) {

                    $(".popup").remove();

                    var div = $(
                    '<div class="popup">' + 
                        '<div class="popup-overlay"></div>' +
                        '<div class="popup-content">' +
                            '<a href="#" class="popup-close"></a>' +
                            response +
                        '</div>' +
                    '</div>'
                    );
    
                    $('body').append(div);
                    div.find(".popup-content").css({
                        "margin-left": "-" + Math.floor(div.find('.popup-content').outerWidth()/2) + "px",
                        "margin-top": "-" + Math.floor(div.find('.popup-content').outerHeight()/2) + "px",
                    });

                    div.find('.popup-close').click(function(){
                        div.fadeOut(function(){
                            div.remove();
                        });
                    });
                    div.find('.popup-overlay').click(function(){
                        div.find('.popup-close').click();
                    });

                },
                crossDomain: true,
            });

            return false;
        });
    };

})(jQuery);
