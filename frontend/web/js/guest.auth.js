$(function(){

    $(document).click(function(){
        $(".menu-auth-box").hide()
    });

    $('.menu-auth-box').click(function(e){
        e.stopPropagation();
    });

    $(document).on('click', ".menu-register-open", function(e){
        e.stopPropagation();
		if ($(".menu-register-container").is(":visible"))
        {
			$(".menu-auth-box").hide();
			$(".menu-register-container").hide();
			$(".menu-login-container").hide();
		}
        else
        {
			$(".menu-auth-box").show();
			$(".menu-register-container").show();
			$(".menu-login-container").hide();
		}
	});

    $(document).on('click', ".menu-login-open", function(e){
        e.stopPropagation();
        if ($(".menu-login-container").is(":visible"))
        {
            $(".menu-auth-box").hide();
            $(".menu-register-container").hide();
            $(".menu-login-container").hide();
        }
        else
        {
            $(".menu-auth-box").show();
            $(".menu-login-container").show();
            $(".menu-register-container").hide();
        }
    });

});
