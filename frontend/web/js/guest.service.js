
    
    // Левый глаз
    $(document).on('click', '.result-items .result-left-eye', function(e){
        e.preventDefault();
        e.stopPropagation();
        if (!$(this).parent().find('.description').is(':visible'))
        {
            $('.result-items .result-item-buttons .description').hide();
        }
        $(this).parent().find('.description').toggle();
        return false;
    });

    // На карте
    $(document).on('click', '.result-items .result-btn-map', function(e){
        e.preventDefault();
        $.popup('/service/map?id=0'.replace('0', $(this).attr('rel')));
        return false;
    });

    // В избранное
    $(document).on('click', '.result-items .result-btn-favorite', function(e){
        e.preventDefault();
        $.get('/service/favorite?id=' + $(this).data('id'));
        if ($(this).hasClass('favorite'))
        {
            $(this).removeClass('favorite');
            $(this).html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В избранное');
        }
        else
        {            
            $(this).addClass('favorite');
            $(this).html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В избранном');
        }
    });

    // Телефон
    $(document).on('click', '.result-items .result-btn-phone', function(e){
        e.preventDefault();
        $(this).text($(this).attr('rel'));
        $(this).removeClass('result-btn-phone').addClass('result-btn-phoned');
        $.get('/statistics/service-action?service_id=' + $(this).data('id') + '&type_id=6');
        return false;
    });

    // Показанные телефон
    $(document).on('click', '.result-items .result-btn-phoned', function(e){
        e.preventDefault();
        return false;
    });

    // Правый глаз 
    $(document).on('click', '.result-items .result-right-eye', function(e){
        e.preventDefault();
        e.stopPropagation();
        if (!$(this).parent().find('.contacts').is(':visible'))
        {
            $('.result-items .result-item-buttons .contacts').hide();
        }
        $(this).parent().find('.contacts').toggle();
//        $.popup('/company/contacts?id=0'.replace('0', $(this).data('company_id')));
        return false;
    });

    $(document).on('click', function(e){
        $el = $(e.target);

        if ($el.hasClass('.contacts') || $el.parents('.contacts').length) return;
        if ($el.hasClass('.description') || $el.parents('.description').length) return;

        $('.result-items .result-item-buttons .contacts').hide();
        $('.result-items .result-item-buttons .description').hide();

    });
