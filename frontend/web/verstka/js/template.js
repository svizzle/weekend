$(function(){	
	$(document).mouseup(function(e){
		var container = $(".hoco, .ui-autocomplete"); // hoco = Hide On Click Outside

		if (!container.is(e.target)	&& container.has(e.target).length === 0) {
			container.hide();
		}
	});
	
	
	
	$("#search-form .select-container .search-place").data("autocomplete", ["Место отдыха 1",
		"Место отдыха 2", "Место отдыха 3", "Место отдыха 4", "Место отдыха 5", "Место отдыха 6",
		"Место отдыха 7", "Место отдыха 8", "Место отдыха 9"]);
	
	$("#search-form .select-container .search-type").data("autocomplete", ["Вид отдыха 1",
		"Вид отдыха 2", "Вид отдыха 3", "Вид отдыха 4", "Вид отдыха 5", "Вид отдыха 6",
		"Вид отдыха 7", "Вид отдыха 8", "Вид отдыха 9"]);
	
	$("#search-form .select-container .search-place").data("addit", [["Пункт выбора 1", "Пункт выбора 2", "Пункт выбора 3"],
		["Пункт выбора 4", "Пункт выбора 5", "Пункт выбора 6"],
		["Пункт выбора 7", "Пункт выбора 8", "Пункт выбора 9"],
		["Пункт выбора 10", "Пункт выбора 11", "Пункт выбора 12"],
		["Пункт выбора 13", "Пункт выбора 14", "Пункт выбора 15"],
		["Пункт выбора 16", "Пункт выбора 17", "Пункт выбора 18"],
		["Пункт выбора 19", "Пункт выбора 20", "Пункт выбора 21"],
		["Пункт выбора 22", "Пункт выбора 23", "Пункт выбора 24"],
		["Пункт выбора 25", "Пункт выбора 26", "Пункт выбора 27"]]);
		
	$("#search-form .select-container .search-type").data("addit", [["Подвид 1", "Подвид 2", "Подвид 3"],
		["Подвид 4", "Подвид 5", "Подвид 6"],
		["Подвид 7", "Подвид 8", "Подвид 9"],
		["Подвид 10", "Подвид 11", "Подвид 12"],
		["Подвид 13", "Подвид 14", "Подвид 15"],
		["Подвид 16", "Подвид 17", "Подвид 18"],
		["Подвид 19", "Подвид 20", "Подвид 21"],
		["Подвид 22", "Подвид 23", "Подвид 24"],
		["Подвид 25", "Подвид 26", "Подвид 27"]]);
	
	$(".select-addit-box").on("change", ".select-addit-item input", function(){
		var num = $(this).parent().parent().find("input:checked").length;
		console.log(num);
		if (num == $(this).parent().parent().find("input").length || num == 0) {
			$(this).parent().parent().parent().find(".select-addit").text("ВСЕ");
		}else $(this).parent().parent().parent().find(".select-addit").text(num);
	});
	
	$("#search-form .select-container .select-select").on("autocompleteopen", function(e, ui){
		$(this).parent().find(".select-addit-box").hide();
		$(this).parent().find(".select-addit-box").empty();
		$(this).data('is_open', true);
	});
	
	$('#search-form .select-container .select-select').on('autocompleteclose', function(event, ui) {
		$(this).data('is_open', false);
	});
	
	$("#search-form .select-container .select-select").on("autocompletesearch", function(e, ui){
		if ($(this).data("autocomplete").indexOf($(this).val()) == -1){
			$(this).parent().find(".select-addit").hide();
		}else $(this).parent().find(".select-addit").show();
	});
	
	$("#search-form .select-container .select-select").on("autocompleteselect", function(e, ui){
		$(this).parent().find(".select-addit").show();
	});
	
	$("#search-form .select-container .select-select").each(function(){
		$(this).autocomplete({minLength: 0, source: $(this).data("autocomplete")});
	});
	
	$("#search-form .select-container .select-showall").click(function(){
		var is_open = $(this).parent().find(".select-select").data("is_open");
		if (!is_open)
			$(this).parent().find(".select-select").autocomplete("search", "");
		else $(".ui-autocomplete").hide();
	});
	
	$(".select-addit").click(function(){
		var selbox = $(this).parent().find(".select-addit-box");
		var selsel = $(this).parent().find(".select-select");
		
		if (!selbox.is(":hidden")){
			selbox.hide();
		}else{
			if (selbox.is(":empty")){
				var idx = selsel.data("autocomplete").indexOf($(this).parent().find(".select-select").val());
				
				if (idx != -1){
					for (var i = 0; i < selsel.data("addit")[idx].length; i++)
						selbox.append('<li class="select-addit-item"><input type="checkbox" id="addit-check-'
							+$(this).parent().index()+'-'+i+'" /><label for="addit-check-'+$(this).parent().index()
							+'-'+i+'">'+selsel.data("addit")[idx][i]+'</label></li>');
					
				}
			}
			
			selbox.show();
		}
	});
	
	$(".search-date").on("keydown click focus", function(){
		setCaretToPos(this, 2);
	});
});

// Tabs Control

$(function(){
	$(".the-tab").click(function(){
		$(this).parent().find(".the-tab.tab-active").removeClass("tab-active");
		$(this).addClass("tab-active");
	});
	
	$(".scroll-box .scroll-left").click(function(){
		$(this).addClass("scroll-disabled");
		$(this).parent().find(".scroll-right").removeClass("scroll-disabled");
		$(this).parent().parent().parent().find(".tabs-container").scrollLeft(0);
	});
	
	$(".scroll-box .scroll-right").click(function(){
		$(this).addClass("scroll-disabled");
		$(this).parent().find(".scroll-left").removeClass("scroll-disabled");
		$(this).parent().parent().parent().find(".tabs-container").scrollLeft(10000);
	});
});

// Advanced Control

$(function(){
	$(".advanced-tab").click(function(){
		$(this).parent().find(".active-advanced-tab").removeClass("active-advanced-tab");
		$(this).addClass("active-advanced-tab");
	});
});

// Details Page

$(function(){
	var scrolling;
	
	$(".detail-submenu-up").on('mouseenter', function(){
			var themenu = $(this).parent().find(".detail-submenu");
			
			scrolling = setInterval(function(){
					themenu.scrollTop(themenu.scrollTop() - 23);
				}, 100);
		});
	
	$(".detail-submenu-up").on('mouseleave', function(){
			scrolling && clearInterval(scrolling);
		});
	
	$(".detail-submenu-down").on('mouseenter', function(){
			var themenu = $(this).parent().find(".detail-submenu");
			
			scrolling = setInterval(function(){
					themenu.scrollTop(themenu.scrollTop() + 23);
				}, 100);
		});
	
	$(".detail-submenu-down").on('mouseleave', function(){
			scrolling && clearInterval(scrolling);
		});
});
