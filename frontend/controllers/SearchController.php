<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use yii\web\Response;

use common\models\ServiceType;
use common\models\ServiceTypeAttributeValue;
use common\models\ServiceTypeGroup;

class SearchController extends \yii\web\Controller
{
    public function actionSave()
    {
        $post = Yii::$app->request->post();

        $model = new ServiceTypeGroup([
            'user_id' => Yii::$app->user->id,
            'name' => $post['name'],
            'active_tab' => $post['activeTab'],
        ]);

        $model->save();

        $tabs = [];

        foreach($post['tabs'] as $tab)
        {
            $tabs[] = $tab['id'];
            if (!isset($tab['attributes']) || !count($tab['attributes'])) continue;
            foreach($tab['attributes'] as $attribute)
            {
                foreach($attribute['values'] as $value_id)
                {
                    $value = ServiceTypeAttributeValue::findOne($value_id);
                    $model->link('values', $value);
                }
            }
        }

        $model->tab_ids = json_encode($tabs);
        $model->save();

    }

}
