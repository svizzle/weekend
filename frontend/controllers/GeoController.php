<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use \common\models\GeoRegion;

class GeoController extends \yii\web\Controller
{
    public function actionLocations($region_id)
    {
        $region = GeoRegion::findOne($region_id);
        if (!$region) throw new NotFoundHttpException;

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return array_map(function($location){
            return [
                'id' => $location->id,
                'name' => $location->name,
            ];
        }, $region->getLocations()->orderBy('name')->all());
    }

    public function actionLocationUpdate()
    {
        $model = new \frontend\forms\GeoLocationUpdate;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            Yii::$app->session->set('location_id', $model->location_id);
            Yii::$app->session->set('location_confirmed', true);

            if (!Yii::$app->user->isGuest){
                Yii::$app->user->identity->location_id = $model->location_id;
                Yii::$app->user->identity->save();
            }

            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    function actionConfirmedLocation(){
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        if (Yii::$app->user->isGuest){

        }else{
            Yii::$app->user->identity->updated_at = time();
            Yii::$app->user->identity->save(false);
        }

        Yii::$app->session->set('location_confirmed', true);
    }
}
