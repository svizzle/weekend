<?php

namespace frontend\controllers;

use common\models\Company;
use Yii;
use yii\web\NotFoundHttpException;

use common\models\Page;
use common\models\User;
use common\models\UserSocial;

class UserController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        $attributes = $client->getUserAttributes();

        if ($client->id == 'odnoklassniki') $attributes['id'] = $attributes['uid'];

        $auth = UserSocial::find()->where([
            'source' => $client->id,
            'source_id' => $attributes['id'],
        ])->one();

        if (!$auth)
        {
            $user = new User;

            switch($client->id)
            {
                case 'google':
                    $user->first_name = $attributes['name']['givenName'];
                    break;
                case 'vkontakte':
                case 'odnoklassniki':
                    $user->first_name = $attributes['first_name'];
                    $user->last_name = $attributes['last_name'];
                    break;
                case 'facebook':
                    list($user->first_name, $user->last_name) = explode(" ",$attributes['name']);
                    break;
            }

            $user->save();
            $auth = new UserSocial([
                'user_id' => $user->id,
                'source' => $client->id,
                'source_id' => $attributes['id'],
            ]);
            $auth->save();

        }
        if ($auth->user->enabled)
        {
            Yii::$app->user->login($auth->user, 60*60*24*365);
        }
//        return $this->redirect(['/site/index']);
    }

    public function actionEmailConfirm($token = false)
    {
        if (!$token)
        {
            return $this->render('email-confirm', [
            ]);
        }

        $user = User::findOne(['activation_token' => $token]);
        if (!$user) return $this->redirect(['/site/index']);

        // У юзера есть мэйл - значит обычная регистрация по мэйлу и паролю
        if (!empty($user->email))
        {
            $user->activation_token = null;
            $user->save(false, ['activation_token']);
        }   
        // Мэйла нет, значит регистрация через соц. сети
        else
        {
            $social = $user->socials[0];
            $oldUser = User::findOne(['email' => $social->email]);
            // Смотрим, есть ли уже юзер с таким мэйлом, если есть то привязываем соц. сеть к нему
            if ($oldUser)
            {
                $social->user_id = $oldUser->id;        
                $social->save(false, ['user_id']);
                $user->delete();
                $user = $oldUser;
            }
            else
            {
                $user->email = $social->email;
                $user->save(false, ['email']);
            }
        }

        Yii::$app->user->logout();
        Yii::$app->user->login($user, 60*60*24*365);
        return $this->redirect(['/site/index']);
    }

    public function actionEmail()
    {
        if (Yii::$app->user->isGuest) return $this->redirect(['/site/index']);

        $model = new \frontend\forms\UserEmail;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $user = Yii::$app->user->identity;
            $user->activation_token = Yii::$app->security->generateRandomString(32);
            $user->save();
            $social = $user->socials[0];
            $social->email = $model->email;
            $social->save();

            Yii::$app->mailer->compose('user_email-confirm', ['model' => Yii::$app->user->identity])
                     ->setTo($model->email)
                     ->setSubject('Подтверждение адреса почты')
                     ->send();

            return $this->redirect(['/user/email-confirm']);
        }

        return $this->render('email', [
            'model' => $model,
        ]);
    }

    public function actionReseted()
    {
        return $this->render('reseted', [
        ]);
    }

    public function actionReset($token)
    {
        $user = User::find()->andWhere(['password_reset_token' => $token])->one();
        if (!$user) throw new NotFoundHttpException;

        $model = new \frontend\forms\UserReset;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $user->setPassword($model->password);
            $user->password_reset_token = null;
            $user->save();
            Yii::$app->user->login($user);
            return $this->redirect(['/user/reseted']);
        }

        return $this->render('reset', [
            'model' => $model,
        ]);
    }

    public function actionRestored()
    {
        return $this->render('restored');
    }

    public function actionRestore()
    {
        $model = new \frontend\forms\UserRestore;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $model->user->password_reset_token = Yii::$app->security->generateRandomString(32);
            $model->user->save(false, ['password_reset_token']);

            Yii::$app->mailer->compose('user_restore', ['model' => $model->user])
                     ->setTo($model->email)
                     ->setSubject('Восстановление пароля')
                     ->send();

            return $this->redirect(['/user/restored']);

        }

        $page = Page::findOne(8);

        return $this->render('restore', [
            'page' => $page,
            'model' => $model,
        ]);
    }

    public function actionRegistered()
    {
        return $this->render('registered', [
        ]);
    }

    public function actionRegister($code = false)
    {
        if ($code) return $this->redirect(Yii::$app->backendUrlManager->createUrl(['/user/register', 'code' => $code]));

        $model = new \frontend\forms\UserRegister;

        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $user = new User([
                    'email' => $model->email,                    
                    'password' => $model->password,
                    'activation_token' => Yii::$app->security->generateRandomString(),
                ]);
                $user->save();
                
                Yii::$app->mailer->compose('user_email-confirm', ['model' => $user])
                     ->setTo($model->email)
                     ->setSubject('Подтверждение адреса почты')
                     ->send();

                return $this->redirect(['/user/email-confirm']);
            }
        }

        return $this->renderAjax('register', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionLogin()
    {
        $model = new \frontend\forms\UserLogin;

        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $user = $model->getUser();
                Yii::$app->user->login($user, 60*60*24*365);
                // Этот хак нужен чтобы pjax перезагрузил страницу. Он перезагружает ее при пустом ответе сервера. При 302 не работает

                if (Yii::$app->user->can('partner')){
                    $haveServices = Company::find()->where(['user_id'=>Yii::$app->user->id])->count();

                    if ($haveServices == 0)
                        return $this->redirect(['/cabinet/company/form']);
                }

                if ($user->location_id){
                    Yii::$app->session->set('location_id', $user->location_id);
                    if ((time() - strtotime($user->updated_at)) > (3 * 24 * 60 * 60)){
                        Yii::$app->session->setFlash('its_time_to_confirm_location', true);
                    }
                }
                else{
                    Yii::$app->session->setFlash('show_location_modal', true);
                }
                return $this->redirect(\yii::$app->request->referrer, 200);
            }
        }

        return $this->renderAjax('login', [
            'model' => $model,
        ]);
    }
}
