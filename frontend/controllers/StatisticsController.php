<?php

namespace frontend\controllers;

use common\models\Statistics;

class StatisticsController extends \yii\web\Controller
{
    public function actionServiceAction($service_id, $type_id)
    {
        $statistics = new Statistics([
            'type_id' => $type_id,
            'service_id' => $service_id,
        ]);
        $statistics->save();
    }
}
