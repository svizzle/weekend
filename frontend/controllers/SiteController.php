<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;

use common\models\Service;
use common\models\Setting;

use frontend\helpers\CompanyHelper;

class SiteController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSimpleSearch($term)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $query = Service::find()
                        ->enabled()
                        ->andWhere([
                            'or',
                            ['like', 'service.code', $term],
                            ['like', 'service.name', $term],
                            ['like', 'company.name', $term],
                        ])
                        ->limit(5);

        $services = $query->all();

        if (count($services) == 0)
        {
            $term = $this->reverseTerm($term);
            $query = Service::find()
                            ->enabled()
                            ->andWhere([
                                'or',
                                ['like', 'service.code', $term],
                                ['like', 'service.name', $term],
                                ['like', 'company.name', $term],
                            ])
                            ->limit(5);

            $services = $query->all();
        }

        return array_map(function($service){
            return [
                'id' => $service->id,
                'url' => Url::to(['/service/view', 'id' => $service->id]),
                'name' => $service->name,
                'code' => $service->code,
                'company' => [
                    'name' => $service->company->name,
                    'address' => CompanyHelper::getAddress($service->company),
                ],
            ];
        },$services);

    }

    public function actionFeedback()
    {
        $model = new \frontend\forms\SiteFeedback;

        $success = false;
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            Yii::$app->mailer->compose('site_feedback', ['model' => $model])
                     ->setTo(Setting::findOne(['name' => 'feedback_email'])->value)
                     ->setSubject(Setting::findOne(['name' => 'feedback_subject'])->value)
                     ->send();

            $model = new \frontend\forms\SiteFeedback;
            $success = true;
        }
        return $this->renderAjax('feedback', [
            'model' => $model,
            'success' => $success,
        ]);
    }

    public function actionIndex()
    {
        // Сбрасываем настройки поиска
        Yii::$app->session->set('search-params', false);

        // Раз в n дней просим подтвердить локацию
        if (!Yii::$app->user->isGuest){
            $user = Yii::$app->user->identity;

            if ($user->location_id){
                Yii::$app->session->set('location_id', $user->location_id);
                if ((time() - strtotime($user->updated_at)) > (3 * 24* 60 * 60)){
                    Yii::$app->session->setFlash('its_time_to_confirm_location', true);
                }
            }
        }else{
            if (!Yii::$app->session->get('location_confirmed')){
                Yii::$app->session->setFlash('its_time_to_confirm_location', true);
            }
        }

        return $this->render('index');
    }

    public function reverseTerm($term)
    {
        $replacement = [
            'q' => 'й',
            'w' => 'ц',
            'e' => 'у',
            'r' => 'к',
            't' => 'е',
            'y' => 'н',
            'u' => 'г',
            'i' => 'ш',
            'o' => 'щ',
            'p' => 'з',
            '[' => 'х',
            ']' => 'ъ',
            'a' => 'ф',
            's' => 'ы',
            'd' => 'в',
            'f' => 'а',
            'g' => 'п',
            'h' => 'р',
            'j' => 'о',
            'k' => 'л',
            'l' => 'д',
            ';' => 'ж',
            '\'' => 'э',
            'z' => 'я',
            'x' => 'ч',
            'c' => 'с',
            'v' => 'м',
            'b' => 'и',
            'n' => 'т',
            'm' => 'ь',
            ',' => 'б',
            '.' => 'ю',
            '/' => '.',
        ];

        return strtr($term, $replacement);
    }
}
