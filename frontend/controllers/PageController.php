<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;

use common\models\Page;

class PageController extends \yii\web\Controller
{
    public function actionView($id)
    {
        $model = Page::findOne($id);
        if (!$model) throw NotFoundHttpException;

        return $this->render('view',[
            'model' => $model,
        ]);
    }
}
