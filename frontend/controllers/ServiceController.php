<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

use common\models\GeoLocation;
use common\models\Page;
use common\models\Service;
use common\models\ServiceComment;
use common\models\ServiceType;
use common\models\ServiceTypeGroup;
use common\models\Statistics;

use frontend\data\ServiceActiveDataProvider;

class ServiceController extends \yii\web\Controller
{
    public function actionFavorite($id)
    {
        if (Yii::$app->user->isGuest) throw new ForbiddenHttpException;

        $model = Service::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        $user = Yii::$app->user->identity;
        if ($user->getFavoriteServices()->andWhere(['id' => $model->id])->exists())
        {
            $user->unlink('favoriteServices', $model, true);            
            $statistics = new Statistics([
                'type_id' => Statistics::TYPE_FAVORITE_REMOVED,
                'service_id' => $model->id,
            ]);
            $statistics->save();
        }
        else
        {
            $user->link('favoriteServices', $model);
            $statistics = new Statistics([
                'type_id' => Statistics::TYPE_FAVORITE_ADDED,
                'service_id' => $model->id,
            ]);
            $statistics->save();
        }
    }

    public function actionCountSearch()
    {
        $dataProvider = new ServiceActiveDataProvider([
            'options' => Yii::$app->request->post(),
        ]);

        return $dataProvider->calculateTotalCount();
    }

    public function actionPrint($id)
    {
        $model = Service::find()->enabled()
                                ->andWhere(['service.id' => $id])
                                ->one();
        if (!$model) throw new NotFoundHttpException;

        $this->layout = 'print';
        return $this->render('print', ['model' => $model]);
    }

    public function actionMap($id)
    {
        $model = Service::find()->enabled()
                                ->andWhere(['service.id' => $id])
                                ->one();

        if (!$model) throw new NotFoundHttpException;

        $statistics = new Statistics([
            'type_id' => Statistics::TYPE_SERVICE_SHOW_MAP,
            'service_id' => $model->id,
        ]);
        $statistics->save();

        return $this->renderAjax('map', [
            'model' => $model
        ]);
    }

    public function actionSearch($text)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Service::find()->enabled()->andWhere([
                'or',
                [
                    'id' => $text,
                ],
                [
                    'like',
                    'name',
                    $text,
                ],
            ]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionAdvancedSearch($q)
    {
        $location = GeoLocation::findOne(Yii::$app->session->get('location_id'));

        $sort = new Sort([
            'attributes' => [
                'price' => [
                    'asc' => ['min_price' => SORT_ASC],
                    'desc' => ['max_price' => SORT_DESC],
                    'default' => SORT_ASC,
                    'label' => 'Цена',
                ],
                'rate' => [
                    'asc' => ['rate' => SORT_ASC],
                    'desc' => ['rate' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'Рейтинг',
                ],
                'distance' => [
                    'asc' => ['distance' => SORT_ASC],
                    'desc' => ['distance' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'Расстояние',
                ],
            ],
        ]);

        $ascSort = new Sort([
            'params' => array_merge($_GET, ['sort' => '-price']),
            'attributes' => [
                'price' => [
                    'default' => SORT_ASC,
                ],
                'rate' => [
                    'default' => SORT_ASC,
                ],
                'distance' => [
                    'default' => SORT_ASC,
                ],
            ],
        ]);

        $descSort = new Sort([
            'params' => array_merge($_GET, ['sort' => 'price']),
            'attributes' => [
                'price' => [
                    'default' => SORT_DESC,
                ],
                'rate' => [
                    'default' => SORT_DESC,
                ],
                'distance' => [
                    'default' => SORT_DESC,
                ],
            ],
        ]);

        $params = json_decode(base64_decode($q), true);
        Yii::$app->session->set('search-params', $params);

        $tabs = [];
        foreach($params['tabs'] as $tab)
        {
            $tabs[] = ServiceType::findOne($tab['id']);
        }   

        $activeTab = isset($params['activeTab']) && $params['activeTab'] ? ServiceType::findOne($params['activeTab']) : new ServiceType;
//        var_dump($activeTab)

        $result_limit = null;
        $pagination = [
            'pageSize' => 10,
        ];
        if (Yii::$app->user->isGuest){
            $result_limit = 10;
            $pagination = false;
        }

        $dataProvider = new ServiceActiveDataProvider([
            'lat' => $location->lat,
            'lon' => $location->lon,
            'options' => $params,
            'pagination' => $pagination,
            'sort' => $sort,
            'limit'=>$result_limit
        ]);

        // Сохраняем статистику о просмотрах
        foreach($dataProvider->getModels() as $model)
        {
            $statistics = new Statistics([
                'type_id' => Statistics::TYPE_SERVICE_DISPLAY,
                'service_id' => $model->id,                
            ]);
            $statistics->save();
        }



        return $this->render('list', [
            'searchParams' => $params,
            'tabs' => $tabs,
            'activeTab' => $activeTab,
            'dataProvider' => $dataProvider,
            'sort' => $sort,
            'ascSort' => $ascSort,
            'descSort' => $descSort,
        ]);
    }

    public function actionAdd()
    {
        $model = new \frontend\forms\ServiceAdd;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            Yii::$app->mailer->compose('service_add', ['model' => $model])
                     ->setSubject('Добавление услуги')
                     ->send();
            
            $model = new \frontend\forms\ServiceAdd;

            $this->view->registerJs('alert("Спасибо, ваше сообщение успешно отправлено")');
        }

        $page = Page::findOne(7); // Добавить услугу        

        return $this->render('add', [
            'model' => $model,
            'page' => $page,
        ]);
    }

    public function actionView($id)
    {
        $model = Service::find()->enabled()
                                ->andWhere(['service.id' => $id])
                                ->one();

        if (!$model) throw new NotFoundHttpException;

        // Обновляем просмотры услуги
        $statistics = new Statistics([
            'type_id' => Statistics::TYPE_SERVICE_VIEW,
            'service_id' => $model->id,
        ]);
        $statistics->save();

        $additionalServices = Service::find()->enabled()
                                             ->andWhere([
                                                 'service.company_id' => $model->company_id,
                                             ])
                                             ->andWhere(['!=', 'service.id', $id])
                                             ->all();
        $comments = new ActiveDataProvider([
            'query' => $model->getComments(),
        ]);

        $comment = new ServiceComment([
            'service_id' => $model->id,
        ]);

        if ($comment->load(Yii::$app->request->post()) && $comment->save())
        {
            // Пересчитываем рейтинг услуги
            $model->rate = round($model->getComments()->sum('rate') / $model->getComments()->count());
            $model->save(false, ['rate']);
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('view', [
            'model' => $model,
            'additionalServices' => $additionalServices,
            'comments' => $comments,
            'comment' => $comment,
        ]);
    }
}
