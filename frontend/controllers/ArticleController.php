<?php

namespace frontend\controllers;

use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use common\models\Article;

class ArticleController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = Article::findOne($id);
        if (!$model) throw NotFoundHttpException;

        return $this->render('view',[
            'model' => $model,
        ]);
    }
}
