<?php

namespace frontend\controllers;

use Yii;

use common\models\Page;

class AdvertiseController extends \yii\web\Controller
{
    public function actionAdd()
    {
        $model = new \frontend\forms\AdvertiseAdd;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {            
            Yii::$app->mailer->compose('advertise_add', ['model' => $model])
                     ->setSubject('Реклама на сайте')
                     ->send();
            
            $model = new \frontend\forms\AdvertiseAdd;

            $this->view->registerJs('alert("Спасибо, ваше сообщение успешно отправлено")');
        }

        $page = Page::findOne(4); // Реклама на сайте
    
        return $this->render('add', [
            'model' => $model,
            'page' => $page,
        ]);
    }
}
