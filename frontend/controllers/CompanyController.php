<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;

use common\models\Company;
use common\models\Statistics;

class CompanyController extends \yii\web\Controller
{
    public function actionMap($id)
    {
        $model = Company::findOne($id);

        if (!$model) throw new NotFoundHttpException;

        $statistics = new Statistics([
            'type_id' => Statistics::TYPE_COMPANY_SHOW_MAP,
            'company_id' => $model->id,
        ]);
        $statistics->save();

        return $this->renderAjax('map', [
            'model' => $model
        ]);
    }

    public function actionView($id)
    {
        $model = Company::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        return $this->render('view', [
            'favorite' => false,
            'model' => $model,
        ]);
    }

    public function actionContacts($id)
    {
        $model = Company::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        return $this->renderAjax('contacts', [
            'model' => $model
        ]);
    }
}
