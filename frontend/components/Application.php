<?php

namespace frontend\components;

use Yii;

use common\models\Statistics;

class Application extends \yii\web\Application
{    
    public function preInit(&$config)
    {
        parent::preInit($config);
        $config['components']['urlManager'] = $config['components']['frontendUrlManager'];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;

        if (!Yii::$app->user->isGuest && empty(Yii::$app->user->identity->email))
        {
            $route = $action->controller->id."/".$action->id;
            if (!in_array($route, ['user/email', 'user/email-confirm']))
            {
                return Yii::$app->response->redirect(['/user/email']);
            }
        }

        if (Yii::$app->request->isAjax || ($action->controller->id == 'service' && in_array($action->id, ['view', 'advanced-search'])))
        {
            // Идет поиск, не удаляем его параметры
        }
        else
        {
            Yii::$app->session->remove('search-params');
        }

        if (!Yii::$app->session->get('location_id'))
        {
            $ip = ip2long(Yii::$app->request->userIP);
            $sql = "SELECT `geo_location_id` FROM `net_ru` WHERE `end_ip` >= $ip and `begin_ip` <= $ip";
            $location_id = Yii::$app->db->createCommand($sql)->queryScalar();
            if (!$location_id)
            {
                $location_id = 4400; // Москва
            }

            Yii::$app->session->set('location_id', $location_id);
        }

        $statistics = new Statistics([
            'type_id' => Statistics::TYPE_USER_VISIT,
        ]);
        $statistics->save();

        return true;
    }
}
