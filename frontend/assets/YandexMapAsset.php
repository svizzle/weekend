<?php
namespace frontend\assets;

class YandexMapAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        '//api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];

    public $depends = [
    ];
}
