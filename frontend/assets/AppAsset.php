<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
        'css/guest.styles.min.css',
        'css/darktooltip.css',
    ];

    public $js = [
        //        'http://code.jquery.com/ui/1.11.4/jquery-ui.js',
        '//api-maps.yandex.ru/2.1/?lang=ru_RU',    
        'js/guest.scripts.min.js',
        'js/jquery.darktooltip.js',
        'js/toastr.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\SlickAsset',
        'frontend\assets\YandexMapAsset',
    ];
}
