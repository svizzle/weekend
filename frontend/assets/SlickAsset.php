<?php

namespace frontend\assets;

class SlickAsset extends \yii\web\AssetBundle
{
    public $sourcePath = "@bower/slick-carousel/slick";

    public $css = [
        'slick.css'
    ];

    public $js = [
        'slick.min.js'
    ];

    public $depends = [
    ];
}
