module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            guest: {
                options: {
                    compress: true
                },
                files: {
                    "web/css/guest.styles.min.css": "less/guest.less"
                }
            }
        },

        concat: {
            guest: {
                src: [
                    'web/js/jquery.base64.js',
                    'web/js/guest.auth.js',
                    'web/js/guest.popup.js',
                    'web/js/guest.search.js',
                    'web/js/guest.share.js',
                    'web/js/guest.service.js',
                    'web/js/toastr.min.js',
                    'web/js/guest.common.js',
                ],
                dest: 'web/js/guest.scripts.js'
            },
        },
        uglify: {
            guest: {
                src: 'web/js/guest.scripts.js',
                dest: 'web/js/guest.scripts.min.js'
            },
        },

    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['less', 'concat', 'uglify']);
};
