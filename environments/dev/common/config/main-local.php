<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=weekend',
            'username' => 'weekend',
            'password' => 'weekend',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'svizzle@yandex.ru',
                'password' => '020791yandex',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
    ],
];
