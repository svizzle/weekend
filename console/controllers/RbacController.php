<?php

namespace console\controllers;

use Yii;
use common\rbac\UserRoleRule;

class RbacController extends \yii\console\Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $userRoleRule = new UserRoleRule;
        $auth->add($userRoleRule);

        //

        $userRole = $auth->createRole('user');
        $userRole->ruleName = $userRoleRule->name;
        $auth->add($userRole);

        $partnerRole = $auth->createRole('partner');
        $partnerRole->ruleName = $userRoleRule->name;
        $auth->add($partnerRole);

        $adminRole = $auth->createRole('admin');
        $adminRole->ruleName = $userRoleRule->name;
        $auth->add($adminRole);

        //
        
        $auth->addChild($partnerRole, $userRole);
        $auth->addChild($adminRole, $partnerRole);

    }
}
