<?php

namespace console\controllers;

use common\models\GeoLocation;
use common\models\GeoRegion;

class YandexController extends \yii\console\Controller
{
    public function actionImportWeatherCities()
    {
        $xml = @file_get_contents('https://pogoda.yandex.ru/static/cities.xml');
        if (!$xml) die('Error');
        $xml = new \SimpleXMLElement($xml);

        foreach($xml->country as $country)
        {
            if ($country['name'] == 'Россия')
            {
                foreach($country->city as $city)
                {
                    $model = GeoLocation::find()->joinWith(['region'])->andWhere([
                        'geo_region.name' => $city['part'],
                        'geo_location.name' => (string)$city,
                    ])->one();

                    if (!$model) continue;

                    $model->yandex_id = $city['id'];                    
                    $model->save();
                    echo $model->name."\n";
                }
            }
        }
    }
}
