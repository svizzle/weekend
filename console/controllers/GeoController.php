<?php

namespace console\controllers;

use common\models\GeoLocation;

class GeoController extends \yii\console\Controller
{
    public function actionImportLocationCoordinates()
    {
        $locations = GeoLocation::find()->andWhere(['is', 'lon', null])->all();
        foreach($locations as $location)
        {           
            $query = strtr($location->region->name.', '.$location->name,[' ' => '+']);

            echo $query." ";

            $json = file_get_contents('https://geocode-maps.yandex.ru/1.x/?format=json&kind=locality&geocode='.$query);
            $json = json_decode($json, true);
            foreach($json['response']['GeoObjectCollection']['featureMember'] as $object)
            {
                $kind = $object['GeoObject']['metaDataProperty']['GeocoderMetaData']['kind'];
                if ($kind == 'locality')
                {
                    list($location->lon, $location->lat) = explode(" ", $object['GeoObject']['Point']['pos']);
                    $location->save(false, ['lon', 'lat']);
                    echo $location->lon." ".$location->lat;
                    break;
                }
            }
            echo "\n";
        }
    }
}
