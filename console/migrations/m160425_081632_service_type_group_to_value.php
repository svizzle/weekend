<?php

use yii\db\Migration;

class m160425_081632_service_type_group_to_value extends Migration
{
    public $tableName = '{{%service_type_group_to_value}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'group_id'         => $this->integer(),
            'value_id'         => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('service_type_g2v_group', $this->tableName, 'group_id', 'service_type_group', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('service_type_g2v_value', $this->tableName, 'value_id', 'service_type_attribute_value', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('service_type_g2v_group', $this->tableName);
        $this->dropForeignKey('service_type_g2v_value', $this->tableName);
        $this->dropTable($this->tableName);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
