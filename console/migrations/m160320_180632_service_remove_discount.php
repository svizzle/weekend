<?php

use yii\db\Migration;

class m160320_180632_service_remove_discount extends Migration
{
    public function up()
    {
        $this->dropColumn('service', 'discount');
        $this->addColumn('service_price', 'discount', 'VARCHAR(32)');
    }

    public function down()
    {
        echo "m160320_180632_service_remove_discount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
