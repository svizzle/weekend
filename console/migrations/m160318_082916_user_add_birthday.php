<?php

use yii\db\Migration;

class m160318_082916_user_add_birthday extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'birthday', 'DATE NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'birthday');
    }
}
