<?php

use yii\db\Migration;

use common\models\User;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(User::tableName(), [
            'id' => $this->primaryKey(),
            'group_id'             => $this->integer()->notNull(),
            'first_name'           => $this->string()->notNull(),
            'last_name'            => $this->string(),
            'father_name'          => $this->string(),
            'auth_key'             => $this->string(32)->notNull(),
            'password_hash'        => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email'                => $this->string()->notNull()->unique(),
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',
        ], $tableOptions);

        $root = new User([
            'group_id' => 2, // 2 - Administrator
            'first_name' => 'Администратор',        
            'email' => 'root@root',
        ]);

        $root->setPassword('root');
        $root->save();
    }

    public function down()
    {
        $this->dropTable(User::tableName());
    }
}
