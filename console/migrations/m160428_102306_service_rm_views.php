<?php

use yii\db\Migration;

class m160428_102306_service_rm_views extends Migration
{
    public function up()
    {
        $this->dropColumn('service', 'views');
    }

    public function down()
    {
        echo "m160428_102306_service_rm_views cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
