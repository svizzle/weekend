<?php

use yii\db\Migration;

class m160318_093351_company_type_id_fk extends Migration
{
    public function up()
    {
        $this->addForeignKey('company_type_id', '{{%company}}', 'type_id', '{{%company_type}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('company_type_id', '{{%company}}');
    }
}
