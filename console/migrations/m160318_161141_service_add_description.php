<?php

use yii\db\Migration;

class m160318_161141_service_add_description extends Migration
{
    public function up()
    {
        $this->addColumn('{{%service}}', 'description', 'TEXT');
    }

    public function down()
    {
        $this->dropColumn('{{%service}}', 'description');
    }
}
