<?php

use yii\db\Migration;

class m160505_020407_geo_yandex extends Migration
{
    public function up()
    {
        $this->addColumn('geo_region', 'yandex_id', 'INTEGER(11) DEFAULT NULL');
        $this->addColumn('geo_location', 'yandex_id', 'INTEGER(11) DEFAULT NULL');
    }

    public function down()
    {
        echo "m160505_020407_geo_yandex cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
