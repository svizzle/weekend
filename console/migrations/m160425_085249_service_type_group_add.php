<?php

use yii\db\Migration;

class m160425_085249_service_type_group_add extends Migration
{
    public $tableName = '{{%service_type_group}}';

    public function up()
    {
        $this->addColumn($this->tableName, 'active_tab', 'INTEGER(11)');
        $this->addColumn($this->tableName, 'tab_ids', 'VARCHAR(255)');
    }

    public function down()
    {
        echo "m160425_085249_service_type_group_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
