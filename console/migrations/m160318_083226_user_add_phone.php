<?php

use yii\db\Migration;

class m160318_083226_user_add_phone extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'phone', 'VARCHAR(32) NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'phone');
    }
}
