<?php

use yii\db\Migration;

class m160318_073306_user_add_photo extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'photo', 'VARCHAR(255) NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'photo');
    }
}
