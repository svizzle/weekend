<?php

use yii\db\Migration;

class m160605_081345_reg_form_settings extends Migration
{
    public $tableName = '{{%forms_settings}}';

    public function up()
    {
        $this->batchInsert($this->tableName, ['form', 'field_name', 'label', 'hint', 'placeholder'],[
                [3, 'form_header', 'Регистрация', '(регистрация нового партнера)', ''],
                [3, 'window_header', 'Отдых выходного дня', '', ''],
                [3, 'email', '', '', 'Email'],
                [3, 'password', '', '', 'Пароль'],
                [3, 'submit_btn', 'Зарегистрироваться', '', ''],
            ]
        );
    }

    public function down()
    {
        $this->delete($this->tableName, ['form'=>3]);
        return true;
    }
}
