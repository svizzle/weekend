<?php

use yii\db\Migration;

class m160309_203016_geo_location extends Migration
{
    public $tableName = '{{%geo_location}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'region_id' => $this->integer()->notNull(),
            'name'           => $this->string()->notNull(),
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('geo_location_region_id', $this->tableName, 'region_id', '{{%geo_region}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('geo_location_region_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
