<?php

use yii\db\Migration;

class m160322_134143_service_type_attribute_image extends Migration
{
    public $tableName = "{{%service_type_attribute_image}}";

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'attribute_id' => $this->integer(),
            'path'       => $this->string()->notNull(),
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('service_type_attribute_image_attribute_id', $this->tableName, 'attribute_id', '{{%service_type_attribute}}', 'id', 'CASCADE');
    }

    public function down()
    {
    }
}
