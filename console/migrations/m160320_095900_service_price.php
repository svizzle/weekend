<?php

use yii\db\Migration;

class m160320_095900_service_price extends Migration
{
    public $tableName = "{{%service_price}}";

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'service_id' => $this->integer()->notNull(),
            'unit_id'    => $this->integer()->notNull(),
            'type_id'    => $this->integer()->notNull(),
            'date_begin' => 'DATE NOT NULL',
            'date_end'   => 'DATE',
            'time'       => 'BOOLEAN',
            'time_begin' => 'TIME',
            'time_end'   => 'TIME',
            'price'      => 'VARCHAR(32)',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('service_price_service_id', $this->tableName, 'service_id', '{{%service}}', 'id', 'CASCADE');
        $this->addForeignKey('service_price_unit_id', $this->tableName, 'unit_id', '{{%service_price_unit}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('service_price_service_id', $this->tableName);
        $this->dropForeignKey('service_price_unit_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
