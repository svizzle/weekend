<?php

use yii\db\Migration;

class m160603_224442_add_artcile_service_types extends Migration
{
    public $tableName = '{{%article_service_types}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->dropForeignKey( 'article_service_type_id','article');
        $this->dropColumn('article', 'service_type_id');

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->addForeignKey('fk_article', $this->tableName, 'article_id', '{{%article}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_service', $this->tableName, 'service_id', '{{%service_type}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable($this->tableName);

        $this->addColumn('article', 'service_type_id', 'int(11)');
        $this->addForeignKey('article_service_type_id', 'article', 'service_type_id', '{{%service_type}}', 'id', 'CASCADE');
        return true;
    }
}
