<?php

use yii\db\Migration;

class m160322_133324_service_type_attribute_add_icon extends Migration
{
    public function up()
    {
        $this->addColumn('service_type_attribute', 'icon', 'VARCHAR(255)');
    }

    public function down()
    {
    }
}
