<?php

use yii\db\Migration;

class m160318_154207_service_add_type_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%service}}', 'type_id', 'INTEGER');
    }

    public function down()
    {
        $this->dropColumn('{{%service}}', 'type_id');
    }
}
