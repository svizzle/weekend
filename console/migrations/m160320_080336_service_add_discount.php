<?php

use yii\db\Migration;

class m160320_080336_service_add_discount extends Migration
{
    public function up()
    {
        $this->addColumn('service', 'discount', 'varchar(32)');
    }

    public function down()
    {
        $this->dropColumn('service', 'discount');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
