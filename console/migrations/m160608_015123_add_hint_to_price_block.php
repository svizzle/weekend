<?php

use yii\db\Migration;

/**
 * Handles adding hint to table `price_block`.
 */
class m160608_015123_add_hint_to_price_block extends Migration
{
    public $tableName = '{{%forms_settings}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->batchInsert($this->tableName,
            ['form', 'field_name', 'label', 'hint', 'placeholder'],[
                [2, 'price_block_header', '', 'Hint', '']
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'price_block_header']);
        return true;
    }
}
