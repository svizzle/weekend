<?php

use yii\db\Migration;

class m160427_185128_favorite_service extends Migration
{
    public $tableName = '{{%favorite_service}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'service_id'    => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',            
        ], $tableOptions);

        $this->addForeignKey('favorite_service_service_id', $this->tableName, 'service_id', 'service', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('favorite_service_user_id', $this->tableName, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
