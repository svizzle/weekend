<?php

use yii\db\Migration;

class m160416_231955_service_type_group_user_id extends Migration
{
    public function up()
    {
        $this->addColumn('service_type_group', 'user_id', 'INTEGER DEFAULT NULL');
        $this->addForeignKey('service_type_group_user_id', 'service_type_group', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropColumn('service_type_group', 'user_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
