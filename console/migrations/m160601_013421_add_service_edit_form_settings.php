<?php

use yii\db\Migration;

class m160601_013421_add_service_edit_form_settings extends Migration
{
    public $tableName = '{{%forms_settings}}';

    public function up()
    {
        $this->batchInsert($this->tableName, ['form', 'field_name', 'label', 'hint'],[
                [2, 'name', 'Наименование услуги', ''],
                [2, 'type_id', 'Вид услуги', ''],
                [2, 'description', 'Описание услуги', ''],
                //[2, '', '', ''],
            ]
        );
    }

    public function down()
    {
        $this->delete($this->tableName, ['form'=>2]);
        return true;
    }
}
