<?php

use yii\db\Migration;

use common\models\Page;

class m160507_214123_page_restore extends Migration
{
    public function up()
    {
        $model = new Page([
            'name' => 'Восстановление пароля',
            'slug' => 'restore-password',
            'text' => '<p>Текст о восстановлении пароля</p>',
        ]);
        $model->save();

    }

    public function down()
    {
        echo "m160507_214123_page_restore cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
