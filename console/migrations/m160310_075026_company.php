<?php

use yii\db\Migration;

class m160310_075026_company extends Migration
{
    public $tableName = '{{%company}}';

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name'           => $this->string()->notNull(),
            'phone'                => 'VARCHAR(64) NULL',
            'lat'                  => 'VARCHAR(255) NULL',
            'lon'                  => 'VARCHAR(255) NULL',
            'geo_location_id'      => $this->integer(),
            'address'              => 'VARCHAR(255) NULL',
            'type_id'              => $this->integer()->notNull(),
            'description'          => 'LONGTEXT NULL',
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('company_user_id', $this->tableName, 'user_id', '{{%user}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('company_user_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
