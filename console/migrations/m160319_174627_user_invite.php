<?php

use yii\db\Migration;

class m160319_174627_user_invite extends Migration
{
    public $tableName = "{{%user_invite}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'contract_number' => $this->string(),
            'code'  => $this->string(),
            'comment'    => 'TEXT',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('user_invite_user_id', $this->tableName, 'user_id', '{{%user}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('user_invite_user_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
