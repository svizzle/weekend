<?php

use yii\db\Migration;

class m160319_173322_user_add_enabled extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'enabled', 'BOOLEAN NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('user', 'enabled');
    }
}
