<?php

use yii\db\Migration;

class m160318_213643_company_phone extends Migration
{
    public $tableName = "{{%company_phone}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'phone'      => $this->string()->notNull(),
            'comment'    => 'TEXT',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('company_phone_company_id', $this->tableName, 'company_id', '{{%company}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('company_phone_company_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
