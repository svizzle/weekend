<?php

use yii\db\Migration;

class m160321_071216_service_price_date extends Migration
{
    public function up()
    {
        $this->addColumn('service_price', 'date', 'boolean');
        

    }

    public function down()
    {
        $this->dropColumn('service_price', 'date');
    }

}
