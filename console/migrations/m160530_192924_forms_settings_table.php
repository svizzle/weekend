<?php

use yii\db\Migration;

class m160530_192924_forms_settings_table extends Migration
{
    public $tableName = '{{%forms_settings}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'form' => $this->integer()->notNull(),
            'field_name' => $this->binary(),
            'label'=> $this->string(255)->defaultValue('Label'),
            'hint'=>$this->string(255),
            'default'=>$this->string(255),
            'placeholder'=>$this->string(255)

        ], $tableOptions);

        $this->createIndex('form_type_index', $this->tableName, 'form');

        $this->batchInsert($this->tableName, ['form', 'field_name', 'label', 'hint'],[
                [1,'name', 'Наименование компании', ''],
                [1, 'sight', 'Достопримечательность', ''],
                [1, 'type_id', 'Вид компании', ''],
                [1, 'description', 'Описание компании', ''],
                [1, 'contacts', 'Текст в окне контактов', ''],
                [1, 'phone', 'Номер телефона', ''],
                [1, 'comment', 'Комментарий', ''],
                [1, 'region_id', 'Регион', 'hint'],
                [1, 'geo_location_id', 'Населенный пункт', ''],
                [1, 'address', 'Адрес', ''],
                [1, 'photos', 'Фотографии', ''],
                [1, 'fio', 'ФИО сотрудника', ''],
                [1, 'bottom_phone', 'Номер телефона', ''],
                [1, 'bottom_comment', 'Комментарий', 'hint'],
                [1, 'contacts_phones_block', 'Блок с номерами в контактах', ''],
                [1, 'workers_phones', 'Блок с ФИО сотрудников', ''],
                //[1, '', '', 'hint'],
            ]
        );
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
