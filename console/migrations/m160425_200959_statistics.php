<?php

use yii\db\Migration;

class m160425_200959_statistics extends Migration
{
    public $tableName = '{{%statistics}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'type_id'    => $this->integer()->notNull(),
            'service_id' => $this->integer(),
            'user_id'    => $this->integer(),
            'ip'         => 'VARCHAR(15)',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey('statistics_service_id', $this->tableName, 'service_id', 'service', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('statistics_user_id', $this->tableName, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        echo "m160425_200959_statistics cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
