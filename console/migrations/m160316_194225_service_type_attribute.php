<?php

use yii\db\Migration;

class m160316_194225_service_type_attribute extends Migration
{
    public $tableName = "{{%service_type_attribute}}";

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'service_type_id' => $this->integer()->notNull(),
            'name'            => $this->string()->notNull(),
            'type_id'         => $this->integer()->notNull(),
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('service_type_attribute_service_type_id', $this->tableName, 'service_type_id', '{{%service_type}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('service_type_attribute_service_type_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
