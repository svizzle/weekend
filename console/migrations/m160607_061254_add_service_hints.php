<?php

use yii\db\Migration;

class m160607_061254_add_service_hints extends Migration
{
    public $tableName = '{{%forms_settings}}';

    public function up()
    {
        $this->batchInsert($this->tableName,
            ['form', 'field_name', 'label', 'hint', 'placeholder'],[
                [2, 'date', 'Дата', '', ''],
                [2, 'days', 'Дни недели', '', ''],
                [2, 'time', 'Время', '', ''],
                [2, 'dimension', 'Ед.измерения', '', ''],
                [2, 'price', 'Цена', '', ''],
                [2, 'discount', 'Скидка, %', '', ''],
                [2, 'comment', 'Комментарий', '', ''],
                [2, 'photo', 'Фотографии', '', ''],
            ]
        );
    }

    public function down()
    {
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'date']);
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'days']);
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'time']);
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'dimension']);
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'price']);
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'discount']);
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'comment']);
        $this->delete($this->tableName, ['form'=>2, 'field_name'=>'photo']);


        return true;
    }
}
