<?php

use yii\db\Migration;

class m160427_211737_advertise extends Migration
{
    public $tableName = '{{%advertise}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'type_id'    => $this->integer()->notNull(),
            'position'    => $this->integer()->notNull(),
            'name'       => $this->string()->notNull(),
            'description' => 'LONGTEXT NULL',
            'image'       => $this->string(),
            'link'        => $this->string(),
            'code'        => 'LONGTEXT NULL',
            'region_id'  => $this->integer(),
            'location_id' => $this->integer(),
            'service_type_id' => $this->integer(),
            'radius'      => $this->integer(),
            'begin_date'  => 'DATE NULL',
            'end_date'    => 'DATE NULL',
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',            
        ], $tableOptions);

        $this->addForeignKey('advertise_region_id', $this->tableName, 'region_id', '{{%geo_region}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('advertise_location_id', $this->tableName, 'location_id', '{{%geo_location}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('advertise_service_type_id', $this->tableName, 'service_type_id', '{{%service_type}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160427_211737_advertise cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
