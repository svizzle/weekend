<?php

use yii\db\Migration;

class m160429_105638_service_compilation_to_service extends Migration
{
    public $tableName = 'service_compilation_to_service';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'compilation_id' => $this->integer(),
            'service_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('sc2s_compilation_id', $this->tableName, 'compilation_id', '{{%service_compilation}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('sc2s_service_id', $this->tableName, 'service_id', '{{%service}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160429_105638_service_compilation_to_service cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
