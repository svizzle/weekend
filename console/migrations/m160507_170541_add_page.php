<?php

use yii\db\Migration;

use common\models\Page;

class m160507_170541_add_page extends Migration
{
    public function up()
    {
        $model = new Page([
            'name' => 'Добавить услугу',
            'slug' => 'add-service',
            'text' => '<p>Добавить услугу</p>',
        ]);
        $model->save();
    }

    public function down()
    {
        echo "m160507_170541_add_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
