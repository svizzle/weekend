<?php

use yii\db\Migration;

class m160425_083818_delete_service_type_to_group extends Migration
{
    public function up()
    {
        $this->dropTable('service_type_to_group');
    }

    public function down()
    {
        echo "m160425_083818_delete_service_type_to_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
