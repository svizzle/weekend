<?php

use yii\db\Migration;

use common\models\Setting;

class m160516_072442_more_settings extends Migration
{
    public function up()
    {
        $setting = new Setting([
            'name' => 'frontpage_static_banner',
            'value' => '',
        ]);
        $setting->save();
        $setting = new Setting([
            'name' => 'frontpage_dynamic_banner',
            'value' => '',
        ]);
        $setting->save();
    }

    public function down()
    {
        echo "m160516_072442_more_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
