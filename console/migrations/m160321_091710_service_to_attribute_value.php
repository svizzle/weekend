<?php

use yii\db\Migration;

class m160321_091710_service_to_attribute_value extends Migration
{
    public $tableName = 'service_to_attribute_value';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer()->notNull(),
            'value_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('s2av_service_id', $this->tableName, 'service_id', '{{%service}}', 'id', 'CASCADE');
        $this->addForeignKey('s2av_value_id', $this->tableName, 'value_id', '{{%service_type_attribute_value}}', 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m160321_091710_service_to_attribute_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
