<?php

use yii\db\Migration;

class m160318_081825_user_add_contract_number extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'contract_number', 'VARCHAR(255) NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'contract_number');
    }
}
