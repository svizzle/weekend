<?php

use yii\db\Migration;

class m160318_222456_company_employee extends Migration
{
    public $tableName = '{{%company_employee}}';

    public function up()
    {

        $this->dropColumn('{{%company}}', 'phone');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name'       => $this->string()->notNull(),
            'phone'      => $this->string()->notNull(),
            'comment'    => 'TEXT',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('company_employee_company_id', $this->tableName, 'company_id', '{{%company}}', 'id', 'CASCADE');
    }

    public function down()
    {
    }
}
