<?php

use yii\db\Migration;

class m160601_183439_users_add_location_id_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'location_id', 'int(11) null');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'location_id');
        return true;
    }
}
