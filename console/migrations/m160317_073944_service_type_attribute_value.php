<?php

use yii\db\Migration;

class m160317_073944_service_type_attribute_value extends Migration
{
    public $tableName = "{{%service_type_attribute_value}}";

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'service_type_attribute_id' => $this->integer()->notNull(),
            'name'            => $this->string()->notNull(),
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('service_type_attribute_value_service_type_attribute_id', $this->tableName, 'service_type_attribute_id', '{{%service_type_attribute}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('service_type_attribute_value_service_type_attribute_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
