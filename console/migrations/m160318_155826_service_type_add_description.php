<?php

use yii\db\Migration;

class m160318_155826_service_type_add_description extends Migration
{
    public function up()
    {
        $this->addColumn('{{%service_type}}', 'description', 'TEXT');
    }

    public function down()
    {
        $this->dropColumn('{{%service_type}}', 'description');
    }
}
