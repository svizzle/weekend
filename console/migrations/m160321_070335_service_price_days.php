<?php

use yii\db\Migration;

class m160321_070335_service_price_days extends Migration
{
    public function up()
    {
        $this->dropColumn('service_price', 'type_id');
        $this->addColumn('service_price', 'd1', 'BOOLEAN');
        $this->addColumn('service_price', 'd2', 'BOOLEAN');
        $this->addColumn('service_price', 'd3', 'BOOLEAN');
        $this->addColumn('service_price', 'd4', 'BOOLEAN');
        $this->addColumn('service_price', 'd5', 'BOOLEAN');
        $this->addColumn('service_price', 'd6', 'BOOLEAN');
        $this->addColumn('service_price', 'd7', 'BOOLEAN');
    }

    public function down()
    {
    }
}
