<?php

use yii\db\Migration;

class m160508_011613_article_service_type_id extends Migration
{
    public function up()
    {
        $this->addColumn('article', 'service_type_id', 'INT(11) DEFAULT NULL');
        $this->addForeignKey('article_service_type_id', 'article', 'service_type_id', 'service_type', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('article_service_type_id', 'article', 'service_type_id');
        $this->dropColumn('article', 'service_type_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
