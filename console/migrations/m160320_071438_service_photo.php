<?php

use yii\db\Migration;

class m160320_071438_service_photo extends Migration
{
    public $tableName = "{{%service_photo}}";

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'service_id' => $this->integer(),
            'user_id' => $this->integer()->notNull(),
            'path'       => $this->string()->notNull(),
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NULL',
        ], $tableOptions);

        $this->addForeignKey('service_photo_service_id', $this->tableName, 'service_id', '{{%service}}', 'id', 'CASCADE');
        $this->addForeignKey('service_photo_user_id', $this->tableName, 'user_id', '{{%user}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('service_photo_service_id', $this->tableName);
        $this->dropForeignKey('service_photo_user_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
