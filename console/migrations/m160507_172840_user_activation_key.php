<?php

use yii\db\Migration;

class m160507_172840_user_activation_key extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'activation_token', 'varchar(255) default null');
    }

    public function down()
    {
        echo "m160507_172840_user_activation_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
