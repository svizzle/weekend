<?php

use yii\db\Migration;

class m160318_170152_company_type_add_description extends Migration
{
    public function up()
    {
        $this->addColumn('{{%company_type}}', 'description', 'TEXT');
    }

    public function down()
    {
        $this->dropColumn('{{%company_type}}', 'description');
    }
}
