<?php

use yii\db\Migration;

use common\models\Company;
use common\models\Service;

class m160513_191323_service_sluggable extends Migration
{
    public function up()
    {
        $this->addColumn('{{%service}}', 'slug', $this->string());
        $this->addColumn('{{%company}}', 'slug', $this->string());

        foreach(Company::find()->all() as $company) $company->save();
        foreach(Service::find()->all() as $service) $service->save();
    }

    public function down()
    {
        $this->dropColumn('{{%service}}', 'slug');
        $this->dropColumn('{{%company}}', 'slug');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
