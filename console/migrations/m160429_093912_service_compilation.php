<?php

use yii\db\Migration;

class m160429_093912_service_compilation extends Migration
{
    public $tableName = '{{%service_compilation}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id'         => $this->primaryKey(),
            'service_type_id'    => $this->integer()->notNull(),
            'created_at'           => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at'           => 'TIMESTAMP NULL',            
        ], $tableOptions);

        $this->addForeignKey('service_compilation_service_type_id', $this->tableName, 'service_type_id', 'service_type', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160429_093912_service_compilation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
