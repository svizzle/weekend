<?php

namespace common\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\repositories\FormsSettingsRepository;

/**
 * FormsSettingsSearch represents the model behind the search form about `common\repositories\FormsSettingsRepository`.
 */
class FormsSettingsSearch extends FormsSettingsRepository
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'form'], 'integer'],
            [['field_name', 'label', 'hint', 'default', 'placeholder'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormsSettingsRepository::find()->indexBy('id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'form' => $this->form,
        ]);

        $query->andFilterWhere(['like', 'field_name', $this->field_name])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'hint', $this->hint])
            ->andFilterWhere(['like', 'default', $this->default])
            ->andFilterWhere(['like', 'placeholder', $this->placeholder]);

        return $dataProvider;
    }
}
