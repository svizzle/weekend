<?php

namespace common\validators;

use common\models\Company;
use common\models\Service;

class Id extends \yii\validators\Validator
{
    public function validateAttribute($model, $attribute)
    {
        // Первые две цифры ID это регион
        // Следующие 4 цифры это номер компании в пределах региона
        // 1. ID должен быть уникальным в пределах компаний и услуг
        // 2. ID длиной в 8 символов
        // 3. У компаний ID заканчивается на 00
        // 4. У услуг первые 6 цифр должны быть как у компании
        // 5. Если ID услуги не задан, то его нужно сгенерировать. Для компаний ID обязателен
        // 6. У новых приглашений ID не должен совпадать с ID существующих компаний и ID уже приглашенных

        $value = $model->{$attribute};

        // default validator
        if (empty($value))
        {
            if ($model instanceof Company)
            {
                // Ошибка. ID обязателен
            }
            else
            {
                // Генерируем ID для услуги
                $value = $this->generateId($model);
                return;
            }
        }
        else
        {
            // Проверка на длину
            if (strlen($value) != 8)
            {
                return $this->addError($model, $attribute, 'ID должен быть длиной в 8 символов');
            }

            // Проверка на уникальность

        }        
    }

    private function generateId($model)
    {

    }
}
