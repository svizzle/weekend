<?php

use yii\helpers\Html;
use yii\helpers\Url;

echo Html::a('Ссылка для подтверждения адреса почты', Url::to(['/user/email-confirm', 'token' => $model->activation_token], true));

