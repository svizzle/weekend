<?php

use yii\helpers\Html;

$url = Yii::$app->frontendUrlManager->createAbsoluteUrl(['/user/register', 'code' => $model->code]);

if (!empty($model->text))
{
    echo '<p>'.$model->text.'</p>';
}

echo '<p>'.Html::a($url, $url).'</p>';

