<?php

use yii\helpers\Html;
use yii\helpers\Url;

echo Html::a('Ссылка для восстановления пароля', Url::to(['/user/reset', 'token' => $model->password_reset_token], true));

