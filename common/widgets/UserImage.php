<?php

namespace common\widgets;

class UserImage extends \common\widgets\Image
{
    public $baseUrl = '/uploads/users/';

    public function getSource()
    {
        if (empty($this->model->photo)) return null;
        return $this->model->photo;
    }
}
