<?php

namespace common\widgets;

class ServiceImage extends \common\widgets\Image
{
    public $baseUrl = '/uploads/services/';

    public function getSource()
    {
        if (!$this->model) return null;
        return $this->model->path;
    }
}
