<?php

namespace common\widgets;

class ServiceTypeAttributeValueImage extends \common\widgets\Image
{
    public $width = 24;
    public $height = 21;
    public $baseUrl = "/uploads/service_type_attribute_values/";

    public function init()
    {
        parent::init();

        if (!empty($this->model->icon_title)) $this->htmlOptions['title'] = $this->model->icon_title;
    }

    public function getSource()
    {
        if (empty($this->model->icon)) return null;
        return $this->model->icon;
    }
}
