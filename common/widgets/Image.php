<?php

namespace common\widgets;

use Yii;
use yii\helpers\Html;

class Image extends \yii\base\Widget
{
    public $model;

    public $width = false;
    public $height = false;

    public $htmlOptions = [];

    public $baseUrl = "/uploads/";

    public function run()
    {
        $src = $this->getSource();
        if (!$src)
        {
            $src = $this->getDefaultSource();
        }
        elseif(!file_exists($this->getBasePath().$src))
        {
            $src = $this->getDefaultSource();
        }
        else
        {

            if ($this->width === false && $this->height === false)
            {
                $src = $this->baseUrl.$src;
            }
            else
            {
                $directory = $this->width."x".$this->height;

                // Проверяем есть ли папка для изображений такого размера
                if (!is_dir($this->getBasePath().$directory))
                {
                    mkdir($this->getBasePath().$directory);
                }                

                // Проверяем есть ли уже такой файл
                if (!file_exists($this->getBasePath().$directory."/".$src))
                {
                    \yii\imagine\Image::thumbnail($this->getBasePath().$src, $this->width, $this->height)
                        ->save($this->getBasePath().$directory."/".$src, ['quality' => 90]);
                }
                $src = $this->baseUrl.$directory."/".$src;
            }
        }

        return Html::img($src, $this->htmlOptions);
    }

    public function getSource()
    {
        return null; 
    }

    public function getDefaultSource()
    {
        return "https://placeholdit.imgix.net/~text?txt=No+photo&w=".$this->width."&h=".$this->height."&fm=png&txttrack=1";
    }

    public function getBasePath()
    {
        return Yii::getAlias('@frontend').'/web'.$this->baseUrl;
    }
}
