<?php

namespace common\widgets;

use common\models\Advertise;

class AdvertiseImage extends \common\widgets\Image
{
    public $baseUrl = '/uploads/context/';

    public function init()
    {
        parent::init();

        if ($this->model->position == Advertise::POSITION_CONTENT)
        {
            $this->width = 750;
            $this->height = 220;
        }
        else
        {
            $this->width = 222;
            $this->height = 550;
        }
    }

    public function getSource()
    {
        if (!$this->model) return null;
        return $this->model->image;
    }
}
