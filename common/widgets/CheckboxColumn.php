<?php

namespace common\widgets;

class CheckboxColumn extends \yii\grid\CheckboxColumn
{
    public $relatedSelector = '.btn[disabled]';

    public function init()
    {
        parent::init();

        $this->grid->view->registerJs("            
            (function(){

                var relatedElements = $('{$this->relatedSelector}');

                relatedElements.click(function(e){
                    
                    if ($(this).attr('disabled'))
                    {
                        e.preventDefault();
                        return false;
                    }

                });

                $('#{$this->grid->id}').find(\"[name='selection[]'],[name=selection_all]\").change(function(){
                    var ids = $('#{$this->grid->id}').yiiGridView('getSelectedRows');
                    relatedElements.attr('disabled', ids.length == 0);
                });

            })();
        ");
    }
}
