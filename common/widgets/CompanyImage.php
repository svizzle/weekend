<?php

namespace common\widgets;

class CompanyImage extends \common\widgets\Image
{
    public $baseUrl = '/uploads/companies/';

    public function getSource()
    {
        if (!$this->model) return null;
        return $this->model->path;
    }
}
