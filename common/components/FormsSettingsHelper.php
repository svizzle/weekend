<?php

namespace common\components;


use common\repositories\FormsSettingsRepository;

class FormsSettingsHelper
{
    private static $data = null;

    private static function loadSettingsFromDB(){
        if (self::$data) return;
        $rows = FormsSettingsRepository::getFormsSettings();

        foreach ($rows as $row){
            foreach ($row as $key=>$value){
                self::$data[$row['form']][$row['field_name']][$key] = $value;
            }
        }
        // var_dump( self::$data); die();
        return true;
    }

    /**
     * Возвращет настройки поля по id формы и системному названию поля
     * @param $form
     * @param $field_name
     * @return string
     */
    static function getField($form, $field_name, $setting_name){
        self::loadSettingsFromDB();

        if (array_key_exists($form, self::$data)){
            if (array_key_exists($field_name, self::$data[$form])){
                if (array_key_exists($setting_name, self::$data[$form][$field_name])){
                    return self::$data[$form][$field_name][$setting_name];
                }else{
                    return 'Field setting not found';
                }
            }else{
               // var_dump(self::$data[$form]['name']['hint']); die();
                return 'Field not found';
            }
        }else{
            return 'Form settings not found';
        }
    }
}