<?php

namespace common\url\rules;

class Company extends \yii\base\Object implements \yii\web\UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'company/view')
        {
            if (isset($params['id']))
            {
                if ($model = \common\models\Company::findOne($params['id']))
                {
                    return $model->slug;
                }
            }
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        if (preg_match('/^([\w_-]+)$/', $request->pathInfo, $matches))
        {
            $company = \common\models\Company::find()->andWhere(['slug' => $matches[1]])->one();
            if (!$company) return false;

            return ['company/view', ['id' => $company->id]];
        }
        return false;
    }
}
