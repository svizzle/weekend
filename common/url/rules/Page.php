<?php

namespace common\url\rules;

class Page extends \yii\base\Object implements \yii\web\UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'page/view')
        {
            if (isset($params['id']))
            {
                if ($model = \common\models\Page::findOne($params['id']))
                {
                    return $model->slug;
                }
            }
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        if (preg_match('/^([\w_-]+)$/', $request->pathInfo, $matches))
        {
            $page = \common\models\Page::find()->andWhere(['slug' => $matches[1]])->one();
            if (!$page) return false;

            return ['page/view', ['id' => $page->id]];
        }
        return false;
    }
}
