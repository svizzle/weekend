<?php

namespace common\url\rules;

class Service extends \yii\base\Object implements \yii\web\UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'service/view')
        {
            if (isset($params['id']))
            {                
                if ($model = \common\models\Service::findOne($params['id']))
                {
                    unset($params['id']);
                    $params = http_build_query($params);
                    if (strlen($params)) $params = "?".$params;
                    return $model->company->slug.'/'.$model->slug.$params;
                }
            }
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        if (preg_match('/^([\w_-]+)\/([\w_-]+)\??/', $request->pathInfo, $matches))
        {
            $company = \common\models\Company::find()->andWhere(['slug' => $matches[1]])->one();
            if (!$company) return false;

            $service = \common\models\Service::find()->andWhere([
                'company_id' => $company->id,
                'slug' => $matches[2],
            ])->one();

            if ($service)
            {
                return ['service/view', ['id' => $service->id]];
            }
        }
        return false;
    }
}
