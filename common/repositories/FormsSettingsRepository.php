<?php
/**
 * Created by PhpStorm.
 * User: alt
 * Date: 5/30/16
 * Time: 11:38 PM
 */

namespace common\repositories;


use common\models\FormsSettings;

class FormsSettingsRepository extends FormsSettings
{
    const FORM_COMPANY = 1;
    const FORM_EDIT_SERVICE = 2;
    const FORM_CABINET_REG = 3;

    static $forms_list = [
        1 => 'Сведения о компании',
        2 => 'Редактирование услуги',
        3 => 'Регистрация (в cabinet)'
    ];

    function rules(){
        return array_merge(parent::rules(), [

        ]);
    }

    function attributeLabels(){
        return array_merge(parent::attributeLabels(), [
            'hint'=>'Подсказка',
            'form'=>'Форма',
            'field_name'=>'Системное название поля'
        ]);
    }

    function getFormName(){
        return array_key_exists($this->form, self::$forms_list)?self::$forms_list[$this->form]:'unknown with id: ' . intval($this->form);
    }

    public static function getFormsSettings(){
        return self::find()->asArray()->all();
    }
}