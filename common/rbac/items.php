<?php
return [
    'user' => [
        'type' => 1,
        'ruleName' => 'userRole',
    ],
    'partner' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'user',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'partner',
        ],
    ],
];
