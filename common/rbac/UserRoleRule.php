<?php

namespace common\rbac;

use common\models\User;

class UserRoleRule extends \yii\rbac\Rule
{
    public $name = 'userRole';

    public function execute($user, $item, $params)
    {
        $model = User::findOne($user);
        if (!$model) return false;

        if ($item->name === 'admin')
        {
            return $model->role === 'admin';
        }
        elseif ($item->name === 'partner')
        {
            return in_array($model->role, ['admin', 'partner']);
        }
        elseif ($item->name === 'user')
        {
            return in_array($model->role, ['admin', 'partner', 'user']);
        }

        return false;
    }
    
}
