<?php

namespace common\forms;

use Yii;

class UserChangePassword extends \yii\base\Model
{
    public $isNewRecord = false;

    public $old_password;
    public $password;
    public $password_confirmation;

    public function rules()
    {
        return [
            ['old_password', 'required', 'when' => function($model) {
                if ($model->isNewRecord) return false;
                return !empty($model->password) || !empty($model->password_confirmation);
            }],
            ['password', 'required', 'when' => function($model){
                if ($this->isNewRecord) return true;
                return !empty($model->old_password) || !empty($model->password_confirmation);
            }],
            ['password_confirmation', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],
            ['old_password', 'validateOldPassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'old_password' => 'Старый пароль',
            'password' => 'Новый пароль',
            'password_confirmation' => 'Подтверждение пароля',
        ];
    }

    public function validateOldPassword()
    {
        $user = Yii::$app->user->identity;
        if (!$user->validatePassword($this->old_password))
        {
            $this->addError('old_password', 'Пароль указан не верно');
        }
    }
}
