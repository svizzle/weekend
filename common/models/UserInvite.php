<?php

namespace common\models;

use Yii;

use alexeevdv\sms\ru\Sms;

use common\models\User;
use common\models\UserInvite;

class UserInvite extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%user_invite}}';
    }

    public function rules()
    {
        return [
            ['contract_number', 'required'],
            ['contract_number', 'validateContractNumber'],

            ['code', 'default', 'value' => $this->generateCode()],
            ['code', 'validateCode'],

            ['phone', 'default'],
            ['phone_token', 'default'],
            ['phone_status', 'default'],
            ['text', 'default'],
            ['email', 'default'],
            ['email', 'email'],

            ['user_id', 'default', 'value' => null],
            ['comment', 'default', 'value' => null],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'contract_number' => 'Номер договора',
            'user_id' => 'Зарегистрированный пользователь',
            'code' => 'Код регистрации',
            'comment' => 'Комментарий',
            'text' => 'Текст сообщения',
            'email' => 'E-mail',
            'phone' => 'Телефон',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function validateCode()
    {
        if (empty($this->code)) $this->code = $this->generateCode();

        while($invite = UserInvite::findOne(['code' => $this->code]))
        {
            $this->code = $this->generateCode();
        }
    }

    protected function generateCode()
    {
        return substr(md5(rand().time()), 0, 8);
    }

    public function validateContractNumber()
    {
        if ($user = User::findOne(['contract_number' => $this->contract_number]) || $invite = UserInvite::findOne(['contract_number' => $this->contract_number]))
        {
            $this->addError('contract_number', 'Номер договора уже используется');
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert)
        {
            if (!empty($this->phone))
            {
                $response = Yii::$app->sms->send(new Sms([
                    'to' => $this->phone,
                    'text' => $this->text." ".Yii::$app->frontendUrlManager->createAbsoluteUrl(['/user/register', 'code' => $this->code]),
                ]));

                if ($response->code == 100)
                {
                    $this->phone_token = $response->data;
                    $this->save(false, ['phone_token']);
                }
            }

            if (!empty($this->email))
            {
                Yii::$app->mailer->compose('user_invite', ['model' => $this])
                     ->setTo($this->email)
                     ->setSubject('Приглашение')
                     ->send();
            }

        }
    }


}
