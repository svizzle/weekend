<?php

namespace common\models;

use yii\data\ActiveDataProvider;

class ServiceTypeGroupSearch extends ServiceTypeGroup
{
    public function rules()
    {
        return [
            ['name', 'trim'],
        ];
    }

    public function search($params)
    {
        $query = ServiceTypeGroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) return $dataProvider;

        $this->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }    
}
