<?php

namespace common\models;

use Yii;

class ServicePhoto extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%service_photo}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => 'sjaakp\sortable\Sortable',        
                'orderAttribute' => ['service_id' => 'position'],
            ],
        ];
    }

    public function rules()
    {
        return [
            ['service_id', 'default', 'value' => null],
            ['user_id', 'default', 'value' => Yii::$app->user->id],
            ['path', 'required'],
            ['position', 'default', 'value' => 0],
        ];
    }

}
