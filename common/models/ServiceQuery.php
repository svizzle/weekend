<?php

namespace common\models;

class ServiceQuery extends \yii\db\ActiveQuery
{
    public function enabled()
    {
        return $this->joinWith(['company', 'company.user'])->andWhere([
            'service.enabled' => true,
            'company.enabled' => true,
            'company.moderated' => true,
            'user.enabled' => true,
        ]);
    }
}
