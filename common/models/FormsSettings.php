<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "forms_settings".
 *
 * @property integer $id
 * @property integer $form
 * @property resource $field_name
 * @property string $label
 * @property string $hint
 * @property string $default
 * @property string $placeholder
 */
class FormsSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forms_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form'], 'required'],
            [['form'], 'integer'],
            [['field_name'], 'string'],
            [['label', 'hint', 'default', 'placeholder'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'form' => 'Form',
            'field_name' => 'Field Name',
            'label' => 'Label',
            'hint' => 'Hint',
            'default' => 'Default',
            'placeholder' => 'Placeholder',
        ];
    }
}
