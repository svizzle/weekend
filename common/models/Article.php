<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $short_text
 * @property string $full_text
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ArticleServiceTypes[] $articleServiceTypes
 */
class Article extends \yii\db\ActiveRecord
{
    public $service_types_input;

    public static function tableName()
    {
        return '{{%article}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['short_text', 'full_text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
            ['short_text', 'required'],
            ['full_text', 'default', 'value' => null],
            ['service_types_input', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'service_type_id' => 'Вид услуг',
            'name' => 'Название',
            'slug' => 'URL адрес',
            'short_text' => 'Краткое описание',
            'full_text' => 'Полный текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleServiceTypes()
    {
        return $this->hasMany(ArticleServiceTypes::className(), ['article_id' => 'id']);
    }
}
