<?php

namespace common\models;

class CompanyPhone extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%company_phone}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
//            ['company_id', 'required'],
            ['phone', 'required'],
            ['comment', 'default', 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'comment' => 'Комментарий',
        ];
    }
}

