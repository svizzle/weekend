<?php

namespace common\models;

use Yii;

class Company extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%company}}';
    }

    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['type_id', 'required'],
            ['name', 'required'],
            [['lat','lon'], 'default', 'value' => null],
            ['geo_location_id', 'required'],
            ['address', 'default', 'value' => null],
            ['contacts', 'default', 'value' => null],
            ['description', 'required'],
            ['enabled', 'default', 'value' => 1],
            ['moderated', 'default', 'value' => 0],
            ['sight', 'default', 'value' => 0],
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }    

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'name',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование компании',
            'user_id' => 'Пользователь',
            'type_id' => 'Вид компании',
            'geo_location_id' => 'Населенный пункт',
            'address' => 'Адрес',
            'moderated' => 'Данные компании проверены',
            'description' => 'Описание компании',
            'contacts' => 'Текст в окне контактов',
            'photos' => 'Фотографии',
            'enabled' => 'Статус',
            'sight' => 'Достопримечательность',
        ];
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['company_id' => 'id']);
    }

    public function getGeoLocation()
    {
        return $this->hasOne(GeoLocation::className(), ['id' => 'geo_location_id']);
    }

    public function getType()
    {
        return $this->hasOne(CompanyType::className(), ['id' => 'type_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getPhones()
    {
        return $this->hasMany(CompanyPhone::className(), ['company_id' => 'id']);
    }

    public function getEmployee()
    {
        return $this->hasMany(CompanyEmployee::className(), ['company_id' => 'id']);
    }

    public function getPhotos()
    {
        if ($this->isNewRecord)
        {
            return CompanyPhoto::find()->andWhere(['company_id' => null, 'user_id' => Yii::$app->user->id])->orderBy(['position' => SORT_ASC])->all();
        }
        return $this->hasMany(CompanyPhoto::className(), ['company_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getPhoto()
    {
        $photos = $this->photos;
        if (!count($photos)) return null;
        return $photos[0];
    }

    public function getFullAddress()
    {
        return $this->geoLocation->region->name.", ".$this->geoLocation->name.", ".$this->address;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert)
        {
            $photos = CompanyPhoto::find()->andWhere(['company_id' => null, 'user_id' => Yii::$app->user->id])->all();
            foreach($photos as $photo)
            {
                $photo->company_id = $this->id;
                $photo->save();
            }
        }
    }

    public function getRate()
    {
        if (!count($this->services)) return 0;
        $rate = 0;
        foreach($this->services as $service) $rate += $service->rate;
        return round($rate/count($this->services));
    }

    public function getViews()
    {
        return 0;
    }
}
