<?php

namespace common\models;

class ServicePriceUnit extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%service_price_unit}}';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
        ];
    }
}
