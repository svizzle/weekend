<?php

namespace common\models;

use Yii;

use yii\data\ActiveDataProvider;

class UserInviteSearch extends UserInvite
{
    public function rules()
    {
        return [
            [['created_at', 'phone', 'email', 'contract_number', 'comment', 'code', 'user_id'], 'trim'],
        ];
    }

    public function search($params)
    {
        $query = UserInvite::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) return $dataProvider; 

        $query->andFilterWhere(['DATE_FORMAT(created_at, "%d.%m.%Y")' => $this->created_at]);
        $query->andFilterWhere(['like', 'phone', $this->phone]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'contract_number', $this->contract_number]);
        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->andFilterWhere(['like', 'code', $this->code]);

        if ($this->user_id == 1)
        {
            $query->andWhere(['is', 'user_id', null]);
        }
        
        if ($this->user_id == 2) 
        {
            $query->andWhere(['is not', 'user_id', null]);
        }

        return $dataProvider;
    }
}
