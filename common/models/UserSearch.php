<?php
namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public $name;
    public $tarif_id;

    public function rules()
    {
        return [
            [['contract_number', 'name', 'email', 'role', 'tarif_id', 'enabled'], 'trim'],
        ];
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) return $dataProvider;

        $query->andFilterWhere(['like', 'contract_number', $this->contract_number]);
        $query->andFilterWhere(['like', 'CONCAT_WS(" ", last_name, first_name)', $this->name]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['role' => $this->role]);
        $query->andFilterWhere(['enabled' => $this->enabled]);


        return $dataProvider;
    }

}
