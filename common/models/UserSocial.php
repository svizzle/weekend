<?php

namespace common\models;

class UserSocial extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%user_social}}';
    }

    public function getUser()
    {
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }
}
