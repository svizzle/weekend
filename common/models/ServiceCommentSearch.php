<?php

namespace common\models;

use yii\data\ActiveDataProvider;

class ServiceCommentSearch extends ServiceComment
{
    public $serviceName;
    public $userName;
    
    public function rules()
    {
        return [
            [['text', 'serviceName', 'userName', 'rate'], 'trim'],
        ];
    }

    public function search($params)
    {
        $query = ServiceComment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) return $dataProvider;

        $query->joinWith(['service', 'user']);
        $query->andFilterWhere(['like', 'service.name', $this->serviceName]);
        $query->andFilterWhere(['like', 'concat_ws(" ", user.last_name, user.first_name)', $this->userName]);
        $query->andFilterWhere(['like', 'service_comment.text', $this->text]);
        $query->andFilterWhere(['service_comment.rate' => $this->rate]);

        return $dataProvider;
    }
}
