<?php

namespace common\models;

use yii\data\ActiveDataProvider;

class AdvertiseSearch extends Advertise
{
    public function rules()
    {
        return [];
    }

    public function search($params)
    {
        $query = Advertise::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) return $dataProvider;

        return $dataProvider;
    }
}
