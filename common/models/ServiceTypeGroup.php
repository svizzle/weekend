<?php

namespace common\models;

use Yii;

class ServiceTypeGroup extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%service_type_group}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['user_id', 'default', 'value' => Yii::$app->user->can('admin') ? null : Yii::$app->user->id],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
        ];
    }

    public function getValues()
    {
        return $this->hasMany(ServiceTypeAttributeValue::className(), ['id' => 'value_id'])
                    ->viaTable('{{%service_type_group_to_value}}', ['group_id' => 'id']);
    }

    public function getTypes()
    {
        return ServiceType::find()->andWhere(['id' => json_decode($this->tab_ids)])->all();
    }
}
