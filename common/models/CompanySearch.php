<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

class CompanySearch extends Company
{
    public $locationName;
    public $phone;
    public $userContractNumber;
    public $userName;

    public function rules()
    {
        return [
            [['name', 'userName', 'locationName', 'phone', 'type_id', 'userContractNumber', 'enabled', 'moderated'], 'trim'],
        ];
    }

    public function search($params)
    {
        $query = Company::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        if (!($this->load($params) && $this->validate())) return $dataProvider;

        $query->joinWith(['user', 'phones', 'geoLocation', 'geoLocation.region']);

        $query->andFilterWhere(['like', 'user.contract_number', $this->userContractNumber]);

        $query->andFilterWhere(['like', 'company.name', $this->name]);

        $query->andFilterWhere(['type_id' => $this->type_id]);

        $query->andFilterWhere(['like', 'company_phone.phone', $this->phone]);

        $query->andFilterWhere(['like', 'CONCAT_WS(" ",geo_region.name, geo_location.name)', $this->locationName]);

        $query->andFilterWhere(['like', 'CONCAT_WS(" ",user.last_name,user.first_name)', $this->userName]);

        $query->andFilterWhere(['company.enabled' => $this->enabled]);

        $query->andFilterWhere(['company.moderated' => $this->moderated]);
        
        return $dataProvider;
    }
}
