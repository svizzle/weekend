<?php

namespace common\models;

class MenuMain extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%menu_main}}';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['url', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Заголовок',
            'url' => 'Ссылка',
        ];
    }
}
