<?php

namespace common\models;

class GeoRegion extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%geo_region}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['yandex_id', 'default', 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'yandex_id' => 'Яндекс ID',
        ];        
    }

    public function getLocations()
    {
        return $this->hasMany(GeoLocation::className(), ['region_id' => 'id']);
    }
}
