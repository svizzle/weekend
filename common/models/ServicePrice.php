<?php

namespace common\models;

class ServicePrice extends \yii\db\ActiveRecord
{
    const TYPE_DATE = 1;
    const TYPE_WORKDAY = 2;
    const TYPE_HOLIDAY = 3;

    public static function tableName()
    {
        return '{{%service_price}}';
    }

    public function rules()
    {
        return [
            [['date', 'time', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7'], 'default', 'value' => 0],
            [['date', 'time', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7'], 'boolean'],
            
            [['date_begin', 'date_end'], 'date', 'format' => 'php:d.m.Y'],
            [['time_begin', 'time_end'], 'default', 'value' => null],

            ['price', 'default', 'value' => 0],
            ['price', 'required'],
            ['price', 'integer', 'min' => 0],

            ['unit_id', 'required'],

            ['discount', 'default', 'value' => 0],
            ['discount', 'required'],
            ['discount', 'integer', 'min' => 0, 'max' => '100'],

            ['comment', 'default', 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_begin' => 'Начало',
            'type_id' => 'Тип',
            'price' => 'Цена',
            'date_end' => 'Срок',
            'discount' => 'Cкидка, %',
        ];    
    }

    public function getUnit()
    {
        return $this->hasOne(ServicePriceUnit::className(), ['id' => 'unit_id']);
    }

    public function getDays()    
    {
        $values = [];

        for ($i = 1; $i <= 7; $i++)
        {
            if ($this->{'d'.$i} === null || $this->{'d'.$i} === 1)
            {
                $values[] = $i;
            }
        }

        return $values;
    }

    public function afterFind()
    {
        parent::afterFind();

        if ($this->date_begin !== null)
        {
            $this->date_begin = date('d.m.Y', strtotime($this->date_begin));
        }
        if ($this->date_end !== null)
        {
            $this->date_end = date('d.m.Y', strtotime($this->date_end));
        }
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if (!empty($this->date_begin))
        {
            $this->date_begin = date_create_from_format('d.m.Y', $this->date_begin)->format('Y-m-d');
        }
        if (!empty($this->date_end))
        {
            $this->date_end = date_create_from_format('d.m.Y', $this->date_end)->format('Y-m-d');
        }

        return true;
    }

    public static function validateMultiple($models, $attributeNames = null)
    {
        $valid = parent::validateMultiple($models, $attributeNames);

        foreach($models as $model)
        {
            $valid = static::validateMultipleByModel($models, $model) && $valid;
        }

        return $valid;
    }

    public static function validateMultipleByModelDate($models, $model)
    {
        foreach($models as $other)
        {
            if ($other == $model) continue;

            // У одной из дат не задан диапазон - значит пересечение
            if (!$model->date || !$other->date) return false;

            $modelDateBegin = $model->date_begin ? strtotime($model->date_begin) : 0;
            $modelDateEnd = $model->date_end ? strtotime($model->date_end) : strtotime("2100-01-01");

            $otherDateBegin = $other->date_begin ? strtotime($other->date_begin) : 0;
            $otherDateEnd   = $other->date_end ?   strtotime($other->date_end) : strtotime("2100-01-01");

//            if ($modelDateBegin 



        }
        return true;
    }
}
