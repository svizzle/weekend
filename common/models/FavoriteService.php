<?php

namespace common\models;

class FavoriteService extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%favorite_service}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getService()
    {
        return $this->hasOne(Service::className, ['id' => 'service_id']);
    }
}
