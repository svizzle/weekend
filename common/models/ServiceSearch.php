<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

class ServiceSearch extends Service
{
    public $companyName;
    public $userName;

    public function rules()
    {
        return [
            [['code', 'name', 'type_id', 'userName', 'companyName', 'enabled'], 'trim'],
        ];
    }

    public function search($params)
    {
        $query = Service::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) return $dataProvider;

        $query->joinWith(['company', 'company.user']);

        $query->andFilterWhere(['like', 'service.code', $this->code]);
        $query->andFilterWhere(['like', 'service.name', $this->name]);
        $query->andFilterWhere(['service.type_id' => $this->type_id]);
        $query->andFilterWhere(['like', 'company.name', $this->companyName]);
        $query->andFilterWhere(['like', 'CONCAT_WS(" ",user.last_name, user.first_name)', $this->userName]);
        $query->andFilterWhere(['service.enabled' => $this->enabled]);

        return $dataProvider;
    }
}
