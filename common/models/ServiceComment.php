<?php

namespace common\models;

use Yii;

class ServiceComment extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%service_comment}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [            
            ['service_id', 'required'],
            ['user_id', 'default', 'value' => Yii::$app->user->id],
            ['text', 'required'],
            ['text', 'filter', 'filter' => 'strip_tags'],
            ['rate', 'required'],
            ['rate', 'in', 'range' => [1, 2, 3, 4, 5]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
            'rate' => 'Оценка',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }
}
