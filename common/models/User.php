<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $photo_file;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public static function find()
    {
        return new UserQuery(get_called_class());
    }    

    public function rules()
    {
        return [
            ['email', 'email'],
            ['email', 'unique'],
            ['phone', 'default', 'value' => null],
            [['first_name', 'last_name', 'father_name'], 'default'],
            ['birthday', 'date', 'format' => 'php:d.m.Y', 'timestampAttribute' => 'birthday', 'timestampAttributeFormat' => 'php:Y-m-d'],
            ['contract_number', 'default', 'value' => function($model) {
                $user = User::find()->orderBy(['contract_number' => SORT_DESC])->one();
                return $user->contract_number + 100;
            }],
            ['contract_number', 'unique'], 
            ['auth_key', 'default', 'value' => Yii::$app->security->generateRandomString()],
            ['role', 'default', 'value' => 'user'],
            ['enabled', 'default', 'value' => 1],
            ['photo_file', 'image'],
            ['location_id', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'father_name' => 'Отчество',
            'contract_number' => 'ID пользователя',
            'photo_file' => 'Новое фото',
            'phone' => 'Телефон',
            'birthday' => 'Дата рождения',
            'role' => 'Роль',
            'email' => 'E-mail',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['user_id' => 'id']);
    }

    public function getName()
    {
        return trim($this->last_name." ".$this->first_name);
    }

    public function getFavoriteServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
                    ->viaTable('{{%favorite_service}}', ['user_id' => 'id']);
    }

    public function getSocials()
    {
        return $this->hasMany(UserSocial::className(), ['user_id' => 'id']);
    } 
}
