<?php

namespace common\models;

class CompanyEmployee extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%company_employee}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
//            ['company_id', 'required'],
            ['name', 'required'],
            ['phone', 'required'],
            ['comment', 'default', 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'ФИО сотрудника',
            'phone' => 'Номер телефона',
            'comment' => 'Комментарий',
        ];
    }
}

