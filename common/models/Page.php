<?php

namespace common\models;

class Page extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%page}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['slug', 'required'],
            ['text', 'default', 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'slug' => 'URL адрес',
            'text' => 'Текст',
        ];        
    }
}
