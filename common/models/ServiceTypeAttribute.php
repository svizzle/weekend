<?php

namespace common\models;

class ServiceTypeAttribute extends \yii\db\ActiveRecord
{    
    const TYPE_MULTIPLE = 1;
    const TYPE_SINGLE = 2;

    public static function tableName()
    {
        return '{{%service_type_attribute}}';
    }

    public function rules()
    {
        return [
            ['service_type_id', 'required'],
            ['name', 'required'],
            ['type_id', 'required'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'type_id' => 'Тип',
        ];
    }

    public function getType()
    {
        return $this->hasOne(ServiceType::className(), ['id' => 'service_type_id']);
    }

    public function getAttributeValues()
    {
        return $this->hasMany(ServiceTypeAttributeValue::className(), ['service_type_attribute_id' => 'id']);
    }

}
