<?php

namespace common\models;

use Yii;

class ServiceCompilation extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%service_compilation}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'service_type_id' => 'Вид услуги',
        ];
    }

    public function getType()
    {
        return $this->hasOne(ServiceType::className(), ['id' => 'service_type_id']);
    }

    public function getServices()
    {
        if ($this->isNewRecord)
        {
            $command = Yii::$app->db->createCommand('select service_id from service_compilation_to_service where compilation_id is null');

            $query = Service::find()->andWhere([
                'id' => $command->queryColumn(),
            ]);
            $query->multiple = true;
            return $query;
        }

        return $this->hasMany(Service::className(), ['id' => 'service_id'])
                    ->viaTable('{{%service_compilation_to_service}}', ['compilation_id' => 'id']);
    } 
}
