<?php

namespace common\models;

class ServiceType extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%service_type}}';
    }

    public function rules()
    {
        return [
            ['id', 'safe'],
            ['name', 'required'],
            ['description', 'required'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'description' => 'Список вопросов для услуги',
        ];
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['type_id' => 'id']);
    }

    public function getServiceAttributes()
    {
        return $this->hasMany(ServiceTypeAttribute::className(), ['service_type_id' => 'id']);
    }
}
