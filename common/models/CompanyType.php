<?php

namespace common\models;

class CompanyType extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%company_type}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['description', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'description' => 'Список вопросов для анкеты компании',
        ];        
    }
}
