<?php

namespace common\models;

use Yii;

class Statistics extends \yii\db\ActiveRecord    
{
    const TYPE_SERVICE_DISPLAY = 1;         // Показ услуги в поиске или виджетах
    const TYPE_USER_VISIT = 2;              // Посещение сайта
    const TYPE_FAVORITE_ADDED = 3;          // Добавили услугу в избранное
    const TYPE_FAVORITE_REMOVED = 4;        // Удалили услугу из избранного
    const TYPE_SERVICE_VIEW = 5;            // Просмотр страницы услуги
    const TYPE_SERVICE_SHOW_PHONE = 6;      // Запрос просмотра телефона
    const TYPE_SERVICE_SHOW_MAP = 7;        // Запрос точки на карте
    const TYPE_SERVICE_SHOW_CONTACTS = 8;   // Запрос показа контактов
    const TYPE_SERVICE_SHOW_ADDITIONAL = 9; // Запрос показа доп услуг
    const TYPE_COMPANY_SHOW_MAP = 10;       // Запрос точки на карте

    public static function tableName()
    {
        return '{{%statistics}}';
    }

    public function rules()
    {
        return [
            ['type_id', 'required'],        
            ['user_id', 'default', 'value' => Yii::$app->user->isGuest ? null : Yii::$app->user->id],
            ['ip', 'default', 'value' => Yii::$app->request->userIP],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }
}
