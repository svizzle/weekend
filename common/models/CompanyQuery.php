<?php

namespace common\models;

class CompanyQuery extends \yii\db\ActiveQuery
{
    public function enabled()
    {
        return $this->andWhere([
            'company.enabled' => true,
            'company.moderated' => true,
        ]);
    }
}
