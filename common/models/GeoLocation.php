<?php

namespace common\models;

class GeoLocation extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%geo_location}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            [['lon', 'lat'], 'default', 'value' => null],
            ['gismeteo_id', 'default', 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'lon' => 'Долгота',
            'lat' => 'Широта',
            'gismeteo_id' => 'Gismeteo ID',
        ];        
    }

    public function getRegion()
    {
        return $this->hasOne(GeoRegion::className(), ['id' => 'region_id']);
    }
    
}
