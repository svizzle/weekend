<?php

namespace common\models;

class ServiceTypeAttributeValue extends \yii\db\ActiveRecord
{
    public $file;

    public static function tableName()
    {
        return '{{%service_type_attribute_value}}';
    }

    public function rules()
    {
        return [
            ['service_type_attribute_id', 'required'],
            ['name', 'required'],
            ['file', 'image'],
            ['icon_title', 'default'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Значение',
            'file' => 'Новое изображение',
            'icon' => 'Иконка',
            'icon_title' => 'Подсказка',
        ];
    }

    public function getServiceTypeAttribute()
    {
        return $this->hasOne(ServiceTypeAttribute::className(), ['id' => 'service_type_attribute_id']);
    }
}
