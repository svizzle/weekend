<?php

namespace common\models;

use Yii;

class Service extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%service}}';
    }

    public function rules()
    {
        return [
            ['company_id', 'required'],
            ['type_id', 'required'],
            ['name', 'required'],
            ['description', 'required'],
            ['enabled', 'default', 'value' => 1],
            ['rate', 'default', 'value' => 0],
            ['views', 'default', 'value' => 0],
            ['code', 'default', 'value' => function($model){

                $service = Service::find()->andWhere([
                    'company_id' => $this->company_id,
                ])->orderBy(['code' => SORT_DESC])->one();

                if ($service && !empty($service->code)) return $service->code + 1;

                $company = Company::findOne($this->company_id);
                $user = User::findOne($company->user_id);

                if (!empty($user->contract_number)) return $user->contract_number + 1;
                
                return null;
            }],
        ];
    }

    public static function find()
    {
        return new ServiceQuery(get_called_class());
    }    

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'name',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code' => 'ID услуги',
            'name' => 'Наименование услуги',
            'type_id' => 'Вид услуги',
            'company_id' => 'Компания',
            'description' => 'Описание услуги',
            'photos' => 'Фотографии',
            'discount' => 'Скидка, %',
            'enabled' => 'Активна',
        ];
    }

    public function getType()
    {
        return $this->hasOne(ServiceType::className(), ['id' => 'type_id']);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getPhotos()
    {
        if ($this->isNewRecord)
        {
            return ServicePhoto::find()->andWhere(['service_id' => null, 'user_id' => Yii::$app->user->id])->orderBy(['position' => SORT_ASC])->all();
        }
        return $this->hasMany(ServicePhoto::className(), ['service_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getPrices()
    {
        return $this->hasMany(ServicePrice::className(), ['service_id' => 'id']);
    }

    public function getAttributeValues()
    {
        return $this->hasMany(ServiceTypeAttributeValue::className(), ['id' => 'value_id'])
                    ->viaTable('{{%service_to_attribute_value}}', ['service_id' => 'id']);
    } 

    public function getPhoto()
    {
        $photos = $this->photos;
        if (!count($photos)) return null;
        return $photos[0];
    }

    public function getPrice()
    {
        return $this->getPrices()->orderBy(['price' => SORT_ASC])->one();
    }

    public function getComments()
    {
        return $this->hasMany(ServiceComment::className(), ['service_id' => 'id']);
    }

    public function getViews()
    {
        return Statistics::find()->andWhere([
            'type_id' => Statistics::TYPE_SERVICE_VIEW,
            'service_id' => $this->id,
        ])->groupBy('ip')->count();
    }
}
