<?php

namespace common\models;

class Advertise extends \yii\db\ActiveRecord
{
    const TYPE_GOOGLE = 1;
    const TYPE_YANDEX = 2;
    const TYPE_BANNER = 3;
    const TYPE_CUSTOM = 4;

    const POSITION_CONTENT = 1;
    const POSITION_SIDEBAR = 2;

    public $file;

    public static function tableName()
    {
        return '{{%advertise}}';
    }

    public function rules()
    {
        return [
            ['type_id', 'required'],
            ['position', 'required'],
            ['name', 'required'],
            ['description', 'default', 'value' => null],
            ['image', 'default', 'value' => null],
            ['file', 'image'],
            ['link', 'default', 'value' => null],
            ['code', 'default', 'value' => null],
            ['region_id', 'default', 'value' => null],
            ['location_id', 'default', 'value' => null],
            ['service_type_id', 'default', 'value' => null],
            ['radius', 'number'],
            ['begin_date', 'default', 'value' => null],
            ['end_date', 'default', 'value' => null],

        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type_id' => 'Тип',
            'position' => 'Расположение',
            'name' => 'Название',
            'description' => 'Описание',
            'link' => 'Ссылка',
            'code' => 'HTML код',
            'region_id' => 'Регион',
            'location_id' => 'Населенный пункт',
            'service_type_id' => 'Вид услуги',
            'radius' => 'Радиус, км',
            'begin_date' => 'Дата начала',
            'end_date' => 'Дата конца',
            'file' => 'Новое изображение',
        ];
    }
}
